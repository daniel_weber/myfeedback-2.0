//
//  Rating.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 09.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import UIKit
import Bond

class RatingOverview: UIView{
    var average: Double = 0
    var ratingButtons: [UIButton] = []
    var distributionLabels: [UILabel] = []
    var ratingDistribution: [Double] = []
    
    var buttonFactor: CGFloat{
        get{
            if UIDevice.current.userInterfaceIdiom == .pad{
                return 1.5
            }
            return 1
        }
    }
    
}

