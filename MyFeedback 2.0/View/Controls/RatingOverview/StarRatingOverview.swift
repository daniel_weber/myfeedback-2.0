//
//  SmileyRatingOverview.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 10.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import Foundation

//
//  StarRating.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 09.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import UIKit
import Bond
import TinyConstraints

class StarRatingOverview: RatingOverview {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var buttonStackView: UIStackView!
    @IBOutlet weak var scaleMinLabel: UILabel!
    @IBOutlet weak var scaleMaxLabel: UILabel!
    
    override var average: Double {
        didSet{
            self.updateButtonSelectionStates()
        }
    }
    
    var scaleMin:String?{
        didSet{
            self.scaleMinLabel.text = scaleMin
        }
    }
    var scaleMax:String?{
        didSet{
            self.scaleMaxLabel.text = scaleMax
        }
    }
    @IBInspectable var buttonSize: CGSize = CGSize(width: 40, height: 40) {
        didSet {
            setupButtons()
        }
    }
    
    @IBInspectable var buttonCount: Int = 5 {
        didSet {
            setupButtons()
        }
    }
    
    override var ratingDistribution: [Double]{
        didSet{
            self.setupButtons()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    private func setupView() {
        
        let view = viewFromNibForClass()
        view.frame = bounds
        
        // Auto-layout stuff.
        view.autoresizingMask = [
            UIViewAutoresizing.flexibleWidth,
            UIViewAutoresizing.flexibleHeight
        ]
        
        // Show the view.
        addSubview(view)
        self.setupButtons()
    }
    
    //MARK: Private Methods
    
    private func setupButtons() {
        
        // clear any existing buttons
        for button in ratingButtons {
            button.removeFromSuperview()
        }
        ratingButtons.removeAll()
        
        for i in 0..<buttonCount {
            //let button = UIButton(frame: CGRect(x: 0, y: 0, width: starSize.width, height: starSize.height))
            let button = UIButton()
            button.width(buttonSize.width*buttonFactor)
            button.height(buttonSize.height*buttonFactor)
            // Set the button images
            button.setImage(#imageLiteral(resourceName: "button_star_outline"), for: .normal)
            button.setImage(#imageLiteral(resourceName: "button_star_filled"), for: .selected)
            button.setImage(#imageLiteral(resourceName: "button_star_filled"), for: .highlighted)
            button.setImage(#imageLiteral(resourceName: "button_star_filled"), for: [.highlighted, .selected])
        
            
            // Add the button to the stack
            self.buttonStackView.addArrangedSubview(button)
            
            // Add the new button to the rating button array
            ratingButtons.append(button)
        }
        self.buttonStackView.height(buttonSize.height*buttonFactor)
        updateButtonSelectionStates()
    }
    
    //MARK: Button Action
    
    
    private func updateButtonSelectionStates() {
        for (index, button) in ratingButtons.enumerated() {
            // If the index of a button is less than the rating, that button should be selected.
            print("Rating")
            print(Double(index))
            print(average)
            button.isSelected = Double(index+1) <= average
            
        }
    }
    
}


