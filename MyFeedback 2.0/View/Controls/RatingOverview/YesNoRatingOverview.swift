//
//  SmileyRatingOverview.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 10.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import Foundation

//
//  StarRating.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 09.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import UIKit
import Bond
import TinyConstraints
import Localize_Swift

class YesNoRatingOverview: RatingOverview {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var labelStackView: UIStackView!
    @IBOutlet weak var buttonStackView: UIStackView!
    
    @IBInspectable var buttonSize: CGSize = CGSize(width: 60, height: 60) {
        didSet {
            setupButtons()
        }
    }
    
    @IBInspectable var buttonCount: Int = 2 {
        didSet {
            setupButtons()
        }
    }
    
    override var ratingDistribution: [Double]{
        didSet{
            self.setupButtons()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    private func setupView() {
        
        let view = viewFromNibForClass()
        view.frame = bounds
        
        // Auto-layout stuff.
        view.autoresizingMask = [
            UIViewAutoresizing.flexibleWidth,
            UIViewAutoresizing.flexibleHeight
        ]
        
        // Show the view.
        addSubview(view)
        self.setupButtons()
    }
    
    //MARK: Private Methods
    
    private func setupButtons() {
        
        // clear any existing buttons
        for button in ratingButtons {
            button.removeFromSuperview()
        }
        for label in distributionLabels {
            label.removeFromSuperview()
        }
        ratingButtons.removeAll()
        
        for i in 0..<buttonCount {
            let button = UIButton(frame: CGRect.init(x: 0, y: 0, width: buttonSize.width*buttonFactor, height: buttonSize.height*buttonFactor))
            button.width(buttonSize.width*buttonFactor)
            button.height(buttonSize.height*buttonFactor)
            // Set the button images
            button.setBackgroundImage(#imageLiteral(resourceName: "button_round_outline"), for: .normal)
            button.setBackgroundImage(#imageLiteral(resourceName: "button_round_filled"), for: .highlighted)
            button.setBackgroundImage(#imageLiteral(resourceName: "button_round_filled"), for: .selected)
            
            switch i{
            case 0:
                button.setTitle("Yes".localized(), for: .normal)
                button.setTitle("Yes".localized(), for: .selected)
                button.setTitle("Yes".localized(), for: .highlighted)
            case 1:
                button.setTitle("No".localized(), for: .normal)
                button.setTitle("No".localized(), for: .selected)
                button.setTitle("No".localized(), for: .highlighted)
            default:
                button.setTitle("No".localized(), for: .normal)
                button.setTitle("No".localized(), for: .selected)
                button.setTitle("No".localized(), for: .highlighted)
            }
            button.titleLabel?.font = UIFont(name: button.titleLabel!.font.fontName, size: 14*buttonFactor)
            button.contentVerticalAlignment = .center
            button.contentMode = .center
            button.contentHorizontalAlignment = .center
            button.tintColor = MerckColor.RichPurple
            button.setTitleColor(MerckColor.VibrantGreen, for: .normal)
            button.setTitleColor(MerckColor.RichPurple, for: .selected)
            button.setTitleColor(MerckColor.RichPurple, for: .highlighted)
            
            
            
            // Add the button to the stack
            //addSubview(button)
            self.buttonStackView.addArrangedSubview(button)
            
            // Add the new button to the rating button array
            ratingButtons.append(button)
            
            if i < min(self.ratingDistribution.count,2){
                let label = UILabel()
                if RCValues.sharedInstance.getBoolValue(ValueKey.showPercentage){
                    label.text = "\(self.ratingDistribution.reversed()[i])%"
                }else{
                    label.text = "\(Int.init(self.ratingDistribution.reversed()[i]))"
                }
                
                label.textColor = UIColor.white
                label.textAlignment = .center
                label.width(self.buttonSize.width*buttonFactor)
                self.distributionLabels.append(label)
                self.labelStackView.addArrangedSubview(label)
                
            }
        }
        updateButtonSelectionStates()
    }
    private func updateButtonSelectionStates() {
        for (index, button) in ratingButtons.enumerated() {
            // If the index of a button is less than the rating, that button should be selected.
            //button.isSelected = index < rating.value
            // for button rating
            let i =  (ratingButtons.count - 1) - index
            button.isSelected = self.ratingDistribution.count > i && self.ratingDistribution[i] >= 50
            /*button.isSelected = index == rating.value! - 1
             
             if index == rating.value! - 1{
             button.alpha = 1
             }else{
             button.alpha = 0.5
             }*/
            
        }
    }
}


