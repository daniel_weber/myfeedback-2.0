//
//  SmileyRatingOverview.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 10.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import Foundation

//
//  StarRating.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 09.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import UIKit
import Bond
import TinyConstraints

class SmileyRatingOverview: RatingOverview {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var labelStackView: UIStackView!
    @IBOutlet weak var buttonStackView: UIStackView!
    
    @IBInspectable var buttonSize: CGSize = CGSize(width: 60, height: 60) {
        didSet {
            setupButtons()
        }
    }
    
    @IBInspectable var buttonCount: Int = 3 {
        didSet {
            setupButtons()
        }
    }
    
    override var ratingDistribution: [Double]{
        didSet{
            self.setupButtons()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    private func setupView() {
        
        let view = viewFromNibForClass()
        view.frame = bounds
        
        // Auto-layout stuff.
        view.autoresizingMask = [
            UIViewAutoresizing.flexibleWidth,
            UIViewAutoresizing.flexibleHeight
        ]
        
        // Show the view.
        addSubview(view)
        self.setupButtons()
    }
    
    //MARK: Private Methods
    
    private func setupButtons() {
        
        // clear any existing buttons
        for button in ratingButtons {
            button.removeFromSuperview()
        }
        for label in distributionLabels {
            label.removeFromSuperview()
        }
        ratingButtons.removeAll()
        
        // Load Button Images
        let bundle = Bundle(for: type(of: self))
        
        for i in 0..<buttonCount {
            let button = UIButton()
            button.width(buttonSize.width*buttonFactor)
            button.height(buttonSize.height*buttonFactor)
            // Set the button images
            
            switch i{
            case 0:
                button.setImage(#imageLiteral(resourceName: "button_smiley_negative"), for: .normal)
            case 1:
                button.setImage(#imageLiteral(resourceName: "button_smiley_neutral"), for: .normal)
            case 2:
                button.setImage(#imageLiteral(resourceName: "button_smiley_positive"), for: .normal)
            default:
                break
            }
            //button.alpha = 0.5
            
            
            // Setup the button action
            button.addTarget(self, action: #selector(self.ratingButtonTapped(button:)), for: .touchDown)
            
            // Add the button to the stack
            //addSubview(button)
            self.buttonStackView.addArrangedSubview(button)
            if i < min(self.ratingDistribution.count,3){
                let label = UILabel()
                if RCValues.sharedInstance.getBoolValue(ValueKey.showPercentage){
                    label.text = "\(self.ratingDistribution[i])%"
                }else{
                    label.text = "\(Int.init(self.ratingDistribution[i]))"
                }
                label.textColor = UIColor.white
                label.textAlignment = .center
                label.width(self.buttonSize.width*buttonFactor)
                self.distributionLabels.append(label)
                self.labelStackView.addArrangedSubview(label)
                
            }
            // Add the new button to the rating button array
            ratingButtons.append(button)
        }
        self.buttonStackView.height(buttonSize.height*buttonFactor)
        updateButtonSelectionStates()
    }
    
    //MARK: Button Action
    
    @objc func ratingButtonTapped(button: UIButton) {
        guard let index = ratingButtons.index(of: button) else {
            fatalError("The button, \(button), is not in the ratingButtons array: \(ratingButtons)")
        }
        
        // Calculate the rating of the selected button
        /*let selectedRating = index + 1
        
        if selectedRating == rating.value {
            // If the selected star represents the current rating, reset the rating to 0.
            rating.value = 0
        } else {
            // Otherwise set the rating to the selected star
            rating.value = selectedRating
        }
        let userInfo = ["rating":rating]*/
        //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ratingChanged"), object: nil, userInfo: userInfo)
    }
    
    private func updateButtonSelectionStates() {
        for (index, button) in ratingButtons.enumerated() {
            // If the index of a button is less than the rating, that button should be selected.
            //button.isSelected = index < rating.value
            // for button rating
            //button.isSelected = self.ratingDistribution.count > index && (self.ratingDistribution[index] >= 50 || self.ratingDistribution[index] == 33.3)
            /*button.isSelected = index == rating.value! - 1
            
            if index == rating.value! - 1{
                button.alpha = 1
            }else{
                button.alpha = 0.5
            }*/
            button.alpha = 0.5
            if self.ratingDistribution.count > index && self.ratingDistribution[index] > 0{
                button.alpha = CGFloat(0.5 + (0.5 * (self.ratingDistribution[index]/100)))
            }
        }
    }
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
}

