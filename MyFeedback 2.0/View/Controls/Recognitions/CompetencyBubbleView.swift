//
//  CompetencyCarousel.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 13.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import UIKit
import Bond
import TinyConstraints

class CompetencyBubbleView: UIView{
    
    @IBOutlet weak var bubbleImageView: UIImageView!
    
    var sloganLabel: UILabel?
    
    var competencyFocused: Observable<Competency?> = Observable<Competency?>(nil)
    
    var service = try! Dip.container.resolve() as FeedbackService
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    private func setupView() {
        
        let view = viewFromNibForClass()
        view.frame = bounds
        
        // Auto-layout stuff.
        view.autoresizingMask = [
            UIViewAutoresizing.flexibleWidth,
            UIViewAutoresizing.flexibleHeight
        ]
        // Show the view.
        addSubview(view)
        
    }
    
    func changeCompetency(competency: Competency){
        if competency.key != self.competencyFocused.value?.key{
            self.bubbleImageView.alpha = 0
            self.bubbleImageView.image = competency.bubble
            UIView.animate(withDuration: 0.2, animations: {
                self.bubbleImageView.alpha = 1
            })
        }
        self.competencyFocused.value = competency
        
    }
    
}

