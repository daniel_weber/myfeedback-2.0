//
//  CompetencyCarousel.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 13.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import UIKit
import iCarousel
import Bond
import TinyConstraints
import Hero

class CompetencyCarouselView: UIView, iCarouselDataSource, iCarouselDelegate {
    

    var result: ((Competency, Bool) -> Bool)?
    
    var competencyFocused: Observable<Competency> = Observable<Competency>(Competency.FutureOriented)
    
    @IBOutlet weak var carouselView: iCarousel!
    
    var isScrolling: Observable<Bool> = Observable<Bool>(false)
    
    var service = try! Dip.container.resolve() as FeedbackService
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    private func setupView() {
        
        let view = viewFromNibForClass()
        view.frame = bounds
        
        // Auto-layout stuff.
        view.autoresizingMask = [
            UIViewAutoresizing.flexibleWidth,
            UIViewAutoresizing.flexibleHeight
        ]
        
       
        
        // Show the view.
        addSubview(view)
        
        carouselView.delegate = self
        carouselView.dataSource = self
        carouselView.type = .rotary
        carouselView.bounces = true
        carouselView.bounceDistance = 0.5
        carouselView.decelerationRate = 0.5
        carouselView.clipsToBounds = true
        
    }
    
    func numberOfItems(in carousel: iCarousel) -> Int {
        return Competency.Competencies.count
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        let competencyImageView: UIImageView
        if Device.IS_IPHONE_5 || Device.IS_IPHONE_4_OR_LESS{
            competencyImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 212, height: 190))
        }else{
            competencyImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 256, height: 230))
        }
        let competencyBadge = Competency.Competencies[index].badge
        competencyImageView.image = competencyBadge
        return competencyImageView
    }
    
    func carouselDidEndScrollingAnimation(_ carousel: iCarousel) {
        self.competencyFocused.value = Competency.Competencies[carousel.currentItemIndex]
        for view in carousel.visibleItemViews{
            (view as? UIView)?.hero.id = nil
            //carousel.currentItemView?.hero.id = "recognitioncard"
        }
        self.isScrolling.value = false
    }
    
    func carouselWillBeginDragging(_ carousel: iCarousel) {
        self.isScrolling.value = true
    }
    
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
        guard let r = self.result else {
            return
        }
        
        r(Competency.Competencies[carousel.currentItemIndex], true)
    }
    
    func carouselItemWidth(_ carousel: iCarousel) -> CGFloat {
        return carousel.frame.size.width / 2
        
    }
    
    func setCompetency(_ competency: Competency?){
        
        if let index = Competency.Competencies.index(where: {comp in return comp.key == competency?.key}){
            self.carouselView.scrollToItem(at: index, animated: false)
        }
    }

}
