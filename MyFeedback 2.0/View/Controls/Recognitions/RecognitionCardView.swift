//
//  RecognitionCardView.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 13.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import UIKit
import Bond
import TinyConstraints

class RecognitionCardView: UIView, UITextViewDelegate {

    @IBOutlet weak var recognitionCardImageView: UIImageView!
    @IBOutlet weak var complimentLabel: UILabel!
    
    var placeholder = "Enter your comment*...".localized()
    
    @IBOutlet weak var commentTextView: UITextView!
    
    var comment: Observable<String?> = Observable<String?>(nil)
    
    var competency: Competency?{
        didSet{
            self.recognitionCardImageView.image = competency?.card
            setupView()
        }
    }
    
    var sender: Employee?{
        didSet{
            let text = "A compliment from [Name]".localized()
            self.complimentLabel.text = text.replacingOccurrences(of: "[Name]", with: sender?.getName() ?? "")
        }
    }
    
    
    var service = try! Dip.container.resolve() as FeedbackService
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    private func setupView() {
        
        self.commentTextView?.removeFromSuperview()
        
        self.hero.isEnabled = true
        
        let view = viewFromNibForClass()
        view.frame = bounds
        
        // Auto-layout stuff.
        view.autoresizingMask = [
            UIViewAutoresizing.flexibleWidth,
            UIViewAutoresizing.flexibleHeight
        ]
        // Show the view.
        addSubview(view)

        
        self.recognitionCardImageView.image = competency?.card

        commentTextView.backgroundColor = UIColor.clear
        commentTextView.alpha = 1
        commentTextView.tintColor = MerckColor.RichPurple
        commentTextView.textColor = MerckColor.RichPurple
        commentTextView.text = placeholder
        commentTextView.delegate = self
        self.comment.observeNext(with: {comment in
            if comment != nil && !comment!.isEmpty{
                self.commentTextView?.text = comment!
            }else{
                self.commentTextView?.text = self.placeholder
            }
        })
        
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == self.placeholder{
            textView.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text != placeholder{
            self.comment.value = textView.text
        }else{
            self.comment.value = nil
        }
    }
}
