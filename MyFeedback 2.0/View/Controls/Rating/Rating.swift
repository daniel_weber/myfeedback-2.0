//
//  Rating.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 09.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import UIKit
import Bond

class Rating: UIView{
    var rating: Observable<Int?> = Observable<Int?>(nil)
    var ratingButtons: [UIButton] = []
    var category: String?
    
    var buttonFactor: CGFloat{
        get{
            if UIDevice.current.userInterfaceIdiom == .pad{
                return 1.5
            }
            return 1
        }
    }
}
