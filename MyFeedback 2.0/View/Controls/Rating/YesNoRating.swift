//
//  StarRating.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 09.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import UIKit
import Bond
import TinyConstraints
import Localize_Swift

class YesNoRating: Rating {
    
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var ratingButtonStackView: UIStackView!
    
    
    override var category: String?{
        didSet{
            self.categoryLabel.text = category
        }
    }
    
    
    @IBInspectable var buttonSize: CGSize = CGSize(width: 60, height: 60) {
        didSet {
            setupButtons()
        }
    }
    
    @IBInspectable var buttonCount: Int = 2 {
        didSet {
            setupButtons()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    private func setupView() {
        
        let view = viewFromNibForClass()
        view.frame = bounds
        
        // Auto-layout stuff.
        view.autoresizingMask = [
            UIViewAutoresizing.flexibleWidth,
            UIViewAutoresizing.flexibleHeight
        ]
        
        // Show the view.
        addSubview(view)
        self.rating.observeNext(with: {_ in
            self.setupButtons()
        })
        self.setupButtons()
        
    }
    
    //MARK: Private Methods
    
    private func setupButtons() {
        
        // clear any existing buttons
        for button in ratingButtons {
            button.removeFromSuperview()
        }
        ratingButtons.removeAll()
        
        // Load Button Images
        let bundle = Bundle(for: type(of: self))
        
        for i in (0..<buttonCount).reversed() {
            let button = UIButton(frame: CGRect.init(x: 0, y: 0, width: buttonSize.width*buttonFactor, height: buttonSize.height*buttonFactor))
            button.width(buttonSize.width*buttonFactor)
            button.height(buttonSize.height*buttonFactor)
            // Set the button images
            button.setBackgroundImage(#imageLiteral(resourceName: "button_round_outline"), for: .normal)
            button.setBackgroundImage(#imageLiteral(resourceName: "button_round_filled"), for: .highlighted)
            button.setBackgroundImage(#imageLiteral(resourceName: "button_round_filled"), for: .selected)
            
            switch i{
                case 1:
                    button.setTitle("Yes".localized(), for: .normal)
                    button.setTitle("Yes".localized(), for: .selected)
                    button.setTitle("Yes".localized(), for: .highlighted)
                case 0:
                    button.setTitle("No".localized(), for: .normal)
                    button.setTitle("No".localized(), for: .selected)
                    button.setTitle("No".localized(), for: .highlighted)
                default:
                    button.setTitle("No".localized(), for: .normal)
                    button.setTitle("No".localized(), for: .selected)
                    button.setTitle("No".localized(), for: .highlighted)
            }
            button.titleLabel?.font = UIFont(name: button.titleLabel!.font.fontName, size: 14*buttonFactor)
            button.contentVerticalAlignment = .center
            button.contentMode = .center
            button.contentHorizontalAlignment = .center
            button.tintColor = MerckColor.RichPurple
            button.setTitleColor(MerckColor.VibrantGreen, for: .normal)
            button.setTitleColor(MerckColor.RichPurple, for: .selected)
            button.setTitleColor(MerckColor.RichPurple, for: .highlighted)
            
            
            // Setup the button action
            button.addTarget(self, action: #selector(self.ratingButtonTapped(button:)), for: .touchDown)
            
            // Add the button to the stack
            //addSubview(button)
            self.ratingButtonStackView.addArrangedSubview(button)
            
            // Add the new button to the rating button array
            ratingButtons.append(button)
        }
        self.ratingButtonStackView.height(buttonSize.height*buttonFactor)
        updateButtonSelectionStates()
    }
    
    //MARK: Button Action
    
    @objc func ratingButtonTapped(button: UIButton) {
        guard let index = ratingButtons.index(of: button) else {
            fatalError("The button, \(button), is not in the ratingButtons array: \(ratingButtons)")
        }
        
        let i =  (ratingButtons.count - 1) - index
        
        // Calculate the rating of the selected button
        let selectedRating = i
        
        if selectedRating == rating.value {
            // If the selected star represents the current rating, reset the rating to 0.
            rating.value = nil
        } else {
            // Otherwise set the rating to the selected star
            rating.value = selectedRating
        }
    }
    
    private func updateButtonSelectionStates() {
        for (index, button) in ratingButtons.enumerated() {
            let i =  (ratingButtons.count - 1) - index
            button.isSelected = i == rating.value
        }
    }
    
}


