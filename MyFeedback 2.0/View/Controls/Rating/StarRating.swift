//
//  StarRating.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 09.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import UIKit
import Bond
import TinyConstraints

class StarRating: Rating {
    
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var ratingButtonStackView: UIStackView!
    @IBOutlet weak var scaleMinLabel: UILabel!
    @IBOutlet weak var scaleMaxLabel: UILabel!
    

    override var category: String?{
        didSet{
            self.categoryLabel.text = category
        }
    }
    var scaleMin:String?{
        didSet{
            self.scaleMinLabel.text = scaleMin
            if scaleMin != nil && scaleMin!.count >= 24{
                self.scaleMinLabel.text = scaleMin!+"..."
            }
        }
    }
    var scaleMax:String?{
        didSet{
            self.scaleMaxLabel.text = scaleMax
            if scaleMax != nil && scaleMax!.count >= 24{
                self.scaleMaxLabel.text = scaleMax!+"..."
            }
        }
    }
    
    
    @IBInspectable var starSize: CGSize = CGSize(width: 40, height: 40) {
        didSet {
            setupButtons()
        }
    }
    
    @IBInspectable var starCount: Int = 5 {
        didSet {
            setupButtons()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    private func setupView() {
        
        let view = viewFromNibForClass()
        view.frame = bounds
        
        // Auto-layout stuff.
        view.autoresizingMask = [
            UIViewAutoresizing.flexibleWidth,
            UIViewAutoresizing.flexibleHeight
        ]
        
        // Show the view.
        addSubview(view)
        self.rating.observeNext(with: {_ in
            self.setupButtons()
        })
        self.setupButtons()
        
    }
    
    //MARK: Private Methods
    
    private func setupButtons() {
        
        // clear any existing buttons
        for button in ratingButtons {
            button.removeFromSuperview()
        }
        ratingButtons.removeAll()
        
        for i in 0..<starCount {
            //let button = UIButton(frame: CGRect(x: 0, y: 0, width: starSize.width, height: starSize.height))
            let button = UIButton()
            button.width(starSize.width*buttonFactor)
            button.height(starSize.height*buttonFactor)
            // Set the button images
            button.setImage(#imageLiteral(resourceName: "button_star_outline"), for: .normal)
            button.setImage(#imageLiteral(resourceName: "button_star_filled"), for: .selected)
            button.setImage(#imageLiteral(resourceName: "button_star_filled"), for: .highlighted)
            button.setImage(#imageLiteral(resourceName: "button_star_filled"), for: [.highlighted, .selected])
            
            // Setup the button action
            button.addTarget(self, action: #selector(self.ratingButtonTapped(button:)), for: .touchDown)
            
            // Add the button to the stack
            //addSubview(button)
            self.ratingButtonStackView.addArrangedSubview(button)
            
            // Add the new button to the rating button array
            ratingButtons.append(button)
        }
        self.ratingButtonStackView.height(starSize.height*buttonFactor)
        updateButtonSelectionStates()
    }
    
    //MARK: Button Action
    
    @objc func ratingButtonTapped(button: UIButton) {
        guard let index = ratingButtons.index(of: button) else {
            fatalError("The button, \(button), is not in the ratingButtons array: \(ratingButtons)")
        }
        
        // Calculate the rating of the selected button
        let selectedRating = index + 1
        
        if selectedRating == rating.value {
            // If the selected star represents the current rating, reset the rating to 0.
            rating.value = nil
        } else {
            // Otherwise set the rating to the selected star
            rating.value = selectedRating
        }
    }
    
    private func updateButtonSelectionStates() {
        for (index, button) in ratingButtons.enumerated() {
            // If the index of a button is less than the rating, that button should be selected.
            button.isSelected = rating.value != nil && index < rating.value!
        }
    }

}
