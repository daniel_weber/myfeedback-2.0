//
//  StarRating.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 09.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import UIKit
import Bond
import TinyConstraints

class SmileyRating: Rating {
    
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var ratingButtonStackView: UIStackView!
    
    
    override var category: String?{
        didSet{
            self.categoryLabel.text = category
        }
    }
    
    
    @IBInspectable var buttonSize: CGSize = CGSize(width: 60, height: 60) {
        didSet {
            setupButtons()
        }
    }
    
    @IBInspectable var buttonCount: Int = 3 {
        didSet {
            setupButtons()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    private func setupView() {
        
        let view = viewFromNibForClass()
        view.frame = bounds
        
        // Auto-layout stuff.
        view.autoresizingMask = [
            UIViewAutoresizing.flexibleWidth,
            UIViewAutoresizing.flexibleHeight
        ]
        
        // Show the view.
        addSubview(view)
        self.rating.observeNext(with: {_ in
            self.setupButtons()
        })
        self.setupButtons()
        
    }
    
    //MARK: Private Methods
    
    private func setupButtons() {
        
        // clear any existing buttons
        for button in ratingButtons {
            button.removeFromSuperview()
        }
        ratingButtons.removeAll()
        
        for i in 0..<buttonCount {
            //let button = UIButton(frame: CGRect(x: 0, y: 0, width: starSize.width, height: starSize.height))
            let button = UIButton()
            button.width(buttonSize.width*buttonFactor)
            button.height(buttonSize.height*buttonFactor)
            // Set the button images
            
            switch i{
            case 0:
                button.setImage(#imageLiteral(resourceName: "button_smiley_negative"), for: .normal)
            case 1:
                button.setImage(#imageLiteral(resourceName: "button_smiley_neutral"), for: .normal)
            case 2:
                button.setImage(#imageLiteral(resourceName: "button_smiley_positive"), for: .normal)
            default:
                break
            }
            button.alpha = 0.5
           
            
            // Setup the button action
            button.addTarget(self, action: #selector(self.ratingButtonTapped(button:)), for: .touchDown)
            
            // Add the button to the stack
            //addSubview(button)
            self.ratingButtonStackView.addArrangedSubview(button)
            
            // Add the new button to the rating button array
            ratingButtons.append(button)
        }
        self.ratingButtonStackView.height(buttonSize.height*buttonFactor)
        updateButtonSelectionStates()
    }
    
    //MARK: Button Action
    
    @objc func ratingButtonTapped(button: UIButton) {
        guard let index = ratingButtons.index(of: button) else {
            fatalError("The button, \(button), is not in the ratingButtons array: \(ratingButtons)")
        }
        
        // Calculate the rating of the selected button
        let selectedRating = index + 1
        
        if selectedRating == rating.value {
            // If the selected star represents the current rating, reset the rating to 0.
            rating.value = nil
        } else {
            // Otherwise set the rating to the selected star
            rating.value = selectedRating
        }
    }
    
    private func updateButtonSelectionStates() {
        for (index, button) in ratingButtons.enumerated() {
            button.isSelected = rating.value != nil && index == rating.value! - 1
            if rating.value != nil && index == rating.value! - 1{
               button.alpha = 1
            }else{
                button.alpha = 0.5
            }
 
        }
    }
    
}

