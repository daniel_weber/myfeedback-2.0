//
//  CallToActionButton.swift
//  MIA
//
//  Created by Daniel Weber on 15.08.17.
//  Copyright © 2017 Merck KGaA. All rights reserved.
//

import UIKit

@IBDesignable
class CallToActionButton: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    var originalColor: UIColor = MerckColor.VibrantGreen
    
    override var isEnabled: Bool{
        didSet{
            super.isEnabled = isEnabled
            if isEnabled{
                self.alpha = 1
                self.backgroundColor = MerckColor.VibrantGreen
            }else{
                self.alpha = 0.5
                self.backgroundColor = MerckColor.DarkerGrey
            }
        }
    }
    
    
    private func setupView() {
        
        if self.backgroundColor == nil || self.backgroundColor == UIColor.clear{
            self.backgroundColor = MerckColor.VibrantGreen
            
        }
        self.originalColor = self.backgroundColor!
        
        //self.layer.masksToBounds = true;
        self.layer.cornerRadius = 5.0
        
        /*self.layer.shadowColor = UIColor(white: 0.0, alpha: 0.3).cgColor
        self.layer.shadowOffset = CGSize(width: 2.0, height: 4.0)
        self.layer.shadowRadius = 8.0
        self.layer.shadowOpacity = 1.0*/
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        //self.layer.transform = CATransform3DMakeScale(0.99, 0.99, 1.0)
        self.backgroundColor = MerckColor.VibrantMagenta
        super.touchesBegan(touches, with: event)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        //self.layer.transform = CATransform3DIdentity
        self.backgroundColor = self.originalColor
        super.touchesEnded(touches, with: event)
        //self.sendActions(for: .touchUpInside)
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        //self.layer.transform = CATransform3DIdentity
        self.backgroundColor = MerckColor.VibrantGreen
        super.touchesCancelled(touches, with: event)
    }
}
