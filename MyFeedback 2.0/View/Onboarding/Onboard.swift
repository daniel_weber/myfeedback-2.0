//
//  Onboard.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 03.04.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import UIKit
import Onboard
import TinyConstraints

class CustomOnboardingViewController : OnboardingViewController{
    
    let statusBar = try! Dip.container.resolve() as UIView
    let service = try! Dip.container.resolve() as FeedbackService
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hero.isEnabled = true
        let backgroundImageView = UIImageView(frame: UIScreen.main.bounds)
        backgroundImageView.image = #imageLiteral(resourceName: "background_first_screen")
        let subViews = self.view.subviews
        self.view.backgroundColor = MerckColor.VibrantMagenta
        self.view.hero.modifiers = [.fade]
        for view in subViews{
            view.hero.modifiers = [.translate(y:1000)]
        }
        self.skipButton.bottom(to: self.view, nil, offset: -10, relation: .equal, priority: .required, isActive: true)
        self.skipButton.right(to: self.view, nil, offset: -8, relation: .equal, priority: .required, isActive: true)
        self.skipButton.height(30)
        self.skipButton.titleLabel?.font = MerckFont.font(size: 14)
        self.pageControl.bottom(to: self.view, nil, offset: -30, relation: .equal, priority: .required, isActive: true)
        self.pageControl.centerX(to: self.view)
        self.pageControl.currentPageIndicatorTintColor = MerckColor.RichPurple
        //self.view.insertSubview(backgroundImageView, at: 0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let navigationBar = try! Dip.container.resolve() as AlternativeNavigationBar
        navigationBar.isHidden = true
        statusBar.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let navigationBar = try! Dip.container.resolve() as AlternativeNavigationBar
        navigationBar.update(title: nil, options: nil)
        navigationBar.isCollapsed = true
        statusBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        let navigationBar = try! Dip.container.resolve() as AlternativeNavigationBar
        navigationBar.backButton.alpha = 0
        navigationBar.isHidden = false
        self.statusBar.isHidden = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        let navigationBar = try! Dip.container.resolve() as AlternativeNavigationBar
        navigationBar.backButton.alpha = 1
    }
}


class Onboarding {
    
    var pages: [OnboardingContentViewController] = []
    let onboardingViewController: CustomOnboardingViewController

    
    init(){
         let firstPage = OnboardingContentViewController(title: nil, body: nil, image: UIImage.init(named: "onboardingScreen01_"+Language.getCurrentLanguage().key.lowercased()), buttonText: nil, action: nil)
         let secondPage = OnboardingContentViewController(title: nil, body: nil, image: UIImage.init(named: "onboardingScreen02_"+Language.getCurrentLanguage().key.lowercased()), buttonText: nil, action: nil)
         let thirdPage = OnboardingContentViewController(title: nil, body: nil, image: UIImage.init(named: "onboardingScreen03_"+Language.getCurrentLanguage().key.lowercased()), buttonText: nil, action: nil)
         let fourthPage = OnboardingContentViewController(title: nil, body: nil, image: UIImage.init(named: "onboardingScreen04_"+Language.getCurrentLanguage().key.lowercased()), buttonText: nil, action: nil)
         let fifthPage = OnboardingContentViewController(title: nil, body: nil, image: UIImage.init(named: "onboardingScreen05_"+Language.getCurrentLanguage().key.lowercased()), buttonText: nil, action: nil)
         let sixthPage = OnboardingContentViewController(title: nil, body: nil, image: UIImage.init(named: "onboardingScreen06_"+Language.getCurrentLanguage().key.lowercased()), buttonText: nil, action: nil)
         let seventhPage = OnboardingContentViewController(title: nil, body: nil, image: UIImage.init(named: "onboardingScreen07_"+Language.getCurrentLanguage().key.lowercased()), buttonText: nil, action: nil)
        self.pages = [firstPage, secondPage, thirdPage, fourthPage, fifthPage, sixthPage, seventhPage]
        for (index, page) in self.pages.enumerated(){
            page.iconHeight = UIScreen.main.bounds.height
            page.iconWidth = UIScreen.main.bounds.width
            page.topPadding = 0
            page.viewDidAppearBlock = {()->Void in
                if let ovc = page.delegate as? OnboardingViewController{
                    if index == 6{
                        ovc.skipButton.setTitle("Go!".localized(), for: .normal)
                        ovc.skipButton.setTitleColor(MerckColor.RichPurple, for: .normal)
                        ovc.skipButton.setBackgroundImage(#imageLiteral(resourceName: "button_rectangle_green"), for: .normal)
                        ovc.skipButton.setTitleColor(MerckColor.VibrantGreen, for: .highlighted)
                        ovc.skipButton.setBackgroundImage(#imageLiteral(resourceName: "button_rectangle_purple"), for: .highlighted)
                        ovc.skipButton.setTitleColor(MerckColor.VibrantGreen, for: .focused)
                        ovc.skipButton.setBackgroundImage(#imageLiteral(resourceName: "button_rectangle_purple"), for: .focused)
                    }else{
                        ovc.skipButton.setTitle("Skip!".localized(), for: .normal)
                        ovc.skipButton.setTitleColor(MerckColor.VibrantGreen, for: .normal)
                        ovc.skipButton.setBackgroundImage(#imageLiteral(resourceName: "button_rectangle_purple"), for: .normal)
                        ovc.skipButton.setTitleColor(MerckColor.RichPurple, for: .highlighted)
                        ovc.skipButton.setBackgroundImage(#imageLiteral(resourceName: "button_rectangle_green"), for: .highlighted)
                        ovc.skipButton.setTitleColor(MerckColor.RichPurple, for: .focused)
                        ovc.skipButton.setBackgroundImage(#imageLiteral(resourceName: "button_rectangle_green"), for: .focused)
                    }
                }
                
            }
        }
        let backgroundView = UIView(frame: UIScreen.main.bounds)
        backgroundView.backgroundColor = MerckColor.VibrantMagenta
        let onboardingVC = CustomOnboardingViewController(backgroundImage: UIImage.init(view: backgroundView), contents: self.pages)!
        onboardingVC.shouldMaskBackground = false
        onboardingVC.allowSkipping = true
        onboardingVC.skipHandler = {() -> Void in
            Onboarding.hide()
        }
        self.onboardingViewController = onboardingVC
    }
    
    static func show() {
        let navigationController = try! Dip.container.resolve() as BaseNavigationController
        
        let onboard = Onboarding()
        onboard.onboardingViewController.service.AnalyticsOpenViewOnboarding()
        navigationController.pushViewController(onboard.onboardingViewController, animated: true)
    }
    
    static func hide() {
        let navigationController = try! Dip.container.resolve() as BaseNavigationController
        navigationController.popViewController(animated: true)
        let navigationBar = try! Dip.container.resolve() as AlternativeNavigationBar
        navigationBar.isCollapsed = false
    }
}
