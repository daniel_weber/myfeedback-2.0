//
//  FeedbackOverallViewController.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 09.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import UIKit
import SwipeCellKit
import Hero
import Bond
import Hydra

class FeedbackOverallViewController: UIViewController, UITableViewDelegate, SwipeTableViewCellDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backgroundImage: UIImageView!
    
    let service = try! Dip.container.resolve() as FeedbackService
    
    var filteredRequests : MutableObservableArray<Request> = MutableObservableArray([])
    
    var searchTerm : Observable<String?> = Observable("")
    var searchPromise : Promise<[Request]>?
    var searchToken : InvalidationToken?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(RequestTableViewCell.self, forCellReuseIdentifier: "RequestTableViewCell")
        self.tableView.register(EmptySearchEmployeeTableViewCell.self, forCellReuseIdentifier: "EmptySearchEmployeeTableViewCell")
        self.tableView.contentInset = UIEdgeInsetsMake(40, 0, 0, 0)
        self.tableView.delegate = self
        self.filteredRequests.bind(to: self.tableView){ list, indexPath, tableView in
            let sortedList = list.array.sorted(by: {a, b in
                return a.value(forKey: self.service.sortBy.value.0.key).debugDescription > b.value(forKey: self.service.sortBy.value.0.key).debugDescription
            })
            let request = sortedList[indexPath.item]
            if request.id == nil{
                let cell = tableView.dequeueReusableCell(withIdentifier: "EmptySearchEmployeeTableViewCell", for: indexPath) as! EmptySearchEmployeeTableViewCell
                return cell
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: "RequestTableViewCell", for: indexPath) as! RequestTableViewCell
            cell.backgroundColor = UIColor.clear
            cell.title = request.overviewTitle
            cell.name = request.requesterNames
            cell.date = request.date
            cell.readFlag = request.readFlag
            let suffix: String
            if request.hasAnswers{
                suffix = "_filled"
            }else{
                suffix = "_outline"
            }
            
            switch request.type?.key ?? ""{
            case RequestType.Smileys.key:
                cell.icon = UIImage(named: "icon_smiley"+suffix)
            case RequestType.Stars.key:
                cell.icon = UIImage(named: "icon_star"+suffix)
            case RequestType.YesNo.key:
                cell.icon = UIImage(named: "icon_checkmark"+suffix)
            default:
                cell.icon = UIImage()
            }
            cell.thankYouIndicator = request.thankYouFlag
            cell.delegate = self
            cell.highlightedString = self.searchTerm.value
            return cell
        }
        
        hero.isEnabled = true
        self.tableView.hero.modifiers = [.translate(y:1000)]
        self.backgroundImage.hero.modifiers = [.fade]
        self.view.backgroundColor = MerckColor.RichPurple
        self.tableView.refreshControl = UIRefreshControl()
        self.tableView.refreshControl?.addTarget(self, action: #selector(self.handleRefresh), for: .valueChanged)
        self.service.sortBy.observeNext(with: {_ in
            self.tableView.reloadData()
        })
        self.service.activeFilters.observeNext(with: {e in
            if self.service.activeFilters.array.count == 0{
                self.filteredRequests.replace(with: self.service.myRequests.array)
            }else{
                self.filteredRequests.replace(with: self.service.myRequests.array.filter({r in return self.filter(r)}))
            }
        })
        self.service.myRequests.observeNext(with: {e in
            if self.service.activeFilters.array.count == 0{
                self.filteredRequests.replace(with: e.dataSource.array)
            }else{
               self.filteredRequests.replace(with: e.dataSource.array.filter({r in return self.filter(r)}))
            }
        })
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let searchBar = try! Dip.container.resolve() as BaseSearchBar
        
        searchBar.searchTextField.reactive.text.bidirectionalBind(to: self.searchTerm)
        self.setNavigationBar()
        self.service.activeFilters.observeNext(with: {_ in
            self.setNavigationBar()
            if self.searchTerm.value != nil{
                self.searchTerm.value! += ""
            }else{
                self.searchTerm.value = ""
            }
            
        })
        let _ =  self.searchTerm.combineLatest(with: self.service.myRequests){(term, requests) -> [Request] in
            /*UIView.animate(withDuration: 0.2, delay: 0.0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0.0, options: [.curveEaseOut], animations: {
                
                self.tableView.contentOffset = CGPoint(x: 0, y: -70)
                
            }, completion: nil)*/
            
            self.tableView.layer.opacity = 0.4
            self.tableView.isUserInteractionEnabled = false
            
            self.searchPromise = self.filter(requests: requests.dataSource.array, by: term).then(in: Context.main, { (reqs) in
                
                self.filteredRequests.replace(with: reqs)
                
                self.tableView.layer.opacity = 1.0
                self.tableView.isUserInteractionEnabled = true
                
                self.searchPromise = nil
                
            })
            
            return []
            }.observeNext { (reqs) in
                
        }
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func setNavigationBar(){
        let navigationBar = try! Dip.container.resolve() as AlternativeNavigationBar
        let filterIcon: UIImage
        if self.service.activeFilters.array.count > 0{
            filterIcon = #imageLiteral(resourceName: "Icon_Funnel_Filled")
        }else{
            filterIcon = #imageLiteral(resourceName: "Icon_Funnel")
        }
        let filterOptions = BarOption(title: "Filter", icon: filterIcon) { () -> Void in
            let mvc = try! Dip.container.resolve() as ModalViewController
            
            let filterMV = FilterTableView()
            
            mvc.present(modal: filterMV, edge: .bottom)
        }
        
        let searchOptions = BarOption(title: "Search", icon: #imageLiteral(resourceName: "Icon_Search")) { () -> Void in
            let searchBar = try! Dip.container.resolve() as BaseSearchBar
            searchBar.isCollapsed = false
        }
        navigationBar.update(title: "Feedback Overall".localized(), options: [filterOptions, searchOptions])
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 92
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.cellForRow(at: indexPath) is EmptySearchEmployeeTableViewCell{
            return
        }
        let navigationcontroller = try! Dip.container.resolve() as BaseNavigationController
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let sortedList = self.filteredRequests.array.sorted(by: {a, b in
            return a.value(forKey: self.service.sortBy.value.0.key).debugDescription > b.value(forKey: self.service.sortBy.value.0.key).debugDescription
        })
        let request = sortedList[indexPath.row]
        let vc: UIViewController
        if request.responses.count > 1{
            vc = storyboard.instantiateViewController(withIdentifier: "RequestOverviewViewController") as! RequestOverviewViewController
            (vc as! RequestOverviewViewController).request = request
        }else{
            vc = storyboard.instantiateViewController(withIdentifier: "RequestViewController") as! RequestViewController
            (vc as! RequestViewController).request = request
            (vc as! RequestViewController).response = request.responses.first
        }
        navigationcontroller.pushViewController(vc, animated: true)
    }
    
    // MARK: SwipeTableViewCellDelegate
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        let sortedList = self.filteredRequests.array.sorted(by: {a, b in
            return a.value(forKey: self.service.sortBy.value.0.key).debugDescription > b.value(forKey: self.service.sortBy.value.0.key).debugDescription
        })
        let request = sortedList[indexPath.row]
        let cell = self.tableView.cellForRow(at: indexPath)
        let deleteAction = SwipeAction(style: .default, title: "Delete".localized()) { action, indexPath in
            let callToDelete = CallToActionButton()
            callToDelete.setTitle("Delete".localized(), for: .normal)
            callToDelete.backgroundColor = MerckColor.RichRed
            let alertMV = AlertModalView.show(title: nil, message: "Delete".localized()+"?", enableCancel: true)
            alertMV.close = {success in
                if success{
                    self.tableView.beginUpdates()
                    self.service.deleteRequest(request: request).then({_ in
                        action.fulfill(with: .delete)
                        self.tableView.endUpdates()
                    }).catch({e in
                        print("ERROR")
                        print(e)
                    })
                }else{
                    action.fulfill(with: .reset)
                }
            }
            /*self.service.deleteRequest(request: request).then({_ in
                
            }).catch({e in
                print("ERROR")
                print(e)
            })*/
            
        }
        let thankYouAction = SwipeAction(style: .default, title: "Thank You!".localized()) { action, indexPath in
            Loading.show()
            self.service.thankForAllResponses(request: request).then({resp in
                if resp.success && resp.message != nil{
                    AlertModalView.show(title: "Success".localized(), message: resp.message!)
                    tableView.reloadRows(at: [indexPath], with: .none)
                }
            }).catch({e in
                print("ERROR")
                print(e)
            })
            action.fulfill(with: .reset)
        }
        
        let readAllAction = SwipeAction(style: .default, title: "Mark as read".localized()) { action, indexPath in
            self.service.readAllResponses(request: request).then({_ in
                tableView.reloadRows(at: [indexPath], with: .none)
            }).catch({e in
                print("ERROR")
                print(e)
            })
            action.fulfill(with: .reset)
        }
        
        
        // customize the action appearance
        
        let deleteButton = CallToActionButton(frame: CGRect.init(x: 0, y: 0, width: 90, height: 104))
        deleteButton.backgroundColor = MerckColor.RichRed
        deleteButton.setTitle("Delete".localized(), for: .normal)
        //deleteAction.image = UIImage.imageWithView(view: deleteButton)
        deleteAction.textColor = UIColor.white
        deleteAction.backgroundColor = MerckColor.RichRed
        //deleteAction.transitionDelegate = ScaleTransition.init(duration: 0.3, initialScale: 0.5, threshold: 0.5)
        
        let thankYouButton = CallToActionButton(frame: CGRect.init(x: 0, y: 0, width: 90, height: 104))
        thankYouButton.backgroundColor = MerckColor.VibrantGreen
        thankYouButton.setTitle("Thank You".localized(), for: .normal)
        //thankYouAction.image = UIImage.imageWithView(view: thankYouButton)
        thankYouAction.textColor = UIColor.white
        thankYouAction.backgroundColor = MerckColor.VibrantGreen
        //thankYouAction.transitionDelegate = ScaleTransition.init(duration: 0.3, initialScale: 0.5, threshold: 0.5)
        
        readAllAction.backgroundColor = MerckColor.RichBlue
        
        if orientation == .right{
            if !request.thankYouAllowed{
                return [deleteAction]
            }
            return [deleteAction,thankYouAction]
        }else if orientation == .left && !request.readFlag{
            return[readAllAction]
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeTableOptions {
        var options = SwipeTableOptions()
        options.expansionStyle = .destructive(automaticallyDelete: false)
        options.transitionStyle = .drag
        options.backgroundColor = UIColor.clear
        return options
    }
    
    // MARK: Request Search
    
    private func filter(requests: [Request], by term: String?) -> Promise<[Request]> {
        
        self.searchToken?.invalidate()
        self.searchToken = InvalidationToken()
        
        return Promise<[Request]>(in: Context.background, token: self.searchToken, { (resolve, reject, operation) in
            
            
            var filtered : [Request] = []
            
            for req in requests {
                
                if !self.filter(req){
                    continue
                }
                
                if operation.isCancelled {
                    break
                }
                
                if (term == nil || term! == ""){
                    if self.service.activeFilters.array.count == 0{
                        continue
                    }else{
                        filtered.append(req)
                        continue
                    }
                    
                }
                
                let doAND = true
                if (doAND){
                    if self.considerAND(term: term, request: req) {filtered.append(req)}
                }
                else {
                    if self.considerOR(term: term, request: req) {filtered.append(req)}
                }
            }
            
            
            if (operation.isCancelled){
                operation.cancel()
                return
            }
            
            if (filtered.count == 0 && (term == nil || term! == "") && self.service.activeFilters.count == 0){
                filtered.append(contentsOf: requests)
            }
            
            if (filtered.count == 0){
                let empty = Request.invalid()
                filtered.append(empty)
            }
            
            
            
            resolve(filtered)
        })
    }
    
    // Checks if an Request applies to the active
    private func filter(_ request: Request)->Bool{
        for filter in self.service.activeFilters.array{
            switch filter.type{
            case .Category:
                if !service.activeFilters.array.filter({f in return f.type == FilterType.Category}).contains(where: {f in return request.category?.name == (f.value as? RequestCategory)?.name}){
                    return false
                }
            case .RequestType:
                if !service.activeFilters.array.filter({f in return f.type == FilterType.RequestType}).contains(where: {f in return request.type?.key == (f.value as? RequestType)?.key}){
                    return false
                }
            case .Employee:
                if !request.responses.contains(where: {resp in return resp.rater.userId == (filter.value as? Employee)?.userId}){
                    return false
                }
            case .DateFrom:
                if request.date! < filter.value as! Date{
                    return false
                }
            case .DateTo:
                if request.date! > filter.value as! Date{
                    return false
                }
            case .SeriesOnly:
                if request.continuousRepetitions == 1{
                    return false
                }
            case .Series:
                if !service.activeFilters.array.filter({f in return f.type == FilterType.Series}).contains(where: {f in return request.id == (f.value as? (id:Int?, title:String?))?.id}) && self.service.activeFilters.array.contains(where: {f in return f.type == FilterType.SeriesOnly}){
                    return false
                }
            default:
                break
            }
        }
        return true
    }
    
    func considerAND(term : String?, request: Request) -> Bool {
        
        if let searchTerms = term?.split(separator: " ") {
            
            for t in searchTerms {
                
                if request.searchTerm.contains(t.lowercased()) { continue }
                
                // When there is no hit for one item it's out (AND)
                return false
            }
            
            // When every item in the loop had a hit, this search term fits (AND)
            return true
        }
        
        return false
    }
    
    func considerOR(term : String?, request: Request) -> Bool {
        
        if let searchTerms = term?.split(separator: " ") {
            
            for t in searchTerms {
                
                if request.searchTerm.contains(t.lowercased()) { return true }
                
            }
            
            // When no item in the loop had a hit, this search term does not fit (OR)
            return false
        }
        
        return false
    }
    
    @objc func handleRefresh(){
        self.tableView.refreshControl?.endRefreshing()
        Loading.show()
        self.service.updateSentRequests().then({_ in
            self.tableView.reloadData()
            Loading.hide()
        })
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
