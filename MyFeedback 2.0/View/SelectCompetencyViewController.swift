//
//  SelectCompetencyViewController.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 13.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import UIKit
import iCarousel

class SelectCompetencyViewController: UIViewController {
    
    
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var nexButton: CallToActionButton!
    @IBOutlet weak var competencyIconImageView: UIImageView!
    @IBOutlet weak var competencyBubbleView: CompetencyBubbleView!
    @IBOutlet weak var competencyCarouselView: CompetencyCarouselView!
    
    var bubbleView: UIView?
    
    let service = try! Dip.container.resolve() as FeedbackService

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        self.competencyCarouselView.isScrolling.bind(to: self.competencyBubbleView.reactive.isHidden)
        self.service.AnalyticsActionSendRecognition()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        setContent()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupView(){
        
        self.view.backgroundColor = MerckColor.RichPurple
        self.backgroundView.backgroundColor = MerckColor.RichPurple
        self.titleLabel.numberOfLines = 0
        hero.isEnabled = true
        self.competencyBubbleView.hero.modifiers = [.translate(y:1000)]
        self.competencyCarouselView.hero.modifiers = [.translate(y:1000)]
        self.nexButton.hero.modifiers = [.translate(y:1000)]
        for view in view.subviews{
            view.hero.modifiers = [.translate(y:1000)]
        }
        self.backgroundView.hero.modifiers = [.fade]
        
        self.competencyCarouselView.competencyFocused.observeNext(with: {competency in
            if competency.key != self.competencyBubbleView.competencyFocused.value?.key{
                self.competencyBubbleView.changeCompetency(competency: competency)
                self.service.recognitionDraft.value?.competency = competency
            }
        })
        self.competencyCarouselView.result = {(competency, success) in
            self.service.recognitionDraft.value?.competency = competency
            self.navigateToRecognitionCard()
            return success
        }
        
        if Device.IS_IPHONE_5 || Device.IS_IPHONE_4_OR_LESS{
            competencyCarouselView.height(200)
        }else{
            competencyCarouselView.height(240)
        }
        if Device.IS_IPAD{
            self.competencyIconImageView.height(200)
        }
    }
    
    func setContent(){
        let navigationBar = try! Dip.container.resolve() as AlternativeNavigationBar
        navigationBar.update(title: "Send Recognition".localized(), options: nil)
        
        self.titleLabel.text = "Which situation/behaviour would you like to acknowledge?".localized()
        self.nexButton.setTitle("Next".localized(), for: .normal)
        if self.service.recognitionDraft.value == nil{
            self.service.recognitionDraft.value = Recognition(id: nil, sender: loggedInUser.value!, competency: Competency.FutureOriented, text: "", date: Date(), repetition: nil)
        }else if let competency = self.service.recognitionDraft.value?.competency{
            self.competencyBubbleView.changeCompetency(competency: competency)
            self.competencyCarouselView.setCompetency(competency)
        }
    }
    
   
    @IBAction func handleNextButtonClicked(_ sender: CallToActionButton) {
       self.navigateToRecognitionCard()
    }
    
    func navigateToRecognitionCard(){
        let navigationcontroller = try! Dip.container.resolve() as BaseNavigationController
        let main = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = main.instantiateViewController(withIdentifier: "RecognitionCardViewController") as! RecognitionCardViewController
        vc.isInEditMode = true
        navigationcontroller.pushViewController(vc, animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
