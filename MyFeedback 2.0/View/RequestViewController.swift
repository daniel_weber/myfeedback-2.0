//
//  RequestViewController.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 09.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import UIKit
import SwiftDate
import TinyConstraints
import IQKeyboardManagerSwift
import MessageUI

class RequestViewController: UIViewController {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var requestTextView: UITextView!
    @IBOutlet weak var responseStackView: UIStackView!
    @IBOutlet weak var raterNameLabel: UILabel!
    @IBOutlet weak var responseDateLabel: UILabel!
    @IBOutlet weak var responseTextView: UITextView!
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    let service = try! Dip.container.resolve() as FeedbackService
    
    var ratingView: Rating?
    var response: RequestResponse?{
        didSet{
           self.ratingView?.rating.value = response?.rating
        }
    }
    var request: Request?{
        didSet{
            self.ratingView?.category = request?.category?.localName
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = MerckColor.RichPurple
        // Do any additional setup after loading the view.
        
        hero.isEnabled = true
        self.view.backgroundColor = MerckColor.RichPurple
        setupView()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let navigationBar = try! Dip.container.resolve() as AlternativeNavigationBar
        let shareOption = BarOption(title: "Share", icon: #imageLiteral(resourceName: "Icon_Feedback")) { [unowned self] () -> Void in
            self.myShareButton()
        }
        if self.response?.isAnswered == true && response?.rater.userId != loggedInUser.value?.userId{
            navigationBar.update(title: self.request?.title, options: [shareOption])
        }else{
            navigationBar.update(title: self.request?.title, options: nil)
        }
        
        self.setupView()
        self.setupRatingView()
        //KeyboardAvoiding.avoidingView = self.responseTextView
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
    }
    
    func setupView(){
        self.ratingView?.removeFromSuperview()
        
        self.nameLabel.text = self.request?.requester.getName()
        self.requestTextView.text = self.request?.text
        self.raterNameLabel.text = "\(self.response?.rater.getName() ?? "") "+("wrote:".localized())
        self.responseDateLabel.text = self.response?.responseTimestamp?.string(custom: "dd.MM.yyyy")
        self.responseTextView.text = response?.text
        
        let scaleFactorY = backgroundImage.bounds.size.height/backgroundImage.image!.size.height
        let scaleFactorX = backgroundImage.bounds.size.width/backgroundImage.image!.size.width
        if self.request?.type?.key == RequestType.Stars.key{
            let starRatingView = StarRating(frame: CGRect.init(x: 100*scaleFactorX, y: 410*scaleFactorY, width: 520*scaleFactorX, height: 220*scaleFactorY))
            starRatingView.rating.value = self.response?.rating
            starRatingView.category = self.request?.category?.localName
            starRatingView.scaleMin = self.request?.scaleTextMin
            starRatingView.scaleMax = self.request?.scaleTextMax
            self.ratingView = starRatingView
            self.ratingView?.alpha = 0
            self.scrollView.addSubview(starRatingView)
        }else if self.request?.type?.key == RequestType.Smileys.key{
            let smileyRatingView = SmileyRating(frame: CGRect.init(x: 100*scaleFactorX, y: 410*scaleFactorY, width: 520*scaleFactorX, height: 220*scaleFactorY))
            smileyRatingView.rating.value = self.response?.rating
            smileyRatingView.category = self.request?.category?.localName
            self.backgroundImage.image = #imageLiteral(resourceName: "background_request_inverted")
            self.ratingView = smileyRatingView
            self.ratingView?.alpha = 0
            self.scrollView.addSubview(smileyRatingView)
        }else if self.request?.type?.key == RequestType.YesNo.key{
            let yesNoRatingView = YesNoRating(frame: CGRect.init(x: 100*scaleFactorX, y: 410*scaleFactorY, width: 520*scaleFactorX, height: 220*scaleFactorY))
            yesNoRatingView.rating.value = self.response?.rating
            yesNoRatingView.category = self.request?.category?.localName
            self.ratingView = yesNoRatingView
            self.ratingView?.alpha = 0
            self.scrollView.addSubview(yesNoRatingView)
        }
        self.requestTextView.bottomToTop(of: self.ratingView!)
        self.requestTextView.isEditable = false
        
        self.responseTextView.topToBottom(of: self.responseStackView, offset: 8, relation: .equal, priority: .required, isActive: true)
        
        self.responseTextView.height(min(200, self.view.frame.maxY - self.responseTextView.frame.minY))
        //self.responseTextView.height(200, relation: .equalOrLess, priority: .required, isActive: true)
        //self.responseTextView.bottom(to: self.view, nil, offset: -8, relation: .equal, priority: .required, isActive: true)
        self.responseStackView.topToBottom(of: self.ratingView!, offset: 20, relation: .equal, priority: .defaultLow, isActive: true)
        
        if self.request?.isEditable == true{
            let respondButton = CallToActionButton(frame: CGRect.zero)
            respondButton.setTitle("Send".localized(), for: .normal)
            respondButton.titleLabel?.font = UIFont(name: "HelveticaNeue-Bold", size: 15)
            respondButton.setTitleColor(UIColor.white, for: .normal)
            self.scrollView.addSubview(respondButton)
            respondButton.height(40)
            respondButton.bottom(to: self.scrollView, nil, offset: -20, relation: .equal, priority: .required, isActive: true)
            respondButton.width(min(self.responseTextView.bounds.width, 400))
            respondButton.centerXToSuperview()
            responseTextView.isEditable = true
            self.ratingView?.isUserInteractionEnabled = true
            self.raterNameLabel.text = "Enter your comment*:".localized()
            self.responseTextView.bottomToTop(of: respondButton, offset: -20, relation: .equal, priority: .required, isActive: true)
            self.ratingView?.rating.observeNext(with: {rating in
                self.response?.rating = rating
                respondButton.isEnabled = self.response?.isValid ?? false
            })
            self.responseTextView.reactive.text.observeNext(with: {text in
                self.response?.text = text
                respondButton.isEnabled = self.response?.isValid ?? false
            })
            respondButton.isEnabled = self.response?.isValid ?? false
            respondButton.addTarget(self, action: #selector(self.handleSendButton), for: .touchUpInside)
        }else{
            responseTextView.isEditable = false
            self.ratingView?.isUserInteractionEnabled = false
            self.responseTextView.bottom(to: self.scrollView, nil, offset: -8, relation: .equal, priority: .required, isActive: true)
            if self.response != nil && !self.response!.isRead{
                self.service.readResponse(response: self.response!).then({_ in})
            }
            
        }
        
        
        
        responseTextView.layer.borderColor = UIColor.lightText.cgColor
        responseTextView.layer.cornerRadius = 5
        responseTextView.layer.borderWidth = 0.5
        responseTextView.backgroundColor = UIColor.init(hexString: "#999999").withAlphaComponent(0.1)
        responseTextView.textColor = UIColor.white
        responseTextView.tintColor = UIColor.white
        for v in self.scrollView.subviews{
            v.hero.modifiers = [.translate(y:1000)]
        }
        self.backgroundImage.hero.modifiers = [.fade]
        
    }
    
    func setupRatingView(){
        UIView.animate(withDuration: 0.2, animations: {
            self.ratingView?.alpha = 1
        })
        
    }
    
    @objc func handleSendButton(){
        self.sendResponse()
    }
    
    func sendResponse(){
        Loading.show()
        self.service.answerRequest(request: self.request!, requestResponse: self.response!).then({resp in
            if resp.success && resp.message != nil {
                AlertModalView.show(title: "Success".localized(), message: resp.message!)
                let navigationcontroller = try! Dip.container.resolve() as BaseNavigationController
                navigationcontroller.popViewController(animated: true)
            }else{
                ErrorView.show(message: "Unknown Error")
            }
        }).catch({e in ErrorView.show(error: e)})
    }
    
    func myShareButton() {
        let shareText = "\(self.response?.rater.getName() ?? "") "+"wrote"+":\n \(self.response?.text ?? "")"
        //displayShareSheet(shareContent: shareText)
        
        //New:
        
        self.service.AnalyticsActionShareFeedback()
        
        let shareView = UIView(frame: self.view.bounds)
        let bgImageView = UIImageView(frame: shareView.bounds)
        bgImageView.image = self.backgroundImage.image
        bgImageView.clipsToBounds = true
        shareView.addSubview(bgImageView)
        
        let viewCopy = UIImageView(image: UIImage(view: view))
        shareView.addSubview(viewCopy)
        let shareImage = UIImage(view: shareView)
        let subject = "Received Feedback via MyFeedback"
        
        Share.show(parentViewController: self, body: shareText, image: shareImage)
        return
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        let cancelButton = UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: nil)
        let mailButton = UIAlertAction(title: "Mail", style: .default, handler: {action in
            self.shareViaMail(subject: subject, text: shareText, image: shareImage)
        })
        mailButton.setValue(#imageLiteral(resourceName: "Icon_iOS_Mail").withRenderingMode(.alwaysOriginal), forKey: "image")
        let messageButton = UIAlertAction(title: "Message", style: .default, handler: {action in
            self.shareViaMessage(subject: subject, text: shareText, image: shareImage)
        })
        messageButton.setValue(#imageLiteral(resourceName: "Icon_iOS_Message").withRenderingMode(.alwaysOriginal), forKey: "image")
        
        alertController.addAction(mailButton)
        alertController.addAction(messageButton)
        alertController.addAction(cancelButton)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    func displayShareSheet(shareContent:String) {
        self.service.AnalyticsActionShareFeedback()
        
        let shareView = UIView(frame: self.view.bounds)
        let bgImageView = UIImageView(frame: shareView.bounds)
        bgImageView.image = self.backgroundImage.image
        bgImageView.clipsToBounds = true
        shareView.addSubview(bgImageView)
        
        let viewCopy = UIImageView(image: UIImage(view: view))
        shareView.addSubview(viewCopy)
        let shareImage = UIImage(view: shareView)
        
        
        let activityViewController = UIActivityViewController(activityItems: [shareContent as NSString, shareImage as UIImage], applicationActivities: nil)
        activityViewController.completionWithItemsHandler = {(act, success, a, error) in
            print("Activity dismissed")
        }
        activityViewController.excludedActivityTypes = [.addToReadingList, .airDrop, .assignToContact, .openInIBooks, .postToFacebook, .postToTwitter, .postToVimeo, .postToWeibo, .postToFlickr, .saveToCameraRoll]
        let subject = "Received Feedback via MyFeedback"
        activityViewController.setValue(subject, forKey: "Subject")
        if let popoverController = activityViewController.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        DispatchQueue.main.async {
            self.present(activityViewController, animated: true, completion: {
                print("Activity shown")
            })
        }
        
    }
    
    func shareViaMail(subject: String, text: String, image: UIImage){
        
        DummyEmailViewController.showMailComposer(ownerViewController: self, toRecipients: [], subject: subject, mailBody: text, mailNotConfiguredTitle: "Error", mailNotConfiguredMessage: "Not able to send Mail through App", attachment: image);
    }
    
    func shareViaMessage(subject: String, text: String, image: UIImage){
        DummyMessageViewController.show(ownerViewController: self, toRecipients: nil, subject: subject, mailBody: text, notConfigured: "Error", notConfiguredMessage: "Not able to send MEssage through App", attachment: image)
    }
    
    func shareFeedback(image: UIImage) {
        
        
        
        
        guard MFMailComposeViewController.canSendMail() else {
            AlertModalView.show(title: "Error", message: "Not able to send Mail through App")
            return
        }
        
        
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
