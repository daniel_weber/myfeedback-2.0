//
//  SettingsViewController.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 09.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    let service = try! Dip.container.resolve() as FeedbackService
    
    var easterCounter = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(SettingsTableViewCell.self, forCellReuseIdentifier: "SettingsTableViewCell")
        service.userSettings.observeNext(with: {userSettings in
            print("Settings changed")
        })
        self.tableView.contentInset = UIEdgeInsetsMake(40, 0, 0, 0)
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let navigationBar = try! Dip.container.resolve() as AlternativeNavigationBar
        let helpOption = BarOption(title: "FAQ", icon: #imageLiteral(resourceName: "Icon_Help")) { () -> Void in
            Onboarding.show()
        }
        navigationBar.update(title: "Settings".localized(), options: [helpOption])
        self.service.AnalyticsOpenViewSettings()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // TableView Data Source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    // TableView Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsTableViewCell", for: indexPath) as! SettingsTableViewCell
        cell.selectionStyle = .none
        cell.backgroundColor = UIColor.clear
        
        switch indexPath.row{
        case 0:
            cell.title = "Logged in as:".localized()
            cell.selectionButton.setTitle("\(loggedInUser.value?.getName() ?? "") (\(loggedInUser.value?.userId ?? ""))", for: .normal)
            cell.selectionButton.backgroundColor = UIColor.clear
            cell.selectionButton.isEnabled = false
            cell.selectionButton.removeTarget(nil, action: nil, for: .touchUpInside)
            cell.selectionButton.addTarget(self, action: #selector(self.handleEasterEgg), for: .touchUpInside)
        case 1:
            cell.title = "Version:".localized()
            cell.selectionButton.setTitle(Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String, for: .normal)
            cell.selectionButton.backgroundColor = UIColor.clear
            cell.selectionButton.isEnabled = false
        case 2:
            cell.title = "Display received requests from last:".localized()
            cell.selectionButton.setTitle(self.service.userSettings.value.showReceivedDuration.localizedName, for: .normal)
            cell.labels = DurationSetting.Durations.map{$0.localizedName}
            cell.options = DurationSetting.Durations.map{$0.key}
            cell.result = { (value, completed) -> Bool in
                let newSettings = self.service.userSettings.value.copy() as! UserSettings
                newSettings.showReceivedDuration = DurationSetting.getDuration(key: value)
                self.updateSettings(settings: newSettings)
                print("New Setting: \(self.service.userSettings.value.showReceivedDuration)")
                return true
            }
        case 3:
            cell.title = "Display own requests from last:".localized()
            cell.selectionButton.setTitle(self.service.userSettings.value.showSendDuration.localizedName, for: .normal)
            cell.labels = DurationSetting.Durations.map{$0.localizedName}
            cell.options = DurationSetting.Durations.map{$0.key}
            cell.result = { (value, completed) -> Bool in
                
                let newSettings = self.service.userSettings.value.copy() as! UserSettings
                newSettings.showSendDuration = DurationSetting.getDuration(key: value)
                self.updateSettings(settings: newSettings)
                print("New Setting: \(self.service.userSettings.value.showSendDuration)")
                return true
            }
        case 4:
            cell.title = "Push Notifications:".localized()
            cell.selectionButton.setTitle(self.service.userSettings.value.pushNotification.localizedName, for: .normal)
            cell.labels = NotificationSetting.Notifications.map{$0.localizedName}
            cell.options = NotificationSetting.Notifications.map{$0.key}
            cell.result = { (value, completed) -> Bool in
                
                let newSettings = self.service.userSettings.value.copy() as! UserSettings
                newSettings.pushNotification = NotificationSetting.getNotificationSetting(key: value)
                self.updateSettings(settings: newSettings)
                print("New Setting: \(self.service.userSettings.value.pushNotification)")
                return true
            }
        case 5:
            cell.title = "Email Notifications:".localized()
            cell.selectionButton.setTitle(self.service.userSettings.value.mailNotification.localizedName, for: .normal)
            cell.labels = NotificationSetting.Notifications.map{$0.localizedName}
            cell.options = NotificationSetting.Notifications.map{$0.key}
            cell.result = { (value, completed) -> Bool in
                
                let newSettings = self.service.userSettings.value.copy() as! UserSettings
                newSettings.mailNotification = NotificationSetting.getNotificationSetting(key: value)
                self.updateSettings(settings: newSettings)
                print("New Setting: \(self.service.userSettings.value.mailNotification)")
                return true
            }
        case 6:
            cell.title = "Language:".localized()
            cell.selectionButton.setTitle(self.service.userSettings.value.language.labelString, for: .normal)
            cell.labels = Language.Languages.map{$0.labelString}
            cell.options = Language.Languages.map{$0.key}
            cell.result = { (value, completed) -> Bool in
                
                let newSettings = self.service.userSettings.value.copy() as! UserSettings
                newSettings.language = Language.getLanguage(key: value)
                self.updateSettings(settings: newSettings)
                return true
            }
        default:
            break
            
        }
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    @objc func handleEasterEgg(){
        easterCounter += 1
        if easterCounter == 10{
            easterCounter = 0
            let mvc : ModalViewController = try! Dip.container.resolve() as ModalViewController
            let alertView = AlertModalView()
            alertView.message = "You just unlocked a new feature!"
            alertView.title = "Happy Easter"
            mvc.present(modal: alertView)
        }
    }
    
    func updateSettings(settings: UserSettings){
        self.service.writeSettings(userSettings: settings).then({_ in
            DispatchQueue.main.async {
                let navigationBar = try! Dip.container.resolve() as AlternativeNavigationBar
                navigationBar.update(title: "Settings".localized())
                self.tableView.reloadData()
            }
        })
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
