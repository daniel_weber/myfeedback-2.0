//
//  RecognitionsOverallViewController.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 15.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import UIKit
import SwipeCellKit
import Bond
import Hydra

class RecognitionsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, SwipeTableViewCellDelegate{
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backgroundImage: UIImageView!
    
    var competency: Competency?
    
    var filteredRecognitions : MutableObservableArray<Recognition> = MutableObservableArray([])
    
    let service = try! Dip.container.resolve() as FeedbackService
    
    var searchTerm : Observable<String?> = Observable("")
    var searchPromise : Promise<[Recognition]>?
    var searchToken : InvalidationToken?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(RecognitionTableViewCell.self, forCellReuseIdentifier: "RecognitionTableViewCell")
        self.tableView.register(EmptySearchEmployeeTableViewCell.self, forCellReuseIdentifier: "EmptySearchEmployeeTableViewCell")
        self.tableView.contentInset = UIEdgeInsetsMake(40, 0, 0, 0)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        
        hero.isEnabled = true
        self.tableView.hero.modifiers = [.translate(y:1000)]
        self.backgroundImage.hero.modifiers = [.fade]
        self.view.backgroundColor = MerckColor.RichPurple
        self.backgroundImage.backgroundColor = MerckColor.RichPurple
        self.service.sortBy.observeNext(with: {_ in
            self.tableView.reloadData()
        })
        self.filteredRecognitions.observeNext(with: {e in
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        })
        // Do any additional setup after loading the view.
    }
    
    func setNavigationBar(){
        let navigationBar = try! Dip.container.resolve() as AlternativeNavigationBar
        let filterIcon: UIImage
        if self.service.activeFilters.array.count > 0{
            filterIcon = #imageLiteral(resourceName: "Icon_Funnel_Filled")
        }else{
            filterIcon = #imageLiteral(resourceName: "Icon_Funnel")
        }
        let filterOptions = BarOption(title: "Filter", icon: filterIcon) { () -> Void in
            let mvc = try! Dip.container.resolve() as ModalViewController
            
            let filterMV = RecognitionFilterTableView()
            
            mvc.present(modal: filterMV, edge: .bottom)
        }
        
        let searchOptions = BarOption(title: "Search", icon: #imageLiteral(resourceName: "Icon_Search")) { () -> Void in
            let searchBar = try! Dip.container.resolve() as BaseSearchBar
            searchBar.isCollapsed = false
        }
        navigationBar.update(title: self.competency?.localName, options: [filterOptions, searchOptions])
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.setNavigationBar()
        
        let searchBar = try! Dip.container.resolve() as BaseSearchBar
        searchBar.searchTextField.reactive.text.bidirectionalBind(to: self.searchTerm)
        self.service.activeFilters.observeNext(with: {_ in
            self.setNavigationBar()
            if self.searchTerm.value != nil{
                self.searchTerm.value! += ""
            }else{
                self.searchTerm.value = ""
            }
            
        })
        let _ =  self.searchTerm.combineLatest(with: self.service.myRecognition){(term, recognitions) -> [Recognition] in
            self.tableView.layer.opacity = 0.4
            self.tableView.isUserInteractionEnabled = false
            
            self.searchPromise = self.filter(recognitions: recognitions.dataSource.array.filter({recognition in return recognition.competency.name == self.competency?.name}), by: term).then(in: Context.main, { (recs) in
                
                self.filteredRecognitions.replace(with: recs)
                
                self.tableView.layer.opacity = 1.0
                self.tableView.isUserInteractionEnabled = true
                
                self.searchPromise = nil
                
            })
            
            return []
            }.observeNext { (reqs) in
                
        }
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filteredRecognitions.array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sortedList = self.filteredRecognitions.array.sorted(by: {a, b in
            return a.value(forKey: self.service.sortBy.value.0.key).debugDescription > b.value(forKey: self.service.sortBy.value.0.key).debugDescription
        })
        let recognition = sortedList[indexPath.row]
        if recognition.id == nil{
            let cell = tableView.dequeueReusableCell(withIdentifier: "EmptySearchEmployeeTableViewCell", for: indexPath) as! EmptySearchEmployeeTableViewCell
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "RecognitionTableViewCell", for: indexPath) as! RecognitionTableViewCell
        cell.icon = recognition.competency.icon
        cell.name = recognition.sender.getName()
        cell.recognitionText = recognition.text
        cell.date = recognition.date
        cell.thankYouFlag = recognition.receivers.first?.isThanked
        if recognition.competency.name == Competency.ThankYouNote.name{
            cell.thankYouFlag = false
        }
        cell.readFlag = recognition.isRead
        cell.delegate = self
        cell.highlightedString = self.searchTerm.value
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let navigationcontroller = try! Dip.container.resolve() as BaseNavigationController
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let sortedList = self.filteredRecognitions.array.sorted(by: {a, b in
            return a.value(forKey: self.service.sortBy.value.0.key).debugDescription > b.value(forKey: self.service.sortBy.value.0.key).debugDescription
        })
        let recognition = sortedList[indexPath.row]
        let vc: UIViewController
        if recognition.competency.name == Competency.ThankYouNote.name{
            vc = storyboard.instantiateViewController(withIdentifier: "ThankYouCardViewController") as! ThankYouCardViewController
            (vc as! ThankYouCardViewController).recognition.value = recognition
        }else{
            vc = storyboard.instantiateViewController(withIdentifier: "RecognitionCardViewController") as! RecognitionCardViewController
            (vc as! RecognitionCardViewController).recognition.value = recognition
            (vc as! RecognitionCardViewController).isInEditMode = false
        }
        
        navigationcontroller.pushViewController(vc, animated: true)
    }
    
    // MARK: SwipeTableViewCellDelegate
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        let sortedList = self.filteredRecognitions.array.sorted(by: {a, b in
            return a.value(forKey: self.service.sortBy.value.0.key).debugDescription > b.value(forKey: self.service.sortBy.value.0.key).debugDescription
        })
        let recognition = sortedList[indexPath.row]
        let cell = self.tableView.cellForRow(at: indexPath)
        let deleteAction = SwipeAction(style: .default, title: nil) { action, indexPath in
            let alertMV = AlertModalView.show(title: nil, message: "Delete".localized()+"?", enableCancel: true)
            alertMV.close = {success in
                if success{
                    let callBack = {
                        if let index = self.filteredRecognitions.array.index(where: {rec in return rec.id == recognition.id}){
                            self.filteredRecognitions.remove(at: index)
                            self.tableView.deleteRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
                            self.tableView.endUpdates()
                            if self.filteredRecognitions.array.count == 0{
                                let navigation = try! Dip.container.resolve() as BaseNavigationController
                                navigation.popViewController(animated: true)
                            }
                        }
                    }
                    self.tableView.beginUpdates()
                    if recognition.competency.name == Competency.ThankYouNote.name{
                        self.service.hideThankYouNote(recognition: recognition).then({_ in
                            callBack()
                        }).catch({e in
                            print("ERROR")
                            print(e)
                            self.handleError(e)
                            action.fulfill(with: ExpansionFulfillmentStyle.reset)
                        })
                    }else{
                        self.service.deleteRecognition(recognition: recognition).then({_ in
                            callBack()
                        }).catch({e in
                            print("ERROR")
                            print(e)
                            self.handleError(e)
                            action.fulfill(with: ExpansionFulfillmentStyle.reset)
                        })
                    }
                    
                }else{
                    action.fulfill(with: .reset)
                }
            }
        }
        let thankYouAction = SwipeAction(style: .default, title: nil) { action, indexPath in
            self.service.thankForRecognition(recognition: recognition).then({resp in
                if resp.success{
                    self.handleAlert(alertText: resp.message!, alertTitle: "Success".localized(), alertIcon: nil)
                }else{
                    self.handleError("Unknown Error!")
                }
                tableView.reloadRows(at: [indexPath], with: .none)
            }).catch({e in self.handleError(e)})
            action.fulfill(with: .reset)
        }
        
        
        // customize the action appearance
        
        let deleteButton = CallToActionButton(frame: CGRect.init(x: 0, y: 0, width: 100, height: 104))
        deleteButton.backgroundColor = MerckColor.RichRed
        deleteButton.setTitle("Delete".localized(), for: .normal)
        deleteAction.image = UIImage.imageWithView(view: deleteButton)
        deleteAction.textColor = MerckColor.RichRed
        deleteAction.backgroundColor = UIColor.clear
        deleteAction.transitionDelegate = ScaleTransition.init(duration: 0.3, initialScale: 0.5, threshold: 0.5)
        
        let thankYouButton = CallToActionButton(frame: CGRect.init(x: 0, y: 0, width: 100, height: 104))
        thankYouButton.backgroundColor = MerckColor.VibrantGreen
        thankYouButton.setTitle("Thank You!".localized(), for: .normal)
        thankYouAction.image = UIImage.imageWithView(view: thankYouButton)
        thankYouAction.textColor = MerckColor.RichRed
        thankYouAction.backgroundColor = UIColor.clear
        thankYouAction.transitionDelegate = ScaleTransition.init(duration: 0.3, initialScale: 0.5, threshold: 0.5)
        
        if orientation == .left{
            return nil
        }else if orientation == .right{
            if recognition.receivers.first?.isThanked == true || recognition.competency.name == Competency.ThankYouNote.name{
                return [deleteAction]
            }
            return [deleteAction,thankYouAction]
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeTableOptions {
        var options = SwipeTableOptions()
        options.expansionStyle = .destructive(automaticallyDelete: false)
        options.transitionStyle = .drag
        options.backgroundColor = UIColor.clear
        return options
    }
    
    // MARK: Recognition Search
    
    private func filter(recognitions: [Recognition], by term: String?) -> Promise<[Recognition]> {
        
        self.searchToken?.invalidate()
        self.searchToken = InvalidationToken()
        
        return Promise<[Recognition]>(in: Context.background, token: self.searchToken, { (resolve, reject, operation) in
            
            
            var filtered : [Recognition] = []
            
            for rec in recognitions {
                
                if !self.filter(rec){
                    continue
                }
                
                if operation.isCancelled {
                    break
                }
                
                if (term == nil || term! == ""){
                    if self.service.activeFilters.array.count == 0{
                        continue
                    }else{
                        filtered.append(rec)
                        continue
                    }
                    
                }
                
                let doAND = true
                if (doAND){
                    if self.considerAND(term: term, recognition: rec) {filtered.append(rec)}
                }
                else {
                    if self.considerOR(term: term, recognition: rec) {filtered.append(rec)}
                }
            }
            
            
            if (operation.isCancelled){
                operation.cancel()
                return
            }
            
            if (filtered.count == 0 && (term == nil || term! == "") && self.service.activeFilters.count == 0){
                filtered.append(contentsOf: recognitions)
            }
            
            if (filtered.count == 0){
                let empty = Recognition.invalid()
                filtered.append(empty)
            }
            
            
            
            resolve(filtered)
        })
    }
    
    // Checks if an Recognition applies to the active
    private func filter(_ recognition: Recognition)->Bool{
        for filter in self.service.activeFilters.array{
            switch filter.type{
            case .Competency:
                if !service.activeFilters.array.filter({f in return f.type == FilterType.Competency}).contains(where: {f in return recognition.competency.name == (f.value as? Competency)?.name}){
                    return false
                }
            case .Employee:
                if !recognition.receivers.contains(where: {receiver in return receiver.receiver.userId == (filter.value as? Employee)?.userId}) && recognition.sender.userId != (filter.value as? Employee)?.userId{
                    return false
                }
            case .DateFrom:
                if recognition.date! < filter.value as! Date{
                    return false
                }
            case .DateTo:
                if recognition.date! > filter.value as! Date{
                    return false
                }
            default:
                break
            }
        }
        return true
    }
    
    func considerAND(term : String?, recognition: Recognition) -> Bool {
        
        if let searchTerms = term?.split(separator: " ") {
            
            for t in searchTerms {
                
                if recognition.searchTerm.contains(t.lowercased()) { continue }
                
                // When there is no hit for one item it's out (AND)
                return false
            }
            
            // When every item in the loop had a hit, this search term fits (AND)
            return true
        }
        
        return false
    }
    
    func considerOR(term : String?, recognition: Recognition) -> Bool {
        
        if let searchTerms = term?.split(separator: " ") {
            
            for t in searchTerms {
                
                if recognition.searchTerm.contains(t.lowercased()) { return true }
                
            }
            
            // When no item in the loop had a hit, this search term does not fit (OR)
            return false
        }
        
        return false
    }
    
}

