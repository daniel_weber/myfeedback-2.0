//
//  RecognitionCardViewController.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 13.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import UIKit
import Bond
import TinyConstraints

class RecognitionCardViewController: UIViewController {
    
    @IBOutlet weak var toButton: UIButton!
    @IBOutlet weak var sendButton: CallToActionButton!
    @IBOutlet weak var recognitionCardView: RecognitionCardView!
    @IBOutlet weak var competencyLabel: UILabel!
    @IBOutlet weak var toLabel: UILabel!
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    var employeeTimer: Timer?
    
    var isInEditMode:Bool = true
    
    var recognition: Observable<Recognition?> = Observable<Recognition?>(nil)
    
    let service = try! Dip.container.resolve() as FeedbackService

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupView()
        let shareOption = BarOption(title: "Share", icon: #imageLiteral(resourceName: "Icon_Feedback")) { [unowned self] () -> Void in
            self.myShareButton()
        }
         let navigationBar = try! Dip.container.resolve() as AlternativeNavigationBar
        if isInEditMode || self.recognition.value?.sender.userId == loggedInUser.value?.userId{
            navigationBar.update(title: self.recognition.value?.competency.localName, options: nil)
        }else{
           navigationBar.update(title: self.recognition.value?.competency.localName, options: [shareOption])
        }

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
        self.employeeTimer?.invalidate()
    }

    func setupView(){
        if Device.IS_IPAD{
            self.recognitionCardView.width(400)
        }
        self.sendButton.setTitle("Send".localized(), for: .normal)
        self.toLabel.text = "To:".localized()
        self.hero.isEnabled = true
        self.backgroundImageView.hero.modifiers = [.translate(y:1000)]
        self.recognition.observeNext(with: {recognition in
            self.recognitionCardView.competency = recognition?.competency
            self.recognitionCardView.sender = recognition?.sender
            self.competencyLabel.text = recognition?.competency.localName
        })
        if isInEditMode{
            self.recognition.value = service.recognitionDraft.value
            self.toButton.addTarget(self, action: #selector(self.handeEmployeSelectionClicked), for: .touchUpInside)
            self.sendButton.isEnabled = service.checkIfValid(self.recognition.value)
            /*if self.service.employeeList.array.filter({emp in return emp.isSelected}).count == 0{
                self.employeeTimer?.invalidate()
                self.employeeTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: {_ in
                    self.showEmployeeSelection()
                })
            }*/
            var selectedEmployeeString = service.employeeList.array.filter({emp in return emp.isSelected}).map{$0.getName()}.joined(separator: ", ")
            if selectedEmployeeString.isEmpty{
                selectedEmployeeString = "Select Employees".localized()
                if(RCValues.sharedInstance.demoMode()){
                    selectedEmployeeString = "Select Users"
                }
            }
            self.toButton.setTitle(selectedEmployeeString, for: .normal)
        }else{
            self.toButton.setTitle(self.recognition.value?.receivers.first?.receiver.getName(), for: .normal)
            if self.recognition.value != nil && !self.recognition.value!.isRead{
                self.service.readRecognition(recognition: self.recognition.value!).then({_ in
                })
            }
        }
        self.toButton.layer.cornerRadius = 5
        self.toButton.isEnabled = isInEditMode
        self.sendButton.isHidden = !isInEditMode
        self.recognitionCardView.commentTextView?.isEditable = isInEditMode
        self.recognitionCardView.comment.value = self.recognition.value?.text
        self.recognitionCardView.comment.observeNext(with: {comment in
            self.recognition.value?.text = comment ?? ""
            self.sendButton.isEnabled = self.service.checkIfValid(self.recognition.value)
        })
        
    }
    
    @IBAction func handleSendButton(_ sender: CallToActionButton) {
        self.sendRecognition()
    }
    
    func sendRecognition(){
        Loading.show()
        self.service.sendRecognition(recognition: self.recognition.value!, receivers: self.service.selectedEmployees).then({resp in
            if resp.success && resp.message != nil {
                AlertModalView.show(title: "Success".localized(), message: resp.message!)
                let navigationcontroller = try! Dip.container.resolve() as BaseNavigationController
                navigationcontroller.popToRootViewController(animated: true)
            }else{
                ErrorView.show(message: "Unknown Error")
            }
        }).catch({e in ErrorView.show(error: e)})
    }
    
    
    @objc func handeEmployeSelectionClicked(){
        self.showEmployeeSelection()
    }
    
    func showEmployeeSelection(){
        let mvc : ModalViewController = try! Dip.container.resolve() as ModalViewController
        let searchEmployeeView = SearchEmployeeView()
        searchEmployeeView.result = { (value, completed) -> Bool in
            var selectedEmployeeString = value.map{$0.getName()}.joined(separator: ", ")
            if selectedEmployeeString.isEmpty{
                selectedEmployeeString = "Select Employees".localized()
                if(RCValues.sharedInstance.demoMode()){
                    selectedEmployeeString = "Select Users"
                }
            }
            self.toButton.setTitle(selectedEmployeeString, for: .normal)
            self.sendButton.isEnabled = self.service.checkIfValid(self.recognition.value)
            return true
        }
        //searchEmployeeView.hero.id = "reAssignToEMployee"
        self.view.endEditing(true)
        mvc.present(modal: searchEmployeeView)
    }
    func myShareButton() {
        let shareText = "\(self.recognition.value?.sender.getName() ?? "") "+"wrote"+":\n \(self.recognition.value?.text ?? "")"
        //displayShareSheet(shareContent: shareText)
        
        let shareView = UIView(frame: self.view.bounds)
        let bgImageView = UIImageView(frame: shareView.bounds)
        bgImageView.image = self.backgroundImageView.image
        bgImageView.clipsToBounds = true
        shareView.addSubview(bgImageView)
        let viewCopy = UIImageView(image: UIImage(view: view))
        shareView.addSubview(viewCopy)
        let shareImage = UIImage(view: shareView)
        
        Share.show(parentViewController: self, body: shareText, image: shareImage)
    }
    
    
    func displayShareSheet(shareContent:String) {
        self.service.AnalyticsActionShareFeedback()
        
        let shareView = UIView(frame: self.view.bounds)
        let bgImageView = UIImageView(frame: shareView.bounds)
        bgImageView.image = self.backgroundImageView.image
        bgImageView.clipsToBounds = true
        shareView.addSubview(bgImageView)
        //let scrollViewCopy = scrollView.copyView() as UIScrollView
        let viewCopy = UIImageView(image: UIImage(view: view))
        shareView.addSubview(viewCopy)
        let shareImage = UIImage(view: shareView)
        let activityViewController = UIActivityViewController(activityItems: [shareContent as NSString, shareImage as UIImage], applicationActivities: nil)
        let subject = "Received Recognition via MyFeedback"
        activityViewController.setValue(subject, forKey: "Subject")
        if let popoverController = activityViewController.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        self.present(activityViewController, animated: true, completion: {})
    }

}
