//
//  SignupDemoViewController.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 16.07.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import UIKit
import Alamofire
import Hydra
import SwiftyJSON


class SignupDemoViewController: UIViewController {
    
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var repeatPasswordTextfield: UITextField!
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func signup(username: String, email: String, password: String, firstName: String, lastName: String)->Promise<(success:Bool, message:String?)>{
        let params: [String:Any] = [
            "username": username,
            "email": email,
            "password": password,
            "firstName": firstName,
            "lastName": lastName
        ]
        let headers: HTTPHeaders = [
            "Content-Type"  : "application/json",
            "x-api-key"  : "IHQywccM4O4IuLD6KqFYP9ZXqCuqIAsv9rrT3Gx7"
        ]
        return Promise(in: .background,  {resolve, reject, _ in
            Alamofire.request("https://ah7v0qwn6g.execute-api.us-east-1.amazonaws.com/prod/sign-up", method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    let id = json["ID"]
                    print(id)
                    resolve((true, nil))
                case .failure(let error):
                    print(error)
                    reject(error)
                }
            }
        })
    }
    

    @IBAction func handleSubmit(_ sender: Any) {
        self.errorLabel.text = nil
        if(userNameTextField.text?.isEmpty != false){
            self.errorLabel.text = "Username must not be empty"
        }else if(emailTextField.text?.isEmpty != false){
            self.errorLabel.text = "Email must not be empty"
        }else if(passwordTextField.text?.isEmpty != false){
            self.errorLabel.text = "Password must not be empty"
        }else if(passwordTextField.text != repeatPasswordTextfield.text){
            self.errorLabel.text = "Passwords do not match"
        }else{
            let alert = UIAlertController(title: "Signed up sucessfully", message: "You can now login using username and password", preferredStyle: .alert)
            let ok = UIAlertAction(title: "Okay", style: .default, handler: {_ in
                self.dismiss(animated: true, completion: nil)
            })
            alert.addAction(ok)
            self.signup(username: userNameTextField.text!, email: emailTextField.text!, password: passwordTextField.text!, firstName: firstName.text ?? "", lastName: lastName.text ?? "").then({resp in
                self.present(alert, animated: true, completion: nil)
            })
            
        }
        
        
    }
    
    @IBAction func handleCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
