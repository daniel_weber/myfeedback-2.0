//Om Sri Sai Ram
//  PreRootViewController.swift
//  CostCenters
//
//  Created by Prasad Babu on 10/04/18.
//  Copyright © 2018 Merck KGaA. All rights reserved.
// This is the pre root(landing) View Controller
// This class will decide to load either Main storyboard or Authentication storayboard

import Foundation
import UIKit
import KeychainSwift
import TinyConstraints

// Replaced enum by Struct to make SAML url dynamic and use currently used HANA instance by using 'SAPcpms.shared.scpAccountId' instead of hard coded scp account id
public struct SamlRedirectionURLs{
    let key: String
    var url:String
    
    static let ADLoginRequired = SamlRedirectionURLs(key: "ADLoginRequired", url: "https://fs.merckgroup.com/adfs/ls/?client-request-id")
    static let AzureAuthenticationRequired = SamlRedirectionURLs(key: "SamlRedirectionURLs", url: "https://login.microsoftonline.com/login.srf?client-request-id")
    static let Authenticated = SamlRedirectionURLs(key: "SamlRedirectionURLs", url: "https://mobile-\(SAPcpms.shared.scpAccountId).hana.ondemand.com/SAMLAuthLauncher?finishEndpointParam=someUnusedValue")
    static let Blank = SamlRedirectionURLs(key: "SamlRedirectionURLs", url: "about:blank")
    static let URLs:[SamlRedirectionURLs] = [
        .ADLoginRequired,
        .AzureAuthenticationRequired,
        .Authenticated,
        .Blank
    ]
    static func fromKey(key: String) -> SamlRedirectionURLs?{
        if let result = URLs.first(where: {url in return url.key == key}){
            return result
        }
        return nil
    }
}

class LoginViewController: UIViewController, UIWebViewDelegate, XMLParserDelegate{
    
    private let authenticatedUrl = ""
    
    
    
    /*private enum SamlRedirectionURLs: String{
     
     //    case Login = "https://mobile-aa4d355e1.hana.ondemand.com/SAMLAuthLauncher"
     case ADLoginRequired = "https://fs.merckgroup.com/adfs/ls/?client-request-id"
     case AzureAuthenticationRequired = "https://login.microsoftonline.com/login.srf?client-request-id" // 2 step auth
     
     // Replaced Static App ID with 'scpAccountId'
     case Authenticated = "https://mobile-aa4d355e1.hana.ondemand.com/SAMLAuthLauncher?finishEndpointParam=someUnusedValue"
     case Blank = "about:blank"
     }*/
    
    //    MARK: Attributes
    var samlInputsMaskPopover: (UIViewController & SamlAuthInputMaskVCDelegate)?
    
    var xmlElement = ""
    var muid: String? = nil
    var firstName: String? = nil
    var lastName: String? = nil
    var email: String? = nil
    
    var employee: Employee? = nil
    
    //    MARK: Outlets
    @IBOutlet weak var myFeedbackLabel: UILabel!
    @IBOutlet weak var webViewSaml: UIWebView!
    @IBOutlet weak var loadingMaskContainer: UIView!
    @IBOutlet weak var reloadBtn: UIButton!
    @IBOutlet weak var errorMessageLabel: UILabel!
    @IBOutlet weak var loginWrapperView: UIView!
    
    //    MARK: View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.errorMessageLabel.textColor = MerckColor.RichRed
        //AuthenticationController.restoreCookies();
        checkValidSessionExists()
        if Device.IS_IPHONE_5{
            self.loginWrapperView.width(250)
            self.myFeedbackLabel.font = MerckFont.font(size: 20)
            self.myFeedbackLabel.height(35)
            self.loginWrapperView.topToSuperview(nil, offset: 100, relation: .equal, priority: .required, isActive: true)
        }else if Device.IS_IPAD{
            self.loginWrapperView.width(400)
            self.myFeedbackLabel.font = MerckFont.font(size: 35)
            self.myFeedbackLabel.height(70)
            self.loginWrapperView.topToSuperview(nil, offset: 250, relation: .equal, priority: .required, isActive: true)
        }else{
            self.loginWrapperView.width(300)
            self.myFeedbackLabel.height(40)
            self.loginWrapperView.topToSuperview(nil, offset: 120, relation: .equal, priority: .required, isActive: true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //    MARK:
    // Check session validity and redirect accordingly.
    func checkValidSessionExists()  {
        loadingMask(isVisible: true, isReplaceable: true)
        SAPcpms.shared.registerDevice(considerKeyChainValue:false).then({(success, appCid) in
            self.loadingMask(isVisible: false)
            if appCid != nil {
                AuthenticationController.createInstaceWithAppID(appCID: appCid!);
                AuthenticationController.resetUserAgent();
                AuthenticationController.sharedController()?.storeCookies();
                
                // Redirect to Cost Centers Home page
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                if let muid = KeychainSwift().get("userId"){
                    if self.employee == nil{
                        self.employee = Employee(userId: muid, muid: muid, firstName: KeychainSwift().get("firstName") ?? "", lastName: KeychainSwift().get("lastName") ?? "", email: KeychainSwift().get("email") ?? "", orgUnitName: "")
                    }
                    loggedInUser.value = self.employee
                }
                appDelegate.redirect2MainVC();
            } else {
                //Redirect to Login Page
                self.loadLoginPage();
            }
        });
    }
    
    func loadLoginPage()
    {
        AuthenticationController.customizeUserAgent();
        webViewSaml.delegate = self
        let urlStr: String? = "https://mobile-\(SAPcpms.shared.scpAccountId).hana.ondemand.com/SAMLAuthLauncher"
        self.webViewSaml.loadRequest(URLRequest(url: URL(string: urlStr!)!))
    }
    
    //    MARK: Update Visibility
    // Update webView and loading mask visibility
    func loadingMask(isVisible: Bool, isReplaceable: Bool=false){
        
        if self.samlInputsMaskPopover == nil || self.viewIfLoaded?.window != nil{
            UIView.beginAnimations(nil, context: nil)
            UIView.setAnimationBeginsFromCurrentState(true)
            UIView.setAnimationDuration(1)
            UIView.setAnimationCurve(.easeInOut)
            self.loadingMaskContainer.alpha = CGFloat(NSNumber(booleanLiteral: isVisible))
            self.webViewSaml.alpha = CGFloat(NSNumber(booleanLiteral: !isVisible))
            UIView.commitAnimations()
            
            self.loadingAnimation(isVisible: isVisible, isReplaceable: isReplaceable)
        }
    }
    
    func loadingAnimation(isVisible: Bool, isReplaceable: Bool){
        
        if isVisible{
            LoadingAnimator.startAnimating(containerView: self.errorMessageLabel.superview!, referenceView: self.errorMessageLabel, isReplaceable: isReplaceable)
        }else{
            LoadingAnimator.stopAnimating(referenceView: nil)
        }
    }
    
    func loadingFailed(with errorMessage: String="", isVisible: Bool){
        
        // set error message
        self.errorMessageLabel.textColor = MerckColor.RichRed
        errorMessageLabel.text = errorMessage
        
        UIView.animate(withDuration: 0.4, animations: {
            self.reloadBtn.alpha = CGFloat(exactly: NSNumber(booleanLiteral: isVisible))!
            self.errorMessageLabel.alpha = CGFloat(exactly: NSNumber(booleanLiteral: isVisible))!
        }) { (_) in
            self.errorMessageLabel.isHidden = !isVisible
            self.reloadBtn.isHidden = !isVisible
        }
    }
    
    //    MARK: UIWebView Delegate
    func webViewDidStartLoad(_ webView: UIWebView) {
        loadingMask(isVisible: true)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        loadingMask(isVisible: false)
        var finishTimer: Timer?
        finishTimer?.invalidate()
        //check for SAML Response
        if let samlResponse = webView.stringByEvaluatingJavaScript(from: "document.getElementsByName(\"SAMLResponse\")[0].value"){
            if !samlResponse.isEmpty{
                self.getSamlResponsefromHTML(encodedXML: samlResponse)
            }
        }
        
        guard let urlStr = webView.request?.url?.absoluteString else{return}
        let authUrl = "https://mobile-\(SAPcpms.shared.scpAccountId).hana.ondemand.com/SAMLAuthLauncher"
        
        if urlStr.starts(with: SamlRedirectionURLs.ADLoginRequired.url){
            // AD authentication, User name and password required page
            let errorMsg = self.checkForError()
            if self.samlInputsMaskPopover != nil{
                // present error message
                self.samlInputsMaskPopover!.displayError(errorMessage: errorMsg)
            }
            else{
                // request login
                presentAuthenticationLoginMask()
            }
        }
            //        else if urlStr.starts(with: SamlRedirectionURLs.AzureAuthenticationRequired.rawValue){
            //            // 2 step auth
            //            // Handle different 2 step authentication types
            //            dismissInputMaskPopOver();
            //        }
        else if urlStr == SamlRedirectionURLs.Authenticated.url{
            // Authenticated
            checkValidSessionExists(); //Register device and if valid session it will redirect to main VC
            self.webViewSaml.loadRequest(URLRequest(url: URL(string: "about:blank")!))
        }
        else if urlStr == SamlRedirectionURLs.Blank.url{
            self.dismiss(animated: true, completion: nil)
        }else if  urlStr == authUrl && webView.stringByEvaluatingJavaScript(from: "document.documentElement.outerHTML") == "<html><head></head><body></body></html>"{
            finishTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: {_ in self.reloadAction(nil) })
        }
        else{
            // unhandled scenarios
            dismissInputMaskPopOver();
        }
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        
        LoadingAnimator.stopAnimating(referenceView: self.errorMessageLabel)
        dismissInputMaskPopOver();
        self.loadingFailed(with: error.localizedDescription, isVisible: true)
    }
    
    //    MARK: Action
    @IBAction func reloadAction(_ sender: UIButton?) {
        
        self.loadingFailed(isVisible: false)
        self.checkValidSessionExists()
    }
    
    func presentAuthenticationLoginMask(){
        
        dismissInputMaskPopOver();
        
        webViewSaml.isHidden = true;
        
        guard let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "loginViewController") as? AuthenticationLoginMaskViewController else{return}
        loginVC.authReqDelegate = self
        samlInputsMaskPopover = loginVC
        present(loginVC, animated: false, completion: nil)
    }
    
    func dismissInputMaskPopOver(){
        
        webViewSaml.isHidden=false
        if(self.samlInputsMaskPopover != nil){ //Check with child loading view
            self.samlInputsMaskPopover?.dismiss(animated: true, completion: nil)
            self.samlInputsMaskPopover = nil;
        }
    }
    
    //    MARK: Validation
    func checkForError()->String? {
        
        if let errorMessage = self.webViewSaml.stringByEvaluatingJavaScript(from: "$('.tfa_error_text:visible').eq(0).html()"), !errorMessage.isEmpty {
            return errorMessage
        }
        if let errorMessage = self.webViewSaml.stringByEvaluatingJavaScript(from: "document.getElementById(\"errorText\").innerHTML"), !errorMessage.isEmpty {
            return errorMessage
        }
        if let errorMessage = self.webViewSaml.stringByEvaluatingJavaScript(from: "document.getElementById(\"tfa_error_container\").innerHTML"), errorMessage.contains("style=\"display: block;\"") {
            return errorMessage
        }
        if let errorMessage = self.webViewSaml.stringByEvaluatingJavaScript(from: "$('#tfa_results_container').children('.tfa_results_text:visible').html();"), !errorMessage.isEmpty {
            return errorMessage
        }
        return nil
    }
    
    // MARK: XMLParser Delegate
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        if elementName == "NameID" {
            xmlElement = elementName
        }else if attributeDict.first(where: {(k,v) in return k == "Name" && v == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/givenname"}) != nil{
            xmlElement = "firstName"
        }else if let kvp = attributeDict.first(where: {(k,v) in return k == "Name" && v == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/surname"}){
            xmlElement = "lastName"
        }else if let kvp = attributeDict.first(where: {(k,v) in return k == "Name" && v == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress"}){
            xmlElement = "email"
        }else if xmlElement == "firstName" || xmlElement == "lastName" || xmlElement == "email"{
            xmlElement = xmlElement+elementName
        }else{
            xmlElement = ""
        }
        
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        if xmlElement == "NameID"{
            KeychainSwift().set(string, forKey: "userId")
            self.muid = string
        }else if xmlElement == "firstNameAttributeValue"{
            KeychainSwift().set(string, forKey: "firstName")
            self.firstName = string
        }else if xmlElement == "lastNameAttributeValue"{
            self.lastName = string
            KeychainSwift().set(string, forKey: "lastName")
        }else if xmlElement == "emailAttributeValue"{
            self.email = string
            KeychainSwift().set(string, forKey: "email")
        }
        
        if(self.muid != nil && self.firstName != nil && self.lastName != nil && self.email != nil){
            self.employee = Employee(userId: muid!, muid: muid!, firstName: firstName!, lastName: lastName!, email: email!, orgUnitName: "")
            loggedInUser.value = employee
        }
    }
    
    func getSamlResponsefromHTML(encodedXML:String){
        if let data = Data(base64Encoded: encodedXML){
            let parser = XMLParser(data: data)
            parser.delegate = self
            parser.parse()
        }
    }
}

extension LoginViewController: SamlAuthInputMaskOwnerDelegate{
    
    func validateCredentials(username: String, password: String) {
        
        // inject value in webView
        if username.isEmpty || password.isEmpty {
            self.samlInputsMaskPopover!.displayError(errorMessage: "Username and Password must not be empty.")
            return
        }
        let pwReplaced = password.replacingOccurrences(of: "\'", with: "\\'")
        
        // Select "Keep me signed in" checkbox
        var js = "var myelement = document.getElementById('kmsiInput');myelement.click();"
        // Fill Azure id into respective Field
        js += "var myelement = document.getElementById('userNameInput');myelement.value= '\(username)';"
        // Fill  Windows password into respective Field
        js += "var myelement = document.getElementById('passwordInput');myelement.value= '\(pwReplaced)';"
        // Submit the login form
        js += "document.forms[\'loginForm\'].submit();"
        print(js);
        DispatchQueue.main.async {
            self.webViewSaml?.stringByEvaluatingJavaScript(from: js)
        }
    }
    
    func validateAuthenticatorAppToken(token: String)
    {}
    
    func validateOTP(otpNumber: String)
    {}
    
}
