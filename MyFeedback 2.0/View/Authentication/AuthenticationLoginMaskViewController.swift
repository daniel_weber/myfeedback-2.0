//
//  AuthenticationLoginMaskViewController.swift
//  CostCenters
//
//  Created by Daniel Weber on 14.09.17.
//  Copyright © 2017 Merck KGaA. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import TinyConstraints
import Hydra
import Alamofire
import KeychainSwift
import SwiftyJSON

class AuthenticationLoginMaskViewController: UIViewController {
    
    // MARK: Attributes
    var authReqDelegate: SamlAuthInputMaskOwnerDelegate?
    
    // MARK: Outlets
    @IBOutlet weak var loginWrapperView: UIView!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var myFeedbackLabel: UILabel!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var signupButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
   
        usernameTextField.reactive.controlEvents(.editingDidEndOnExit).observe(with: {_ in
            self.passwordTextField.becomeFirstResponder()
        })
        passwordTextField.reactive.controlEvents(.editingDidEndOnExit).observe(with: {_ in
            self.loginButtonPressed()
        })
        
        if Device.IS_IPHONE_5{
            self.loginWrapperView.width(250)
            self.myFeedbackLabel.font = MerckFont.font(size: 20)
            self.myFeedbackLabel.height(35)
            self.loginWrapperView.topToSuperview(nil, offset: 100, relation: .equal, priority: .required, isActive: true)
            self.loginButton.width(130)
            self.loginButton.topToBottom(of: self.passwordTextField, offset: 15, relation: .equal, priority: .required, isActive: true)
        }else if Device.IS_IPAD{
            self.loginWrapperView.width(400)
            self.myFeedbackLabel.font = MerckFont.font(size: 35)
            self.myFeedbackLabel.height(70)
            self.loginWrapperView.topToSuperview(nil, offset: 250, relation: .equal, priority: .required, isActive: true)
            self.loginButton.width(200)
            loginButton.leftToSuperview(nil, offset: 100, relation: .equal, priority: .required, isActive: true)
            self.loginButton.topToBottom(of: self.passwordTextField, offset: 30, relation: .equal, priority: .required, isActive: true)
        }else{
            self.loginWrapperView.width(300)
            self.myFeedbackLabel.height(40)
            self.loginButton.width(150)
            self.loginButton.topToBottom(of: self.passwordTextField, offset: 20, relation: .equal, priority: .required, isActive: true)
            self.loginWrapperView.topToSuperview(nil, offset: 120, relation: .equal, priority: .required, isActive: true)
        }
        
        // Do any additional setup after loading the view.
        self.signupButton.isHidden = !RCValues.sharedInstance.demoMode()
        if(RCValues.sharedInstance.demoMode()){
            self.usernameTextField.placeholder = "Username"
            self.passwordTextField.placeholder = "Password"
        }else{
            self.usernameTextField.placeholder = "e.g. M123456@eu.merckgroup.com"
            self.passwordTextField.placeholder = "Windows Password"
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func demosignin(username: String, password: String)->Promise<(success:Bool, message:String?)>{
        let params: [String:Any] = [
            "username": username,
            "password": password
        ]
        let headers: HTTPHeaders = [
            "Content-Type"  : "application/json",
            "x-api-key"  : "IHQywccM4O4IuLD6KqFYP9ZXqCuqIAsv9rrT3Gx7"
        ]
        return Promise(in: .background,  {resolve, reject, _ in
            Alamofire.request("https://ah7v0qwn6g.execute-api.us-east-1.amazonaws.com/prod/log-in", method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    print(json)
                    let success = json["success"].boolValue
                    let message = json["message"].rawString() ?? ""
                    resolve((success, message))
                case .failure(let error):
                    print(error)
                    reject(error)
                }
            }
        })
    }
    
    func demoLogin(){
        self.showActivityIndicator()
        self.view.endEditing(true)
        clearErrorMessage()
        if let username = self.usernameTextField.text, let password = self.passwordTextField.text {
            // inject values in webView and validate
            self.demosignin(username: username, password: password).then({resp in
                print(resp.success)
                if(resp.success){
                    let json = JSON.init(parseJSON: resp.message!)
                    let _userPw = "\(username):\(password)"
                    let _userPwBase64 = _userPw.data(using: .utf8)?.base64EncodedString()
                    let basicString = "Basic TVlGRUVEQkFDS19ERU1POmJDU0pzNm1G"
                    //let basicString = "Basic \(_userPwBase64 ?? "")"
                    KeychainSwift().set(basicString, forKey: "basicAuth")
                    KeychainSwift().set(json["username"].stringValue, forKey: "userId")
                    KeychainSwift().set(json["firstName"].stringValue, forKey: "firstName")
                    KeychainSwift().set(json["lastName"].stringValue, forKey: "lastName")
                    KeychainSwift().set(json["email"].stringValue, forKey: "email")
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    SAPcpms.shared.httpHeaders["Authorization"] = KeychainSwift().get("basicAuth") ?? ""
                    if let muid = KeychainSwift().get("userId"){
                        let employee = Employee(userId: muid, muid: muid, firstName: KeychainSwift().get("firstName") ?? "", lastName: KeychainSwift().get("lastName") ?? "", email: KeychainSwift().get("email") ?? "", orgUnitName: "")
                        loggedInUser.value = employee
                    }
                    appDelegate.redirect2MainVC();
                }
                
            })
        }
    }
    
    func handleLogin(){
        self.showActivityIndicator()
        self.view.endEditing(true)
        clearErrorMessage()
        if let username = self.usernameTextField.text, let password = self.passwordTextField.text {
            // inject values in webView and validate
            if authReqDelegate != nil{
                authReqDelegate?.validateCredentials(username: username, password: password)
            }
        }
    }
    
    // MARK: Actions
    @IBAction func loginButtonPressed() {
        if(RCValues.sharedInstance.demoMode()){
            self.demoLogin()
        }else{
            self.handleLogin()
        }
    }
    
    @IBAction func helpButtonPressed(){
        if(RCValues.sharedInstance.demoMode()){
            self.performSegue(withIdentifier: "DataPrivacy", sender: nil)
        }else{
            let mvc = ModalViewController()
            mvc.modalPresentationStyle = .overCurrentContext
            let alertModal = AlertModalView()
            alertModal.title = "Info"
            alertModal.message = "For your first log-in, please enter your UserID@[xx].merckgroup.com and your Windows password.\n\n[XX] stands for your domain area:\nap = Asia Pacific\neu = Europe\nla = Latin America\nna = North America\n\nIf you are a Sigma-Aldrich user please use [SHORTNAME]@global.sial.com"
            alertModal.cancelButton.isHidden = true
            alertModal.close = {closed in
                Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false, block: {_ in
                    mvc.dismiss(animated: false, completion: nil)
                })
            }
            
            self.present(mvc, animated: true, completion: {
                mvc.present(modal: alertModal)
            })
        }
    }
    
    func showActivityIndicator() {
        
        LoadingAnimator.startAnimating(containerView: self.loginWrapperView, referenceView: self.errorLabel, isReplaceable: true)
        self.loginButton.isEnabled = false
        UIView.animate(withDuration: 0.3) {
            self.myFeedbackLabel.alpha = 0
            self.loginButton.alpha = 0.7
        }
    }
    
    func hideActivityIndicator() {
        
       LoadingAnimator.stopAnimating(referenceView: self.errorLabel)
        self.loginButton.isEnabled = true
        UIView.animate(withDuration: 0.3) {
            self.myFeedbackLabel.alpha = 1
            self.loginButton.alpha = 1.0
        }
    }
    
    func handleAzureError(_ errorMessage: String) {
        
        self.errorLabel.text = errorMessage.trimmingCharacters(in: .whitespacesAndNewlines)
        self.errorLabel.textColor = MerckColor.RichRed
        
        UIView.animate(withDuration: 0.5, animations: {
            self.errorLabel.alpha = 1
        })
        self.hideActivityIndicator()
    }
    
    func clearErrorMessage() {
        
        // Fixed: Animation delays clearing error label, causing it to clear after the new error text is set.
        //self.usernameTextField.changeColor(color: UIColor.white)
        //self.passwordTextField.changeColor(color: UIColor.white)
        self.errorLabel.text = nil
        
    }
    
    
}

extension AuthenticationLoginMaskViewController: SamlAuthInputMaskVCDelegate{
    
    func displayError(errorMessage: String?) {
        
        guard let message = errorMessage else{return}
        self.handleAzureError(message)
    }
}
