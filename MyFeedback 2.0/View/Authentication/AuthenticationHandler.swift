//
//  AuthenticationHandler.swift
//  CostCenters
//
//  Created by Prateek Pande on 11/04/18.
//  Copyright © 2018 Merck KGaA. All rights reserved.
//

import Foundation

protocol SamlAuthInputMaskOwnerDelegate{
    
    /**
     Validate credentials by injecting in webview
     */
    func validateCredentials(username: String, password: String)
    
    /**
     Fetch token from authenticator and inject in webview
     */
    func validateAuthenticatorAppToken(token: String)
    
    /**
     Validate otp by injectinng in webview
     */
    func validateOTP(otpNumber: String)
}

protocol SamlAuthInputMaskVCDelegate{
    
    /**
     Check error status from webView
     - parameter errorMessage: authentication error response
     */
    func displayError(errorMessage: String?)
}
