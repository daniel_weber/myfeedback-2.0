// Om Sri Sai Ram
//  DummyEmailViewController.swift
//  CostCenters
//
//  Created by Thomas Kosiewski on 05.12.17.
//  Copyright © 2017 Merck KGaA. All rights reserved.
//

import UIKit
import MessageUI;

class DummyMessageViewController: UIViewController, MFMessageComposeViewControllerDelegate{
    
    static var sharedView:DummyMessageViewController = DummyMessageViewController();
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.alpha = 0
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        controller.dismiss(animated: true) {
            self.view.removeFromSuperview();
        }
    }
    
    class func show(ownerViewController: UIViewController, toRecipients: [String]?, subject: String, mailBody: String, notConfigured: String, notConfiguredMessage: String, attachment: UIImage?) {
        
        
        guard MFMessageComposeViewController.canSendAttachments() else {
            AlertModalView.showOnViewController(parentViewController: ownerViewController, title: notConfigured, message: notConfiguredMessage);
            return
        }
        
        let dvc : DummyMessageViewController = DummyMessageViewController();
        dvc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        dvc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        
        let composeVC = MFMessageComposeViewController();
        composeVC.messageComposeDelegate = dvc
        composeVC.body = mailBody
        if attachment != nil, let data = UIImagePNGRepresentation(attachment!) as Data?{
            composeVC.addAttachmentData(data, typeIdentifier: "image/png", filename: "feedback.png")
        }
        
        composeVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        composeVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        ownerViewController.view.addSubview(dvc.view);
        dvc.present(composeVC, animated: true, completion: nil);
    }
}

class DummyEmailViewController: UIViewController, MFMailComposeViewControllerDelegate {
    
    static var sharedView:DummyEmailViewController = DummyEmailViewController();
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.alpha = 0
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true) {
            self.view.removeFromSuperview();
            //            self.dismiss(animated: false, completion: nil);
        }
    }
    
    class func showMailComposer(ownerViewController: UIViewController, toRecipients: [String]?, subject: String, mailBody: String, mailNotConfiguredTitle: String, mailNotConfiguredMessage: String, attachment: UIImage?) {
        
        
        guard MFMailComposeViewController.canSendMail() else {
            AlertModalView.showOnViewController(parentViewController: ownerViewController, title: mailNotConfiguredTitle, message: mailNotConfiguredMessage);
            return
        }
        
        let dvc : DummyEmailViewController = DummyEmailViewController();
        dvc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        dvc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        
        let composeVC = MFMailComposeViewController();
        composeVC.mailComposeDelegate = dvc
        composeVC.setToRecipients(toRecipients)
        composeVC.setSubject(subject)
        composeVC.setMessageBody(mailBody, isHTML: true)
        if attachment != nil{
            composeVC.addAttachmentData(UIImageJPEGRepresentation(attachment!, CGFloat(1.0))!, mimeType: "image/jpeg", fileName:  "feedback.jpeg")
        }
        composeVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        composeVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        ownerViewController.view.addSubview(dvc.view);
        dvc.present(composeVC, animated: true, completion: nil);
    }
    
}
