//
//  GetFeedbackViewController.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 11.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class GetFeedbackViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backgroundImage: UIImageView!
    
    let service = try! Dip.container.resolve() as FeedbackService
    
    var repetitionCellIsShown: Bool = false
    
    var employeeTimer: Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(NewRequestSelectionTableViewCell.self, forCellReuseIdentifier: "NewRequestSelectionTableViewCell")
        self.tableView.register(NewRequestTextFieldTableViewCell.self, forCellReuseIdentifier: "NewRequestTextFieldTableViewCell")
        self.tableView.register(NewRequestTextViewTableViewCell.self, forCellReuseIdentifier: "NewRequestTextViewTableViewCell")
        self.tableView.register(NewRequestScaleTableViewCell.self, forCellReuseIdentifier: "NewRequestScaleTableViewCell")
        self.tableView.register(NewRequestSelectionOnlyTableViewCell.self, forCellReuseIdentifier: "NewRequestSelectionOnlyTableViewCell")
        
        self.tableView.contentInset = UIEdgeInsetsMake(40, 0, 0, 0)
        
        hero.isEnabled = true
        self.tableView.hero.modifiers = [.translate(y:1000)]
        self.backgroundImage.hero.modifiers = [.fade]
        self.view.backgroundColor = MerckColor.RichPurple
        
        if service.requestDraft.value == nil{
            service.requestDraft.value = Request(id: nil, requester: loggedInUser.value!, category: nil, type: nil, text: "", title: nil, date: Date(), continuousInterval: ContinuousInterval.OneTime, continuousRepetitions: 1, scaleTextMin: nil, scaleTextMax: nil, averageResponseRating: nil)
        }
        self.service.AnalyticsActionRequestFeedback()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        let navigationBar = try! Dip.container.resolve() as AlternativeNavigationBar
        navigationBar.update(title: "Get Feedback".localized(), options: nil)
        /*if self.service.employeeList.array.filter({emp in return emp.isSelected}).count == 0{
            self.employeeTimer?.invalidate()
            self.employeeTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: {_ in
                self.showEmployeeSelection()
            })
        }*/
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.employeeTimer?.invalidate()
        self.view.endEditing(true)
        //service.deselectEmployees()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // TableView Data Source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    // TableView Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var noOfRows = 7
        if self.service.requestDraft.value?.type?.key == RequestType.Stars.key{
            noOfRows += 1
        }
        if self.service.requestDraft.value?.continuousInterval != nil && self.service.requestDraft.value?.continuousInterval?.key != ContinuousInterval.OneTime.key{
            noOfRows += 1
        }
        
        return noOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell
        switch indexPath.row{
        case 0:
            cell = tableView.dequeueReusableCell(withIdentifier: "NewRequestSelectionTableViewCell", for: indexPath) as! NewRequestSelectionTableViewCell
            (cell as! NewRequestSelectionTableViewCell).title = "To:".localized()
            var selectedEmployeeString = service.selectedEmployees.map{$0.getName()}.joined(separator: ", ")
            if selectedEmployeeString.isEmpty{
                selectedEmployeeString = "Select Employees".localized()
                if(RCValues.sharedInstance.demoMode()){
                    selectedEmployeeString = "Select Users"
                }
            }
            (cell as! NewRequestSelectionTableViewCell).selectionButton.setTitle(selectedEmployeeString, for: .normal)
            (cell as! NewRequestSelectionTableViewCell).selectionButton.removeTarget(cell, action: nil, for: .touchUpInside)
            (cell as! NewRequestSelectionTableViewCell).selectionButton.addTarget(self, action: #selector(self.handeEmployeSelectionClicked), for: .touchUpInside)
        case 1:
            cell = tableView.dequeueReusableCell(withIdentifier: "NewRequestSelectionTableViewCell", for: indexPath) as! NewRequestSelectionTableViewCell
            (cell as! NewRequestSelectionTableViewCell).title = "Feedback category *".localized()
            (cell as! NewRequestSelectionTableViewCell).selectionButton.setTitle(self.service.requestDraft.value?.category?.localName ?? "Please choose".localized(), for: .normal)
            (cell as! NewRequestSelectionTableViewCell).labels = RequestCategory.Categories.map{$0.localName}
            (cell as! NewRequestSelectionTableViewCell).options = RequestCategory.Categories.map{$0.name}
            (cell as! NewRequestSelectionTableViewCell).result = { (value, completed) -> Bool in
                self.service.requestDraft.value?.category = RequestCategory.fromId(name: value)
                return true
            }
        case 2:
            cell = tableView.dequeueReusableCell(withIdentifier: "NewRequestTextFieldTableViewCell", for: indexPath) as! NewRequestTextFieldTableViewCell
            (cell as! NewRequestTextFieldTableViewCell).title = "Feedback title".localized()
            (cell as! NewRequestTextFieldTableViewCell).placeHolder = "Add Feedback Title here".localized()
            (cell as! NewRequestTextFieldTableViewCell).textField.text = self.service.requestDraft.value?.title
            (cell as! NewRequestTextFieldTableViewCell).limit = 25
            (cell as! NewRequestTextFieldTableViewCell).textField.delegate = self
            (cell as! NewRequestTextFieldTableViewCell).textField.reactive.text.observeNext { text in
                self.service.requestDraft.value?.title = text
            }
            if let title = self.service.requestDraft.value?.title{
                (cell as! NewRequestTextFieldTableViewCell).textField.text = title
            }
            cell.backgroundColor = UIColor.clear
            cell.selectionStyle = .none
        case 3:
            cell = tableView.dequeueReusableCell(withIdentifier: "NewRequestTextViewTableViewCell", for: indexPath) as! NewRequestTextViewTableViewCell
            (cell as! NewRequestTextViewTableViewCell).title = "Feedback request *".localized()
            (cell as! NewRequestTextViewTableViewCell).placeHolder = "Add individual feedback question here, e.g. 'How did you like my presentation today? Is there anything I can improve?'".localized()
            (cell as! NewRequestTextViewTableViewCell).comment = self.service.requestDraft.value?.text
            (cell as! NewRequestTextViewTableViewCell).result = { (value, completed) -> Bool in
                if completed{
                    self.service.requestDraft.value?.text = value
                }
                return true
            }
        case 4:
            cell = tableView.dequeueReusableCell(withIdentifier: "NewRequestSelectionTableViewCell", for: indexPath) as! NewRequestSelectionTableViewCell
            (cell as! NewRequestSelectionTableViewCell).title = "What kind of feedback *".localized()
            (cell as! NewRequestSelectionTableViewCell).selectionButton.setTitle(self.service.requestDraft.value?.type?.localName ?? "Please choose".localized(), for: .normal)
            (cell as! NewRequestSelectionTableViewCell).labels = RequestType.Types.map{$0.localName}
            (cell as! NewRequestSelectionTableViewCell).options = RequestType.Types.map{$0.key}
            (cell as! NewRequestSelectionTableViewCell).result = { (value, completed) -> Bool in
                if(value != self.service.requestDraft.value?.type?.key){
                    self.service.requestDraft.value?.type = RequestType.fromKey(key: value)
                    self.toggleScaleCell(show: value == RequestType.Stars.key)
                }
                self.service.requestDraft.value?.type = RequestType.fromKey(key: value)
                return true
            }
        case 5:
            if self.service.requestDraft.value?.type?.key == RequestType.Stars.key{
                cell = self.renderScaleCell(indexPath: indexPath)
            }else{
                cell = self.renderContinousIntervalCell(indexPath: indexPath)
            }
        case 6:
            if indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1{
                cell = self.renderSendButtonCell()
            }else if self.service.requestDraft.value?.continuousInterval != nil && self.service.requestDraft.value?.continuousInterval?.key != ContinuousInterval.OneTime.key && self.service.requestDraft.value?.type?.key != RequestType.Stars.key{
                cell = self.renderRepetitionsCell(indexPath: indexPath)
            }else{
                cell = self.renderContinousIntervalCell(indexPath: indexPath)
            }
        case 7:
            if indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1{
                cell = self.renderSendButtonCell()
            }else{
                cell = self.renderRepetitionsCell(indexPath: indexPath)
            }
            
        case 8:
            cell = self.renderSendButtonCell()
        default:
            cell = UITableViewCell()
        }
        
        cell.backgroundColor = UIColor.clear
        cell.selectionStyle = .none
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 3{
            return 150
        }
        return 75
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //showEmployeeSelection()
    }
    
    func renderScaleCell(indexPath: IndexPath)->NewRequestScaleTableViewCell{
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "NewRequestScaleTableViewCell", for: indexPath) as! NewRequestScaleTableViewCell
        cell.scaleMaxTextField.text = self.service.requestDraft.value?.scaleTextMax
        cell.scaleMinTextField.text = self.service.requestDraft.value?.scaleTextMin
        
        cell.scaleMaxTextField.reactive.text.observeNext { text in
            self.service.requestDraft.value?.scaleTextMax = text
        }
        cell.scaleMinTextField.reactive.text.observeNext { text in
            self.service.requestDraft.value?.scaleTextMin = text
        }
        
        
        return cell
    }
    
    func renderSendButtonCell()->UITableViewCell{
        let cell = UITableViewCell()
        let sendButton = CallToActionButton()
        sendButton.setTitle("Send".localized(), for: .normal)
        sendButton.titleLabel?.font = UIFont(name: "HelveticaNeue-Bold", size: 15)
        cell.addSubview(sendButton)
        sendButton.top(to: cell, nil, offset: 8, relation: .equal, priority: .required, isActive: true)
//        sendButton.left(to: cell, nil, offset: 16, relation: .equal, priority: .required, isActive: true)
//        sendButton.right(to: cell, nil, offset: -16, relation: .equal, priority: .required, isActive: true)
        sendButton.height(40)
        sendButton.width(min(UIScreen.main.bounds.width - 32, 400))
        sendButton.centerXToSuperview()
        sendButton.isEnabled = self.service.checkIfValid(self.service.requestDraft.value)
        sendButton.addTarget(self, action: #selector(self.handleSend), for: .touchUpInside)
        self.service.requestDraft.value?.isValid.observeNext(with: {isValid in
            sendButton.isEnabled = self.service.checkIfValid(self.service.requestDraft.value)
        })
        
        return cell
    }
    
    func renderContinousIntervalCell(indexPath: IndexPath)->NewRequestSelectionTableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewRequestSelectionTableViewCell", for: indexPath) as! NewRequestSelectionTableViewCell
        cell.title = "Single or Repeat?".localized()
        cell.selectionButton.setTitle(self.service.requestDraft.value?.continuousInterval?.localName ?? "Please choose".localized(), for: .normal)
        cell.labels = ContinuousInterval.Intervals.map{$0.localName}
        cell.options = ContinuousInterval.Intervals.map{$0.key}
        cell.result = { (value, completed) -> Bool in
            if(value != self.service.requestDraft.value?.continuousInterval?.key){
                self.service.requestDraft.value?.continuousInterval = ContinuousInterval.fromKey(key: value)
                self.toggleRepetitionCell(show: value != ContinuousInterval.OneTime.key)
            }
            self.service.requestDraft.value?.continuousInterval = ContinuousInterval.fromKey(key: value)
            return true
        }
        return cell
    }
    
    func renderRepetitionsCell(indexPath: IndexPath)->NewRequestSelectionOnlyTableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewRequestSelectionOnlyTableViewCell", for: indexPath) as! NewRequestSelectionOnlyTableViewCell
        cell.selectionButton.setTitle(self.service.requestDraft.value?.continuousRepetitions.description ?? "2", for: .normal)
        cell.labels = ["2","3","4","5"]
        cell.options = ["2","3","4","5"]
        cell.result = { (value, completed) -> Bool in
            self.service.requestDraft.value?.continuousRepetitions = Int(value) ?? 1
            return true
        }
        
        return cell
    }
    
    func toggleScaleCell(show: Bool){
        let indexPath = IndexPath(row: 5, section: 0)
        if show{
            self.tableView.insertRows(at: [indexPath], with: .top)
        }else if (self.tableView.cellForRow(at: indexPath) as? NewRequestScaleTableViewCell) != nil{
            self.tableView.deleteRows(at: [indexPath], with: .bottom)
        }
        
    }
    
    func toggleRepetitionCell(show: Bool){
        if show && !self.repetitionCellIsShown{
            let indexPath = IndexPath(row: self.tableView.numberOfRows(inSection: 0)-1, section: 0)
            self.tableView.insertRows(at: [indexPath], with: .top)
        }else if (self.tableView.cellForRow(at: IndexPath(row: self.tableView.numberOfRows(inSection: 0)-2, section: 0)) as? NewRequestSelectionOnlyTableViewCell) != nil, show == false{
            self.tableView.deleteRows(at: [IndexPath(row: self.tableView.numberOfRows(inSection: 0)-2, section: 0)], with: .bottom)
        }
        self.repetitionCellIsShown = show
        
    }
    
    @objc func handeEmployeSelectionClicked(){
        self.showEmployeeSelection()
    }
    
    @objc func handleSend(){
        if let request = self.service.requestDraft.value{
            Loading.show()
            self.service.sendFeedbackRequest(request: request, receivers: self.service.selectedEmployees).then({response in
                if response.success{
                    AlertModalView.show(title: "Success".localized(), message: response.message!)
                    let navigationcontroller = try! Dip.container.resolve() as BaseNavigationController
                    navigationcontroller.popToRootViewController(animated: true)
                }
            }).catch({e in
                ErrorView.show(error: e)
            })
        }
        
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        // Prevent title length to bbe longer than 97 characters
        if !string.isEmpty{
            if let count = textField.text?.count{
                if count >= 150{
                    return false
                }
                
            }
        }
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        // Prevent title length to bbe longer than 97 characters
        if textField.text != nil{
            if let count = textField.text?.count{
                if count >= 149{
                    let index = textField.text!.index(textField.text!.startIndex, offsetBy: 149)
                    textField.text = textField.text!.substring(to: index)
                    return true
                }
            }
        }
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        
        let doneToolbar: UIToolbar = UIToolbar(frame: textField.bounds)
        doneToolbar.barStyle = UIBarStyle.blackOpaque
        doneToolbar.setShadowImage(UIImage(), forToolbarPosition: .any)
        doneToolbar.barTintColor = MerckColor.VibrantGreen
        doneToolbar.tintColor = MerckColor.RichPurple
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done".localized(), style: UIBarButtonItemStyle.done, target: self, action: #selector(self.doneButtonAction))
        let suggestions: UIBarButtonItem = UIBarButtonItem(title: "Select from Calendar".localized(), style: .done, target: self, action: #selector(self.suggestionButtonAction))
        var items = [UIBarButtonItem]()
        items.append(suggestions)
        items.append(flexSpace)
        items.append(done)
        
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        textField.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction(){
        self.view.endEditing(true)
        
    }
    @objc func suggestionButtonAction(){
        let mvc : ModalViewController = try! Dip.container.resolve() as ModalViewController
        let selectEventView = SelectEventTableView()
        selectEventView.result = { (title, completed) -> Bool in
            if title != nil{
                self.service.requestDraft.value?.title = title
                DispatchQueue.main.async {
                    self.tableView.reloadRows(at: [IndexPath.init(row: 2, section: 0)], with: .automatic)
                }
            }
            return true
        }
        self.view.endEditing(true)
        mvc.present(modal: selectEventView)
        
    }
    
    func showEmployeeSelection(){
        let mvc : ModalViewController = try! Dip.container.resolve() as ModalViewController
        let searchEmployeeView = SearchEmployeeView()
        searchEmployeeView.result = { (value, completed) -> Bool in
            if completed{
                print(value)
                self.tableView.reloadRows(at: [IndexPath.init(row: 0, section: 0), IndexPath.init(row: self.tableView.numberOfRows(inSection: 0)-1, section: 0)], with: .none)
            }
            return true
        }
        //searchEmployeeView.hero.id = "reAssignToEMployee"
        mvc.present(modal: searchEmployeeView)
    }

}
