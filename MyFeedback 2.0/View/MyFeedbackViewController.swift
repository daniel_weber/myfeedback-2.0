//
//  MyFeedbackViewController.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 09.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import UIKit
import Hero
import Bond

class MyFeedbackViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backgroundImage: UIImageView!
    
    let service = try! Dip.container.resolve() as FeedbackService
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(MyFeedbackTableViewCell.self, forCellReuseIdentifier: "MyFeedbackTableViewCell")
        self.tableView.contentInset = UIEdgeInsetsMake(40, 0, 0, 0)
        hero.isEnabled = true
        self.tableView.hero.modifiers = [.translate(y:1000)]
        self.backgroundImage.hero.modifiers = [.fade]
        self.view.backgroundColor = MerckColor.RichPurple
        self.tableView.refreshControl = UIRefreshControl()
        self.tableView.refreshControl?.addTarget(self, action: #selector(self.handleRefresh), for: .valueChanged)
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let navigationBar = try! Dip.container.resolve() as AlternativeNavigationBar
        navigationBar.update(title: "MyFeedback", options: nil)
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
        self.service.AnalyticsOpenViewMyFeedback()
    }

    // TableView Data Source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    // TableView Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyFeedbackTableViewCell", for: indexPath) as! MyFeedbackTableViewCell
        switch indexPath.row{
        case 0:
            cell.cellType = MyFeedbackCellType.MyFeedback
            cell.amountTotal = self.service.amountRequestsTotal.value
            self.service.amountRequestsTotal.observeNext(with: {value in
                cell.amountTotal = value
            })
            cell.amountUnread = self.service.amountUnreadResponses.value
            self.service.amountUnreadResponses.observeNext(with: {value in
                cell.amountUnread = value
            })
        case 1:
            cell.cellType = MyFeedbackCellType.MyRecognitions
            cell.amountTotal = self.service.amountRecognitionsTotal.value
            self.service.amountRecognitionsTotal.observeNext(with: {value in
                cell.amountTotal = value
            })
            cell.amountUnread = self.service.amountUnreadRecognitions.value
            self.service.amountUnreadRecognitions.observeNext(with: {value in
                cell.amountUnread = value
            })
        case 2:
            cell.cellType = MyFeedbackCellType.MyTrophies
        default:
            break
        }
        
        cell.backgroundColor = UIColor.clear
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let height: CGFloat
        if Device.IS_IPAD{
            height = min((tableView.frame.size.height - 90) / 3, 300)
        }else{
            height = min((tableView.frame.size.height - 90) / 3, 200)
        }
        if height == 200 || height == 300{
            let top = (self.view.frame.height - height*3) / 2
            tableView.contentInset = UIEdgeInsetsMake(top, 0, 0, 0)
        }
        
        return height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let navigationcontroller = try! Dip.container.resolve() as BaseNavigationController
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let cell = tableView.cellForRow(at: indexPath)
        switch indexPath.row{
        case 0:
            let vc = storyboard.instantiateViewController(withIdentifier: "FeedbackOverallViewController")
            navigationcontroller.pushViewController(vc, animated: true)
        case 1:
            let vc = storyboard.instantiateViewController(withIdentifier: "RecognitionsOverallViewController")
            navigationcontroller.pushViewController(vc, animated: true)
        case 2:
            let vc = storyboard.instantiateViewController(withIdentifier: "TrophiesViewController")
            navigationcontroller.pushViewController(vc, animated: true)
        default:
            break
        }
    }
    
    @objc func handleRefresh(){
        self.tableView.refreshControl?.endRefreshing()
        Loading.show()
        self.service.updateSentRequests().then({_ in
            self.service.updateMyRecognitions().then({_ in
                self.service.updateMyTrophies().then({_ in
                    self.tableView.reloadData()
                    Loading.hide()
                })
            })
        })
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
