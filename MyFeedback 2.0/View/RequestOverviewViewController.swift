//
//  RequestOverviewViewController.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 10.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import UIKit
import SwiftDate
import Bond
import TinyConstraints
import IQKeyboardManagerSwift
import EasyAnimation
import SwipeCellKit

class RequestOverviewViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, SwipeTableViewCellDelegate {

    @IBOutlet weak var requestTextView: UITextView!
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    let service = try! Dip.container.resolve() as FeedbackService
    
    var requestTextViewOrigin: CGRect = CGRect.zero
    
    var ratingOverviewView: RatingOverview?
    var request: Request?{
        didSet{
            //self.ratingView?.category = request?.category.localName
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = MerckColor.RichPurple
        // Do any additional setup after loading the view.
        
        hero.isEnabled = true
        self.view.backgroundColor = MerckColor.RichPurple
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(RequestTableViewCell.self, forCellReuseIdentifier: "RequestTableViewCell")
        self.setupView()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let navigationBar = try! Dip.container.resolve() as AlternativeNavigationBar
        let shareOption = BarOption(title: "Share", icon: #imageLiteral(resourceName: "Icon_Feedback")) { [unowned self] () -> Void in
            self.myShareButton()
        }
        navigationBar.update(title: self.request?.title, options: [shareOption])
        self.setupView()
        self.setupRatingView()
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
        //KeyboardAvoiding.avoidingView = self.responseTextView
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.request?.responses.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        /*if self.ratingOverviewView!.alpha > 0 && self.ratingOverviewView!.alpha < 1{
            scrollView.setContentOffset(CGPoint.init(x: 0, y: -(20 + self.ratingOverviewView!.frame.origin.y + self.ratingOverviewView!.frame.size.height)), animated: true)
        }*/
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let diff = scrollView.contentOffset.y+scrollView.contentInset.top
        if( diff > 0  && self.ratingOverviewView != nil){
            self.ratingOverviewView?.alpha = (self.ratingOverviewView!.frame.height-diff) / self.ratingOverviewView!.frame.height
        }
        let diff2 = scrollView.contentOffset.y+self.requestTextViewOrigin.maxY
        if diff2 > 0{
            let newHeight = self.requestTextViewOrigin.height - diff2
            if newHeight > 30{
                self.requestTextView.frame.size.height = newHeight
                if self.requestTextView.frame.origin.y < self.requestTextViewOrigin.origin.y{
                    UIView.animate(withDuration: 0.5, animations: {
                        self.requestTextView.frame.origin = self.requestTextViewOrigin.origin
                    })
                }
            }else{
                UIView.animate(withDuration: 0.5, animations: {
                    self.requestTextView.frame.origin.y = -100
                })
            }
            
        }else{
            if self.requestTextView.frame.origin.y < self.requestTextViewOrigin.origin.y{
                UIView.animate(withDuration: 0.5, animations: {
                    self.requestTextView.frame.origin = self.requestTextViewOrigin.origin
                })
            }
            self.requestTextView.transform = CGAffineTransform.identity
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let response = self.request!.responses[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "RequestTableViewCell", for: indexPath) as! RequestTableViewCell
        cell.backgroundColor = UIColor.clear
        cell.title = ""
        cell.name = response.rater.getName()
        cell.date = response.responseTimestamp
        
        let suffix: String
        if response.isAnswered{
            suffix = "_filled"
        }else{
            suffix = "_outline"
        }
        
        switch self.request!.type?.key ?? ""{
        case RequestType.Smileys.key:
            cell.icon = UIImage(named: "icon_smiley"+suffix)
        case RequestType.Stars.key:
            cell.icon = UIImage(named: "icon_star"+suffix)
        case RequestType.YesNo.key:
            cell.icon = UIImage(named: "icon_checkmark"+suffix)
        default:
            cell.icon = UIImage()
        }
        cell.thankYouIndicator = response.isThanked
        cell.readFlag = response.isRead
        cell.delegate = self
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let navigationcontroller = try! Dip.container.resolve() as BaseNavigationController
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let request = self.request!
        let response = self.request!.responses[indexPath.row]
        let vc = storyboard.instantiateViewController(withIdentifier: "RequestViewController") as! RequestViewController
        vc.request = request
        vc.response = response
        
        //self.backgroundImage.hero.modifiers = []
        //vc.backgroundImage.hero.modifiers = []
        navigationcontroller.pushViewController(vc, animated: true)
    }
    
    // MARK: SwipeTableViewCellDelegate
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        let response = self.request?.responses[indexPath.row]
        let thankYouAction = SwipeAction(style: .default, title: "Thank You".localized()) { action, indexPath in
            if response != nil{
                Loading.show()
                self.service.thankForResponse(response!).then({response in
                    if response.success && response.message != nil{
                        AlertModalView.show(title: "Success".localized(), message: response.message!)
                         tableView.reloadRows(at: [indexPath], with: .none)
                    }
                }).catch({e in ErrorView.show(error: e)})
            }
            action.fulfill(with: .reset)
           
        }
        
        
        thankYouAction.textColor = UIColor.white
        thankYouAction.backgroundColor = MerckColor.VibrantGreen
        
        if orientation == .right && response?.isThanked != true && response?.isAnswered == true{
            return [thankYouAction]
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeTableOptions {
        var options = SwipeTableOptions()
        options.expansionStyle = .destructive(automaticallyDelete: false)
        options.transitionStyle = .drag
        options.backgroundColor = UIColor.clear
        return options
    }
    
    func setupView(){
        self.ratingOverviewView?.removeFromSuperview()
        
        self.requestTextView.text = self.request?.text
        
        let scaleFactorY = backgroundImage.bounds.size.height/backgroundImage.image!.size.height
        let scaleFactorX = backgroundImage.bounds.size.width/backgroundImage.image!.size.width
        if self.request?.type?.key == RequestType.Smileys.key{
            let smileyRatingView = SmileyRatingOverview(frame: CGRect.init(x: 100*scaleFactorX, y: 410*scaleFactorY, width: 520*scaleFactorX, height: 230*scaleFactorY))
            self.backgroundImage.image = #imageLiteral(resourceName: "background_request_inverted")
            self.ratingOverviewView = smileyRatingView
            self.ratingOverviewView?.isHidden = true
            self.view.insertSubview(smileyRatingView, at: 1)
            smileyRatingView.ratingDistribution = self.request?.ratingDistribution ?? []
            smileyRatingView.titleLabel.text = self.request?.category?.localName
        }else if self.request?.type?.key == RequestType.YesNo.key{
            let ratingView = YesNoRatingOverview(frame: CGRect.init(x: 100*scaleFactorX, y: 410*scaleFactorY, width: 520*scaleFactorX, height: 230*scaleFactorY))
            self.ratingOverviewView = ratingView
            self.ratingOverviewView?.isHidden = true
            self.view.insertSubview(ratingView, at: 1)
            ratingView.ratingDistribution = self.request?.ratingDistribution ?? []
            ratingView.titleLabel.text = self.request?.category?.localName
        }else if self.request?.type?.key == RequestType.Stars.key{
            let ratingView = StarRatingOverview(frame: CGRect.init(x: 100*scaleFactorX, y: 410*scaleFactorY, width: 520*scaleFactorX, height: 230*scaleFactorY))
            self.ratingOverviewView = ratingView
            self.ratingOverviewView?.isHidden = true
            self.view.insertSubview(ratingView, at: 1)
            ratingView.average = self.request?.averageResponseRating ?? 0
            ratingView.scaleMax = self.request?.scaleTextMax
            ratingView.scaleMin = self.request?.scaleTextMin
            ratingView.titleLabel.text = self.request?.category?.localName
        }
        self.requestTextView.bottomToTop(of: self.ratingOverviewView!)
        self.requestTextView.isEditable = false
        self.requestTextViewOrigin = self.requestTextView.frame
        //self.tableView.topToBottom(of: self.ratingOverviewView!)
        self.tableView.edges(to: self.view)
        self.tableView.contentInset = UIEdgeInsetsMake( self.ratingOverviewView!.frame.origin.y + self.ratingOverviewView!.frame.size.height, 0, 0, 0)
        self.tableView.setContentOffset(CGPoint.init(x: 0, y: -(20 + self.ratingOverviewView!.frame.origin.y + self.ratingOverviewView!.frame.size.height)), animated: true)
        
        for v in self.view.subviews{
            v.hero.modifiers = [.translate(y:1000)]
        }
        self.backgroundImage.hero.modifiers = [.fade]
    }
    
    func setupRatingView(){
        self.ratingOverviewView?.alpha = 0
        self.ratingOverviewView?.isHidden = false
        UIView.animate(withDuration: 0.2, animations: {
            self.ratingOverviewView?.alpha = 1
        })
        
    }
    
    func myShareButton() {
        let shareText = "Received Feedback for \(self.request?.title ?? self.request?.text ?? "")"
        //displayShareSheet(shareContent: shareText)
        
        let shareView = UIView(frame: self.view.bounds)
        let bgImageView = UIImageView(frame: shareView.bounds)
        bgImageView.image = self.backgroundImage.image
        bgImageView.clipsToBounds = true
        shareView.addSubview(bgImageView)
        let viewCopy = UIImageView(image: UIImage(view: view))
        shareView.addSubview(viewCopy)
        let shareImage = UIImage(view: shareView)
        
        Share.show(parentViewController: self, body: shareText, image: shareImage)
    }
    
    
    func displayShareSheet(shareContent:String) {
        self.service.AnalyticsActionShareFeedback()
        
        let shareView = UIView(frame: self.view.bounds)
        let bgImageView = UIImageView(frame: shareView.bounds)
        bgImageView.image = self.backgroundImage.image
        bgImageView.clipsToBounds = true
        shareView.addSubview(bgImageView)
        //let scrollViewCopy = scrollView.copyView() as UIScrollView
        let viewCopy = UIImageView(image: UIImage(view: view))
        shareView.addSubview(viewCopy)
        let shareImage = UIImage(view: shareView)
        let activityViewController = UIActivityViewController(activityItems: [shareContent as NSString, shareImage as UIImage], applicationActivities: nil)
        let subject = "Received Feedback via MyFeedback"
        activityViewController.setValue(subject, forKey: "Subject")
        if let popoverController = activityViewController.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        self.present(activityViewController, animated: true, completion: {})
    }

}
