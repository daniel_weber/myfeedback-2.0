//
//  SettingsTableViewCell.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 08.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import UIKit
import Localize_Swift

class NewRequestTextViewTableViewCell: UITableViewCell, UITextViewDelegate {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    
    
    var title: String?{
        didSet{
            self.titleLabel.text = title
        }
    }
    var placeHolder: String?{
        didSet{
            self.textView.text = placeHolder?.localized() ?? ""
        }
    }
    
    var comment: String?{
        didSet{
            if comment != nil && !comment!.isEmpty{
                self.textView.text = comment
            }
        }
    }
    
    var result: ((String, Bool) -> Bool)?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupView()
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setupView()
    }
    
    private func setupView() {
        
        let view = viewFromNibForClass()
        view.frame = bounds
        
        // Auto-layout stuff.
        view.autoresizingMask = [
            UIViewAutoresizing.flexibleWidth,
            UIViewAutoresizing.flexibleHeight
        ]
        
        self.textView.backgroundColor = UIColor(hexString: "#999999").withAlphaComponent(0.1)
        self.textView.layer.cornerRadius = 5
        
        self.textView.layer.borderColor = UIColor.lightText.cgColor
        self.textView.layer.borderWidth = 0.5
        self.textView.textColor = UIColor.white
        self.textView.tintColor = UIColor.white
        self.textView.alpha = 0.5
        
        
        self.textView.delegate = self
        
        
        //        self.selectionStyle = UITableViewCellSelectionStyle.none
        
        // Show the view.
        addSubview(view)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == self.placeHolder?.localized(){
            textView.alpha = 1
            textView.text = ""
        }
    }
    
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty{
            textView.text = self.placeHolder?.localized() ?? ""
            textView.alpha = 0.5
            if let r = self.result{
                _ = r("", true)
            }
        }else{
            textView.alpha = 1
            if let r = self.result{
                _ = r(textView.text, true)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
