//
//  NewRequestScaleTableViewCell.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 12.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import UIKit

class NewRequestScaleTableViewCell: UITableViewCell, UITextFieldDelegate {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var scaleMinTextField: UITextField!
    @IBOutlet weak var scaleMaxTextField: UITextField!
    
    
    var scaleMin: String?{
        didSet{
            self.scaleMinTextField.text = scaleMin
        }
    }
    var scaleMax: String?{
        didSet{
            self.scaleMaxTextField.text = scaleMax
        }
    }
    
    var result: ((String, Bool) -> Bool)?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupView()
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setupView()
    }
    
    private func setupView() {
        
        let view = viewFromNibForClass()
        view.frame = bounds
        
        // Auto-layout stuff.
        view.autoresizingMask = [
            UIViewAutoresizing.flexibleWidth,
            UIViewAutoresizing.flexibleHeight
        ]
        
        self.titleLabel.text = "Range".localized()
        
        self.scaleMinTextField.backgroundColor = UIColor.white.withAlphaComponent(0.8)
        self.scaleMinTextField.layer.cornerRadius = 5
        self.scaleMinTextField.placeholder = "Needs improvement".localized()
        self.scaleMaxTextField.backgroundColor = UIColor.white.withAlphaComponent(0.8)
        self.scaleMaxTextField.layer.cornerRadius = 5
        self.scaleMaxTextField.placeholder = "Excellent".localized()
        self.scaleMaxTextField.delegate = self
        self.scaleMinTextField.delegate = self
        
        
        //        self.selectionStyle = UITableViewCellSelectionStyle.none
        
        // Show the view.
        addSubview(view)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        // Prevent title length to bbe longer than 97 characters
        if !string.isEmpty{
            if let count = textField.text?.count{
                if count >= 24{
                    return false
                }
                
            }
        }
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        // Prevent title length to bbe longer than 97 characters
        if textField.text != nil{
            if let count = textField.text?.count{
                if count >= 24{
                    let index = textField.text!.index(textField.text!.startIndex, offsetBy: 24)
                    textField.text = textField.text!.substring(to: index)
                    return true
                }
            }
        }
        return true
    }
    
}

