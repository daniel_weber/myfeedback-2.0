//
//  SettingsTableViewCell.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 08.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import UIKit

class NewRequestTextFieldTableViewCell: UITableViewCell, UITextFieldDelegate {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
   
    var limit: Int?
    
    var title: String?{
        didSet{
            self.titleLabel.text = title
        }
    }
    var placeHolder: String?{
        didSet{
            self.textField.placeholder = placeHolder
        }
    }
    
    var result: ((String, Bool) -> Bool)?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupView()
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setupView()
    }
    
    private func setupView() {
        
        let view = viewFromNibForClass()
        view.frame = bounds
        
        // Auto-layout stuff.
        view.autoresizingMask = [
            UIViewAutoresizing.flexibleWidth,
            UIViewAutoresizing.flexibleHeight
        ]
        
        self.textField.backgroundColor = UIColor.white.withAlphaComponent(0.8)
        self.textField.layer.cornerRadius = 5
        self.textField.delegate = self
        
        //        self.selectionStyle = UITableViewCellSelectionStyle.none
        
        // Show the view.
        addSubview(view)
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        // Prevent title length to bbe longer than 97 characters
        if self.limit != nil && !string.isEmpty{
            if let count = textField.text?.count{
                if count >= self.limit!{
                    return false
                }
                
            }
        }
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        // Prevent title length to bbe longer than 97 characters
        if self.limit != nil && textField.text != nil{
            if let count = textField.text?.count{
                if count >= self.limit!{
                    let index = textField.text!.index(textField.text!.startIndex, offsetBy: self.limit!)
                    textField.text = textField.text!.substring(to: index)
                    return true
                }
            }
        }
        return true
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
