//
//  SettingsTableViewCell.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 08.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class NewRequestSelectionTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var selectionButton: UIButton!
    
    
    var title: String?{
        didSet{
            self.titleLabel.text = title
        }
    }
    var labels: [String] = []
    var options: [String] = []
    
    var result: ((String, Bool) -> Bool)?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupView()
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setupView()
    }
    
    private func setupView() {
        
        let view = viewFromNibForClass()
        view.frame = bounds
        
        // Auto-layout stuff.
        view.autoresizingMask = [
            UIViewAutoresizing.flexibleWidth,
            UIViewAutoresizing.flexibleHeight
        ]
        
        self.selectionButton.backgroundColor = UIColor.white.withAlphaComponent(0.8)
        self.selectionButton.layer.cornerRadius = 5
        
        
        //        self.selectionStyle = UITableViewCellSelectionStyle.none
        
        // Show the view.
        addSubview(view)
    }
    @IBAction func handleSelectionButtonCLicked(_ sender: UIButton) {
        var selected = 0
        if let index = self.labels.index(where: {label in return label == sender.currentTitle}){
            selected = index
        }
        let stringPicker = ActionSheetStringPicker(title: "", rows: self.labels, initialSelection: selected, doneBlock: { picker, index, value in
            sender.setTitle(value as? String, for: .normal)
            if let r = self.result{
                _ = r(self.options[index], true)
            }
            
        }, cancel: {_ in }, origin: sender)
        let image = #imageLiteral(resourceName: "background_picker")
        if let size = stringPicker?.viewSize{
            stringPicker?.pickerBackgroundColor = UIColor(patternImage: image.resizeImage(targetSize: size))
        }
        stringPicker?.setDoneButton(UIBarButtonItem.init(title: "Done".localized(), style: .done, target: nil, action: nil))
        stringPicker?.setCancelButton(UIBarButtonItem.init(title: "Cancel".localized(), style: .plain, target: nil, action: nil))
        stringPicker?.toolbarBackgroundColor = MerckColor.VibrantGreen
        stringPicker?.setTextColor(UIColor.white)
        stringPicker?.pickerTextAttributes.addEntries(from: [NSAttributedStringKey.font : UIFont.systemFont(ofSize: 20)])
        stringPicker?.toolbarButtonsColor = MerckColor.RichPurple
        stringPicker?.show()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

