//
//  AlertModalView.swift
//  CostCenters
//
//  Created by Thomas Kosiewski on 30.11.17.
//  Copyright © 2017 Merck KGaA. All rights reserved.
//

import UIKit

class AlertModalView: ModalView {

	var title: String? {
		didSet {
			alertTitle.text = title
            self.headerView.isHidden = title == nil
		}
	}
    
    var close: ((Bool) -> Void)?

	var message: String? {
		didSet {
			guard message != nil else {
				return
			}

			attributedMessage = nil
			alertLabel.text = message
		}
	}

	var attributedMessage: NSAttributedString? {
		didSet {
			guard attributedMessage != nil else {
				return
			}

			message = nil
			alertLabel.attributedText = attributedMessage
		}
	}

	@IBOutlet weak var alertTitle: UILabel!
	@IBOutlet weak var alertLabel: UILabel!
    @IBOutlet weak var okButton: CallToActionButton!
    @IBOutlet weak var buttonStackView: UIStackView!
    @IBOutlet weak var contentWrapperView: UIView!
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var cancelButton: CallToActionButton!
    override init(frame: CGRect) {
		super.init(frame: frame)
		setupView()
	}

	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		setupView()
	}

	private func setupView() {
		let view = viewFromNibForClass()
		view.frame = bounds

		// Auto-layout stuff.
		view.autoresizingMask = [
			UIViewAutoresizing.flexibleWidth,
			UIViewAutoresizing.flexibleHeight
		]

		// Show the view.
		addSubview(view)
        
        self.contentWrapperView.layer.cornerRadius = 5.0
        self.contentWrapperView.layer.masksToBounds = true
        
        self.okButton.setTitle("Ok".localized(), for: .normal)
        self.cancelButton.setTitle("Cancel".localized(), for: .normal)
		layer.cornerRadius = 5.0
		layer.masksToBounds = true
	}

	override func setConstraints(container: UIView) {
		centerX(to: container)
		centerY(to: container, nil, offset: -20.0, priority: .required, isActive: true)
		width(min: 200.0, max: min(container.bounds.width - 80, 400), priority: .required, isActive: true)
		//        self.width(80.0)
		//        self.height(55.0)
	}

	@IBAction func handleCloseButton(_ sender: Any) {
		close?(false)
        done?(false)
	}

	@IBAction func handleOkButton(_ sender: Any) {
		close?(true)
        done?(true)
	}
}

extension AlertModalView {
    
    static func showOnViewController(parentViewController : UIViewController, title: String, message:String)
    {
        showOnViewController(parentViewController: parentViewController, title: title, message: message, attributedMessage: nil);
    }
    static func showOnViewController(parentViewController : UIViewController, title: String, attributedMessage: NSAttributedString)
    {
        showOnViewController(parentViewController: parentViewController, title: title, message: nil, attributedMessage: attributedMessage);
    }
    
    private static func showOnViewController(parentViewController : UIViewController, title: String, message:String?, attributedMessage: NSAttributedString?)
    {
        //Modal VC will be created on fly and removed on fly...
        let modalVC = ModalViewController(nibName: "ModalViewController", bundle: nil) as ModalViewController;
        parentViewController.view.addSubview(modalVC.view)
        modalVC.view.edges(to: parentViewController.view)
        
        let alertMV = AlertModalView()
        alertMV.title = title
        if nil != message
        {
            alertMV.message = message
        }
        if nil != attributedMessage
        {
            alertMV.attributedMessage = attributedMessage
        }
        modalVC.present(modal: alertMV, edge: .bottom){
            modalVC.view.removeFromSuperview();
            modalVC.removeFromParentViewController();
        }
    }
	static func show(title: String?, message: String) -> AlertModalView {
		let mvc = try! Dip.container.resolve() as ModalViewController

		let alertMV = AlertModalView()
		alertMV.title = title
		alertMV.message = message
        alertMV.cancelButton.removeFromSuperview()
        
		mvc.present(modal: alertMV, edge: .bottom)
        return alertMV
	}
    
    static func show(title: String?, message: String, enableCancel: Bool) -> AlertModalView {
        let mvc = try! Dip.container.resolve() as ModalViewController
        
        let alertMV = AlertModalView()
        alertMV.title = title
        alertMV.message = message
        
        if !enableCancel{
            alertMV.cancelButton.removeFromSuperview()
        }
        
        mvc.present(modal: alertMV, edge: .bottom)
        return alertMV
    }

	static func show(title: String?, attributedMessage: NSAttributedString) -> AlertModalView {
		let mvc = try! Dip.container.resolve() as ModalViewController

		let alertMV = AlertModalView()
		alertMV.title = title
		alertMV.attributedMessage = attributedMessage

		mvc.present(modal: alertMV, edge: .bottom)
        
        return alertMV
	}
    
    static func show(title: String?, message: String, button: CallToActionButton) -> AlertModalView {
        let mvc = try! Dip.container.resolve() as ModalViewController
        
        let alertMV = AlertModalView()
        alertMV.title = title
        alertMV.message = message
        alertMV.addSubview(button)
        alertMV.cancelButton.removeFromSuperview()
        alertMV.buttonStackView.addArrangedSubview(button)
        
        //button.topToBottom(of: alertMV.alertLabel, offset: 13, relation: .equal, priority: .defaultHigh, isActive: true)
        //button.centerX(to: alertMV)
        button.width(to: alertMV.okButton)
        button.height(to: alertMV.okButton)
        //alertMV.okButton.topToBottom(of: button, offset: 13, relation: .equal, priority: .defaultHigh, isActive: true)
        
        //button.setTitleColor(MerckColor.VibrantCyan, for: .normal)
        //button.topToBottom(of: alertMV.alertLabel)
        //button.height(25)
        //button.left(to: alertMV)
        //button.right(to: alertMV)
        
        
        mvc.present(modal: alertMV, edge: .bottom)
        return alertMV
    }

	static func hide() {
		let mvc: ModalViewController = try! Dip.container.resolve() as ModalViewController

		mvc.dismiss {
		}
	}
}
