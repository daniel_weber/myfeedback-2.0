//
//  EnterTextModalView.swift
//  MIA
//
//  Created by Daniel Weber on 19.09.17.
//  Copyright © 2017 Merck KGaA. All rights reserved.
//

import UIKit

class EnterTextModalView: ModalView, ModalViewResulting {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleSubLabel: UILabel!
    @IBOutlet weak var valueTextField: UITextField!
    @IBOutlet weak var contentWrapperView: UIView!
    @IBOutlet weak var saveButton: CallToActionButton!
    
    var title : String? {
        didSet {
            self.titleLabel.text = self.title
        }
    }
    
    var subTitle : String? {
    
        didSet {
            self.titleSubLabel.text = self.subTitle
        }
    }
    
    var value : String? = "" {
        didSet {
            
            self.valueTextField.text = self.value
        }
    }
    
    var result: ((String, Bool) -> Bool)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    private func setupView() {
        
        let view = viewFromNibForClass()
        view.frame = bounds
        
        // Auto-layout stuff.
        view.autoresizingMask = [
            UIViewAutoresizing.flexibleWidth,
            UIViewAutoresizing.flexibleHeight
        ]
        
        // Show the view.
        addSubview(view)
        
        self.contentWrapperView.layer.cornerRadius = 5.0
        self.contentWrapperView.layer.masksToBounds = true
        //self.layer.cornerRadius = 5.0
        //self.layer.masksToBounds = true
        
        let _ = self.valueTextField.reactive.text.observeNext { (value) in
            
            guard let v = value, !value!.isEmpty else {
                self.saveButton.isEnabled = false
                return
            }
            self.saveButton.isEnabled = true
            self.value = v
        }
        
    }
    
    override func setConstraints(container: UIView) {
        
        self.centerX(to: container)
        self.centerY(to: container, nil, offset: -60.0, priority: .required, isActive: true)
        self.width(to: container, nil, multiplier: 1.0, offset: -40.0, relation: .equal, priority: .required, isActive: true)
    }
    
    override func didMoveToSuperview() {
        self.valueTextField.becomeFirstResponder()
    }
    
    override func willBeDismissed() {
        self.valueTextField.resignFirstResponder()
    }
    
    @IBAction func handleCloseButton(_ sender: Any) {
        self.result?("", false)
        self.done?(false)
    }
    
    @IBAction func handleSaveButton(_ sender: Any) {
        
        guard let r = self.result else {
            self.done?(false)
            return
        }
        
        let accept = r(self.value!, true)
        
        if (accept) {
            self.done?(accept)
        }
    }

}
