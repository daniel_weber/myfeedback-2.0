//
//  LoadingModalView.swift
//  CostCenters
//
//  Created by Marco Koeppel on 19.09.17.
//  Copyright © 2017 Merck KGaA. All rights reserved.
//

import UIKit
import TinyConstraints
import NVActivityIndicatorView

class LoadingModalView: ModalView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }

    private func setupView() {

        let view = viewFromNibForClass()
        view.frame = bounds

        // Auto-layout stuff.
        view.autoresizingMask = [
            UIViewAutoresizing.flexibleWidth,
            UIViewAutoresizing.flexibleHeight
        ]

        // Show the view.
        addSubview(view)

        self.layer.cornerRadius = 5.0
        self.layer.masksToBounds = true

        let indicator = NVActivityIndicatorView(frame: self.bounds, type: NVActivityIndicatorType.ballPulse, color: MerckColor.RichPurple, padding: 10.0)

        indicator.startAnimating()
        self.addSubview(indicator)

        indicator.center(in: self)

        self.userCancellable = false
    }

    override func setConstraints(container: UIView) {

        self.centerX(to: container)
        self.centerY(to: container, nil, offset: -20.0, priority: .required, isActive: true)

        self.width(80.0)
        self.height(55.0)
    }
}

class Loading {

    static func show() {

        let mvc: ModalViewController = try! Dip.container.resolve() as ModalViewController

        let loading = LoadingModalView()
        mvc.present(modal: loading, edge: .bottom)
    }

    static func hide() {

        let mvc: ModalViewController = try! Dip.container.resolve() as ModalViewController

        mvc.dismiss {

        }
    }
}
