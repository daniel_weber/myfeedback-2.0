//
//  ModalView.swift
//  MIA
//
//  Created by Daniel Weber on 24.08.17.
//  Copyright © 2017 Merck KGaA. All rights reserved.
//

import UIKit

class ModalView: UIView {

    var userCancellable : Bool = true
    
    var done : ((Bool) -> Void)?
    
    func setConstraints(container : UIView){
        assert(false, "setConstraints(container:) must be exclusively implemented in a subclass")
    }
    
    func willBeDismissed() {
        
    }
}
