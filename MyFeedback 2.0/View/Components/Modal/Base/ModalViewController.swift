//
//  ModalViewController.swift
//  MIA
//
//  Created by Daniel Weber on 24.08.17.
//  Copyright © 2017 Merck KGaA. All rights reserved.
//

import UIKit
import TinyConstraints
import Dip
import EasyAnimation

class ModalViewController: UIViewController {
    
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var backgroundView: UIView!
    
    var modalView : ModalView?
    var modalDismissEdge : ModalViewPresentEdge = .bottom
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        //isHeroEnabled = true
        
        self.view.isHidden = true
        self.view.isUserInteractionEnabled = false
        self.backgroundView.alpha = 0.0
        
        Dip.container.register(.singleton) { self as ModalViewController }
        
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func present(modal: ModalView){
        self.present(modal: modal, edge: .bottom)
    }
    
//    func present(modal: ModalView, edge : ModalViewPresentEdge){
//
//        if (self.modalView != nil){
//
//            let modalViewToRemove = self.modalView!
//            self.modalView = nil
//
//            modalViewToRemove.willBeDismissed()
//
//            var to : CGAffineTransform
//            switch self.modalDismissEdge {
//
//            case .bottom:
//                to = CGAffineTransform(translationX: 0.0, y: self.view.frame.size.height)
//            case .left:
//                to = CGAffineTransform(translationX: -self.view.frame.size.width, y: 0.0)
//            case .top:
//                to = CGAffineTransform(translationX: 0.0, y: -self.view.frame.size.height)
//            case .right:
//                to = CGAffineTransform(translationX: self.view.frame.size.width, y: 0.0)
//            }
//
//            UIView.animate(withDuration: 0.6, delay: 0.0, usingSpringWithDamping: 0.96, initialSpringVelocity: 0.0, options: [.curveEaseIn], animations: {
//
//                modalViewToRemove.transform = to
//
//            }, completion: { completed in
//
//                modalViewToRemove.removeFromSuperview()
//            })
//
//        }
//
//        self.modalView = modal
//        self.modalView?.done = { completed in
//            self.dismiss(completed: nil)
//        }
//
//        self.modalDismissEdge = edge
//
//        var from : CGAffineTransform
//        var to : CGAffineTransform
//
//        switch edge {
//
//        case .bottom:
//            from = CGAffineTransform(translationX: 0.0, y: self.view.frame.size.height)
//            to = CGAffineTransform.identity
//        case .left:
//            from = CGAffineTransform(translationX: -self.view.frame.size.width, y: 0.0)
//            to = CGAffineTransform.identity
//        case .top:
//            from = CGAffineTransform(translationX: 0.0, y: -self.view.frame.size.height)
//            to = CGAffineTransform.identity
//        case .right:
//            from = CGAffineTransform(translationX: self.view.frame.size.width, y: 0.0)
//            to = CGAffineTransform.identity
//        }
//
//        self.container.addSubview(self.modalView!)
//
//        self.modalView?.transform = from
//
//        self.view.isHidden = false
//        self.view.isUserInteractionEnabled = true
//
//        self.modalView?.setConstraints(container: self.container)
//
//        UIView.animate(withDuration: 0.2, delay: 0.0, options: [.curveEaseOut], animations: {
//            self.backgroundView.alpha = 0.8
//        }, completion: nil)
//
//        UIView.animate(withDuration: 0.68, delay: 0.0, usingSpringWithDamping: 0.76, initialSpringVelocity: 0.1, options: [.curveEaseOut], animations: {
//
//            self.modalView?.transform = to
//
//        }, completion: {completed in
//
//            let subviews = self.container.subviews
//            for s in subviews {
//
//                if (s != self.modalView){
//                    s.removeFromSuperview()
//                }
//            }
//        })
//    }
    
    func present(modal: ModalView, edge: ModalViewPresentEdge, dismissCallback: (() -> Void)? = nil) {
        
        if (self.modalView != nil) {
            
            let modalViewToRemove = self.modalView!
            self.modalView = nil
            
            modalViewToRemove.willBeDismissed()
            
            var to: CGAffineTransform
            switch self.modalDismissEdge {
                
            case .bottom:
                to = CGAffineTransform(translationX: 0.0, y: self.view.frame.size.height)
            case .left:
                to = CGAffineTransform(translationX: -self.view.frame.size.width, y: 0.0)
            case .top:
                to = CGAffineTransform(translationX: 0.0, y: -self.view.frame.size.height)
            case .right:
                to = CGAffineTransform(translationX: self.view.frame.size.width, y: 0.0)
            }
            
            UIView.animate(withDuration: 0.6, delay: 0.0, usingSpringWithDamping: 0.96, initialSpringVelocity: 0.0, options: [.curveEaseIn], animations: {
                
                modalViewToRemove.transform = to
                
            }, completion: { _ in
                
                modalViewToRemove.removeFromSuperview()
            })
            
        }
        
        self.modalView = modal
        self.modalView?.done = { completed in
            self.dismiss(completed: dismissCallback)
        }
        
        self.modalDismissEdge = edge
        
        var from: CGAffineTransform
        var to: CGAffineTransform
        
        switch edge {
            
        case .bottom:
            from = CGAffineTransform(translationX: 0.0, y: self.view.frame.size.height)
            to = CGAffineTransform.identity
        case .left:
            from = CGAffineTransform(translationX: -self.view.frame.size.width, y: 0.0)
            to = CGAffineTransform.identity
        case .top:
            from = CGAffineTransform(translationX: 0.0, y: -self.view.frame.size.height)
            to = CGAffineTransform.identity
        case .right:
            from = CGAffineTransform(translationX: self.view.frame.size.width, y: 0.0)
            to = CGAffineTransform.identity
        }
        
        self.container.addSubview(self.modalView!)
        
        self.modalView?.transform = from
        
        self.view.isHidden = false
        self.view.isUserInteractionEnabled = true
        
        self.modalView?.setConstraints(container: self.container)
        
        UIView.animate(withDuration: 0.2, delay: 0.0, options: [.curveEaseOut], animations: {
            self.backgroundView.alpha = 0.8
        }, completion: nil)
        
        UIView.animate(withDuration: 0.68, delay: 0.0, usingSpringWithDamping: 0.76, initialSpringVelocity: 0.1, options: [.curveEaseOut], animations: {
            
            self.modalView?.transform = to
            
        }, completion: {_ in
            
            let subviews = self.container.subviews
            for s in subviews {
                
                if (s != self.modalView) {
                    s.removeFromSuperview()
                }
            }
        })
    }
    
    func dismiss(completed callback: (() -> ())?){
        
        self.modalView?.willBeDismissed()
        
        var to : CGAffineTransform
        switch self.modalDismissEdge {
            
        case .bottom:
            to = CGAffineTransform(translationX: 0.0, y: self.view.frame.size.height)
        case .left:
            to = CGAffineTransform(translationX: -self.view.frame.size.width, y: 0.0)
        case .top:
            to = CGAffineTransform(translationX: 0.0, y: -self.view.frame.size.height)
        case .right:
            to = CGAffineTransform(translationX: self.view.frame.size.width, y: 0.0)
        }
        
        UIView.animate(withDuration: 0.2, delay: 0.6, options: [.curveEaseOut], animations: {
            self.backgroundView.alpha = 0.0
        }, completion: nil)
        
        UIView.animate(withDuration: 0.6, delay: 0.18, usingSpringWithDamping: 0.96, initialSpringVelocity: 0.0, options: [.curveEaseIn], animations: {
            
            self.modalView?.transform = to
            self.view.isUserInteractionEnabled = false
            
        }, completion: { completed in
            
            self.modalView?.removeFromSuperview()
            self.modalView = nil
            
            self.view.isHidden = true
            
            super.dismiss(animated: true, completion: callback)
            //callback?()
        })
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        super.touchesEnded(touches, with: event)
        
        if (self.view.hitTest(touches.first!.location(in: self.view), with: nil) != nil && self.view.hitTest(touches.first!.location(in: self.view), with: nil)! == self.container){
            self.handleBackgroundTap(self)
        }
        
    }
    
    
    @IBAction func handleBackgroundTap(_ sender: Any) {
        
        if (!self.modalView!.userCancellable){
            return
        }
        
        self.dismiss(completed: nil)
    }
}

enum ModalViewPresentEdge {
    
    case bottom
    case left
    case top
    case right
}

