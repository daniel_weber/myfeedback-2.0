//
//  ModalViewResulting.swift
//  MIA
//
//  Created by Daniel Weber on 25.08.17.
//  Copyright © 2017 Merck KGaA. All rights reserved.
//

import Foundation

protocol ModalViewResulting {
    
    associatedtype T
    var result : ((T, Bool) -> Bool)? { get set }
}
