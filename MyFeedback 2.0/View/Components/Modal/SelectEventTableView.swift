//
//  FilterDetailTableView.swift
//  MIA
//
//  Created by Daniel Weber on 18.10.17.
//  Copyright © 2017 zero2one. All rights reserved.
//

import UIKit

class SelectEventTableView: ModalView, ModalViewResulting, UITableViewDelegate, UITableViewDataSource {
    
    
    // MARK: Outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    
    
    let service = try! Dip.container.resolve() as FeedbackService
    
    var result: ((String?, Bool) -> Bool)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    private func setupView() {
        
        let view = viewFromNibForClass()
        view.frame = bounds
        
        // Auto-layout stuff.
        view.autoresizingMask = [
            UIViewAutoresizing.flexibleWidth,
            UIViewAutoresizing.flexibleHeight
        ]
        
        
        // Show the view.
        addSubview(view)
        
        view.layer.cornerRadius = 5.0
        view.layer.masksToBounds = true
        
        self.titleLabel.text = "Events".localized()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
    }
    
    
    
    override func setConstraints(container: UIView) {
        
        self.centerX(to: container)
        self.top(to: container, nil, offset: 40.0, relation: .equal, priority: .required, isActive: true)
        //let height:CGFloat = 50*5+50
        //self.height(height)
        self.bottom(to: container)
        self.width(to: container, nil, multiplier: 1.0, offset: -40.0, relation: .equal, priority: .required, isActive: true)
    }
    
    override func didMoveToSuperview() {
        
    }
    
    override func willBeDismissed() {
        
    }
    
    @IBAction func handleCloseButton(_ sender: Any) {
        self.done?(false)
        self.result?(nil,false)
    }
    
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.service.events.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = self.service.events[indexPath.row].title
        cell.textLabel?.textColor = MerckColor.Grey
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let r = self.result{
            let event = self.service.events[indexPath.row]
            let success = r(event.title, true)
            self.done?(success)
        }
        
    }
    
}
