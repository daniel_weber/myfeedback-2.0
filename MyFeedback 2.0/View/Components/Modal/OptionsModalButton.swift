//
//  OptionsModalButton.swift
//  CostCenters
//
//  Created by Marco Koeppel on 12.09.17.
//  Copyright © 2017 Merck KGaA. All rights reserved.
//

import UIKit

class OptionsModalButton: UIButton {

    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var label: UILabel!

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()

    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }

    private func setupView() {

        let view = viewFromNibForClass()
        view.frame = bounds

        // Auto-layout stuff.
        view.autoresizingMask = [
            UIViewAutoresizing.flexibleWidth,
            UIViewAutoresizing.flexibleHeight
        ]

        // Show the view.
        addSubview(view)
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)

        UIView.animate(withDuration: 0.1) {
            self.subviews[0].backgroundColor = MerckColor.VeryLightGrey
        }
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)

        if (self.pointHitView(point: touches.first?.location(in: self))) {
            self.sendActions(for: .touchUpInside)
        }

        UIView.animate(withDuration: 0.1) {
            self.subviews[0].backgroundColor = UIColor.white
        }
    }
}
