//
//  OptionsModalView.swift
//  CostCenters
//
//  Created by Marco Koeppel on 12.09.17.
//  Copyright © 2017 Merck KGaA. All rights reserved.
//

import UIKit
import TinyConstraints

class OptionsModalView: ModalView {

    @IBOutlet weak var stack: UIStackView!

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()

    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }

    private func setupView() {

        let view = viewFromNibForClass()
        view.frame = bounds

        // Auto-layout stuff.
        view.autoresizingMask = [
            UIViewAutoresizing.flexibleWidth,
            UIViewAutoresizing.flexibleHeight
        ]

        // Show the view.
        addSubview(view)

        self.layer.cornerRadius = 5.0
        self.layer.masksToBounds = true
    }

    override func setConstraints(container: UIView) {

        self.right(to: container, offset: -20.0)
        self.top(to: container, offset: 54.0)
        self.width(220.0)
    }

    func setOptions(options: [BarOption]) {

        for option in options {

            let ob = OptionsModalButton(type: UIButtonType.custom)

            ob.iconView.image = option.icon
            ob.label.text = option.title

            _ = ob.reactive.controlEvents(.touchUpInside).observeNext {
                option.action()
            }

            ob.height(50.0)

            self.stack.addArrangedSubview(ob)
        }
    }
}
