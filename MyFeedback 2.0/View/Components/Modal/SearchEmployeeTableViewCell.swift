//
//  SearchEmployeeTableViewCell.swift
//  MIA
//
//  Created by Daniel Weber on 01.09.17.
//  Copyright © 2017 Merck KGaA. All rights reserved.
//

import UIKit

class SearchEmployeeTableViewCell: UITableViewCell {

    @IBOutlet weak var ownerLabel: UILabel!
    @IBOutlet weak var ownerIdLabel: UILabel!
    @IBOutlet weak var costCenterLabel: UILabel!
    @IBOutlet weak var costCenterIdLabel: UILabel!
    
    var owner : String? {
        didSet{
            self.ownerLabel.text = self.owner
        }
    }
    
    var ownerId : String? {
        didSet{
            self.ownerIdLabel.text = self.ownerId
        }
    }
    
    var costCetnerId : String? {
        didSet{
            self.costCenterIdLabel.text = self.costCetnerId
        }
    }
    
    var costCenterTitle : String? {
        didSet {
            self.costCenterLabel.text = self.costCenterTitle ?? "CostCenter"
        }
    }
    
    var highlightedString : String? {
        
        didSet{
            
            guard let _ = self.highlightedString else {
                return
            }
            
            if let highlightedParts = self.highlightedString?.split(separator: " ") {
                
                if let owner = self.owner {
                    
                    let attText = NSMutableAttributedString(string: owner)
                    
                    for hp in highlightedParts {
                        let range = NSString(string: attText.string.lowercased()).range(of: hp.lowercased())
                        
                        if (range.location != -1){
                            attText.addAttribute(NSAttributedStringKey.font, value: UIFont.systemFont(ofSize: 16.0, weight: UIFont.Weight.bold), range: range)
                            self.ownerLabel.attributedText = attText
                        }
                    }
                }
                
                if let ownerId = self.ownerId {
                    
                    let attText = NSMutableAttributedString(string: ownerId)
                    
                    for hp in highlightedParts {
                        let range = NSString(string: attText.string.lowercased()).range(of: hp.lowercased())
                        
                        if (range.location != -1){
                            attText.addAttribute(NSAttributedStringKey.font, value: UIFont.systemFont(ofSize: 14.0, weight: UIFont.Weight.bold), range: range)
                            self.ownerIdLabel.attributedText = attText
                        }
                    }
                }
                
                if let costCenterId = self.costCetnerId {
                    
                    let attText = NSMutableAttributedString(string: costCenterId)
                    
                    for hp in highlightedParts {
                        let range = NSString(string: attText.string.lowercased()).range(of: hp.lowercased())
                        
                        if (range.location != -1){
                            attText.addAttribute(NSAttributedStringKey.font, value: UIFont.systemFont(ofSize: 14.0, weight: UIFont.Weight.bold), range: range)
                            self.costCenterIdLabel.attributedText = attText
                        }
                    }
                }
                
                if let costCenterTitle = self.costCenterTitle {
                    
                    let attText = NSMutableAttributedString(string: costCenterTitle)
                    
                    for hp in highlightedParts {
                        let range = NSString(string: attText.string.lowercased()).range(of: hp.lowercased())
                        
                        if (range.location != -1){
                            attText.addAttribute(NSAttributedStringKey.font, value: UIFont.systemFont(ofSize: 16.0, weight: UIFont.Weight.bold), range: range)
                            self.costCenterLabel.attributedText = attText
                        }
                    }
                }
            
            
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupView()
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setupView()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
        // Initialization code
    }
    
    private func setupView() {
        
        let view = viewFromNibForClass()
        view.frame = bounds
        
        // Auto-layout stuff.
        view.autoresizingMask = [
            UIViewAutoresizing.flexibleWidth,
            UIViewAutoresizing.flexibleHeight
        ]
        
        // Show the view.
        addSubview(view)
        
        self.ownerLabel.text = self.owner
        self.ownerIdLabel.text = self.ownerId
        self.costCenterIdLabel.text = self.costCetnerId
        
        self.selectionStyle = UITableViewCellSelectionStyle.none
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
       
        super.touchesBegan(touches, with: event)
        
        UIView.animate(withDuration: 0.1) {
            self.subviews[1].backgroundColor = MerckColor.VeryLightGrey
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        
        UIView.animate(withDuration: 0.1) {
            self.subviews[1].backgroundColor = UIColor.white
        }
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesCancelled(touches, with: event)
        
        UIView.animate(withDuration: 0.1) {
            self.subviews[1].backgroundColor = UIColor.white
        }
    }
}
