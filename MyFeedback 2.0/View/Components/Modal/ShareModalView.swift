//
//  ShareModalView.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 26.04.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import UIKit
import MessageUI

class ShareModalView: ModalView {
    
    
    @IBOutlet weak var stackView: UIView!
    @IBOutlet weak var shareLabel: UILabel!
    @IBOutlet weak var cancelButton: CallToActionButton!
    
    @IBOutlet weak var buttonStackView: UIStackView!
    var image: UIImage?
    let subject = "Received Feedback via MyFeedback"
    
    var body:String?
    
    var parentViewController: UIViewController?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    private func setupView() {
        
        let view = viewFromNibForClass()
        view.frame = bounds
        
        // Auto-layout stuff.
        view.autoresizingMask = [
            UIViewAutoresizing.flexibleWidth,
            UIViewAutoresizing.flexibleHeight
        ]
        
        // Show the view.
        addSubview(view)
        
        self.cancelButton.setTitle("Cancel".localized(), for: .normal)
        
        let mailButton = UIButton(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        mailButton.setImage(#imageLiteral(resourceName: "Icon_iOS_Mail"), for: .normal)
        mailButton.addTarget(self, action: #selector(self.handleMail), for: .touchUpInside)
        let messageButton = UIButton(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        messageButton.setImage(#imageLiteral(resourceName: "Icon_iOS_Message"), for: .normal)
        messageButton.addTarget(self, action: #selector(self.handleMessage), for: .touchUpInside)
        let moreButton = UIButton(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        moreButton.setImage(#imageLiteral(resourceName: "Icon_iOS_More"), for: .normal)
        moreButton.addTarget(nil, action: #selector(self.handleMore), for: .touchUpInside)
        
        if MFMailComposeViewController.canSendMail(){
            self.buttonStackView.addArrangedSubview(mailButton)
            mailButton.width(60)
            mailButton.height(60)
        }
        if MFMessageComposeViewController.canSendAttachments(){
            self.buttonStackView.addArrangedSubview(messageButton)
            messageButton.width(60)
            messageButton.height(60)
        }
        
        self.buttonStackView.addArrangedSubview(moreButton)
        moreButton.width(60)
        moreButton.height(60)
        
        self.stackView.layer.cornerRadius = 5.0
        self.stackView.clipsToBounds = true
        self.stackView.layer.masksToBounds = true
        
    }
    
    override func setConstraints(container: UIView) {
        
        self.centerX(to: container)
        self.centerY(to: container, nil, offset: -20.0, priority: .required, isActive: true)
        
        self.width(min: 250.0, max: container.bounds.width - 80, priority: .required, isActive: true)

    }
    
    
    @IBAction func handleCloseButton(_ sender: Any) {
        
        self.done?(false)
    }
    
    @objc func handleMail(){
        let mvc : ModalViewController = try! Dip.container.resolve() as ModalViewController
        mvc.dismiss{}
        DummyEmailViewController.showMailComposer(ownerViewController: self.parentViewController!, toRecipients: [], subject: self.subject, mailBody: self.body ?? "", mailNotConfiguredTitle: "Error", mailNotConfiguredMessage: "Not able to send Mail through App", attachment: self.image);
    }
    
    @objc func handleMessage(){
        let mvc : ModalViewController = try! Dip.container.resolve() as ModalViewController
        mvc.dismiss{}
        DummyMessageViewController.show(ownerViewController: self.parentViewController!, toRecipients: nil, subject: self.subject, mailBody: self.body ?? "", notConfigured: "Error", notConfiguredMessage: "Not able to send MEssage through App", attachment: self.image)
    }
    
    @objc func handleMore(){
        let activityViewController = UIActivityViewController(activityItems: [self.body! as NSString, self.image as! UIImage], applicationActivities: nil)
        activityViewController.completionWithItemsHandler = {(act, success, a, error) in
            print("Activity dismissed")
        }
        activityViewController.excludedActivityTypes = [.addToReadingList, .mail, .message, .assignToContact, .openInIBooks, .saveToCameraRoll]
        let subject = "Received Feedback via MyFeedback"
        activityViewController.setValue(subject, forKey: "Subject")
        if let popoverController = activityViewController.popoverPresentationController {
            popoverController.sourceView = self
            popoverController.sourceRect = CGRect(x: self.bounds.midX, y: self.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        let mvc : ModalViewController = try! Dip.container.resolve() as ModalViewController
        mvc.dismiss{}
        self.parentViewController?.present(activityViewController, animated: true, completion: nil)
    }
    
}

class Share {
    
    static func show(parentViewController : UIViewController, body:String, image:UIImage?)
    {
        //Modal VC will be created on fly and removed on fly...
        /*let modalVC = ModalViewController(nibName: "ModalViewController", bundle: nil) as ModalViewController;
        parentViewController.view.addSubview(modalVC.view)
        modalVC.view.edges(to: parentViewController.view)*/
        let mvc = try! Dip.container.resolve() as ModalViewController
        
        let smv = ShareModalView()
        smv.body = body
        smv.image = image
        smv.parentViewController = parentViewController
        mvc.present(modal: smv)
    }
    
    static func hide() {
        
        let mvc : ModalViewController = try! Dip.container.resolve() as ModalViewController
        
        mvc.dismiss {
            
        }
    }
}
