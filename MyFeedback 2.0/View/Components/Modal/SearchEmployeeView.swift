//
//  SearchEmployeeView.swift
//  MIA
//
//  Created by Daniel Weber on 01.09.17.
//  Copyright © 2017 Merck KGaA. All rights reserved.
//

import UIKit
import Bond
import Hydra
import Dip
import KeyboardHelper
import TinyConstraints
import Localize_Swift
import IQKeyboardManagerSwift

class SearchEmployeeView: ModalView, ModalViewResulting, UITableViewDelegate, UITableViewDataSource, KeyboardNotificationDelegate {
    
    
    var result: (([Employee], Bool) -> Bool)?
    
    @IBOutlet weak var textEntryView: UIView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var resultsTableView: UITableView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var selectedEmployeesView: UIView!
    @IBOutlet weak var selectedEmployeesCountLabel: UILabel!
    @IBOutlet weak var selectedEmployeesNameLabel: UILabel!
    
    var keyboardHelper : KeyboardHelper?
    
    var view: UIView?
    
    var service = try! Dip.container.resolve() as FeedbackService
    
    //    var filteredemployees : [Employee]?
    var filteredEmployees : MutableObservableArray<Employee> = MutableObservableArray([])
    var suggestedEmployees : [Employee]{
        get{
            //return filteredEmployees.array.filter({emp in return emp.isSuggested && !emp.isSelected})
            //return self.service.suggestedEmployees.array
            return self.filterEmployeeArray(employees: self.service.suggestedEmployees.array)
        }
    }
    var searchTerm : Observable<String?> = Observable("")
    
    var searchTimer: Timer?
    
    var searchPromise : Promise<[Employee]>?
    var searchToken : InvalidationToken?
    
    var oldIndex:Int = 0
    var currentPage:Int = 0
    
    var sections: [String] = []
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    private func setupView() {
        
        let view = viewFromNibForClass()
        view.frame = bounds
        
        // Auto-layout stuff.
        view.autoresizingMask = [
            UIViewAutoresizing.flexibleWidth,
            UIViewAutoresizing.flexibleHeight
        ]
        
        self.keyboardHelper = KeyboardHelper(delegate: self)
        
        
        // Show the view.
        addSubview(view)
        self.view = view
        
        self.layer.cornerRadius = 5.0
        self.layer.masksToBounds = true
        
        self.headerLabel.text = "Select Employees".localized()
        if(RCValues.sharedInstance.demoMode()){
            self.headerLabel.text = "Select Users"
        }else{
            self.headerLabel.text = "Select Employees".localized()
        }
        
        
        //self.selectedEmployeesView.backgroundColor = MerckColor.VibrantGreen
        self.selectedEmployeesNameLabel.textColor = MerckColor.Grey
        self.selectedEmployeesCountLabel.textColor = MerckColor.Grey
        self.updateSelectedHeader()
        
        self.resultsTableView.register(SearchEmployeeTableViewCell.self, forCellReuseIdentifier: "ResultTableViewCell")
        self.resultsTableView.register(EmptySearchEmployeeTableViewCell.self, forCellReuseIdentifier: "EmptyTableViewCell")
        
        self.resultsTableView.delegate = self
        self.resultsTableView.dataSource = self
        
        self.searchTextField.reactive.text.bidirectionalBind(to: self.searchTerm)
        self.searchTextField.reactive.text.observeNext(with: {searchString in
            self.currentPage = 0
            self.oldIndex = 0
            self.resultsTableView.layer.opacity = 0.4
            self.resultsTableView.isUserInteractionEnabled = false
            self.searchTimer?.invalidate()
            guard searchString != nil, !searchString!.isEmpty else{
                self.filteredEmployees.replace(with: [])
                self.loadItems(paging: 0)
                self.resultsTableView.layer.opacity = 0.4
                self.resultsTableView.isUserInteractionEnabled = true
                return
            }
            if searchString!.count > 3 || searchString!.contains(" "){
                self.searchTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: {_ in
                    self.service.searchEmployees(searchString: searchString!).then({emps in
                        let filteredEmps = self.filterEmployeeArray(employees: emps)
                        self.filteredEmployees.replace(with: filteredEmps)
                        self.resultsTableView.layer.opacity = 1.0
                        self.resultsTableView.isUserInteractionEnabled = true
                    })
                })
            }else{
                self.resultsTableView.layer.opacity = 1.0
                self.resultsTableView.isUserInteractionEnabled = true
            }
            
        })
        
        self.filteredEmployees.observeNext(with: {e in
            guard e.dataSource.array.count > 0, (e.dataSource.count - self.resultsTableView.numberOfRows(inSection: self.resultsTableView.numberOfSections-1)) > 0 else{
                self.resultsTableView.reloadData()
                return
            }
            var indexPathArray:[IndexPath] = []
            for i in 0..<(e.dataSource.count - self.resultsTableView.numberOfRows(inSection: self.resultsTableView.numberOfSections-1)){
                indexPathArray.append(IndexPath.init(row: self.currentPage+i, section: self.resultsTableView.numberOfSections-1))
            }
            self.oldIndex = self.currentPage
            self.resultsTableView.reloadData()
            
            
        })
        
    }
    
    
    
    override func setConstraints(container: UIView) {
        
        self.centerX(to: container)
        self.top(to: container, nil, offset: 40.0, relation: .equal, priority: .required, isActive: true)
        self.bottom(to: container)
        self.width(to: container, nil, multiplier: 1.0, offset: -40.0, relation: .equal, priority: .required, isActive: true)
    }
    
    override func didMoveToSuperview() {
        //self.searchTextField.becomeFirstResponder()
        IQKeyboardManager.sharedManager().shouldResignOnTouchOutside = false
    }
    
    override func willBeDismissed() {
        self.searchTextField.resignFirstResponder()
        IQKeyboardManager.sharedManager().shouldResignOnTouchOutside = true
    }
    
    @IBAction func handleCloseButton(_ sender: Any) {
        self.done?(true)
        self.result?(self.service.selectedEmployees,true)
    }
    
    @IBAction func handleSelectedHeaderTapped(){
        self.resultsTableView.setContentOffset(CGPoint.zero, animated: true)
    }
    
    func keyboardWillAppear(_ info: KeyboardAppearanceInfo) {
        
        info.animateAlong({ () -> Void in
            let insets = UIEdgeInsetsMake(0, 0, info.endFrame.size.height, 0)
            self.resultsTableView.contentInset = insets
            self.resultsTableView.scrollIndicatorInsets = insets
        })  { finished in }
    }
    
    func keyboardWillDisappear(_ info: KeyboardAppearanceInfo) {
        
        info.animateAlong({ () -> Void in
            let insets = UIEdgeInsets.zero
            self.resultsTableView.contentInset = insets
            self.resultsTableView.scrollIndicatorInsets = insets
        })  { finished in }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var noSections = 0
        self.sections = []
        
        if self.service.selectedEmployees.count > 0{
            noSections += 1
            self.sections.append("selectedEmployees")
        }
        if self.suggestedEmployees.count > 0{
            noSections += 1
            self.sections.append("suggestedEmployees")
        }
        noSections += 1
        self.sections.append("merckEmployees")
        
        return noSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch self.sections[section]{
        case "selectedEmployees":
            return self.service.selectedEmployees.count
        case "suggestedEmployees":
            if searchTerm.value == nil || searchTerm.value!.isEmpty{
                return self.suggestedEmployees.count
            }else{
                return self.suggestedEmployees.filter({emp in return self.considerAND(term: searchTerm.value, emp: emp)}).count
            }
        case "merckEmployees":
            return self.filteredEmployees.array.filter{!$0.isSelected}.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch self.sections[section]{
        case "selectedEmployees":
            if(RCValues.sharedInstance.demoMode()){
                return "Selected Users"+" \(self.service.selectedEmployees.count)/50"
            }
            return "Selected Employees".localized()+" \(self.service.selectedEmployees.count)/50"
        case "suggestedEmployees":
             return "Suggestions".localized()
        case "merckEmployees":
            if(RCValues.sharedInstance.demoMode()){
                return "Users"
            }
            return  "Merck Employees".localized()
        default:
           return  nil
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
        
        if let header = view as? UITableViewHeaderFooterView{
            header.textLabel?.textColor = UIColor.white
            header.backgroundColor = MerckColor.VibrantGreen
            header.backgroundView?.backgroundColor = MerckColor.VibrantGreen
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let list: [Employee]
        switch self.sections[indexPath.section]{
            case "selectedEmployees":
                list = self.service.selectedEmployees
            case "suggestedEmployees":
                if searchTerm.value == nil || searchTerm.value!.isEmpty{
                    list = self.suggestedEmployees
                }else{
                    list = self.self.suggestedEmployees.filter({emp in return self.considerAND(term: searchTerm.value, emp: emp)})
                }
            case "merckEmployees":
                list = self.filteredEmployees.array.filter{!$0.isSelected}
                if indexPath.row == list.count - 1 && self.searchTextField.text?.isEmpty != false{ // last cell
                    self.loadItems(paging: 20)
                }
            default:
                list = []
        }
        
        let item = list[indexPath.item]
        
        if (item.isValid){
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ResultTableViewCell") as? SearchEmployeeTableViewCell
            cell?.owner = item.getName()
            cell?.ownerId = item.orgUnitName
            cell?.costCenterTitle = item.userId
            
            cell?.highlightedString = self.searchTerm.value
            
            return cell!
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EmptyTableViewCell") as? EmptySearchEmployeeTableViewCell
            cell?.textLabel?.textColor = UIColor.black
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //Loading.show()
        let list: [Employee]
        switch self.sections[indexPath.section]{
        case "selectedEmployees":
            list = self.service.selectedEmployees
        case "suggestedEmployees":
            if searchTerm.value == nil || searchTerm.value!.isEmpty{
                list = self.suggestedEmployees
            }else{
                list = self.self.suggestedEmployees.filter({emp in return self.considerAND(term: searchTerm.value, emp: emp)})
            }
        case "merckEmployees":
            list = self.filteredEmployees.array.filter{!$0.isSelected}
            if indexPath.row == list.count - 1 && self.searchTextField.text?.isEmpty != false{ // last cell
                self.loadItems(paging: 20)
            }
        default:
            list = []
        }
        
        let employee = list[indexPath.item]
        self.resultsTableView.selectRow(at: nil, animated: false, scrollPosition: .none)
        self.service.toggleEmployeeSelection(employee: employee)
        DispatchQueue.main.async {
            self.updateSelectedHeader()
            self.resultsTableView.reloadData()
        }
        
    }
    
    func filterEmployeeArray(employees: [Employee])->[Employee]{
        return employees.filter({employee in
            return !self.service.selectedEmployees.contains(where: {emp in return emp.userId == employee.userId})
            
        })
    }
    
    func considerAND(term: String?, emp: Employee) -> Bool {
        
        if let searchTerms = term?.split(separator: " ") {
            
            for t in searchTerms {
                
                if emp.searchTerm.lowercased().contains(t.lowercased()) { continue }
                
                // When there is no hit for one item it's out (AND)
                return false
            }
            
            // When every item in the loop had a hit, this search term fits (AND)
            return true
        }
        
        return false
    }
    
    func updateSelectedHeader(){
        self.selectedEmployeesCountLabel.text = "Selected Employees".localized()+" \(self.service.selectedEmployees.count)/50"
        if(RCValues.sharedInstance.demoMode()){
            self.selectedEmployeesCountLabel.text = "Selected Users".localized()+" \(self.service.selectedEmployees.count)/50"
        }
        self.selectedEmployeesNameLabel.text = self.service.selectedEmployees.map{$0.getName()}.joined(separator: ", ")
    }
    
    func loadItems(paging: Int){
        self.currentPage += paging
        self.service.updateEmployees(page: self.currentPage).then({employees in
            self.filteredEmployees.insert(contentsOf: employees, at: self.filteredEmployees.array.count)
            self.resultsTableView.layer.opacity = 1.0
        }).catch({e in
            print(e)
            self.resultsTableView.layer.opacity = 1.0
        })
    }
}
