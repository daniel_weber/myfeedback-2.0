//
//  SettingsModalView.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 14.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import UIKit

class SettingsModalView: ModalView, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loggedInAsLabel: UILabel!
    @IBOutlet weak var loggedInAsButton: UIButton!
    
    var result: ((UserSettings, Bool) -> Bool)?
    
    let service = try! Dip.container.resolve() as FeedbackService
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    private func setupView() {
        
        let view = viewFromNibForClass()
        view.frame = bounds
        
        // Auto-layout stuff.
        view.autoresizingMask = [
            UIViewAutoresizing.flexibleWidth,
            UIViewAutoresizing.flexibleHeight
        ]
        
        // Show the view.
        addSubview(view)
        
        self.layer.cornerRadius = 5.0
        self.layer.masksToBounds = true
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(SettingsTableViewCell.self, forCellReuseIdentifier: "SettingsTableViewCell")
        service.userSettings.observeNext(with: {userSettings in
            print("Settings changed")
        })
        
        loggedInAsButton.setTitle("\(loggedInUser.value?.getName() ?? "") (\(loggedInUser.value?.userId ?? ""))", for: .normal)
        
    }
    
    override func setConstraints(container: UIView) {
        
        self.centerX(to: container)
        self.top(to: container, nil, offset: 40.0, relation: .equal, priority: .required, isActive: true)
        self.bottom(to: container)
        self.width(to: container, nil, multiplier: 1.0, offset: -40.0, relation: .equal, priority: .required, isActive: true)
    }
    
    override func didMoveToSuperview() {
        //self.searchTextField.becomeFirstResponder()
    }
    
    override func willBeDismissed() {
        //self.searchTextField.resignFirstResponder()
    }
    
    // TableView Data Source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    // TableView Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsTableViewCell", for: indexPath) as! SettingsTableViewCell
        cell.selectionStyle = .none
        cell.backgroundColor = UIColor.clear
        cell.titleLabel.textColor = UIColor.black
        cell.selectionButton.backgroundColor = MerckColor.VibrantMagenta
        cell.selectionButton.setTitleColor(UIColor.white, for: .normal)
        
        switch indexPath.row{
        case 0:
            cell.title = "Show received requests for:"
            cell.selectionButton.setTitle(self.service.userSettings.value.showReceivedDuration.localizedName, for: .normal)
            cell.labels = DurationSetting.Durations.map{$0.localizedName}
            cell.options = DurationSetting.Durations.map{$0.key}
            cell.result = { (value, completed) -> Bool in
                var newSettings = self.service.userSettings.value.copy() as! UserSettings
                newSettings.showReceivedDuration = DurationSetting.getDuration(key: value)
                self.service.userSettings.value = newSettings
                print("New Setting: \(self.service.userSettings.value.showReceivedDuration)")
                return true
            }
        case 1:
            cell.title = "Show own requests for:"
            cell.selectionButton.setTitle(self.service.userSettings.value.showSendDuration.localizedName, for: .normal)
            cell.labels = DurationSetting.Durations.map{$0.localizedName}
            cell.options = DurationSetting.Durations.map{$0.key}
            cell.result = { (value, completed) -> Bool in
                
                var newSettings = self.service.userSettings.value.copy() as! UserSettings
                newSettings.showSendDuration = DurationSetting.getDuration(key: value)
                self.service.userSettings.value = newSettings
                print("New Setting: \(self.service.userSettings.value.showSendDuration)")
                return true
            }
        case 2:
            cell.title = "Push Notifications:"
            cell.selectionButton.setTitle(self.service.userSettings.value.pushNotification.localizedName, for: .normal)
            cell.labels = NotificationSetting.Notifications.map{$0.localizedName}
            cell.options = NotificationSetting.Notifications.map{$0.key}
            cell.result = { (value, completed) -> Bool in
                
                var newSettings = self.service.userSettings.value.copy() as! UserSettings
                newSettings.pushNotification = NotificationSetting.getNotificationSetting(key: value)
                self.service.userSettings.value = newSettings
                print("New Setting: \(self.service.userSettings.value.pushNotification)")
                return true
            }
        case 3:
            cell.title = "Email Notifications:"
            cell.selectionButton.setTitle(self.service.userSettings.value.mailNotification.localizedName, for: .normal)
            cell.labels = NotificationSetting.Notifications.map{$0.localizedName}
            cell.options = NotificationSetting.Notifications.map{$0.key}
            cell.result = { (value, completed) -> Bool in
                
                var newSettings = self.service.userSettings.value.copy() as! UserSettings
                newSettings.mailNotification = NotificationSetting.getNotificationSetting(key: value)
                self.service.userSettings.value = newSettings
                print("New Setting: \(self.service.userSettings.value.mailNotification)")
                return true
            }
        case 4:
            cell.title = "Language:"
            cell.selectionButton.setTitle(self.service.userSettings.value.mailNotification.localizedName, for: .normal)
            cell.labels = NotificationSetting.Notifications.map{$0.localizedName}
            cell.options = NotificationSetting.Notifications.map{$0.key}
            cell.result = { (value, completed) -> Bool in
                
                var newSettings = self.service.userSettings.value.copy() as! UserSettings
                newSettings.mailNotification = NotificationSetting.getNotificationSetting(key: value)
                self.service.userSettings.value = newSettings
                print("New Setting: \(self.service.userSettings.value.mailNotification)")
                return true
            }
        default:
            break
            
        }
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    @IBAction func handleCloseButton(_ sender: Any) {
        self.done?(true)
    }

}
