//
//  ErrorModalView.swift
//  MIA
//
//  Created by Daniel Weber on 28.09.17.
//  Copyright © 2017 Merck KGaA. All rights reserved.
//

import UIKit

class ErrorModalView: ModalView {

    var error : String? {
        
        didSet {
            
            self.errorLabel.text = self.error
        }
    }
    
    @IBOutlet weak var stackView: UIView!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var errorIconImageView: UIImageView!
    @IBOutlet weak var errorTitleLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    private func setupView() {
        
        let view = viewFromNibForClass()
        view.frame = bounds
        
        // Auto-layout stuff.
        view.autoresizingMask = [
            UIViewAutoresizing.flexibleWidth,
            UIViewAutoresizing.flexibleHeight
        ]
        
        // Show the view.
        addSubview(view)
        self.errorIconImageView.tintImageColor(color: MerckColor.VibrantYellow)
        //self.layer.cornerRadius = 5.0
        self.stackView.layer.cornerRadius = 5.0
        self.stackView.clipsToBounds = true
        self.stackView.layer.masksToBounds = true
        //self.layer.masksToBounds = true
    }
    
    override func setConstraints(container: UIView) {
        
        self.centerX(to: container)
        self.centerY(to: container, nil, offset: -20.0, priority: .required, isActive: true)
        
        self.width(min: 120.0, max: container.bounds.width - 80, priority: .required, isActive: true)

//        self.width(80.0)
//        self.height(55.0)
    }
    
    @IBAction func handleCloseButton(_ sender: Any) {
        
        self.done?(false)
    }
    
    @IBAction func handleOkButton(_ sender: Any) {
        
        self.done?(false)
    }
    
}

class ErrorView {
    
    static func show(error: Error){
        if error is webError{
            if case webError.webServiceError(let message) = error{
                self.show(message: message)
            }else{
                self.show(message: "\(error)")
            }
        }else{
            self.show(message: error.localizedDescription)
        }
    }
    
    static func show(message : String) {
        
        let mvc : ModalViewController = try! Dip.container.resolve() as ModalViewController
        
        let e = ErrorModalView()
        e.error = message
        
        mvc.present(modal: e, edge: .bottom)
    }
    
    static func hide() {
        
        let mvc : ModalViewController = try! Dip.container.resolve() as ModalViewController
        
        mvc.dismiss {
            
        }
    }
}
