//
//  EnterTextModalView.swift
//  MIA
//
//  Created by Daniel Weber on 19.09.17.
//  Copyright © 2017 Merck KGaA. All rights reserved.
//

import UIKit
import TinyConstraints

class TrophyModalView: ModalView {
    
    @IBOutlet weak var trophyImageView: UIImageView!
    @IBOutlet weak var overviewButton: CallToActionButton!
    @IBOutlet weak var closeButton: CallToActionButton!
    
    var isInOverview: Bool = false{
        didSet{
             self.overviewButton.isHidden = isInOverview
        }
    }
    
    let service = try! Dip.container.resolve() as FeedbackService
    
    var textView: UITextView?
    
    var trophy: Trophy?{
        didSet{
            self.trophyImageView.image = trophy?.cardImage
            if trophy != nil{
                setupText()
            }
        }
    }
    
    var result: ((String, Bool) -> Bool)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    private func setupView() {
        
        let view = viewFromNibForClass()
        view.frame = bounds
        
        // Auto-layout stuff.
        view.autoresizingMask = [
            UIViewAutoresizing.flexibleWidth,
            UIViewAutoresizing.flexibleHeight
        ]
        
        // Show the view.
        addSubview(view)
        
        //view.width(min: 250, max: 500, priority: .required, isActive: true)
        
        self.overviewButton.isHidden = isInOverview
       
        //self.layer.cornerRadius = 5.0
        //self.layer.masksToBounds = true
        if trophy != nil{
            setupText()
        }
        
    }
    
    func setupText(){
        self.textView?.removeFromSuperview()
        let textView = UITextView()
        textView.isEditable = false
        textView.textColor = UIColor.white
        textView.backgroundColor = UIColor.clear
        textView.text = self.trophy?.cardText
        self.trophyImageView.addSubview(textView)
        textView.leftToSuperview(nil, offset: 8, relation: .equal, priority: .required, isActive: true)
        if self.trophy?.name == Trophy.CompetencyChampionLevel0.name{
            textView.topToSuperview(nil, offset: 8, relation: .equal, priority: .required, isActive: true)
            textView.heightToSuperview(nil, multiplier: 0.50, offset: 0, relation: .equal, priority: .required, isActive: true)
            textView.widthToSuperview(nil, multiplier: 0.6, offset: 0, relation: .equal, priority: .required, isActive: true)
        }else{
            textView.bottom(to: self.trophyImageView)
            textView.heightToSuperview(nil, multiplier: 0.60, offset: 0, relation: .equal, priority: .required, isActive: true)
            textView.widthToSuperview(nil, multiplier: 0.45, offset: 0, relation: .equal, priority: .required, isActive: true)
        }
        
    }
    
    override func setConstraints(container: UIView) {
        centerX(to: container)
        centerY(to: container, nil, offset: -60.0, priority: .required, isActive: true)
        width(to: container, nil, multiplier: 1.0, offset: -40.0, relation: .equal, priority: .defaultHigh, isActive: true)
        width(min: 250, max: 300, priority: .required, isActive: true)
    }
    
    
    @IBAction func handleCloseButton(_ sender: Any) {
        self.result?("", false)
        self.done?(false)
    }
    
    @IBAction func handleOverviewButton(_ sender: Any) {
        
        guard let r = self.result else {
            self.done?(false)
            return
        }
        
        let accept = r("", true)
        
        if (accept) {
            self.done?(accept)
        }
    }
    
}

