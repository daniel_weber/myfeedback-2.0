//
//  FilterTableView.swift
//  MIA
//
//  Created by Daniel Weber on 18.10.17.
//  Copyright © 2017 zero2one. All rights reserved.
//

import UIKit
import Bond
import Dip

class FilterTableView: ModalView, ModalViewResulting, UITableViewDelegate, UITableViewDataSource {
    
    
    // MARK: Outlets
    @IBOutlet weak var contentWrapper: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var clearButton: CallToActionButton!
    
    var numberOfRows:Int{
        get{
            var initialRows = 6
            if self.service.activeFilters.array.filter({filter in return filter.type == .SeriesOnly}).count == 1{
                initialRows += 1
                return initialRows
            }else{
                return initialRows
            }
            
        }
    }
    
    var service = try! Dip.container.resolve() as FeedbackService
    var result: ((Any, Bool) -> Bool)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    func setupView() {
        
        let view = viewFromNibForClass()
        view.frame = bounds
        
        // Auto-layout stuff.
        view.autoresizingMask = [
            UIViewAutoresizing.flexibleWidth,
            UIViewAutoresizing.flexibleHeight
        ]
        
        self.titleLabel.text = "Filters".localized()
        self.clearButton.setTitle("Clear".localized(), for: .normal)
        
        // Show the view.
        addSubview(view)
        
        let height = view.height(50*CGFloat.init(numberOfRows)+50+40)
        view.top(to: self, nil, offset: 40.0, relation: .equal, priority: .required, isActive: true)
        view.centerX(to: self)
        view.width(to: self, nil, multiplier: 1.0, offset: -40.0, relation: .equal, priority: .required, isActive: true)
        self.clearButton.height(40)
        self.clearButton.bottom(to: view, nil, offset: 0, relation: .equal, priority: .required, isActive: true)
        contentWrapper.bottomToTop(of: self.clearButton, offset: -16, relation: .equal, priority: .required, isActive: true)
        self.tableView.bottom(to: self.contentWrapper)
        
        service.activeFilters.observeNext(with: {e in
            if e.dataSource.array.filter({f in return f.type == .SeriesOnly}).count == 1 && self.tableView.numberOfRows(inSection: 0) == 6{
                self.tableView.insertRows(at: [IndexPath.init(row: 6, section: 0)], with: .top)
                
                height.constant += 50
                self.clearButton.bottom(to: view, nil, offset: 0, relation: .equal, priority: .required, isActive: true)
                UIViewPropertyAnimator(duration: 1, dampingRatio: 0.4) {
                    self.layoutIfNeeded()
                    }.startAnimation()
                
            }else if e.dataSource.array.filter({f in return f.type == .SeriesOnly}).count == 0 && self.tableView.numberOfRows(inSection: 0) == 7{
                self.tableView.deleteRows(at: [IndexPath.init(row: 6, section: 0)], with: .fade)
                
                height.constant -= 50
                UIViewPropertyAnimator(duration: 1, dampingRatio: 0.4) {
                    self.layoutIfNeeded()
                    }.startAnimation()
            }
            var indexPaths:[IndexPath] = []
            for i in 0..<self.tableView.numberOfRows(inSection: 0){
                if i != 5{
                    indexPaths.append(IndexPath.init(row: i, section: 0))
                }
            }
            self.tableView.reloadRows(at: indexPaths, with: .none)
            //self.tableView.reloadData()
        })
        
        self.contentWrapper.layer.cornerRadius = 5.0
        self.contentWrapper.layer.masksToBounds = true
        
        self.tableView.register(FilterTableViewCell.self, forCellReuseIdentifier: "FilterTableViewCell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
    }
    
   
    
    override func setConstraints(container: UIView) {
        
        self.centerX(to: container)
        self.top(to: container, nil, offset: 40.0, relation: .equal, priority: .required, isActive: true)
        let height:CGFloat = 50*CGFloat.init(numberOfRows)+50+40+50+50
        self.height(height)
        //self.bottom(to: container)
        self.width(to: container, nil, multiplier: 1.0, offset: -40.0, relation: .equal, priority: .required, isActive: true)
    }
    
    override func didMoveToSuperview() {
        
    }
    
    override func willBeDismissed() {
        
    }
    
    @IBAction func handleClearButton(_ sender: Any) {
        //self.done?(false)
        //self.result?(false,false)
        service.activeFilters.removeAll()
        self.tableView.reloadData()
    }
    @IBAction func handleCloseButton(_ sender: Any) {
        self.done?(false)
        self.result?(false,false)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FilterTableViewCell", for: indexPath) as! FilterTableViewCell
        switch indexPath.row{
        /*case 0:
            let cell2 = UITableViewCell.init(style: .value1, reuseIdentifier: nil)
            cell2.textLabel?.text = "Sort".localized()
            cell2.detailTextLabel?.text = self.service.sortBy.value.0.label
            return cell2*/
        case 0:
            cell.filterType = FilterType.Category
        case 1:
            cell.filterType = FilterType.RequestType
        case 2:
            cell.filterType = FilterType.Employee
        case 3:
            cell.filterType = FilterType.DateFrom
        case 4:
            cell.filterType = FilterType.DateTo
        case 5:
            cell.filterType = FilterType.SeriesOnly
        case 6:
            cell.filterType = FilterType.Series
        default:
            break
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mvc = try! Dip.container.resolve() as ModalViewController
        if let cell = tableView.cellForRow(at: indexPath) as? FilterTableViewCell{
            if cell.filterType == FilterType.DateTo || cell.filterType == FilterType.DateFrom{
                let dateSelectView = EnterDateModalView()
                dateSelectView.filterType = cell.filterType
                mvc.present(modal: dateSelectView)
            }else if cell.filterType == FilterType.SeriesOnly{
                return
            }else{
                let filterDetailView = FilterDetailTableView()
                filterDetailView.filterType = cell.filterType
                mvc.present(modal: filterDetailView)
            }
        }else{
            let sortView = SortModalView()
            sortView.list = SortType.Request
            mvc.present(modal: sortView)
        }
        
        
    }

}
