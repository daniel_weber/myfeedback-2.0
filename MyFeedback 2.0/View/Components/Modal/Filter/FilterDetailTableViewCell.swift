//
//  FilterDetailTableViewCell.swift
//  MIA
//
//  Created by Daniel Weber on 18.10.17.
//  Copyright © 2017 zero2one. All rights reserved.
//

import UIKit

class FilterDetailTableViewCell: UITableViewCell {
    
    var filter: Filter?{
        didSet{
            if filter != nil{
                self.setFilter(filter!)
            }
        }
    }
    var service = try! Dip.container.resolve() as FeedbackService
    
    // MARK: Outlets
    @IBOutlet weak var filterLabel: UILabel!
    @IBOutlet weak var selectedIndicatorImageView: UIImageView!
    

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupView()
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setupView()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
        // Initialization code
    }
    
    private func setupView() {
        
        let view = viewFromNibForClass()
        view.frame = bounds
        self.selectedIndicatorImageView.tintImageColor(color: MerckColor.RichPurple)
        // Auto-layout stuff.
        view.autoresizingMask = [
            UIViewAutoresizing.flexibleWidth,
            UIViewAutoresizing.flexibleHeight
        ]
        
        // Show the view.
        addSubview(view)
        self.selectedIndicatorImageView.tintColor = MerckColor.VibrantMagenta
        self.selectedIndicatorImageView.image = #imageLiteral(resourceName: "Button_Checkmark").withRenderingMode(.alwaysTemplate)
        self.selectionStyle = UITableViewCellSelectionStyle.none
    }
    
    func setFilter(_ filter: Filter){
        switch filter.type{
        case .Category:
            if let cat = filter.value as? RequestCategory{
                self.filterLabel.text = cat.localName
            }
        case .RequestType:
            if let type = filter.value as? RequestType{
                self.filterLabel.text = type.localName
            }
        case .Employee:
            if let employee = filter.value as? Employee{
                self.filterLabel.text = employee.getName()
            }
        case .Series:
            if let series = filter.value as? (id: Int?, title: String?){
                self.filterLabel.text = series.title
            }
        case .Competency:
            if let comp = filter.value as? Competency{
                self.filterLabel.text = comp.localName
            }
        default:
            break
        }
        self.selectedIndicatorImageView.isHidden = !self.isActiveFilter()
    }
    
    func isActiveFilter()->Bool{
        if self.filter?.type != nil{
            switch self.filter!.type{
            case .Category:
                if let category = filter?.value as? RequestCategory{
                    return service.activeFilters.array.contains(where: {filter in
                        if let cat = filter.value as? RequestCategory{
                            return cat.name == category.name
                        }
                        return false
                    })
                }
            case .RequestType:
                if let type = filter?.value as? RequestType{
                    return service.activeFilters.array.contains(where: {filter in
                        if let t = filter.value as? RequestType{
                            return t.key == type.key
                        }
                        return false
                    })
                }
            case .Employee:
                if let rater = filter?.value as? Employee{
                    return service.activeFilters.array.contains(where: {filter in
                        if let emp = filter.value as? Employee{
                            return emp.userId == rater.userId
                        }
                        return false
                    })
                }
            case .Series:
                if let series = filter?.value as? (id:Int?, title:String?){
                    return service.activeFilters.array.contains(where: {filter in
                        if let s = filter.value as? (id:Int?, title:String?){
                            return s.id == series.id
                        }
                        return false
                    })
                }
            case .Competency:
                if let competency = filter?.value as? Competency{
                    return service.activeFilters.array.contains(where: {filter in
                        if let comp = filter.value as? Competency{
                            return comp.name == competency.name
                        }
                        return false
                    })
                }
            default:
                return false
            }
        }
        
        return false
    }
    
    func toggleState(){
        if self.isActiveFilter(){
            self.removeFilter()
        }else{
            self.addFilter()
        }
    }
    
    func addFilter(){
        if self.filter != nil{
            service.activeFilters.append(self.filter!)
            self.selectedIndicatorImageView.isHidden = false
        }
        
    }
    
    func removeFilter(){
        if self.filter != nil{
            var index: Int? = nil
            switch self.filter!.type{
            case .Category:
                if let category = filter?.value as? RequestCategory{
                    index = service.activeFilters.array.index(where: {filter in
                        if let cat = filter.value as? RequestCategory{
                            return cat.name == category.name
                        }
                        return false
                    })
                }
            case .RequestType:
                if let type = filter?.value as? RequestType{
                    index = service.activeFilters.array.index(where: {filter in
                        if let t = filter.value as? RequestType{
                            return t.key == type.key
                        }
                        return false
                    })
                }
            case .Employee:
                if let rater = filter?.value as? Employee{
                    index = service.activeFilters.array.index(where: {filter in
                        if let emp = filter.value as? Employee{
                            return emp.userId == rater.userId
                        }
                        return false
                    })
                }
            case .Series:
                if let series = filter?.value as? (id:Int?, title:String?){
                    index = service.activeFilters.array.index(where: {filter in
                        if let s = filter.value as? (id:Int?, title:String?){
                            return s.id == series.id
                        }
                        return false
                    })
                }
            case .Competency:
                if let competency = filter?.value as? Competency{
                    index = service.activeFilters.array.index(where: {filter in
                        if let comp = filter.value as? Competency{
                            return comp.name == competency.name
                        }
                        return false
                    })
                }
            default:
                index = nil
            }
            if index != nil{
               service.activeFilters.remove(at: index!)
                self.selectedIndicatorImageView.isHidden = true
            }
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        super.touchesBegan(touches, with: event)
        
        UIView.animate(withDuration: 0.1) {
            self.subviews[1].backgroundColor = MerckColor.VeryLightGrey
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        
        UIView.animate(withDuration: 0.1) {
            self.subviews[1].backgroundColor = UIColor.white
        }
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesCancelled(touches, with: event)
        
        UIView.animate(withDuration: 0.1) {
            self.subviews[1].backgroundColor = UIColor.white
        }
    }

}
