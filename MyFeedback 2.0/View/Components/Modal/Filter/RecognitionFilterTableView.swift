//
//  RecognitionFilterTableView.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 30.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import UIKit

class RecognitionFilterTableView: FilterTableView{
    
    override func setupView() {
        
        let view = viewFromNibForClass()
        view.frame = bounds
        
        // Auto-layout stuff.
        view.autoresizingMask = [
            UIViewAutoresizing.flexibleWidth,
            UIViewAutoresizing.flexibleHeight
        ]
        
        
        // Show the view.
        addSubview(view)
        
        self.titleLabel.text = "Filters".localized()
        self.clearButton.setTitle("Clear".localized(), for: .normal)
        
        let height = view.height(50*CGFloat.init(4)+50+40)
        view.top(to: self, nil, offset: 40.0, relation: .equal, priority: .required, isActive: true)
        view.centerX(to: self)
        view.width(to: self, nil, multiplier: 1.0, offset: -40.0, relation: .equal, priority: .required, isActive: true)
        self.clearButton.height(40)
        self.clearButton.bottom(to: view, nil, offset: 0, relation: .equal, priority: .required, isActive: true)
        contentWrapper.bottomToTop(of: self.clearButton, offset: -16, relation: .equal, priority: .required, isActive: true)
        self.tableView.bottom(to: self.contentWrapper)
        
        service.activeFilters.observeNext(with: {e in
            var indexPaths:[IndexPath] = []
            for i in 0..<self.tableView.numberOfRows(inSection: 0){
                indexPaths.append(IndexPath.init(row: i, section: 0))
            }
            self.tableView.reloadRows(at: indexPaths, with: .none)
            //self.tableView.reloadData()
        })
        
        self.contentWrapper.layer.cornerRadius = 5.0
        self.contentWrapper.layer.masksToBounds = true
        
        self.tableView.register(FilterTableViewCell.self, forCellReuseIdentifier: "FilterTableViewCell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
    }
    
    override func setConstraints(container: UIView) {
        
        self.centerX(to: container)
        self.top(to: container, nil, offset: 40.0, relation: .equal, priority: .required, isActive: true)
        let height:CGFloat = 50*CGFloat.init(4)+50+40+50
        self.height(height)
        //self.bottom(to: container)
        self.width(to: container, nil, multiplier: 1.0, offset: -40.0, relation: .equal, priority: .required, isActive: true)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FilterTableViewCell", for: indexPath) as! FilterTableViewCell
        switch indexPath.row{
        /*case 0:
            let cell2 = UITableViewCell.init(style: .value1, reuseIdentifier: nil)
            cell2.textLabel?.text = "Sort".localized()
            cell2.detailTextLabel?.text = self.service.sortBy.value.0.label
            return cell2*/
        case 0:
            cell.filterType = FilterType.Competency
        case 1:
            cell.filterType = FilterType.Employee
        case 2:
            cell.filterType = FilterType.DateFrom
        case 3:
            cell.filterType = FilterType.DateTo
        default:
            break
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mvc = try! Dip.container.resolve() as ModalViewController
        if let cell = tableView.cellForRow(at: indexPath) as? FilterTableViewCell{
            if cell.filterType == FilterType.DateTo || cell.filterType == FilterType.DateFrom{
                let dateSelectView = EnterDateModalView()
                dateSelectView.filterType = cell.filterType
                mvc.present(modal: dateSelectView)
            }else if cell.filterType == FilterType.SeriesOnly{
                return
            }else{
                let filterDetailView = FilterDetailTableView()
                filterDetailView.fromRecognition = true
                filterDetailView.filterType = cell.filterType
                mvc.present(modal: filterDetailView)
            }
        }else{
            let sortView = SortModalView()
            sortView.list = SortType.Recognition
            mvc.present(modal: sortView)
        }
    }
}
