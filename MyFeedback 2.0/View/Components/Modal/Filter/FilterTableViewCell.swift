//
//  FilterTableViewCell.swift
//  MIA
//
//  Created by Daniel Weber on 18.10.17.
//  Copyright © 2017 zero2one. All rights reserved.
//

import UIKit

class FilterTableViewCell: UITableViewCell {
    
    var filterType: FilterType?{
        didSet{
            if filterType != nil{
                self.setFilterType(filterType!)
            }
        }
    }
    
    let service = try! Dip.container.resolve() as FeedbackService
    
    @IBOutlet weak var filterTypeLabel: UILabel!
    @IBOutlet weak var selectedValuesLabel: UILabel!
    @IBOutlet weak var toggleSwitch: UISwitch!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupView()
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setupView()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
        // Initialization code
    }
    
    private func setupView() {
        
        let view = viewFromNibForClass()
        view.frame = bounds
        
        // Auto-layout stuff.
        view.autoresizingMask = [
            UIViewAutoresizing.flexibleWidth,
            UIViewAutoresizing.flexibleHeight
        ]
        
        // Show the view.
        addSubview(view)
        
        self.selectionStyle = UITableViewCellSelectionStyle.none
    }
    
    func setFilterType(_ filterType: FilterType){
        self.filterTypeLabel.text = filterType.rawValue.localized()
        let filters = service.activeFilters.array.filter({filter in return filter.type == filterType})
        
        var selectString = ""
        toggleSwitch.isHidden = true
        selectedValuesLabel.isHidden = false
        if filters.count == 0{
            selectString = "All"
            if filterType == .SeriesOnly{
                self.toggleSwitch.isHidden = false
                if self.toggleSwitch.isOn{
                    self.toggleSwitch.setOn(false, animated: false)
                }
                selectedValuesLabel.isHidden = true
            }
        }else{
            for (index, filter) in filters.enumerated(){
                switch filterType{
                case .Category:
                    if let category = filter.value as? RequestCategory{
                        selectString += category.localName
                    }
                case .DateTo, .DateFrom:
                    let formatter = DateFormatter()
                    formatter.dateFormat = "dd.MM.yyyy"
                    if let date = filter.value as? Date{
                        selectString += formatter.string(from: date)
                    }
                case .Employee:
                    if let employee = filter.value as? Employee{
                        selectString += employee.getName()
                    }
                case .RequestType:
                    if let type = filter.value as? RequestType{
                        selectString += type.localName
                    }
                case .Series:
                    if let series = filter.value as? (id: Int?, title: String?), series.title != nil{
                        selectString += series.title!
                    }
                case .SeriesOnly:
                    if let isTrue = filter.value as? Bool, isTrue{
                        self.toggleSwitch.isHidden = false
                        if !self.toggleSwitch.isOn{
                            self.toggleSwitch.setOn(true, animated: false)
                        }
                        selectedValuesLabel.isHidden = true
                    }
                case .Competency:
                    if let competency = filter.value as? Competency{
                        selectString += competency.localName
                    }
                }
                
                if index < filters.count-1{
                    selectString += ", "
                }
            }
        }
        self.selectedValuesLabel.text = selectString.localized()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        if self.filterType == .SeriesOnly{
            return
        }
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        super.touchesBegan(touches, with: event)
        
        UIView.animate(withDuration: 0.1) {
            self.subviews[1].backgroundColor = MerckColor.VeryLightGrey
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        
        UIView.animate(withDuration: 0.1) {
            self.subviews[1].backgroundColor = UIColor.white
        }
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesCancelled(touches, with: event)
        
        UIView.animate(withDuration: 0.1) {
            self.subviews[1].backgroundColor = UIColor.white
        }
    }
    @IBAction func handleSwitch(_ sender: UISwitch) {
        if sender.isOn{
            self.addFilter()
        }else{
            self.removeFilter()
        }
    }
    
    func addFilter(){
        let filter = Filter(type: .SeriesOnly, value: true)
        if let index = self.service.activeFilters.array.index(where: {f in return f.type == .SeriesOnly}){
            self.service.activeFilters.remove(at: index)
        }
        service.activeFilters.append(filter)
        
    }
    
    func removeFilter(){
        if let index = self.service.activeFilters.array.index(where: {f in return f.type == .SeriesOnly}){
            self.service.activeFilters.remove(at: index)
        }
    }
    
}
