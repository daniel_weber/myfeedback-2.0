//
//  RecognitionFilterTableView.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 30.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import UIKit

class AnswerFilterTableView: FilterTableView{
    
   override var numberOfRows:Int{
        get{
            var initialRows = 7
            if self.service.activeFilters.array.filter({filter in return filter.type == .SeriesOnly}).count == 1{
                initialRows += 1
                return initialRows
            }else{
                return initialRows
            }
            
        }
    }
    
    override func setupView() {
        
        let view = viewFromNibForClass()
        view.frame = bounds
        
        // Auto-layout stuff.
        view.autoresizingMask = [
            UIViewAutoresizing.flexibleWidth,
            UIViewAutoresizing.flexibleHeight
        ]
        
        
        // Show the view.
        addSubview(view)
        self.titleLabel.text = "Filters".localized()
        self.clearButton.setTitle("Clear".localized(), for: .normal)
        
        let height = view.height(50*CGFloat.init(numberOfRows)+50+40)
        view.top(to: self, nil, offset: 40.0, relation: .equal, priority: .required, isActive: true)
        view.centerX(to: self)
        view.width(to: self, nil, multiplier: 1.0, offset: -40.0, relation: .equal, priority: .required, isActive: true)
        self.clearButton.height(40)
        self.clearButton.bottom(to: view, nil, offset: 0, relation: .equal, priority: .required, isActive: true)
        contentWrapper.bottomToTop(of: self.clearButton, offset: -16, relation: .equal, priority: .required, isActive: true)
        self.tableView.bottom(to: self.contentWrapper)
        
        service.activeFilters.observeNext(with: {e in
            if e.dataSource.array.filter({f in return f.type == .SeriesOnly}).count == 1 && self.tableView.numberOfRows(inSection: 0) == 7{
                self.tableView.insertRows(at: [IndexPath.init(row: 7, section: 0)], with: .top)
                
                height.constant += 50
                self.clearButton.bottom(to: view, nil, offset: 0, relation: .equal, priority: .required, isActive: true)
                UIViewPropertyAnimator(duration: 1, dampingRatio: 0.4) {
                    self.layoutIfNeeded()
                    }.startAnimation()
                
            }else if e.dataSource.array.filter({f in return f.type == .SeriesOnly}).count == 0 && self.tableView.numberOfRows(inSection: 0) == 8{
                self.tableView.deleteRows(at: [IndexPath.init(row: 7, section: 0)], with: .fade)
                
                height.constant -= 50
                UIViewPropertyAnimator(duration: 1, dampingRatio: 0.4) {
                    self.layoutIfNeeded()
                    }.startAnimation()
            }
            var indexPaths:[IndexPath] = []
            for i in 0..<self.tableView.numberOfRows(inSection: 0){
                if i != 6{
                    indexPaths.append(IndexPath.init(row: i, section: 0))
                }
            }
            self.tableView.reloadRows(at: indexPaths, with: .none)
            //self.tableView.reloadData()
        })
        
        self.contentWrapper.layer.cornerRadius = 5.0
        self.contentWrapper.layer.masksToBounds = true
        
        self.tableView.register(FilterTableViewCell.self, forCellReuseIdentifier: "FilterTableViewCell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FilterTableViewCell", for: indexPath) as! FilterTableViewCell
        switch indexPath.row{
        case 0:
            cell.filterType = FilterType.Category
        case 1:
            cell.filterType = FilterType.Competency
        case 2:
            cell.filterType = FilterType.RequestType
        case 3:
            cell.filterType = FilterType.Employee
        case 4:
            cell.filterType = FilterType.DateFrom
        case 5:
            cell.filterType = FilterType.DateTo
        case 6:
            cell.filterType = FilterType.SeriesOnly
        case 7:
            cell.filterType = FilterType.Series
        default:
            break
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let cell = tableView.cellForRow(at: indexPath) as? FilterTableViewCell{
            let mvc = try! Dip.container.resolve() as ModalViewController
            if cell.filterType == FilterType.DateTo || cell.filterType == FilterType.DateFrom{
                let dateSelectView = EnterDateModalView()
                dateSelectView.filterType = cell.filterType
                mvc.present(modal: dateSelectView)
            }else if cell.filterType == FilterType.SeriesOnly{
                return
            }else{
                let filterDetailView = FilterDetailTableView()
                filterDetailView.fromAnswer = true
                filterDetailView.filterType = cell.filterType
                mvc.present(modal: filterDetailView)
            }
        }
    }
}

