//
//  EnterDateModalView.swift
//  MIA
//
//  Created by Daniel Weber on 20.10.17.
//  Copyright © 2017 zero2one. All rights reserved.
//

import UIKit
import Localize_Swift
class SortModalView: ModalView, ModalViewResulting, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var contentWrapperView: UIView!
    @IBOutlet weak var saveButton: CallToActionButton!
    
    var title : String? {
        didSet {
            self.titleLabel.text = self.title?.localized()
        }
    }
    
    var value : SortType = SortType.Date {
        didSet {
            
        }
    }
    
    var order: SortOrder = SortOrder.ascending{
        didSet{
            
        }
    }
    
    var list:[SortType] = []{
        didSet{
            self.pickerView.reloadAllComponents()
        }
    }
    
    var result: ((SortType?, Bool) -> Bool)?
    
    var service = try! Dip.container.resolve() as FeedbackService
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    private func setupView() {
        
        let view = viewFromNibForClass()
        view.frame = bounds
        
        // Auto-layout stuff.
        view.autoresizingMask = [
            UIViewAutoresizing.flexibleWidth,
            UIViewAutoresizing.flexibleHeight
        ]
        
        // Show the view.
        addSubview(view)
        
        self.contentWrapperView.layer.cornerRadius = 5.0
        self.contentWrapperView.layer.masksToBounds = true
        pickerView.delegate = self
        pickerView.showsSelectionIndicator = true
        if let index = self.list.index(where: {sortType in return sortType.key == self.service.sortBy.value.0.key}){
            pickerView.selectRow(index, inComponent: 0, animated: false)
        }
        if self.service.sortBy.value.1.hashValue == SortOrder.ascending.hashValue{
            self.pickerView.selectRow(0, inComponent: 1, animated: false)
        }
        //self.layer.cornerRadius = 5.0
        //self.layer.masksToBounds = true
        
        self.title = "Sort".localized()
        self.saveButton.setTitle("Save".localized(), for: .normal)
       
        
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 {
            return self.list.count
        }else{
            return 2
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if component == 0 {
            return self.list[row].label.localized()
        }else{
            if row == 0{
                return "▲"
            }else{
                return "▼"
            }
        }
    }
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if component == 0 {
            self.value = self.list[row]
        }else{
            if row == 0{
                self.order = SortOrder.ascending
            }else{
                self.order = SortOrder.descending
            }
        }
    }
    
    private func goBack(){
        let filterView = FilterTableView()
        let mvc = try! Dip.container.resolve() as ModalViewController
        mvc.present(modal: filterView, edge: .top)
    }
    
    override func setConstraints(container: UIView) {
        
        self.centerX(to: container)
        self.centerY(to: container, nil, offset: -60.0, priority: .required, isActive: true)
        self.width(to: container, nil, multiplier: 1.0, offset: -40.0, relation: .equal, priority: .required, isActive: true)
    }
    
    override func didMoveToSuperview() {
        
    }
    
    override func willBeDismissed() {
        
    }
    
    @IBAction func handleCloseButton(_ sender: Any) {
        self.goBack()
    }
    
    @IBAction func handleClearButton(_ sender: CallToActionButton) {
        self.goBack()
    }
    
    @IBAction func handleSaveButton(_ sender: Any) {
        self.service.sortBy.value = (self.value, self.order)
        self.goBack()
    }
    
}
