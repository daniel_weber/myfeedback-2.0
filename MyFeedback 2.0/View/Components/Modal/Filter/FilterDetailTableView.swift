//
//  FilterDetailTableView.swift
//  MIA
//
//  Created by Daniel Weber on 18.10.17.
//  Copyright © 2017 zero2one. All rights reserved.
//

import UIKit

class FilterDetailTableView: ModalView, ModalViewResulting, UITableViewDelegate, UITableViewDataSource {
    
    
    // MARK: Outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var contentWrapper: UIView!
    @IBOutlet weak var clearButton: CallToActionButton!
    
    var filterType: FilterType?{
        didSet{
            if self.filterType != nil{
                self.titleLabel.text = filterType!.rawValue.localized()
            }
        }
    }
    var filters = [Filter]()
    
    let service = try! Dip.container.resolve() as FeedbackService
    
    var requests: [Request]{
        get{
            var reqs = [Request]()
            reqs.append(contentsOf: self.service.myRequests.array)
            return reqs
        }
    }
    
    var fromRecognition = false
    var fromAnswer = false
    
    var result: ((Any, Bool) -> Bool)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    private func setupView() {
        
        let view = viewFromNibForClass()
        view.frame = bounds
        
        // Auto-layout stuff.
        view.autoresizingMask = [
            UIViewAutoresizing.flexibleWidth,
            UIViewAutoresizing.flexibleHeight
        ]
        
        
        // Show the view.
        addSubview(view)
        
        self.contentWrapper.layer.cornerRadius = 5.0
        self.contentWrapper.layer.masksToBounds = true
        
        self.clearButton.setTitle("Clear".localized(), for: .normal)
        
        self.tableView.register(FilterDetailTableViewCell.self, forCellReuseIdentifier: "FilterDetailTableViewCell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
    }
    
    
    
    override func setConstraints(container: UIView) {
        
        self.centerX(to: container)
        self.top(to: container, nil, offset: 40.0, relation: .equal, priority: .required, isActive: true)
        //let height:CGFloat = 50*5+50
        //self.height(height)
        self.bottom(to: container)
        self.width(to: container, nil, multiplier: 1.0, offset: -40.0, relation: .equal, priority: .required, isActive: true)
    }
    
    override func didMoveToSuperview() {
        
    }
    
    override func willBeDismissed() {
        
    }
    
    @IBAction func handleCloseButton(_ sender: Any) {
        self.done?(false)
        self.result?(false,false)
    }
    
    @IBAction func handleClearButton(_ sender: Any) {
        let newFilters = service.activeFilters.array.filter({f in
            return f.type != self.filterType!
        })
        service.activeFilters.replace(with: newFilters)
        self.tableView.reloadData()
    }
    @IBAction func handleBackButton(_ sender: UIButton) {
        //self.done?(true)
        //self.result?(true,true)
        
        if fromRecognition{
            self.navigateToRecognitionFilter()
        }else if fromAnswer{
            self.navigateToAnswerFilter()
        }else{
            self.navigateBack()
        }
        
        
    }
    
    func navigateBack(){
        let filterView = FilterTableView()
        
        let mvc = try! Dip.container.resolve() as ModalViewController
        mvc.present(modal: filterView, edge: .top)
    }
    
    func navigateToRecognitionFilter(){
        let filterView = RecognitionFilterTableView()
        
        let mvc = try! Dip.container.resolve() as ModalViewController
        mvc.present(modal: filterView, edge: .top)
    }
    
    func navigateToAnswerFilter(){
        let filterView = AnswerFilterTableView()
        
        let mvc = try! Dip.container.resolve() as ModalViewController
        mvc.present(modal: filterView, edge: .top)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.filters = []
        if self.filterType != nil{
            switch self.filterType!{
            case .Category:
                for category in RequestCategory.Categories{
                    filters.append(Filter(type: .Category, value: category))
                }
            case .RequestType:
                for type in RequestType.Types{
                     filters.append(Filter(type: .RequestType, value: type))
                }
            case .Employee:
                var raters:[Employee] = []
                if fromRecognition{
                    for recognition in self.service.myRecognition.array{
                        raters.append(recognition.sender)
                    }
                }else if fromAnswer{
                    for recognition in self.service.sentRecognitions.array{
                        raters.append(contentsOf: recognition.receivers.map{$0.receiver})
                    }
                    for request in service.newRequests.array{
                        raters.append(request.requester)
                    }
                    for request in service.sentResponses.array{
                        raters.append(request.requester)
                    }
                }else{
                    for request in self.requests{
                        raters.append(contentsOf: request.responses.map{$0.rater})
                    }
                }
                
                for rater in raters{
                    if !filters.contains(where: {filter in
                        if let employee = filter.value as? Employee{
                            return employee.userId == rater.userId
                        }
                        return false
                    }){
                        filters.append(Filter(type: .Employee, value: rater))
                    }
                }
            case .Series:
                if fromAnswer{
                    for request in self.service.sentResponses.array.filter({r in return r.continuousRepetitions > 1}){
                        filters.append(Filter(type: .Series, value: (id: request.id, title:request.title)))
                    }
                }else{
                    for request in self.requests.filter({r in return r.continuousRepetitions > 1}){
                        filters.append(Filter(type: .Series, value: (id: request.id, title:request.title)))
                    }
                }
                
            case .Competency:
                for competency in Competency.Competencies{
                   filters.append(Filter(type: .Competency, value: competency))
                }
            default:
                break
            }
            return filters.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FilterDetailTableViewCell", for: indexPath) as! FilterDetailTableViewCell
        cell.filter = filters[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let cell = tableView.cellForRow(at:  indexPath) as? FilterDetailTableViewCell{
            cell.toggleState()
        }
        self.result?(true,true)
        
    }
    
}
