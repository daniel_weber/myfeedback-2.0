//
//  EnterDateModalView.swift
//  MIA
//
//  Created by Daniel Weber on 20.10.17.
//  Copyright © 2017 zero2one. All rights reserved.
//

import UIKit
import Localize_Swift
class EnterDateModalView: ModalView, ModalViewResulting {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var contentWrapperView: UIView!
    @IBOutlet weak var saveButton: CallToActionButton!
    @IBOutlet weak var clearButton: CallToActionButton!
    
    var title : String? {
        didSet {
            self.titleLabel.text = self.title?.localized()
        }
    }
    
    var value : Date? = Date() {
        didSet {
            if value != nil{
               self.datePicker.setDate(value!, animated: true)
            }
        }
    }
    
    var result: ((Date?, Bool) -> Bool)?
    
    var filterType: FilterType?{
        didSet{
            if self.filterType != nil{
                self.titleLabel.text = filterType!.rawValue.localized()
            }
        }
    }
    
    var service = try! Dip.container.resolve() as FeedbackService
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    private func setupView() {
        
        let view = viewFromNibForClass()
        view.frame = bounds
        
        // Auto-layout stuff.
        view.autoresizingMask = [
            UIViewAutoresizing.flexibleWidth,
            UIViewAutoresizing.flexibleHeight
        ]
        
        // Show the view.
        addSubview(view)
        
        self.contentWrapperView.layer.cornerRadius = 5.0
        self.contentWrapperView.layer.masksToBounds = true
        //self.layer.cornerRadius = 5.0
        //self.layer.masksToBounds = true
        
        self.title = "Select Date".localized()
        self.clearButton.setTitle("Clear".localized(), for: .normal)
        self.saveButton.setTitle("Save".localized(), for: .normal)
        self.datePicker.locale = Locale.init(identifier: Localize.currentLanguage())
        let _ = self.datePicker.reactive.date.observeNext(with: {(value) -> () in
            self.value = value
        })
        
        
    }
    
    private func getFilter()->Filter?{
        if self.filterType != nil{
            let filter = self.service.activeFilters.array.first(where: {f in
                return f.type == self.filterType!
            })
            return filter
        }
        return nil
    }
    
    private func removeFilter(){
        if let index = service.activeFilters.array.index(where: {f in
            return f.type == self.filterType!
        }){
            service.activeFilters.remove(at: index)
        }
    }
    
    private func goBack(){
        let filterView = FilterTableView()
        let mvc = try! Dip.container.resolve() as ModalViewController
        mvc.present(modal: filterView, edge: .top)
    }
    
    override func setConstraints(container: UIView) {
        
        self.centerX(to: container)
        self.centerY(to: container, nil, offset: -60.0, priority: .required, isActive: true)
        self.width(to: container, nil, multiplier: 1.0, offset: -40.0, relation: .equal, priority: .required, isActive: true)
    }
    
    override func didMoveToSuperview() {
        
    }
    
    override func willBeDismissed() {
        
    }
    
    @IBAction func handleCloseButton(_ sender: Any) {
        self.goBack()
    }
    
    @IBAction func handleClearButton(_ sender: CallToActionButton) {
        self.removeFilter()
        self.goBack()
    }
    
    @IBAction func handleSaveButton(_ sender: Any) {
        let filter = Filter(type: self.filterType!, value: self.value!)
        self.removeFilter()
        service.activeFilters.append(filter)
        self.goBack()
    }
    
}
