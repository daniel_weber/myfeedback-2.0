//
//  RequestTableViewCell.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 09.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import UIKit
import SwipeCellKit
import MIBadgeButton_Swift

class RecognitionOverviewTableViewCell: SwipeTableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var countButton: MIBadgeButton!
    
    
    
    var title: String?{
        didSet{
            self.titleLabel.text = self.title
        }
    }
    var icon: UIImage?{
        didSet{
            self.iconImageView.image = icon
        }
    }
    
    var totalCount: Int?{
        didSet{
            self.isUserInteractionEnabled = (self.totalCount ?? 0 ) > 0
            DispatchQueue.main.async {
                self.countButton.setTitle("\(self.totalCount ?? 0)", for: .normal)
            }
            
        }
    }
    
    var unreadCount: Int?{
        didSet{
            DispatchQueue.main.async {
                if self.unreadCount != nil && self.unreadCount! > 0{
                    self.countButton.badgeString = "\(self.unreadCount!)"
                }else{
                    self.countButton.badgeString = nil
                }
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupView()
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setupView()
    }
    
    private func setupView() {
        
        let view = viewFromNibForClass()
        view.frame = bounds
        
        // Auto-layout stuff.
        view.autoresizingMask = [
            UIViewAutoresizing.flexibleWidth,
            UIViewAutoresizing.flexibleHeight
        ]
        addSubview(view)
        
        self.backgroundColor = UIColor.clear
        self.iconImageView.image = self.icon
        self.titleLabel.text = self.title
        self.countButton.setTitle("\(self.totalCount ?? 0)", for: .normal)
        self.countButton.badgeString = "\(self.unreadCount ?? 0)"
        self.countButton.layer.cornerRadius = 20
        self.countButton.badgeBackgroundColor = MerckColor.VibrantMagenta
        self.countButton.badgeEdgeInsets = UIEdgeInsetsMake(5, 0, 0, 5)
        self.countButton.badgeTextColor = UIColor.white
        
        self.isUserInteractionEnabled = (self.totalCount ?? 0 ) > 0
        
        let seperator = UIView()
        seperator.backgroundColor = MerckColor.VeryLightGrey
        view.addSubview(seperator)
        seperator.leftToSuperview()
        seperator.rightToSuperview()
        seperator.bottomToSuperview()
        seperator.height(1)
        
        
        //        self.selectionStyle = UITableViewCellSelectionStyle.none
        
        // Show the view.
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
        // Initialization code
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.layer.transform = CATransform3DMakeScale(0.99, 0.99, 1.0)
        super.touchesBegan(touches, with: event)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.layer.transform = CATransform3DIdentity
        super.touchesEnded(touches, with: event)
        
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.layer.transform = CATransform3DIdentity
        super.touchesCancelled(touches, with: event)
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        if highlighted{
            self.layer.transform = CATransform3DMakeScale(0.99, 0.99, 1.0)
        }else{
            self.layer.transform = CATransform3DIdentity
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        if selected{
            self.layer.transform = CATransform3DMakeScale(0.99, 0.99, 1.0)
        }else{
            self.layer.transform = CATransform3DIdentity
        }
        
    }
    
}

