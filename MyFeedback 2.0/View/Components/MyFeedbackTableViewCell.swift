//
//  MyFeedbackTableViewCell.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 09.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import UIKit
import MIBadgeButton_Swift
import TinyConstraints

enum MyFeedbackCellType{
    case MyFeedback
    case MyRecognitions
    case MyTrophies
}

class MyFeedbackTableViewCell: UITableViewCell {

    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var amountButton: MIBadgeButton!
    
    var buttonFactor: CGFloat{
        get{
            if UIDevice.current.userInterfaceIdiom == .pad{
                return 2
            }
            return 1
        }
    }
    
    var cellType: MyFeedbackCellType?{
        didSet{
            if self.cellType != nil{
                switch self.cellType!{
                case .MyFeedback:
                    if let image = UIImage(named: "Statistic_MyFeedback_\(Language.getCurrentLanguage().key.lowercased())"){
                        self.backgroundImageView.image = image
                    }else{
                        self.backgroundImageView.image = #imageLiteral(resourceName: "Statistic_MyFeedback")
                    }
                    self.amountButton.isHidden = false
                    self.amountButton.backgroundColor = MerckColor.RichPurple
                case .MyRecognitions:
                    if let image = UIImage(named: "Statistic_MyRecognitions_\(Language.getCurrentLanguage().key.lowercased())"){
                        self.backgroundImageView.image = image
                    }else{
                        self.backgroundImageView.image = #imageLiteral(resourceName: "Statistic_MyRecognitions")
                    }
                    self.amountButton.isHidden = false
                    self.amountButton.backgroundColor = MerckColor.VibrantGreen
                case .MyTrophies:
                    if let image = UIImage(named: "Statistic_MyTrophies_\(Language.getCurrentLanguage().key.lowercased())"){
                        self.backgroundImageView.image = image
                    }else{
                        self.backgroundImageView.image = #imageLiteral(resourceName: "Statistic_MyTrophies")
                    }
                    self.amountButton.isHidden = true
                }
            }
        }
    }
    
    var amountTotal: Int?{
        didSet{
            self.amountButton.setTitle(amountTotal?.description, for: .normal)
        }
    }
    
    var amountUnread: Int?{
        didSet{
            if amountUnread != nil && amountUnread! > 0{
                self.amountButton.badgeString = amountUnread?.description
            }else{
                self.amountButton.badgeString = nil
            }
            
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupView()
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setupView()
    }
    
    private func setupView() {
        
        let view = viewFromNibForClass()
        view.frame = bounds
        
        // Auto-layout stuff.
        view.autoresizingMask = [
            UIViewAutoresizing.flexibleWidth,
            UIViewAutoresizing.flexibleHeight
        ]
        self.amountButton.badgeTextColor = UIColor.white
        self.amountButton.badgeBackgroundColor = MerckColor.VibrantMagenta
        self.amountButton.badgeAnchor = .TopRight(topOffset: 0, rightOffset: 0)
        self.amountButton.badgeEdgeInsets = UIEdgeInsetsMake(5, 0, 0, 5)
        self.amountButton.top(to: self.backgroundImageView, nil, offset: 20*buttonFactor, relation: .equal, priority: .required, isActive: true)
        self.amountButton.right(to: self.backgroundImageView, nil, offset: -20*buttonFactor, relation: .equal, priority: .required, isActive: true)
        self.amountButton.width(50, relation: .equalOrGreater, priority: .required, isActive: true)
        self.amountButton.layer.cornerRadius = self.amountButton.frame.height/2
        // Show the view.
        addSubview(view)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
        // Initialization code
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.layer.transform = CATransform3DMakeScale(0.99, 0.99, 1.0)
        super.touchesBegan(touches, with: event)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.layer.transform = CATransform3DIdentity
        super.touchesEnded(touches, with: event)
        
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.layer.transform = CATransform3DIdentity
        super.touchesCancelled(touches, with: event)
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        //super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
