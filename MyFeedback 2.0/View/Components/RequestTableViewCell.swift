//
//  RequestTableViewCell.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 09.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import UIKit
import SwipeCellKit
import SwiftDate

class RequestTableViewCell: SwipeTableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var thankYouImageView: UIImageView!
    
    var view: UIView?
    
    var name: String?{
        didSet{
            self.nameLabel.text = name
        }
    }
    var title: String?{
        didSet{
            self.titleLabel.text = self.title
        }
    }
    var date: Date?{
        didSet{
            self.dateLabel.text = self.date?.string(custom: "dd.MM.yyyy")
        }
    }
    var icon: UIImage?{
        didSet{
            self.iconImageView.image = icon
        }
    }
    var thankYouIndicator: Bool = false{
        didSet{
            self.thankYouImageView.isHidden = !thankYouIndicator
        }
    }
    
    var readFlag: Bool = true{
        didSet{
            if readFlag{
                self.view?.alpha = 0.7
            }else{
                self.view?.alpha = 1
            }
        }
    }
    
    var isLastCell: Bool = false{
        didSet{
            self.seperator?.isHidden = isLastCell
        }
    }
    
    private var seperator: UIView?
    
    var highlightedString : String? {
        
        didSet{
            
            guard let _ = self.highlightedString else {
                return
            }
            
            if let highlightedParts = self.highlightedString?.split(separator: " ") {
                
                if let title = self.title {
                    
                    let attText = NSMutableAttributedString(string: title)
                    
                    for hp in highlightedParts {
                        let range = NSString(string: attText.string.lowercased()).range(of: hp.lowercased())
                        
                        if (range.location != -1){
                            attText.addAttribute(NSAttributedStringKey.font, value: UIFont.systemFont(ofSize: 17.0, weight: UIFont.Weight.bold), range: range)
                            self.titleLabel.attributedText = attText
                        }
                    }
                }
                
                if let name = self.name {
                    
                    let attText = NSMutableAttributedString(string: name)
                    
                    for hp in highlightedParts {
                        let range = NSString(string: attText.string.lowercased()).range(of: hp.lowercased())
                        
                        if (range.location != -1){
                            attText.addAttribute(NSAttributedStringKey.font, value: UIFont.systemFont(ofSize: 17.0, weight: UIFont.Weight.heavy), range: range)
                            self.nameLabel.attributedText = attText
                        }
                    }
                }
                
            }
        }
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupView()
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setupView()
    }
    
    private func setupView() {
        
        let view = viewFromNibForClass()
        view.frame = bounds
        
        // Auto-layout stuff.
        view.autoresizingMask = [
            UIViewAutoresizing.flexibleWidth,
            UIViewAutoresizing.flexibleHeight
        ]
        self.backgroundColor = UIColor.clear
        if self.readFlag{
            view.alpha = 0.7
        }else{
            view.alpha = 1
        }
        
        //        self.selectionStyle = UITableViewCellSelectionStyle.none
        
        // Show the view.
        self.view = view
        addSubview(view)
        self.thankYouImageView.isHidden = !thankYouIndicator
        
        
        self.seperator?.removeFromSuperview()
        let seperator = UIView()
        seperator.backgroundColor = MerckColor.VeryLightGrey
        view.addSubview(seperator)
        seperator.leftToSuperview()
        seperator.rightToSuperview()
        seperator.bottomToSuperview()
        seperator.height(1)
        self.seperator = seperator
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
        // Initialization code
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.layer.transform = CATransform3DMakeScale(0.99, 0.99, 1.0)
        super.touchesBegan(touches, with: event)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.layer.transform = CATransform3DIdentity
        super.touchesEnded(touches, with: event)
        
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.layer.transform = CATransform3DIdentity
        super.touchesCancelled(touches, with: event)
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        //super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}
