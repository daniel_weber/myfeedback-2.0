//
//  RequestTableViewCell.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 09.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import UIKit
import SwipeCellKit
import SwiftDate

class RecognitionTableViewCell: SwipeTableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var thankYouImageView: UIImageView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var containerView: UIView!
    
    var view: UIView?
    
    var name: String?{
        didSet{
            self.nameLabel.text = self.name
        }
    }
    var icon: UIImage?{
        didSet{
            self.iconImageView.image = icon
        }
    }
    
    var recognitionText: String?{
        didSet{
            self.textView.text = recognitionText
        }
    }
    
    var readFlag: Bool = true{
        didSet{
            if readFlag{
                self.view?.alpha = 0.7
            }else{
                self.view?.alpha = 1
            }
        }
    }
    
    var thankYouFlag: Bool?{
        didSet{
            self.thankYouImageView.isHidden = !(thankYouFlag ?? false)
        }
    }
    
    var date: Date?{
        didSet{
            self.dateLabel.text = date?.string(custom: "dd.MM.yyyy")
        }
    }
    
    var highlightedString : String? {
        
        didSet{
            
            guard let _ = self.highlightedString else {
                return
            }
            
            if let highlightedParts = self.highlightedString?.split(separator: " ") {
                
                if let recognitionText = self.recognitionText {
                    
                    let attText = NSMutableAttributedString(string: recognitionText)
                    
                    for hp in highlightedParts {
                        let range = NSString(string: attText.string.lowercased()).range(of: hp.lowercased())
                        
                        if (range.location != -1){
                            attText.addAttribute(NSAttributedStringKey.font, value: UIFont.systemFont(ofSize: 13.0, weight: UIFont.Weight.bold), range: range)
                            self.textView.attributedText = attText
                            self.textView.textColor = UIColor.white
                        }
                    }
                }
                
                if let name = self.name {
                    
                    let attText = NSMutableAttributedString(string: name)
                    
                    for hp in highlightedParts {
                        let range = NSString(string: attText.string.lowercased()).range(of: hp.lowercased())
                        
                        if (range.location != -1){
                            attText.addAttribute(NSAttributedStringKey.font, value: UIFont.systemFont(ofSize: 17.0, weight: UIFont.Weight.bold), range: range)
                            self.nameLabel.attributedText = attText
                        }
                    }
                }
                
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupView()
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setupView()
    }
    
    private func setupView() {
        
        let view = viewFromNibForClass()
        view.frame = bounds
        
        // Auto-layout stuff.
        view.autoresizingMask = [
            UIViewAutoresizing.flexibleWidth,
            UIViewAutoresizing.flexibleHeight
        ]
        self.view = view
        addSubview(view)
        
        if self.readFlag{
            view.alpha = 0.7
        }else{
            view.alpha = 1
        }
        
        self.backgroundColor = UIColor.clear
        self.iconImageView.image = self.icon
        self.nameLabel.text = self.name
        self.textView.text = self.recognitionText
        self.iconImageView.image = self.icon
        self.thankYouImageView.isHidden = !(self.thankYouFlag ?? false)
        self.dateLabel.text = self.date?.string(custom: "dd.MM.yyyy")
        
        self.containerView.layer.borderColor = UIColor.lightText.cgColor
        self.containerView.layer.cornerRadius = 5
        self.containerView.layer.borderWidth = 0.5
        self.containerView.backgroundColor = UIColor(hexString: "#999999").withAlphaComponent(0.1)
        self.containerView.tintColor = UIColor.white
        
        self.selectionStyle = UITableViewCellSelectionStyle.none
        
        // Show the view.
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
        // Initialization code
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.layer.transform = CATransform3DMakeScale(0.99, 0.99, 1.0)
        super.touchesBegan(touches, with: event)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.layer.transform = CATransform3DIdentity
        super.touchesEnded(touches, with: event)
        
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.layer.transform = CATransform3DIdentity
        super.touchesCancelled(touches, with: event)
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        if highlighted{
            self.layer.transform = CATransform3DMakeScale(0.99, 0.99, 1.0)
        }else{
            self.layer.transform = CATransform3DIdentity
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        if selected{
            self.layer.transform = CATransform3DMakeScale(0.99, 0.99, 1.0)
            self.hero.modifiers = [.fade]
        }else{
            self.layer.transform = CATransform3DIdentity
            self.hero.modifiers = []
        }
        
    }
    
}


