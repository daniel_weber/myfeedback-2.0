//
//  LoginViewController.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 01.04.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import UIKit
import WebKit
import KeychainSwift

class LoginViewController_Old: UIViewController, UIWebViewDelegate, XMLParserDelegate {

    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var splashImageView: UIImageView!
    
    var result: ((Employee?, Bool) -> Bool)?
    
    var xmlElement = ""
    
    var muid: String? = nil
    var firstName: String? = nil
    var lastName: String? = nil
    var email: String? = nil
    
    var employee: Employee? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.webView.loadRequest(URLRequest(url: URL(string: "https://mobile-\(SAPcpms.shared.scpAccountId).hana.ondemand.com/SAMLAuthLauncher")!))
        webView.delegate = self
        splashImageView.isHidden = true
        hero.isEnabled = true
        self.navigationBar.backgroundColor = MerckColor.RichPurple
        let width = self.view!.frame.width
        let bg = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 20))
        bg.backgroundColor = MerckColor.RichPurple
        self.view.addSubview(bg)
        bg.topToSuperview()
        navigationBar.topToBottom(of: bg)
        self.webView.topToBottom(of: navigationBar)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: WebView Delegate
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        if let samlResponse = self.webView?.stringByEvaluatingJavaScript(from: "document.getElementsByName(\"SAMLResponse\")[0].value"){
            if !samlResponse.isEmpty{
                self.getSamlResponsefromHTML(encodedXML: samlResponse)
            }
        }
        if webView.stringByEvaluatingJavaScript(from: "document.documentElement.outerHTML") == "<html><head></head><body></body></html>" {
            self.authSuccess()
        }
        if webView.stringByEvaluatingJavaScript(from: "document.documentElement.outerHTML")!.contains("kmsiInput") || webView.stringByEvaluatingJavaScript(from: "document.documentElement.outerHTML")!.contains("userNameInput"){
            UIView.animate(withDuration: 0.2, animations: {self.splashImageView.alpha = 0})
        }
        if webView.stringByEvaluatingJavaScript(from: "document.documentElement.outerHTML")!.contains("hiddenform") {
            Loading.show()
        }else{
            Loading.hide()
        }
    }
    
    // MARK: XMLParser Delegate
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        if elementName == "NameID" {
            xmlElement = elementName
        }else if let kvp = attributeDict.first(where: {(k,v) in return k == "Name" && v == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/givenname"}){
            xmlElement = "firstName"
        }else if let kvp = attributeDict.first(where: {(k,v) in return k == "Name" && v == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/surname"}){
            xmlElement = "lastName"
        }else if let kvp = attributeDict.first(where: {(k,v) in return k == "Name" && v == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress"}){
            xmlElement = "email"
        }else if xmlElement == "firstName" || xmlElement == "lastName" || xmlElement == "email"{
            xmlElement = xmlElement+elementName
        }else{
            xmlElement = ""
        }
        
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        if xmlElement == "NameID"{
            KeychainSwift().set(string, forKey: "userId")
            self.muid = string
        }else if xmlElement == "firstNameAttributeValue"{
            KeychainSwift().set(string, forKey: "firstName")
            self.firstName = string
        }else if xmlElement == "lastNameAttributeValue"{
            self.lastName = string
            KeychainSwift().set(string, forKey: "lastName")
        }else if xmlElement == "emailAttributeValue"{
            self.email = string
            KeychainSwift().set(string, forKey: "email")
        }
        
        if(self.muid != nil && self.firstName != nil && self.lastName != nil && self.email != nil){
            self.employee = Employee(userId: muid!, muid: muid!, firstName: firstName!, lastName: lastName!, email: email!, orgUnitName: "")
            loggedInUser.value = employee
        }
    }
    
    func getSamlResponsefromHTML(encodedXML:String){
        if let data = Data(base64Encoded: encodedXML){
            let parser = XMLParser(data: data)
            parser.delegate = self
            parser.parse()
        }
    }
    
    // MARK: Actions
    
    @IBAction func handleBackButton(_ sender: UIBarButtonItem) {
        if webView.canGoBack{
            webView.goBack()
        }
    }
    
    func authSuccess(){
        if let r = self.result{
            if let muid = KeychainSwift().get("userId"){
                if self.employee == nil{
                    self.employee = Employee(userId: muid, muid: muid, firstName: KeychainSwift().get("firstName") ?? "", lastName: KeychainSwift().get("lastName") ?? "", email: KeychainSwift().get("email") ?? "", orgUnitName: "")
                }
            }
            r(self.employee, true)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
