//
//  AnswerRequestViewController.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 19.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import UIKit
import SwiftDate
import Bond
import Hydra
import SwipeCellKit

class AnswerRequestViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, SwipeTableViewCellDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backgroundImage: UIImageView!
    
    var sections: [String] = []
    
    var filteredItems: MutableObservableArray<Searchable> = MutableObservableArray([])
    
    let service = try! Dip.container.resolve() as FeedbackService
    
    var searchTerm : Observable<String?> = Observable("")
    var searchToken : InvalidationToken?
    
    var searchPromise : Promise<[Searchable]>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(RequestTableViewCell.self, forCellReuseIdentifier: "RequestTableViewCell")
        //self.tableView.contentInset = UIEdgeInsetsMake(40, 0, 0, 0)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        hero.isEnabled = true
        self.tableView.hero.modifiers = [.translate(y:1000)]
        self.backgroundImage.hero.modifiers = [.fade]
        self.view.backgroundColor = MerckColor.RichPurple
        self.tableView.refreshControl = UIRefreshControl()
        self.tableView.refreshControl?.addTarget(self, action: #selector(self.handleRefresh), for: .valueChanged)
        
        let searchBar = try! Dip.container.resolve() as BaseSearchBar
        searchBar.searchTextField.reactive.text.bidirectionalBind(to: self.searchTerm)
        
        // Do any additional setup after loading the view.
        self.setupData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.setNavigationBar()
        
        DispatchQueue.main.async {
            self.setupData()
            self.tableView.reloadData()
        }
    }
    
    func setNavigationBar(){
        let navigationBar = try! Dip.container.resolve() as AlternativeNavigationBar
        let filterIcon: UIImage
        if self.service.activeFilters.array.count > 0{
            filterIcon = #imageLiteral(resourceName: "Icon_Funnel_Filled")
        }else{
            filterIcon = #imageLiteral(resourceName: "Icon_Funnel")
        }
        let filterOptions = BarOption(title: "Filter", icon: filterIcon) { () -> Void in
            let mvc = try! Dip.container.resolve() as ModalViewController
            
            let filterMV = RecognitionFilterTableView()
            
            mvc.present(modal: filterMV, edge: .bottom)
        }
        
        let searchOptions = BarOption(title: "Search", icon: #imageLiteral(resourceName: "Icon_Search")) { () -> Void in
            let searchBar = try! Dip.container.resolve() as BaseSearchBar
            searchBar.isCollapsed = false
        }
        navigationBar.update(title: "Answer Request".localized(), options: [filterOptions, searchOptions])
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var sections = 0
        self.sections = []
        if service.newRequestsFiltered.count > 0{
            sections += 1
            self.sections.append("newRequests")
        }
        if service.sentResponsesFiltered.count > 0{
            sections += 1
            self.sections.append("sentResponses")
        }
        if service.sentRecognitionsFiltered.count > 0{
            sections += 1
            self.sections.append("sentRecognitions")
        }
        
        return sections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch self.sections[section]{
        case "newRequests":
            return service.newRequestsFiltered.filter({r in return self.filteredItems.array.contains(where: {s in return s.id == r.id})}).count
        case "sentResponses":
            return service.sentResponsesFiltered.filter({r in return self.filteredItems.array.contains(where: {s in return s.id == r.id})}).count
        case "sentRecognitions":
            return service.sentRecognitionsFiltered.filter({r in return self.filteredItems.array.contains(where: {s in return s.id == r.id})}).count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch self.sections[section]{
        case "newRequests":
            return "New Requests".localized()
        case "sentResponses":
            return "Feedback sent".localized()
        case "sentRecognitions":
            return "Recognitions sent".localized()
        default:
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = UIColor.white
        header.backgroundColor = MerckColor.VibrantGreen
        header.backgroundView?.backgroundColor = MerckColor.VibrantGreen
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 92
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RequestTableViewCell", for: indexPath) as! RequestTableViewCell
        cell.backgroundColor = UIColor.clear
        let suffix: String
        switch self.sections[indexPath.section]{
        case "newRequests":
            let request = service.newRequestsFiltered.filter({r in return self.filteredItems.array.contains(where: {s in return s.id == r.id})})[indexPath.row]
            cell.title = request.overviewTitle
            cell.name = request.requester.getName()
            cell.date = request.date
            suffix = "_outline"
            switch request.type?.key ?? ""{
            case RequestType.Smileys.key:
                cell.icon = UIImage(named: "icon_smiley"+suffix)
            case RequestType.Stars.key:
                cell.icon = UIImage(named: "icon_star"+suffix)
            case RequestType.YesNo.key:
                cell.icon = UIImage(named: "icon_checkmark"+suffix)
            default:
                cell.icon = UIImage()
            }
            cell.readFlag = false
            cell.delegate = self
        case "sentResponses":
            let request = service.sentResponsesFiltered.filter({r in return self.filteredItems.array.contains(where: {s in return s.id == r.id})})[indexPath.row]
            cell.title = request.overviewTitle
            cell.name = request.requester.getName()
            cell.date = request.date
            suffix = "_filled"
            switch request.type?.key ?? ""{
            case RequestType.Smileys.key:
                cell.icon = UIImage(named: "icon_smiley"+suffix)
            case RequestType.Stars.key:
                cell.icon = UIImage(named: "icon_star"+suffix)
            case RequestType.YesNo.key:
                cell.icon = UIImage(named: "icon_checkmark"+suffix)
            default:
                cell.icon = UIImage()
            }
            cell.readFlag = true
        case "sentRecognitions":
            let recognition = service.sentRecognitionsFiltered.filter({r in return self.filteredItems.array.contains(where: {s in return s.id == r.id})})[indexPath.row]
            cell.title = recognition.competency.localName
            cell.name = recognition.receivers.map{$0.receiver.getName()}.joined(separator: ", ")
            cell.date = recognition.date
            cell.icon = #imageLiteral(resourceName: "icon_recognition_filled")
            cell.readFlag = true
        default:
            break
        }
        
        cell.highlightedString = self.searchTerm.value
        cell.isLastCell = indexPath.row == (tableView.numberOfRows(inSection: indexPath.section) - 1)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let navigationcontroller = try! Dip.container.resolve() as BaseNavigationController
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc: UIViewController
        
        switch sections[indexPath.section]{
            case "newRequests":
                let request = self.service.newRequestsFiltered.filter({r in return self.filteredItems.array.contains(where: {s in return s.id == r.id})})[indexPath.row]
                vc = storyboard.instantiateViewController(withIdentifier: "RequestViewController") as! RequestViewController
                (vc as! RequestViewController).request = request
                (vc as! RequestViewController).response = request.responses.first
            case "sentResponses":
                let request = self.service.sentResponsesFiltered.filter({r in return self.filteredItems.array.contains(where: {s in return s.id == r.id})})[indexPath.row]
                vc = storyboard.instantiateViewController(withIdentifier: "RequestViewController") as! RequestViewController
                (vc as! RequestViewController).request = request
                (vc as! RequestViewController).response = request.responses.first
            case "sentRecognitions":
                let recognition = self.service.sentRecognitionsFiltered.filter({r in return self.filteredItems.array.contains(where: {s in return s.id == r.id})})[indexPath.row]
                vc = storyboard.instantiateViewController(withIdentifier: "RecognitionCardViewController") as! RecognitionCardViewController
                (vc as! RecognitionCardViewController).recognition.value = recognition
                (vc as! RecognitionCardViewController).isInEditMode = false
            default:
                return
        }
        navigationcontroller.pushViewController(vc, animated: true)
    }
    
    // MARK: SwipeTableViewCellDelegate
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        let request = self.service.newRequestsFiltered.filter({r in return self.filteredItems.array.contains(where: {s in return s.id == r.id})})[indexPath.row]
        guard request.responses.first != nil else{
            return nil
        }
        let cell = self.tableView.cellForRow(at: indexPath) as! SwipeTableViewCell
        let dismissAction = SwipeAction(style: .default, title: "Dismiss".localized()) { action, indexPath in
            let alertMV = AlertModalView.show(title: nil, message: "Dismiss".localized()+"?", enableCancel: true)
            alertMV.close = {success in
                if success{
                    cell.delegate = nil
                    self.tableView.beginUpdates()
                    self.service.dismissRequest(request: request, requestResponse: request.responses.first!).then({response in
                        action.fulfill(with: .delete)
                        if tableView.numberOfRows(inSection: indexPath.section) == 1{
                            tableView.deleteSections(IndexSet.init(integer: indexPath.section), with: .automatic)
                        }
                        self.tableView.endUpdates()
                        if response.message != nil{
                            AlertModalView.show(title: "Success".localized(), message: response.message!)
                        }
                    }).catch({e in
                        print("ERROR")
                        print(e)
                        ErrorView.show(error: e)
                    })
                }else{
                    action.fulfill(with: .reset)
                }
            }
            
        }
        
        // customize the action appearance
        
        dismissAction.textColor = UIColor.white
        dismissAction.backgroundColor = MerckColor.RichRed
        
        
        if orientation == .right{
            return [dismissAction]
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeTableOptions {
        var options = SwipeTableOptions()
        options.expansionStyle = .destructive(automaticallyDelete: false)
        options.transitionStyle = .drag
        options.backgroundColor = UIColor.clear
        return options
    }
    
    @objc func handleRefresh(){
        self.tableView.refreshControl?.endRefreshing()
        Loading.show()
        self.service.updateNewRequests().then({_ in
            self.service.updateSentResponses().then({_ in
                self.service.updateSentRecognitions().then({_ in
                    self.setupData()
                    self.tableView.reloadData()
                    Loading.hide()
                })
            })
        })
    }
    
    private func filter(items: [Searchable], by term: String?) -> Promise<[Searchable]> {
        
        self.searchToken?.invalidate()
        self.searchToken = InvalidationToken()
        
        return Promise<[Searchable]>(in: Context.background, token: self.searchToken, { (resolve, reject, operation) in
            
            
            var filtered : [Searchable] = []
            
            for item in items {
                
                if operation.isCancelled {
                    break
                }
                
                if (term == nil || term! == ""){
                    if self.service.activeFilters.array.count == 0{
                        continue
                    }else{
                        filtered.append(item)
                        continue
                    }
                    
                }
                
                if self.considerAND(term: term, item: item) {filtered.append(item)}
            }
            
            
            if (operation.isCancelled){
                operation.cancel()
                return
            }
            
            if (filtered.count == 0 && (term == nil || term! == "") && self.service.activeFilters.count == 0){
                filtered.append(contentsOf: items)
            }
            
            if (filtered.count == 0){
                let empty = Recognition.invalid()
                filtered.append(empty)
            }
            
            
            
            resolve(filtered)
        })
    }
    
    func considerAND(term : String?, item: Searchable) -> Bool {
        
        if let searchTerms = term?.split(separator: " ") {
            
            for t in searchTerms {
                
                if item.searchTerm.contains(t.lowercased()) { continue }
                
                // When there is no hit for one item it's out (AND)
                return false
            }
            
            // When every item in the loop had a hit, this search term fits (AND)
            return true
        }
        
        return false
    }
    
    func setupData(){
        var items: [Searchable] = []
        items.append(contentsOf: self.service.newRequestsFiltered as! [Searchable])
        items.append(contentsOf: self.service.sentRecognitionsFiltered as! [Searchable])
        items.append(contentsOf: self.service.sentResponsesFiltered as! [Searchable])
        
        self.filteredItems.replace(with: items)
        
        self.searchTerm.observeNext(with: {term in
            var items: [Searchable] = []
            items.append(contentsOf: self.service.newRequestsFiltered as! [Searchable])
            items.append(contentsOf: self.service.sentRecognitionsFiltered as! [Searchable])
            items.append(contentsOf: self.service.sentResponsesFiltered as! [Searchable])
            
            
            self.tableView.layer.opacity = 0.4
            self.tableView.isUserInteractionEnabled = false
            
            self.searchPromise = self.filter(items: items, by: term).then(in: Context.main, { (itms) in
                
                self.filteredItems.replace(with: itms)
                
                self.tableView.layer.opacity = 1.0
                self.tableView.isUserInteractionEnabled = true
                
                self.searchPromise = nil
                
            })
        })
        
        self.service.activeFilters.observeNext(with: {_ in
            self.setNavigationBar()
            if self.searchTerm.value != nil{
                self.searchTerm.value! += ""
            }else{
                self.searchTerm.value = ""
            }
            
        })
        
        self.filteredItems.observeNext(with: {_ in
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        })
        
        self.tableView.layer.opacity = 1.0
    }
        
}

