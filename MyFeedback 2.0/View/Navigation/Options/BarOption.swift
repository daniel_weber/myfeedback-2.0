//
//  BarOption.swift
//  CostCenters
//
//  Created by Marco Koeppel on 12.09.17.
//  Copyright © 2017 Merck KGaA. All rights reserved.
//

import Foundation
import UIKit

class BarOption {

    var action : () -> Void?
    var title: String
    var icon: UIImage?

    init(title: String, icon: UIImage?, action: @escaping () -> Void?) {

        self.title = title
        self.icon = icon
        self.action = action
    }
}
