import UIKit

class BaseSearchBar: UIView {
    
    // MARK: Outlets
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var cancelButton: UIButton!
    
    var isCollapsed: Bool = false {
        
        didSet {
            
            self.searchTextField.placeholder = "Search...".localized()
            if (oldValue == self.isCollapsed) {
                return
            }
            
            UIView.animate(withDuration: 0.3, delay: 0.0, options: [.curveEaseOut], animations: {
                
                //self.transform = CGAffineTransform(translationX: 0.0, y: self.isCollapsed ? -60.0 : -1.0)
                self.alpha = self.isCollapsed ? 0.0 : 1.0
                
            }) { (_) in
                
                self.isUserInteractionEnabled = !self.isCollapsed
                if !self.isCollapsed{
                    self.searchTextField.becomeFirstResponder()
                }else{
                    self.searchTextField.resignFirstResponder()
                }
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    // MARK: - Private Helper Methods
    
    // Performs the initial setup.
    private func setupView() {
        let view = viewFromNibForClass()
        view.frame = bounds
        
        // Auto-layout stuff.
        view.autoresizingMask = [
            UIViewAutoresizing.flexibleWidth,
            UIViewAutoresizing.flexibleHeight
        ]
        
        // Show the view.
        //self.cancelButton.setTitle("Cancel".localized(), for: .normal)
        self.searchTextField.placeholder = "Search...".localized()
        view.backgroundColor = MerckColor.VibrantGreen
        addSubview(view)
        
    }
    
    
    @IBAction func handleCancelButton(_ sender: UIButton) {
        self.searchTextField.text = nil
        self.searchTextField.endEditing(true)
        self.isCollapsed = true
        let navigationBar = try! Dip.container.resolve() as AlternativeNavigationBar
        navigationBar.isCollapsed = false
    }
    
}
