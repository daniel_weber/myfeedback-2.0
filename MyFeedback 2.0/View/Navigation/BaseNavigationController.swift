//
//  BaseNavigationController.swift
//  CostCenters
//
//  Created by Marco Koeppel on 31.07.17.
//  Copyright © 2017 Merck KGaA. All rights reserved.
//

import UIKit
import Bond
import Hero
import TinyConstraints

class BaseNavigationController: UINavigationController, UIGestureRecognizerDelegate {

    static let defaultTransitionDuration: Double = 1.02

    override func viewDidLoad() {
        super.viewDidLoad()
        Dip.registerBaseNavigationController(navigationController: self)
        
        self.interactivePopGestureRecognizer?.delegate = nil

        Hero.shared.containerColor = MerckColor.RichPurple

        self.isHeroEnabled = true
        self.heroNavigationAnimationType = .selectBy(presenting: .none, dismissing: .none)
        self.view.backgroundColor = MerckColor.VibrantGreen
//        let bgv = UIImageView(frame: self.view.bounds)
//        bgv.image = #imageLiteral(resourceName: "Background")
//        
//        self.view.insertSubview(bgv, at: 0)
//        bgv.edges(to: self.view)

        // Do any additional setup after loading the view.
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func popViewController(animated: Bool) -> UIViewController? {
        let r = super.popViewController(animated: animated)
        return r
    }

    override func popToRootViewController(animated: Bool) -> [UIViewController]? {
        let r = super.popToRootViewController(animated: animated)
        return r
    }

    override func popToViewController(_ viewController: UIViewController, animated: Bool) -> [UIViewController]? {
        let r = super.popToViewController(viewController, animated: animated)
        return r
    }

    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        super.pushViewController(viewController, animated: animated)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
