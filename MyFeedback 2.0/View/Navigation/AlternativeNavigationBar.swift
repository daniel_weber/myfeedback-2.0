//
//  BaseNavigationBar.swift
//  CostCenters
//
//  Created by Marco Koeppel on 01.08.17.
//  Copyright © 2017 Merck KGaA. All rights reserved.
//

import UIKit
import Bond
import MessageUI
import TinyConstraints

class AlternativeNavigationBar: UIView, MFMailComposeViewControllerDelegate {
    
    @IBInspectable var navigationController: UINavigationController?
    
    private var title: Observable<String> = Observable("")
    private var backButtonHidden: Observable<Bool> = Observable(true)
    private var optionsButtonsHidden: Observable<Bool> = Observable(true)
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var optionsStackView: UIStackView!
    
    var isCollapsed: Bool = false {
        
        didSet {
            
            if (oldValue == self.isCollapsed) {
                return
            }
            
            UIView.animate(withDuration: 0.3, delay: 0.0, options: [.curveEaseOut], animations: {
                
                self.transform = CGAffineTransform(translationX: 0.0, y: self.isCollapsed ? -40.0 : 0.0)
                self.alpha = self.isCollapsed ? 0.0 : 1.0
                
            }) { (_) in
                
                self.isUserInteractionEnabled = !self.isCollapsed
            }
        }
    }
    
    var options: [BarOption]? {
        
        didSet {
            self.optionsButtonsHidden.value = self.options == nil || self.options!.count < 0
            self.setButtons()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    // MARK: - Private Helper Methods
    
    // Performs the initial setup.
    private func setupView() {
        let view = viewFromNibForClass()
        view.frame = bounds
        
        // Auto-layout stuff.
        view.autoresizingMask = [
            UIViewAutoresizing.flexibleWidth,
            UIViewAutoresizing.flexibleHeight
        ]
        
        view.backgroundColor = MerckColor.VibrantGreen
        // Show the view.
        addSubview(view)
        //self.titleLabel.edges(to: view)
        
        self.title.bind(to: self.titleLabel)
        
        let backImage = UIImage(named: "Button_Back")?.withRenderingMode(.alwaysTemplate)
        self.backButton.setImage(backImage, for: .normal)
        self.backButtonHidden.bind(to: self.backButton.reactive.isHidden)
        self.backButtonHidden.map {!$0}.bind(to: self.backButton.reactive.isEnabled)
        
        self.optionsButtonsHidden.bind(to: self.optionsStackView.reactive.isHidden)
        
    }
    
    @IBAction func handleBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func handleHelp(_ sender: Any) {
        
        
    }
    
    func setButtons(){
        for view in self.optionsStackView.arrangedSubviews{
            view.removeFromSuperview()
        }
        if self.options != nil{
            for option in self.options!{
                let button = UIButton(frame: CGRect(x: 0, y: 0, width: 42, height: 42))
                button.tintColor = UIColor.white
                button.setImage(option.icon?.withRenderingMode(.alwaysTemplate), for: .normal)
                button.addTarget(self, action: #selector(self.handleOption(sender:)), for: .touchUpInside)
                self.optionsStackView.addArrangedSubview(button)
            }
        }
        
    }
    
    @objc func handleOption(sender: UIButton){
        if let index = self.optionsStackView.arrangedSubviews.index(where: {view in return (view as? UIButton) === sender}){
            self.options![index].action()
        }
    }
    
    @IBAction func handleOptions(_ sender: Any) {
        
        let omv = OptionsModalView()
        omv.setOptions(options: self.options!)
        
        let mvc: ModalViewController = try! Dip.container.resolve() as ModalViewController
        mvc.present(modal: omv, edge: .top)
    }
    
    func update(title: String?, options: [BarOption]?) {
        
        self.update(title: title)
        self.options = options
    }
    
    func update(title: String?) {
        
        self.title.value = title != nil ? title! : ""
        self.backButtonHidden.value = self.navigationController != nil ? self.navigationController!.viewControllers.count <= 1 : true
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}

