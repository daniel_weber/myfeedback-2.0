//
//  BaseNavigationBar.swift
//  CostCenters
//
//  Created by Marco Koeppel on 01.08.17.
//  Copyright © 2017 Merck KGaA. All rights reserved.
//

import UIKit
import Bond
import MessageUI

class BaseNavigationBar: UIView, MFMailComposeViewControllerDelegate {

    @IBInspectable var navigationController: UINavigationController?

    private var title: Observable<String> = Observable("")
    private var backButtonHidden: Observable<Bool> = Observable(true)
    private var optionsButtonHidden: Observable<Bool> = Observable(true)

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var optionsButton: UIButton!

    var isCollapsed: Bool = false {

        didSet {

            if (oldValue == self.isCollapsed) {
                return
            }

            UIView.animate(withDuration: 0.3, delay: 0.0, options: [.curveEaseOut], animations: {

                self.transform = CGAffineTransform(translationX: 0.0, y: self.isCollapsed ? -40.0 : 0.0)
                self.alpha = self.isCollapsed ? 0.0 : 1.0

            }) { (_) in

                self.isUserInteractionEnabled = !self.isCollapsed
            }
        }
    }

    var options: [BarOption]? {

        didSet {
            self.optionsButtonHidden.value = self.options == nil || self.options!.count < 0
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }

    // MARK: - Private Helper Methods

    // Performs the initial setup.
    private func setupView() {
        let view = viewFromNibForClass()
        view.frame = bounds

        // Auto-layout stuff.
        view.autoresizingMask = [
            UIViewAutoresizing.flexibleWidth,
            UIViewAutoresizing.flexibleHeight
        ]

        // Show the view.
        addSubview(view)

        self.title.bind(to: self.titleLabel)
        
        let backImage = UIImage(named: "Button_Back")?.withRenderingMode(.alwaysTemplate)
        self.backButton.setImage(backImage, for: .normal)
        //self.backButton.tintColor = MerckColor.VibrantGreen
        self.backButtonHidden.bind(to: self.backButton.reactive.isHidden)
        self.backButtonHidden.map {!$0}.bind(to: self.backButton.reactive.isEnabled)
        
        let optionsImage = UIImage(named: "Button_Options")?.withRenderingMode(.alwaysTemplate)
        self.optionsButton.setImage(optionsImage, for: .normal)
        self.optionsButton.setTitleColor(MerckColor.RichPurple, for: .selected)
        //self.optionsButton.tintColor = MerckColor.VibrantGreen
        self.optionsButtonHidden.bind(to: self.optionsButton.reactive.isHidden)
        self.optionsButtonHidden.map {!$0}.bind(to: self.optionsButton.reactive.isEnabled)
        
        let helpButton = UIButton(frame: self.backButton.frame)
        let buttonImage = UIImage(named: "ticket-3")?.withRenderingMode(.alwaysTemplate)
        helpButton.setImage(buttonImage, for: .normal)
        helpButton.tintColor = UIColor.white
        helpButton.addTarget(self, action: #selector(self.handleHelp(_:)), for: .touchUpInside)
        
        self.addSubview(helpButton)
        helpButton.edges(to: self.backButton)
        
        self.backButtonHidden.bind(to: helpButton.reactive.isEnabled)
        self.backButtonHidden.map {!$0}.bind(to: helpButton.reactive.isHidden)
        
    }

    @IBAction func handleBack(_ sender: Any) {

        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func handleHelp(_ sender: Any) {
       
        
    }

    @IBAction func handleOptions(_ sender: Any) {

        let omv = OptionsModalView()
        omv.setOptions(options: self.options!)

        let mvc: ModalViewController = try! Dip.container.resolve() as ModalViewController
        mvc.present(modal: omv, edge: .top)
    }

    func update(title: String?, options: [BarOption]?) {

        self.update(title: title)
        self.options = options
    }

    func update(title: String?) {

        self.title.value = title != nil ? title! : ""
        self.backButtonHidden.value = self.navigationController != nil ? self.navigationController!.viewControllers.count <= 1 : true
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}
