//
//  BaseView.swift
//  CostCenters
//
//  Created by Marco Koeppel on 25.09.17.
//  Copyright © 2017 Merck KGaA. All rights reserved.
//

import UIKit

class BaseView: UIView {

    override func didMoveToWindow() {
        print("window")
        if activityViewControllerWindow != nil{
            if activityViewControllerWindow?.rootViewController == nil{
                activityViewControllerWindow = nil
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.redirect2MainVC()
            }else{
                activityViewControllerWindow?.rootViewController = nil
            }
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }

    override func alignmentRect(forFrame frame: CGRect) -> CGRect {
        return frame
    }
}
