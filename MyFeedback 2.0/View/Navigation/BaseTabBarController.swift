//
//  BaseTabBarController.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 08.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import UIKit

class BaseTabBarController: UITabBarController, UITabBarControllerDelegate {
    
    let service = try! Dip.container.resolve() as FeedbackService

    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        
        self.tabBar.barTintColor = MerckColor.VibrantGreen
        self.tabBar.tintColor = MerckColor.RichPurple
        let frost = UIVisualEffectView(frame: self.tabBar.bounds)
        frost.effect = UIBlurEffect(style: .regular)
        frost.alpha = 0.5
        self.tabBar.insertSubview(frost, at: 0)
        self.tabBar.isTranslucent = true
        let myFeedbackItem = self.tabBar.items![1]
        
        self.service.userSettings.observeNext(with: {_ in
            self.setTitles()
        })
        
        myFeedbackItem.badgeColor = MerckColor.VibrantMagenta
        
        if service.amountUnreadTotal.value > 0{
            myFeedbackItem.badgeColor = MerckColor.VibrantMagenta
            myFeedbackItem.badgeValue = "\(service.amountUnreadTotal.value)"
        }else{
            myFeedbackItem.badgeValue = nil
        }
        
        service.amountUnreadTotal.observeNext(with: {value in
            DispatchQueue.main.async {
                if value > 0{
                    myFeedbackItem.badgeColor = MerckColor.VibrantMagenta
                    myFeedbackItem.badgeValue = "\(value)"
                }else{
                    myFeedbackItem.badgeValue = nil
                }
            }
        })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        hero.isEnabled = true
        self.tabBar.hero.modifiers = [.translate(x: 0, y: 100, z: 0)]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        hero.isEnabled = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: TabView Delegate
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        let tabViewControllers = tabBarController.viewControllers!
        guard let toIndex = tabViewControllers.index(of: viewController) else {
            return false
        }
        
        // Our method
        animateToTab(toIndex: toIndex)
        return true
    }
    
    func animateToTab(toIndex: Int) {
        let tabViewControllers = viewControllers!
        let fromView = selectedViewController!.view
        let toView: UIView?
        if toIndex == 1, let nav = tabViewControllers[toIndex] as? UINavigationController{
            nav.popToRootViewController(animated: false)
            toView = nav.view
        }else{
            toView = tabViewControllers[toIndex].view
        }
        let fromIndex = tabViewControllers.index(of: selectedViewController!)
        
        guard fromIndex != toIndex else {return}
        
        // Add the toView to the tab bar view
        fromView?.superview!.addSubview(toView!)
        
        // Position toView off screen (to the left/right of fromView)
        let screenWidth = UIScreen.main.bounds.size.width;
        let scrollRight = toIndex > fromIndex!;
        let offset = (scrollRight ? screenWidth : -screenWidth)
        toView?.center = CGPoint(x: fromView!.center.x + offset, y: toView!.center.y)
        
        // Disable interaction during animation
        view.isUserInteractionEnabled = false
        
        UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            
            // Slide the views by -offset
            fromView?.center = CGPoint(x: fromView!.center.x - offset, y: fromView!.center.y);
            toView?.center   = CGPoint(x: toView!.center.x - offset, y: toView!.center.y);
            
        }, completion: { finished in
            
            // Remove the old view from the tabbar view.
            fromView?.removeFromSuperview()
            self.selectedIndex = toIndex
            self.view.isUserInteractionEnabled = true
        })
    }
    
    func setTitles(){
        self.tabBar.items![0].title = "Home".localized()
        self.tabBar.items![1].title = "MyFeedback".localized()
        self.tabBar.items![2].title = "Settings".localized()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
