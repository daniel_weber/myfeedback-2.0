//
//  TrophiesViewController.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 16.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import UIKit

class TrophiesViewController: UIViewController {

    @IBOutlet weak var backgroundImage: UIImageView!
    
    let service = try! Dip.container.resolve() as FeedbackService
    
    var factorX: CGFloat = 1
    var factorY: CGFloat = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hero.isEnabled = true
        self.backgroundImage.hero.modifiers = [.translate(y:1000)]
        self.view.backgroundColor = MerckColor.VibrantMagenta
        self.service.AnalyticsOpenViewTrophyOverview()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let navigationBar = try! Dip.container.resolve() as AlternativeNavigationBar
        navigationBar.update(title: "MyTrophies".localized(), options:nil)
        
        factorX =  backgroundImage.bounds.size.width / (backgroundImage.image?.size.width ?? 1)
        factorY = backgroundImage.bounds.size.height / (backgroundImage.image?.size.height ?? 1)
        
        self.setupTrophies()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupTrophies(){
        setupExplorer()
        setupAllStar()
        setupDevelopmentDriver()
        setupFeedbackGuru()
        setupRecognitionHero()
        setupCompetencyChampion()
    }
    
    func setupExplorer(){
        let position: CGPoint
        if Device.IS_IPAD{
            position = CGPoint(x: 566, y: 254)
        }else{
            position = CGPoint(x: 368, y: 266)
        }
        self.addTrophy(trophy: Trophy.ExplorerLevel0, position: position)
    }
    
    func setupAllStar(){
        let position: CGPoint
        if Device.IS_IPAD{
            position = CGPoint(x: 760, y: 353)
        }else{
            position = CGPoint(x: 529, y: 343)
        }
        self.addTrophy(trophy: Trophy.AllStarLevel0, position: position)
    }
    
    func setupDevelopmentDriver(){
        let position: CGPoint
        if Device.IS_IPAD{
            position = CGPoint(x: 92, y: 260)
        }else{
            position = CGPoint(x: 33, y: 300)
        }
        self.addTrophy(trophy: Trophy.DevelopmentDriverLevel0, position: position)
    }
    
    func setupFeedbackGuru(){
        let position: CGPoint
        if Device.IS_IPAD{
            position = CGPoint(x: 322, y: 641)
        }else{
            position = CGPoint(x: 173, y: 586)
        }
        self.addTrophy(trophy: Trophy.FeedbackGuruLevel0, position: position)
    }
    
    func setupRecognitionHero(){
        let position: CGPoint
        if Device.IS_IPAD{
            position = CGPoint(x: 48, y: 991)
        }else{
            position = CGPoint(x: 51, y: 880)
        }
        self.addTrophy(trophy: Trophy.RecognitionHeroLevel0, position: position)
    }
    
    func setupCompetencyChampion(){
        let position: CGPoint
        if Device.IS_IPAD{
            position = CGPoint(x: 771, y: 784)
        }else{
            position = CGPoint(x: 541, y: 708)
        }
        self.addTrophy(trophy: Trophy.CompetencyChampionLevel0, position: position)
    }
    
    func addTrophy(trophy: Trophy, position: CGPoint){
        let imageView = TrophyOverviewImageView()
        let t: Trophy
        if let trophyReceived = self.service.myTrophies.array.first(where: {a in return a.name == trophy.name}){
            t = trophyReceived
            imageView.image = t.overViewImage
        }else{
            t = trophy
            imageView.image = t.overViewImage
        }
        imageView.trophy = t
        imageView.frame = CGRect(x: position.x*factorX, y: position.y*factorY, width: t.overViewImage.size.width*factorX, height: t.overViewImage.size.height*factorY)
        let tgr = UITapGestureRecognizer(target: self, action: #selector(self.showTrophyModal(sender:)))
        imageView.addGestureRecognizer(tgr)
        imageView.isUserInteractionEnabled = true
        self.backgroundImage.addSubview(imageView)
    }
    
    @objc func showTrophyModal(sender: UITapGestureRecognizer){
        let mvc : ModalViewController = try! Dip.container.resolve() as ModalViewController
        let trophyView = TrophyModalView()
        trophyView.isInOverview = true
        trophyView.trophy = (sender.view as? TrophyOverviewImageView)?.trophy
        mvc.present(modal: trophyView)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
