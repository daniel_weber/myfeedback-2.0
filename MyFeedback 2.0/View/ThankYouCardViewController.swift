//
//  RecognitionCardViewController.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 13.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import UIKit
import Bond

class ThankYouCardViewController: UIViewController {
    
    @IBOutlet weak var recognitionCardView: UIView!
    @IBOutlet weak var competencyLabel: UILabel!
    @IBOutlet weak var backgroundImageView: UIImageView!
    var commentTextView: UITextView?
    @IBOutlet weak var thankYouImageView: UIImageView!
    
    var recognition: Observable<Recognition?> = Observable<Recognition?>(nil)
    
    let service = try! Dip.container.resolve() as FeedbackService
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        // Do any additional setup after loading the view.
        /*let shareOption = BarOption(title: "Share", icon: #imageLiteral(resourceName: "Icon_Feedback")) { [unowned self] () -> Void in
            self.myShareButton()
        }*/
        competencyLabel.isHidden = true
        let navigationBar = try! Dip.container.resolve() as AlternativeNavigationBar
        navigationBar.update(title: self.recognition.value?.competency.localSlogan, options: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupView()
        if self.recognition.value != nil{
            self.service.readThankYouNote(recognition: self.recognition.value!).then({_ in
                self.recognition.value?.receivers.first?.readTimestamp = Date()
            })
        }
        
    }
    
    
    func setupView(){
        self.commentTextView?.removeFromSuperview()
        let commentTextView = UITextView()
        self.commentTextView = commentTextView
        self.recognitionCardView.addSubview(commentTextView)
        
        self.hero.isEnabled = true
        self.recognitionCardView.hero.id = "recognitioncard"
        self.backgroundImageView.hero.modifiers = [.translate(y:1000)]
        
        self.thankYouImageView.image = recognition.value?.competency.card
        
        let scaleY = self.thankYouImageView.frame.height / (self.thankYouImageView.image?.size.height ?? 1)
        let scaleX = self.thankYouImageView.frame.width / (self.thankYouImageView.image?.size.width ?? 1)
        commentTextView.backgroundColor = UIColor.clear
        commentTextView.alpha = 1
        commentTextView.tintColor = MerckColor.RichPurple
        commentTextView.textColor = MerckColor.RichPurple
        var text = "\(recognition.value?.sender.getName() ?? "") "
        text += "thanked you for your Feedback".localized()
        text += ":\n\"\(recognition.value?.text ?? "")\""
        commentTextView.text = text
        commentTextView.topToSuperview(nil, offset: 34*scaleY, relation: .equal, priority: .required, isActive: true)
        commentTextView.leftToSuperview(nil, offset: 35*scaleX, relation: .equal, priority: .required, isActive: true)
        commentTextView.heightToSuperview(nil, multiplier: 0.253, offset: 0, relation: .equal, priority: .required, isActive: true)
        commentTextView.widthToSuperview(nil, multiplier: 0.868, offset: 0, relation: .equal, priority: .required, isActive: true)
        
    }
    
    
    
}

