//
//  RecognitionsOverallViewController.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 15.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import UIKit
import Bond
import Hydra

class RecognitionsOverallViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backgroundImage: UIImageView!
    
    let service = try! Dip.container.resolve() as FeedbackService
    
    var filteredRecognitions : MutableObservableArray<Recognition> = MutableObservableArray([])
    
    var searchTerm : Observable<String?> = Observable("")
    var searchPromise : Promise<[Recognition]>?
    var searchToken : InvalidationToken?
    
    var competencies: [Competency]{
        get{
            return Competency.Competencies+[Competency.ThankYouNote]
            let strings = Set(filteredRecognitions.array.map{$0.competency.name})
            var competencies:[Competency] = []
            return strings.map{Competency.getCompetency(name: $0)!}.sorted(by: {a, b in return a.key < b.key })
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(RecognitionOverviewTableViewCell.self, forCellReuseIdentifier: "RecognitionOverviewTableViewCell")
        self.tableView.register(EmptySearchEmployeeTableViewCell.self, forCellReuseIdentifier: "EmptySearchEmployeeTableViewCell")
        self.tableView.contentInset = UIEdgeInsetsMake(40, 0, 0, 0)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.tableView.refreshControl = UIRefreshControl()
        self.tableView.refreshControl?.addTarget(self, action: #selector(self.handleRefresh), for: .valueChanged)
        
        hero.isEnabled = true
        self.tableView.hero.modifiers = [.translate(y:1000)]
        self.backgroundImage.hero.modifiers = [.fade]
        self.view.backgroundColor = MerckColor.RichPurple
        self.backgroundImage.backgroundColor = MerckColor.RichPurple
        self.service.activeFilters.observeNext(with: {_ in
            if self.searchTerm.value != nil{
                self.searchTerm.value! += ""
            }else{
                self.searchTerm.value = ""
            }
            
        })
        let searchBar = try! Dip.container.resolve() as BaseSearchBar
        searchBar.searchTextField.reactive.text.bidirectionalBind(to: self.searchTerm)
        self.service.activeFilters.observeNext(with: {_ in
            self.setNavigationBar()
            if self.searchTerm.value != nil{
                self.searchTerm.value! += ""
            }else{
                self.searchTerm.value = ""
            }
            
        })
        self.filteredRecognitions.observeNext(with: {_ in
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        })
        let _ =  self.searchTerm.combineLatest(with: self.service.myRecognition){(term, recognitions) -> [Recognition] in
            self.tableView.layer.opacity = 0.4
            self.tableView.isUserInteractionEnabled = false
            
            self.searchPromise = self.filter(recognitions: recognitions.dataSource.array, by: term).then(in: Context.main, { (recs) in
                
                self.filteredRecognitions.replace(with: recs)
                
                self.tableView.layer.opacity = 1.0
                self.tableView.isUserInteractionEnabled = true
                
                self.searchPromise = nil
                
            })
            
            return []
            }.observeNext { (reqs) in
                
        }
        self.filteredRecognitions.replace(with: self.service.myRecognitionFiltered)
        self.tableView.alpha = 1
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.setNavigationBar()
    }
    
    func setNavigationBar(){
        let navigationBar = try! Dip.container.resolve() as AlternativeNavigationBar
        let filterIcon: UIImage
        if self.service.activeFilters.array.count > 0{
            filterIcon = #imageLiteral(resourceName: "Icon_Funnel_Filled")
        }else{
            filterIcon = #imageLiteral(resourceName: "Icon_Funnel")
        }
        let filterOptions = BarOption(title: "Filter", icon: filterIcon) { () -> Void in
            let mvc = try! Dip.container.resolve() as ModalViewController
            
            let filterMV = RecognitionFilterTableView()
            
            mvc.present(modal: filterMV, edge: .bottom)
        }
        
        let searchOptions = BarOption(title: "Search", icon: #imageLiteral(resourceName: "Icon_Search")) { () -> Void in
            let searchBar = try! Dip.container.resolve() as BaseSearchBar
            searchBar.isCollapsed = false
        }
        navigationBar.update(title: "Recognitions Overall".localized(), options: [filterOptions, searchOptions])
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return Competency.Competencies.count + 1
        return competencies.count
        let amountDistinctCompetencies = Set(self.filteredRecognitions.array.map{$0.competency.name}).count
        return amountDistinctCompetencies
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       // let competency = Competency.getCompetency(id: indexPath.row)
        let competency = competencies[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "RecognitionOverviewTableViewCell", for: indexPath) as! RecognitionOverviewTableViewCell
        cell.icon = competency.icon
        cell.title = competency.localName
        let counts = service.getCountsForCompetency(competency: competency)
        cell.totalCount = counts.total
        cell.unreadCount = counts.unread
        if competency.name == Competency.ThankYouNote.name{
            cell.titleLabel.textColor = MerckColor.VibrantGreen
        }else{
            cell.titleLabel.textColor = UIColor.white
        }
        self.service.myRecognition.observeNext(with: {_ in
            let counts = self.service.getCountsForCompetency(competency: competency)
            cell.totalCount = counts.total
            cell.unreadCount = counts.unread
        })
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.cellForRow(at: indexPath) is EmptySearchEmployeeTableViewCell{
            return
        }
        let navigationcontroller = try! Dip.container.resolve() as BaseNavigationController
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let competency = competencies[indexPath.row]
        let recognitions = self.filteredRecognitions.array.filter({recognition in return recognition.competency.name == competency.name})
        let vc = storyboard.instantiateViewController(withIdentifier: "RecognitionsViewController") as! RecognitionsViewController
        vc.competency = competency
        vc.filteredRecognitions.replace(with: recognitions)
        navigationcontroller.pushViewController(vc, animated: true)
    }
    
    @objc func handleRefresh(){
        self.tableView.refreshControl?.endRefreshing()
        Loading.show()
        self.service.updateMyRecognitions().then({_ in
            self.tableView.reloadData()
            Loading.hide()
        })
    }
    
    // MARK: Recognition Search
    
    private func filter(recognitions: [Recognition], by term: String?) -> Promise<[Recognition]> {
        
        self.searchToken?.invalidate()
        self.searchToken = InvalidationToken()
        
        return Promise<[Recognition]>(in: Context.background, token: self.searchToken, { (resolve, reject, operation) in
            
            
            var filtered : [Recognition] = []
            
            for rec in recognitions {
                
                if !self.filter(rec){
                    continue
                }
                
                if operation.isCancelled {
                    break
                }
                
                if (term == nil || term! == ""){
                    if self.service.activeFilters.array.count == 0{
                        continue
                    }else{
                        filtered.append(rec)
                        continue
                    }
                    
                }
                
                let doAND = true
                if (doAND){
                    if self.considerAND(term: term, recognition: rec) {filtered.append(rec)}
                }
                else {
                    if self.considerOR(term: term, recognition: rec) {filtered.append(rec)}
                }
            }
            
            
            if (operation.isCancelled){
                operation.cancel()
                return
            }
            
            if (filtered.count == 0 && (term == nil || term! == "") && self.service.activeFilters.count == 0){
                filtered.append(contentsOf: recognitions)
            }
            
            if (filtered.count == 0){
                let empty = Recognition.invalid()
                filtered.append(empty)
            }
            
            
            
            resolve(filtered)
        })
    }
    
    // Checks if an Recognition applies to the active
    private func filter(_ recognition: Recognition)->Bool{
        for filter in self.service.activeFilters.array{
            switch filter.type{
            case .Competency:
                if !service.activeFilters.array.filter({f in return f.type == FilterType.Competency}).contains(where: {f in return recognition.competency.name == (f.value as? Competency)?.name}){
                    return false
                }
            case .Employee:
                if !recognition.receivers.contains(where: {receiver in return receiver.receiver.userId == (filter.value as? Employee)?.userId}) && recognition.sender.userId != (filter.value as? Employee)?.userId{
                    return false
                }
            case .DateFrom:
                if recognition.date! < filter.value as! Date{
                    return false
                }
            case .DateTo:
                if recognition.date! > filter.value as! Date{
                    return false
                }
            default:
                break
            }
        }
        return true
    }
    
    func considerAND(term : String?, recognition: Recognition) -> Bool {
        
        if let searchTerms = term?.split(separator: " ") {
            
            for t in searchTerms {
                
                if recognition.searchTerm.contains(t.lowercased()) { continue }
                
                // When there is no hit for one item it's out (AND)
                return false
            }
            
            // When every item in the loop had a hit, this search term fits (AND)
            return true
        }
        
        return false
    }
    
    func considerOR(term : String?, recognition: Recognition) -> Bool {
        
        if let searchTerms = term?.split(separator: " ") {
            
            for t in searchTerms {
                
                if recognition.searchTerm.contains(t.lowercased()) { return true }
                
            }
            
            // When no item in the loop had a hit, this search term does not fit (OR)
            return false
        }
        
        return false
    }

}
