//
//  FirstScreenViewController.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 08.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import UIKit
import TinyConstraints
import MIBadgeButton_Swift

class FirstScreenViewController: UIViewController {

    @IBOutlet weak var backgroundImage: UIImageView!
    
    let service = try! Dip.container.resolve() as FeedbackService
    
    var actionButtons: [MIBadgeButton] = []
    var actionLabels: [UILabel] = []
    
    var helpButton: UIButton?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        service.userSettings.observeNext(with: {_ in
            self.updateLabels()
        })
        if UIDevice.current.userInterfaceIdiom == .pad{
            self.backgroundImage.image = #imageLiteral(resourceName: "background_first_screen_ipad")
            self.backgroundImage.edges(to: self.view)
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let helpOption = BarOption(title: "FAQ", icon: #imageLiteral(resourceName: "Icon_Help")) { () -> Void in
            Onboarding.show()
        }
        
        let navigationBar = try! Dip.container.resolve() as AlternativeNavigationBar
        navigationBar.update(title: "", options: nil)
        updateLabels()
        navigationBar.isCollapsed = true
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        let navigationBar = try! Dip.container.resolve() as AlternativeNavigationBar
        navigationBar.isCollapsed = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func setupView() {
        hero.isEnabled = true
        self.backgroundImage.hero.modifiers = [.fade]
        view.backgroundColor = MerckColor.VibrantGreen
        
        setupButtons()
    }
    
    func setupButtons(){
        for button in actionButtons{
            button.removeFromSuperview()
        }
        for label in actionLabels{
            label.removeFromSuperview()
        }
        if Device.IS_IPAD_PRO_12{
            setupButtonsIpad12()
        }else if Device.IS_IPHONE_X{
            setupButtonsIphoneX()
        }else if Device.IS_IPAD{
            setupButtonsIpad()
        }else if Device.IS_IPHONE_5 || Device.IS_IPHONE_4_OR_LESS{
            setupButtonsSmallScreen()
        }else{
            setupButtonsDefault()
        }
        self.addHelpButton(right: 20, y: 40)
        self.actionButtons[0].addTarget(self, action: #selector(self.navigateToAnswerRequests), for: .touchUpInside)
        self.actionButtons[1].addTarget(self, action: #selector(self.navigateToNewRequest), for: .touchUpInside)
        self.actionButtons[2].addTarget(self, action: #selector(self.navigateToNewRecognition), for: .touchUpInside)
        DispatchQueue.main.async {
            if self.service.amountNewRequests.value > 0{
                self.actionButtons[0].badgeString = "\(self.service.amountNewRequests.value)"
            }else{
                self.actionButtons[0].badgeString = nil
            }
            self.service.amountNewRequests.observeNext(with: {value in
                if value > 0{
                    self.actionButtons[0].badgeString = "\(value)"
                }else{
                    self.actionButtons[0].badgeString = nil
                }
            })
        }
        
        
    }
    
    func setupButtonsIphoneX(){
        
        let answer = addActionButton(x: 120, y: 300, image: #imageLiteral(resourceName: "button_answer_request_outline"), activeImage: #imageLiteral(resourceName: "button_answer_request_filled"), title: "Answer Request".localized())
        let request = addActionButton(x: 400, y: 240, image: #imageLiteral(resourceName: "button_get_feedback_outline"), activeImage: #imageLiteral(resourceName: "button_get_feedback_filled"), title: "Get Request".localized())
        let recognition = addActionButton(x: 250, y: 500, image: #imageLiteral(resourceName: "button_send_recognition_outline"), activeImage: #imageLiteral(resourceName: "button_send_recognition_filled"), title: "Send Recognition".localized())
        request.button.top(to: self.view, nil, offset: 110, relation: .equal, priority: .required, isActive: true)
        request.label.right(to: self.view, nil, offset: -120, relation: .equal, priority: .required, isActive: true)
        answer.button.top(to: self.view, nil, offset: 200, relation: .equal, priority: .required, isActive: true)
        answer.label.left(to: self.view, nil, offset: 10, relation: .equal, priority: .required, isActive: true)
        recognition.button.topToBottom(of: answer.label, offset: 20, relation: .equalOrGreater, priority: .required, isActive: true)
        recognition.label.left(to: self.view, nil, offset: 50, relation: .equal, priority: .required, isActive: true)
    }
    
    func setupButtonsIpad12(){
        let scaleFactorY = backgroundImage.bounds.size.height/backgroundImage.image!.size.height
        let scaleFactorX = backgroundImage.bounds.size.width/backgroundImage.image!.size.width
        
        let answer = addActionButton(x: 120, y: 300, image: #imageLiteral(resourceName: "button_answer_request_outline"), activeImage: #imageLiteral(resourceName: "button_answer_request_filled"), title: "Answer Request".localized())
        let request = addActionButton(x: 400, y: 240, image: #imageLiteral(resourceName: "button_get_feedback_outline"), activeImage: #imageLiteral(resourceName: "button_get_feedback_filled"), title: "Get Feedback".localized())
        let recognition = addActionButton(x: 250, y: 500, image: #imageLiteral(resourceName: "button_send_recognition_outline"), activeImage: #imageLiteral(resourceName: "button_send_recognition_filled"), title: "Send Recognition".localized())
        request.button.top(to: self.view, nil, offset: 400*scaleFactorY, relation: .equal, priority: .required, isActive: true)
        request.label.right(to: self.view, nil, offset: -900*scaleFactorX, relation: .equal, priority: .required, isActive: true)
        answer.button.top(to: self.view, nil, offset: 600*scaleFactorY, relation: .equal, priority: .required, isActive: true)
        answer.label.left(to: self.view, nil, offset: 80, relation: .equal, priority: .required, isActive: true)
        recognition.button.topToBottom(of: answer.label, offset: 120, relation: .equalOrGreater, priority: .required, isActive: true)
        recognition.label.left(to: self.view, nil, offset: 500*scaleFactorX, relation: .equal, priority: .required, isActive: true)
    }
    
    func setupButtonsIpad(){
        let scaleFactorY = backgroundImage.bounds.size.height/backgroundImage.image!.size.height
        let scaleFactorX = backgroundImage.bounds.size.width/backgroundImage.image!.size.width
        
        let answer = addActionButton(x: 120, y: 300, image: #imageLiteral(resourceName: "button_answer_request_outline"), activeImage: #imageLiteral(resourceName: "button_answer_request_filled"), title: "Answer Request".localized())
        let request = addActionButton(x: 350, y: 240, image: #imageLiteral(resourceName: "button_get_feedback_outline"), activeImage: #imageLiteral(resourceName: "button_get_feedback_filled"), title: "Get Feedback".localized())
        let recognition = addActionButton(x: 250, y: 500, image: #imageLiteral(resourceName: "button_send_recognition_outline"), activeImage: #imageLiteral(resourceName: "button_send_recognition_filled"), title: "Send Recognition".localized())
        request.button.top(to: self.view, nil, offset: 300*scaleFactorY, relation: .equal, priority: .required, isActive: true)
        request.label.right(to: self.view, nil, offset: -950*scaleFactorX, relation: .equal, priority: .required, isActive: true)
        answer.button.top(to: self.view, nil, offset: 400*scaleFactorY, relation: .equal, priority: .required, isActive: true)
        answer.label.left(to: self.view, nil, offset: 40, relation: .equal, priority: .required, isActive: true)
        recognition.button.topToBottom(of: answer.label, offset: 80, relation: .equalOrGreater, priority: .required, isActive: true)
        recognition.label.left(to: self.view, nil, offset: 300*scaleFactorX, relation: .equal, priority: .required, isActive: true)
    }
    
    func setupButtonsSmallScreen(){
        let scaleFactorY = backgroundImage.bounds.size.height/backgroundImage.image!.size.height
        let scaleFactorX = backgroundImage.bounds.size.width/backgroundImage.image!.size.width
        
        let answer = addActionButton(x: 120, y: 300, image: #imageLiteral(resourceName: "button_answer_request_outline"), activeImage: #imageLiteral(resourceName: "button_answer_request_filled"), title: "Answer Request".localized())
        let request = addActionButton(x: 400, y: 240, image: #imageLiteral(resourceName: "button_get_feedback_outline"), activeImage: #imageLiteral(resourceName: "button_get_feedback_filled"), title: "Get Feedback".localized())
        let recognition = addActionButton(x: 250, y: 500, image: #imageLiteral(resourceName: "button_send_recognition_outline"), activeImage: #imageLiteral(resourceName: "button_send_recognition_filled"), title: "Send Recognition".localized())
        request.button.top(to: self.view, nil, offset: 140*scaleFactorY, relation: .equal, priority: .required, isActive: true)
        request.label.right(to: self.view, nil, offset: -150*scaleFactorX, relation: .equal, priority: .required, isActive: true)
        answer.button.top(to: self.view, nil, offset: 240*scaleFactorY, relation: .equal, priority: .required, isActive: true)
        answer.label.left(to: self.view, nil, offset: 8, relation: .equal, priority: .required, isActive: true)
        recognition.button.topToBottom(of: answer.label, offset: 20, relation: .equalOrGreater, priority: .required, isActive: true)
        recognition.label.left(to: self.view, nil, offset: 70*scaleFactorX, relation: .equal, priority: .required, isActive: true)
    }
    
    func setupButtonsDefault(){
        let scaleFactorY = backgroundImage.bounds.size.height/backgroundImage.image!.size.height
        let scaleFactorX = backgroundImage.bounds.size.width/backgroundImage.image!.size.width
        
        let answer = addActionButton(x: 120, y: 300, image: #imageLiteral(resourceName: "button_answer_request_outline"), activeImage: #imageLiteral(resourceName: "button_answer_request_filled"), title: "Answer Request".localized())
        let request = addActionButton(x: 380, y: 240, image: #imageLiteral(resourceName: "button_get_feedback_outline"), activeImage: #imageLiteral(resourceName: "button_get_feedback_filled"), title: "Get Feedback".localized())
        let recognition = addActionButton(x: 250, y: 500, image: #imageLiteral(resourceName: "button_send_recognition_outline"), activeImage: #imageLiteral(resourceName: "button_send_recognition_filled"), title: "Send Recognition".localized())
        request.button.top(to: self.view, nil, offset: 180*scaleFactorY, relation: .equal, priority: .required, isActive: true)
        request.label.right(to: self.view, nil, offset: -190*scaleFactorX, relation: .equal, priority: .required, isActive: true)
        answer.button.top(to: self.view, nil, offset: 260*scaleFactorY, relation: .equal, priority: .required, isActive: true)
        answer.label.left(to: self.view, nil, offset: 20, relation: .equal, priority: .required, isActive: true)
        recognition.button.topToBottom(of: answer.label, offset: 20, relation: .equalOrGreater, priority: .required, isActive: true)
        recognition.label.left(to: self.view, nil, offset: 70*scaleFactorX, relation: .equal, priority: .required, isActive: true)
    }
    
    func addHelpButton(right: CGFloat, y:CGFloat){
        self.helpButton?.removeFromSuperview()
        let scaleFactorY = backgroundImage.bounds.size.height/backgroundImage.image!.size.height
        let scaleFactorX = backgroundImage.bounds.size.width/backgroundImage.image!.size.width
        
        let size: CGSize
        if Device.IS_IPAD_PRO_12{
            size = CGSize(width: 80, height: 90)
        }else if Device.IS_IPAD{
            size = CGSize(width: 60, height: 60)
        }else{
            size = CGSize(width: 40, height: 40)
        }
        let helpButton = UIButton(frame: CGRect(origin: CGPoint.zero, size: size))
        helpButton.setImage(#imageLiteral(resourceName: "button_help_filled"), for: .normal)
        helpButton.setImage(#imageLiteral(resourceName: "button_help_outline"), for: .focused)
        helpButton.setImage(#imageLiteral(resourceName: "button_help_outline"), for: .highlighted)
        helpButton.addTarget(self, action: #selector(self.handleHelp), for: .touchUpInside)
        self.helpButton = helpButton
        self.view.addSubview(helpButton)
        helpButton.width(size.width)
        helpButton.height(size.height)
        helpButton.topToSuperview(nil, offset: y, relation: .equal, priority: .required, isActive: true)
        helpButton.rightToSuperview(nil, offset: right, relation: .equal, priority: .required, isActive: true)
        helpButton.hero.modifiers = [.translate(y:1000)]
    }
    
    
    func addActionButton(x: CGFloat, y:CGFloat, image: UIImage, activeImage: UIImage, title: String)-> (button: UIButton, label: UILabel){
        let scaleFactorY = backgroundImage.bounds.size.height/backgroundImage.image!.size.height
        let scaleFactorX = backgroundImage.bounds.size.width/backgroundImage.image!.size.width
        
        let size: CGSize
        if Device.IS_IPAD_PRO_12{
            size = CGSize(width: 120, height: 120)
        }else if Device.IS_IPAD{
            size = CGSize(width: 90, height: 90)
        }else{
            size = CGSize(width: 60, height: 60)
        }
        let actionButton = MIBadgeButton(frame: CGRect(origin: CGPoint.zero, size: size))
        actionButton.badgeBackgroundColor = MerckColor.VibrantMagenta
        actionButton.setImage(image, for: .normal)
        actionButton.setImage(activeImage, for: .selected)
        actionButton.setImage(activeImage, for: .highlighted)
        self.view.addSubview(actionButton)
        actionButton.width(size.width)
        actionButton.height(size.height)
        actionButton.center = CGPoint(x: (x)*scaleFactorX, y: (y)*scaleFactorY)
        let actionLabel = UILabel()
        actionLabel.text = title
        //actionLabel.backgroundColor = UIColor.red
        actionLabel.textColor = UIColor.white
        actionLabel.textAlignment = .center
        actionLabel.numberOfLines = 0
        actionLabel.lineBreakMode = .byWordWrapping
        self.view.addSubview(actionLabel)
        var fontSize: CGFloat = 15
        if Device.IS_IPHONE_5 || Device.IS_IPHONE_4_OR_LESS{
            fontSize = 12
        }else if Device.IS_IPAD_PRO_12{
            fontSize = 21
        }else if Device.IS_IPAD{
            fontSize = 18
        }
        actionLabel.font = UIFont(descriptor: actionLabel.font.fontDescriptor, size: fontSize)
        actionLabel.width(120)
        actionLabel.topToBottom(of: actionButton)
        actionLabel.centerX(to: actionButton)
        actionButton.hero.modifiers = [.translate(y:1000)]
        actionLabel.hero.modifiers = [.translate(y:1000)]
        self.actionButtons.append(actionButton)
        self.actionLabels.append(actionLabel)
        
        return (actionButton, actionLabel)
    }
    
    @objc func navigateToNewRequest(){
        let navigationcontroller = try! Dip.container.resolve() as BaseNavigationController
        let main = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = main.instantiateViewController(withIdentifier: "GetFeedbackViewController") as! GetFeedbackViewController
        navigationcontroller.pushViewController(vc, animated: true)
        
    }
    
    @objc func navigateToNewRecognition(){
        let navigationcontroller = try! Dip.container.resolve() as BaseNavigationController
        let main = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = main.instantiateViewController(withIdentifier: "SelectCompetencyViewController") as! SelectCompetencyViewController
        navigationcontroller.pushViewController(vc, animated: true)
    }
    
    @objc func navigateToAnswerRequests(){
        let navigationcontroller = try! Dip.container.resolve() as BaseNavigationController
        let main = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = main.instantiateViewController(withIdentifier: "AnswerRequestViewController") as! AnswerRequestViewController
        navigationcontroller.pushViewController(vc, animated: true)
    }
    
    @objc func handleHelp(){
        Onboarding.show()
    }
    
    func updateLabels(){
        self.actionLabels[0].text = "Answer Request".localized()
        self.actionLabels[1].text = "Get Feedback".localized()
        self.actionLabels[2].text = "Send Recognition".localized()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
