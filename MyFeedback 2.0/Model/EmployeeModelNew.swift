//
//  EmployeeModelNew.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 12.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import Foundation

class EmployeeModelNew{
    let mUID : String?
    let email : String?
    let firstName : String?
    let lastName : String?
    let department : String?
    let departmentText : String?
    let departmentOrg : String?
    let firstNameLower : String?
    let lastNameLower : String?
    let userID : String?
    let fullName : String?
    let globalidentifier : String?
    let businesssector : String?
    
    enum CodingKeys: String, CodingKey {
        
        case mUID = "MUID"
        case email = "email"
        case firstName = "firstName"
        case lastName = "lastName"
        case department = "department"
        case departmentText = "departmentText"
        case departmentOrg = "departmentOrg"
        case firstNameLower = "firstNameLower"
        case lastNameLower = "lastNameLower"
        case userID = "UserID"
        case fullName = "fullName"
        case globalidentifier = "globalidentifier"
        case businesssector = "businesssector"
    }
    
    required public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        mUID = try values.decodeIfPresent(String.self, forKey: .mUID)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        firstName = try values.decodeIfPresent(String.self, forKey: .firstName)
        lastName = try values.decodeIfPresent(String.self, forKey: .lastName)
        department = try values.decodeIfPresent(String.self, forKey: .department)
        departmentText = try values.decodeIfPresent(String.self, forKey: .departmentText)
        departmentOrg = try values.decodeIfPresent(String.self, forKey: .departmentOrg)
        firstNameLower = try values.decodeIfPresent(String.self, forKey: .firstNameLower)
        lastNameLower = try values.decodeIfPresent(String.self, forKey: .lastNameLower)
        userID = try values.decodeIfPresent(String.self, forKey: .userID)
        fullName = try values.decodeIfPresent(String.self, forKey: .fullName)
        globalidentifier = try values.decodeIfPresent(String.self, forKey: .globalidentifier)
        businesssector = try values.decodeIfPresent(String.self, forKey: .businesssector)
    }
    
    func EmployeeFromModel() -> Employee?{
        guard userID != nil, mUID != nil, firstName != nil, lastName != nil, email != nil, department != nil else{
            return nil
        }
        return Employee(userId: userID!, muid: mUID!, firstName: firstName!, lastName: lastName!, email: email!, orgUnitName: department!)
    }
}
