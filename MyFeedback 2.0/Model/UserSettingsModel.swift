/*
 Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import Foundation
import SwiftDate

/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class UserSettingsModel {
    public var userID : String?
    public var showReceivedDuration : String?
    public var showSendDuration : String?
    public var pushNotification : String?
    public var mailNotification : String?
    public var language : String?
    public var background : String?
    public var defaultFeedbackType : String?
    public var lastOpenedDesktopTimestamp : String?
    public var lastOpenedMobileTimestamp : String?
    public var lastOpenedMobileVersion : String?
    public var userMasterdata : UserMasterdata?
    
    /**
     Returns an array of models based on given dictionary.
     
     Sample usage:
     let json4Swift_Base_list = Json4Swift_Base.modelsFromDictionaryArray(someDictionaryArrayFromJSON)
     
     - parameter array:  NSArray from JSON dictionary.
     
     - returns: Array of Json4Swift_Base Instances.
     */
    public class func modelsFromDictionaryArray(array:NSArray) -> [UserSettingsModel]
    {
        var models:[UserSettingsModel] = []
        for item in array
        {
            models.append(UserSettingsModel(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    /**
     Constructs the object based on the given dictionary.
     
     Sample usage:
     let json4Swift_Base = Json4Swift_Base(someDictionaryFromJSON)
     
     - parameter dictionary:  NSDictionary from JSON.
     
     - returns: Json4Swift_Base Instance.
     */
    required public init?(dictionary: NSDictionary) {
        
        userID = dictionary["UserID"] as? String
        showReceivedDuration = dictionary["showReceivedDuration"] as? String
        showSendDuration = dictionary["showSendDuration"] as? String
        pushNotification = dictionary["pushNotification"] as? String
        mailNotification = dictionary["mailNotification"] as? String
        language = dictionary["language"] as? String
        background = dictionary["background"] as? String
        defaultFeedbackType = dictionary["defaultFeedbackType"] as? String
        lastOpenedDesktopTimestamp = dictionary["lastOpenedDesktopTimestamp"] as? String
        lastOpenedMobileTimestamp = dictionary["lastOpenedMobileTimestamp"] as? String
        lastOpenedMobileVersion = dictionary["lastOpenedMobileVersion"] as? String
        if (dictionary["UserMasterdata"] != nil) { userMasterdata = UserMasterdata(dictionary: dictionary["UserMasterdata"] as! NSDictionary) }
    }
    
    
    /**
     Returns the dictionary representation for the current instance.
     
     - returns: NSDictionary.
     */
    public func dictionaryRepresentation() -> NSDictionary {
        
        let dictionary = NSMutableDictionary()
        
        dictionary.setValue(self.userID, forKey: "UserID")
        dictionary.setValue(self.showReceivedDuration, forKey: "showReceivedDuration")
        dictionary.setValue(self.showSendDuration, forKey: "showSendDuration")
        dictionary.setValue(self.pushNotification, forKey: "pushNotification")
        dictionary.setValue(self.mailNotification, forKey: "mailNotification")
        dictionary.setValue(self.language, forKey: "language")
        dictionary.setValue(self.background, forKey: "background")
        dictionary.setValue(self.defaultFeedbackType, forKey: "defaultFeedbackType")
        dictionary.setValue(self.lastOpenedDesktopTimestamp, forKey: "lastOpenedDesktopTimestamp")
        dictionary.setValue(self.lastOpenedMobileTimestamp, forKey: "lastOpenedMobileTimestamp")
        dictionary.setValue(self.lastOpenedMobileVersion, forKey: "lastOpenedMobileVersion")
        dictionary.setValue(self.userMasterdata?.dictionaryRepresentation(), forKey: "UserMasterdata")
        
        return dictionary
    }
    
    func settings()->UserSettings?{
        
        guard self.language != nil, self.showReceivedDuration != nil, self.showSendDuration != nil, self.pushNotification != nil, self.mailNotification != nil else {
            return nil
        }
        
        let lang = Language.getLanguage(key: self.language!)
        let received = DurationSetting.getDuration(key: self.showReceivedDuration!)
        let send = DurationSetting.getDuration(key: self.showSendDuration!)
        let push = NotificationSetting.getNotificationSetting(key: self.pushNotification!)
        let mail = NotificationSetting.getNotificationSetting(key: self.pushNotification!)
        let lastOpened = self.getDate(str: self.lastOpenedMobileVersion)

        
        let settings = UserSettings(language: lang, showReceivedDuration: received, showSendDuration: send, pushNotification: push, mailNotification: mail, lastOpenedMobileTimestamp: lastOpened, lastOpenedMobileVersion: self.lastOpenedMobileVersion)
        
        settings.background = background
        
        return settings
    }
    
    func getDate(str: String?)->Date?{
        if str == nil{
            return nil
        }
        var splittetTimeStamp = str!.split(separator: "(")
        splittetTimeStamp = splittetTimeStamp[1].split(separator: ")")
        let timeStampStr = String(splittetTimeStamp[0])
        let timeInterval = Double(timeStampStr)
        if timeInterval != nil{
            let resultDate = Date(timeIntervalSince1970: timeInterval!/1000)
            return resultDate
        }else{
            return nil
        }
    }
    
}

