//
//  RecognitionModel.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 19.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import Foundation

public class RecognitionModel {
    public var recognitionID : Int?
    public var competencyID : String?
    public var sendTimestamp : String?
    public var text : String?
    public var deleteAllowed : Int?
    public var thankYouUnreadCount : Int?
    public var receiver : [ReceiverModel]?
    
    public var thankYouFlag: Int?
    public var readFlag: Int?
    
    /**
     Returns an array of models based on given dictionary.
     
     Sample usage:
     let results_list = Results.modelsFromDictionaryArray(someDictionaryArrayFromJSON)
     
     - parameter array:  NSArray from JSON dictionary.
     
     - returns: Array of Results Instances.
     */
    public class func modelsFromDictionaryArray(array:NSArray) -> [RecognitionModel]
    {
        var models:[RecognitionModel] = []
        for item in array
        {
            models.append(RecognitionModel(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    /**
     Constructs the object based on the given dictionary.
     
     Sample usage:
     let results = Results(someDictionaryFromJSON)
     
     - parameter dictionary:  NSDictionary from JSON.
     
     - returns: Results Instance.
     */
    required public init?(dictionary: NSDictionary) {
        
        recognitionID = dictionary["recognitionID"] as? Int
        competencyID = dictionary["competencyID"] as? String
        sendTimestamp = dictionary["sendTimestamp"] as? String
        text = dictionary["text"] as? String
        deleteAllowed = dictionary["deleteAllowed"] as? Int
        thankYouUnreadCount = dictionary["thankYouUnreadCount"] as? Int
        if (dictionary["Receiver"] as? NSDictionary != nil && (dictionary["Receiver"] as! NSDictionary)["results"] != nil) { receiver = ReceiverModel.modelsFromDictionaryArray(array: (dictionary["Receiver"] as! NSDictionary)["results"] as! NSArray) }
    }
    
    
    /**
     Returns the dictionary representation for the current instance.
     
     - returns: NSDictionary.
     */
    public func dictionaryRepresentation() -> NSDictionary {
        
        let dictionary = NSMutableDictionary()
        
        dictionary.setValue(self.recognitionID, forKey: "recognitionID")
        dictionary.setValue(self.competencyID, forKey: "competencyID")
        dictionary.setValue(self.sendTimestamp, forKey: "sendTimestamp")
        dictionary.setValue(self.text, forKey: "text")
        dictionary.setValue(self.deleteAllowed, forKey: "deleteAllowed")
        dictionary.setValue(self.thankYouUnreadCount, forKey: "thankYouUnreadCount")
        dictionary.setValue(self.receiver?.map({rec in rec.dictionaryRepresentation()}), forKey: "Receiver")
        
        dictionary.setValue(self.readFlag, forKey: "readFlag")
        dictionary.setValue(self.thankYouFlag, forKey: "thankYouFlag")
        
        return dictionary
    }
    
    func recognition()->Recognition?{
        var receivers:[RecognitionReceiver] = []
        guard self.competencyID != nil, let competency = Competency.getCompetency(name: self.competencyID!), self.recognitionID != nil, loggedInUser.value != nil, self.text != nil else{
            return nil
        }
        if self.receiver != nil{
            for model in self.receiver!{
                if let receiverResponse = model.recognitionReceiver(){
                    receivers.append(receiverResponse)
                }
            }
        }
        
        let recognition = Recognition(id: self.recognitionID!, sender: loggedInUser.value!, competency: competency, text: self.text!, date: getDate(str: self.sendTimestamp), repetition: nil)
        recognition.receivers = receivers
        return recognition
    }
    
    func getDate(str: String?)->Date?{
        if str == nil{
            return nil
        }
        var splittetTimeStamp = str!.split(separator: "(")
        splittetTimeStamp = splittetTimeStamp[1].split(separator: ")")
        let timeStampStr = String(splittetTimeStamp[0])
        let timeInterval = Double(timeStampStr)
        if timeInterval != nil{
            let resultDate = Date(timeIntervalSince1970: timeInterval!/1000)
            return resultDate
        }else{
            return nil
        }
    }
    
}
