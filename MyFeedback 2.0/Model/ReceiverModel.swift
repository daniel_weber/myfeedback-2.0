//
//  ReceiverModel.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 19.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import Foundation

public class ReceiverModel {
    public var recognitionID : Int?
    public var appraiseeUID : String?
    public var thankYouFlag : Int?
    public var thankYouReadFlag : Int?
    public var userMasterdata : UserMasterdata?
    
    /**
     Returns an array of models based on given dictionary.
     
     Sample usage:
     let json4Swift_Base_list = Json4Swift_Base.modelsFromDictionaryArray(someDictionaryArrayFromJSON)
     
     - parameter array:  NSArray from JSON dictionary.
     
     - returns: Array of Json4Swift_Base Instances.
     */
    public class func modelsFromDictionaryArray(array:NSArray) -> [ReceiverModel]
    {
        var models:[ReceiverModel] = []
        for item in array
        {
            models.append(ReceiverModel(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    /**
     Constructs the object based on the given dictionary.
     
     Sample usage:
     let json4Swift_Base = Json4Swift_Base(someDictionaryFromJSON)
     
     - parameter dictionary:  NSDictionary from JSON.
     
     - returns: Json4Swift_Base Instance.
     */
    required public init?(dictionary: NSDictionary) {
        
        recognitionID = dictionary["recognitionID"] as? Int
        appraiseeUID = dictionary["appraiseeUID"] as? String
        thankYouFlag = dictionary["thankYouFlag"] as? Int
        thankYouReadFlag = dictionary["thankYouReadFlag"] as? Int
        if (dictionary["UserMasterdata"] as? NSDictionary != nil) { userMasterdata = UserMasterdata(dictionary: dictionary["UserMasterdata"] as! NSDictionary) }
    }
    
    
    /**
     Returns the dictionary representation for the current instance.
     
     - returns: NSDictionary.
     */
    public func dictionaryRepresentation() -> NSDictionary {
        
        let dictionary = NSMutableDictionary()
        
        dictionary.setValue(self.recognitionID, forKey: "recognitionID")
        dictionary.setValue(self.appraiseeUID, forKey: "appraiseeUID")
        dictionary.setValue(self.thankYouFlag, forKey: "thankYouFlag")
        dictionary.setValue(self.thankYouReadFlag, forKey: "thankYouReadFlag")
        dictionary.setValue(self.userMasterdata?.dictionaryRepresentation(), forKey: "UserMasterdata")
        
        return dictionary
    }
    
    func recognitionReceiver() -> RecognitionReceiver?{
        let emp = self.userMasterdata?.employee()
        guard self.recognitionID != nil, emp != nil else{
            return nil
        }
        return RecognitionReceiver(recognitionId: self.recognitionID!, receiver: emp!, readTimestamp: nil, thankYouTimestamp: nil, hideTimestamp: nil)
    }
    
}
