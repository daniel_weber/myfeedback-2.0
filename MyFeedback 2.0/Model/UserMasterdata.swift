

import Foundation

/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class UserMasterdata {
    public var mUID : String?
    public var userID : String?
    public var businesssector : String?
    public var city : String?
    public var country : String?
    public var department : String?
    public var email : String?
    public var firstName : String?
    public var firstNameGlobal : String?
    public var globalidentifier : String?
    public var lastName : String?
    public var lastNameGlobal : String?
    public var mobile : String?
    public var number : String?
    public var phone : String?
    public var region : String?
    public var street : String?
    
    /**
     Returns an array of models based on given dictionary.
     
     Sample usage:
     let userMasterdata_list = UserMasterdata.modelsFromDictionaryArray(someDictionaryArrayFromJSON)
     
     - parameter array:  NSArray from JSON dictionary.
     
     - returns: Array of UserMasterdata Instances.
     */
    public class func modelsFromDictionaryArray(array:NSArray) -> [UserMasterdata]
    {
        var models:[UserMasterdata] = []
        for item in array
        {
            models.append(UserMasterdata(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    /**
     Constructs the object based on the given dictionary.
     
     Sample usage:
     let userMasterdata = UserMasterdata(someDictionaryFromJSON)
     
     - parameter dictionary:  NSDictionary from JSON.
     
     - returns: UserMasterdata Instance.
     */
    required public init?(dictionary: NSDictionary) {
        
        mUID = dictionary["MUID"] as? String
        userID = dictionary["UserID"] as? String
        businesssector = dictionary["businesssector"] as? String
        city = dictionary["city"] as? String
        country = dictionary["country"] as? String
        department = dictionary["department"] as? String
        email = dictionary["email"] as? String
        firstName = dictionary["firstName"] as? String
        firstNameGlobal = dictionary["firstNameGlobal"] as? String
        globalidentifier = dictionary["globalidentifier"] as? String
        lastName = dictionary["lastName"] as? String
        lastNameGlobal = dictionary["lastNameGlobal"] as? String
        mobile = dictionary["mobile"] as? String
        number = dictionary["number"] as? String
        phone = dictionary["phone"] as? String
        region = dictionary["region"] as? String
        street = dictionary["street"] as? String
    }
    
    
    /**
     Returns the dictionary representation for the current instance.
     
     - returns: NSDictionary.
     */
    public func dictionaryRepresentation() -> NSDictionary {
        
        let dictionary = NSMutableDictionary()
        
        dictionary.setValue(self.mUID, forKey: "MUID")
        dictionary.setValue(self.userID, forKey: "UserID")
        dictionary.setValue(self.businesssector, forKey: "businesssector")
        dictionary.setValue(self.city, forKey: "city")
        dictionary.setValue(self.country, forKey: "country")
        dictionary.setValue(self.department, forKey: "department")
        dictionary.setValue(self.email, forKey: "email")
        dictionary.setValue(self.firstName, forKey: "firstName")
        dictionary.setValue(self.firstNameGlobal, forKey: "firstNameGlobal")
        dictionary.setValue(self.globalidentifier, forKey: "globalidentifier")
        dictionary.setValue(self.lastName, forKey: "lastName")
        dictionary.setValue(self.lastNameGlobal, forKey: "lastNameGlobal")
        dictionary.setValue(self.mobile, forKey: "mobile")
        dictionary.setValue(self.number, forKey: "number")
        dictionary.setValue(self.phone, forKey: "phone")
        dictionary.setValue(self.region, forKey: "region")
        dictionary.setValue(self.street, forKey: "street")
        
        return dictionary
    }
    
    func employee() -> Employee?{
        guard self.userID != nil, self.mUID != nil, self.firstName != nil, self.lastName != nil, self.email != nil, self.department != nil else{
            return nil
        }
        return Employee(userId: self.userID!, muid: self.mUID!, firstName: self.firstName!, lastName: self.lastName!, email: self.email!, orgUnitName: self.department!)
    }
    
}

