//
//  SAPcpms.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 08.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import Foundation

import Foundation
import Alamofire
import KeychainSwift
import Hydra
import Bond
import SwiftyJSON

enum webError : Error {
    case webServiceError(String)
}

class SAPcpms{
    
    static public let shared = SAPcpms()
    
    #if JX4
        let scpAccountId:String = "aa4d355e1"
    #else
        let scpAccountId:String = "a3a4a7fd8"
    #endif
    
    var serviceRoot: String{
        get{
            //return "https://jx4aa4d355e1.hana.ondemand.com/com/merckgroup/fee2/services/"
            //return "https://mobile-aa4d355e1.hana.ondemand.com/de.merck.myfeedback/"
            if(RCValues.sharedInstance.demoMode()){
                return "https://jx4aa4d355e1.hana.ondemand.com/com/merckgroup/fee2/services/"
            }
            return "https://mobile-\(self.scpAccountId).hana.ondemand.com/de.merck.myfeedback/"
        }
    }
    var usalRoot: String{
        get{
            //return "https://jx4aa4d355e1.hana.ondemand.com/com/merckgroup/fee2/services/"
            //return "https://mobile-aa4d355e1.hana.ondemand.com/usal.logic.crud/"
            if(RCValues.sharedInstance.demoMode()){
                return "https://jx4aa4d355e1.hana.ondemand.com/com/merckgroup/usal/logic/xsjs/crud"
            }
            return "https://mobile-\(self.scpAccountId).hana.ondemand.com/usal.logic.crud/"
        }
    }
    
    /**
     Required information for SAP Mobile Services User registration
     **/
    var httpHeaders : HTTPHeaders{
        didSet{
            self.manager.session.configuration.httpAdditionalHeaders = self.httpHeaders
        }
    }
    
    var manager = SessionManager.default
    
    private let registrationInfo: Parameters = [
        "DeviceType": UIDevice.current.model,
        "DeviceModel": "\(UIDevice.current.systemName) \(UIDevice.current.systemVersion)",
        "UserName": UIDevice.current.name
    ]
    
    init(){
        
        // Set Default Headers for HTTP Requests to SAPcpms
        self.httpHeaders = [
            "Content-Type"  : "application/json",
            "Accept"  : "application/json",
            //"Authorization": "Basic TTIwMjY3NjpNZXJjazIwMTc="
        ]
        
        if(RCValues.sharedInstance.demoMode()){
            self.httpHeaders = [
                "Content-Type"  : "application/json",
                "Accept"  : "application/json",
                "Authorization": KeychainSwift().get("basicAuth") ?? ""
            ]
        }
        
        self.manager.session.configuration.httpAdditionalHeaders = httpHeaders
    }
    
    func getHttpHeaders()->HTTPHeaders{
        if(RCValues.sharedInstance.demoMode()){
            self.httpHeaders.updateValue(KeychainSwift().get("basicAuth") ?? "", forKey: "Authorization")
        }
        
        self.manager.session.configuration.httpAdditionalHeaders = httpHeaders
        
        return self.httpHeaders
    }
    
    func fetchReceivedRecognitions()->Promise<[RecognitionReceivedModel]>{
        return Promise(in: .background,  {resolve, reject, _ in
            Alamofire.request(self.serviceRoot+"odata/feedback.xsodata/RecognitionReceived?$expand=UserMasterdata", method: .get, headers: self.getHttpHeaders()).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    let results = json["d"]["results"]
                    resolve(RecognitionReceivedModel.modelsFromDictionaryArray(array: results.arrayObject as! NSArray))
                case .failure(let error):
                    let e = self.handleError(response: response, action: "Get Recognition Received", error: error)
                    print("fetchReceivedRecognitions ERROR:")
                    print(error)
                    reject(e)
                }
            }
        })
    }
    
    func fetchSentRequests()->Promise<[RequestModel]>{
        return Promise(in: .background,  {resolve, reject, _ in
            Alamofire.request(self.serviceRoot+"odata/feedback.xsodata/RequestStateWithData?$expand=Response/UserMasterdata", method: .get, headers: self.getHttpHeaders()).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    let results = json["d"]["results"]
                    resolve(RequestModel.modelsFromDictionaryArray(array: results.arrayObject as! NSArray))
                case .failure(let error):
                    let e = self.handleError(response: response, action: "Get Requests Sent", error: error)
                    print(error)
                    reject(e)
                }
            }
        })
    }
    
    func fetchNewRequests()->Promise<[RequestModel]>{
        return Promise(in: .background,  {resolve, reject, _ in
            Alamofire.request(self.serviceRoot+"odata/feedback.xsodata/RequestResponsePending?$expand=UserMasterdata", method: .get, headers: self.getHttpHeaders()).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    let results = json["d"]["results"]
                    resolve(RequestModel.modelsFromDictionaryArray(array: results.arrayObject as! NSArray))
                case .failure(let error):
                    let e = self.handleError(response: response, action: "Get New Requests", error: error)
                    print("fetchNewRequests ERROR:")
                    print(error)
                    reject(e)
                }
            }
        })
    }
    
    func fetchSentResponses()->Promise<[RequestModel]>{
        return Promise(in: .background,  {resolve, reject, _ in
            Alamofire.request(self.serviceRoot+"odata/feedback.xsodata/RequestResponseAnswered?$expand=UserMasterdata", method: .get, headers: self.getHttpHeaders()).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    let results = json["d"]["results"]
                    resolve(RequestModel.modelsFromDictionaryArray(array: results.arrayObject as! NSArray))
                case .failure(let error):
                    let e = self.handleError(response: response, action: "Get Responses Sent", error: error)
                    print("fetchSentResponses ERROR:")
                    print(error)
                    reject(e)
                }
            }
        })
    }
    
    func getSuggestedUserMasterdata(emails: [String], userId:String)->Promise<[UserMasterdata]>{
        return Promise(in: .background,  {resolve, reject, _ in
            var filterString = "("
            for (i, email) in emails.enumerated(){
                filterString += "tolower(email) eq '\(email.lowercased())'"
                if i < emails.count - 1{
                    filterString += " or "
                }
            }
            filterString += ") and UserID ne '\(userId)'"
            Alamofire.request(self.serviceRoot+"odata/feedback.xsodata/UserMasterdata?$filter="+(filterString.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? ""), method: .get, headers: self.getHttpHeaders()).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    let results = json["d"]["results"]
                    resolve(UserMasterdata.modelsFromDictionaryArray(array: results.arrayObject as! NSArray))
                case .failure(let error):
                    let e = self.handleError(response: response, action: "Get UserMasterData Suggested", error: error)
                    print("getSuggestedUserMasterdata ERROR:")
                    print(error)
                    reject(e)
                }
            }
        })
    }
    
    func searchUserMasterdata(searchWords:[String], userId:String)->Promise<[UserMasterdata]>{
        return Promise(in: .background,  {resolve, reject, _ in
            var filterString = "UserID ne '\(userId)'"
            for str in searchWords{
                filterString += " and substringof('\(str.lowercased())',tolower(concat(concat(firstName,lastName),concat(UserID,department))))"
            }
            Alamofire.request(self.serviceRoot+"odata/feedback.xsodata/UsersRanked?$filter="+(filterString.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? "")+"&$orderby=rank%20desc", method: .get, headers: self.getHttpHeaders()).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    let results = json["d"]["results"]
                    resolve(UserMasterdata.modelsFromDictionaryArray(array: results.arrayObject as! NSArray))
                case .failure(let error):
                    let e = self.handleError(response: response, action: "Get UserMasterData Search", error: error)
                    print("searchUserMasterdata ERROR:")
                    print(error)
                    reject(e)
                }
            }
        })
    }
    
    func fetchSentRecognitions()->Promise<[RecognitionModel]>{
        return Promise(in: .background,  {resolve, reject, _ in
            Alamofire.request(self.serviceRoot+"odata/feedback.xsodata/RecognitionSent?$expand=Receiver/UserMasterdata", method: .get, headers: self.getHttpHeaders()).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    let results = json["d"]["results"]
                    resolve(RecognitionModel.modelsFromDictionaryArray(array: results.arrayObject as! NSArray))
                case .failure(let error):
                    let e = self.handleError(response: response, action: "Get Recognitions Sent", error: error)
                    print("fetchSentRecognitions ERROR:")
                    print(error)
                    reject(e)
                }
            }
        })
    }
    
    func fetchUserMasterdata(from: Int, amount: Int)->Promise<[UserMasterdata]?>{
        return Promise(in: .background,  {resolve, reject, _ in
            Alamofire.request(self.serviceRoot+"odata/feedback.xsodata/UsersRanked?$orderby=rank%20desc&$skip=\(from)&$top=\(amount)", method: .get, headers: self.getHttpHeaders()).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    let results = json["d"]["results"]
                    resolve(UserMasterdata.modelsFromDictionaryArray(array: results.arrayObject as! NSArray))
                case .failure(let error):
                    let e = self.handleError(response: response, action: "Get UserMasterData Pagination", error: error)
                    print("fetchUserMasterdata From ERROR:")
                    print(error)
                    reject(e)
                }
            }
        })
    }
    
    func fetchUserMasterData(userId: String)->Promise<UserMasterdata?>{
        return Promise(in: .background,  {resolve, reject, _ in
            Alamofire.request(self.serviceRoot+"odata/feedback.xsodata/UserMasterdata('\(userId)')", method: .get, headers: self.getHttpHeaders()).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    let results = json["d"]["results"]
                    resolve(UserMasterdata.modelsFromDictionaryArray(array: results.arrayObject as! NSArray).first)
                case .failure(let error):
                    let e = self.handleError(response: response, action: "Get UserMasterData Single User", error: error)
                    print("fetchUserMasterData userID ERROR:")
                    print(error)
                    reject(e)
                }
            }
        })
    }
    
    func fetchResponses(requestID: Int)->Promise<[ResponseModel]>{
        return Promise(in: .background,  {resolve, reject, _ in
            Alamofire.request(self.serviceRoot+"odata/feedback.xsodata/Response?$filter=requestID%20eq%20\(requestID)&$expand=UserMasterdata", method: .get, headers: self.getHttpHeaders()).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    let results = json["d"]["results"]
                    resolve(ResponseModel.modelsFromDictionaryArray(array: results.arrayObject as! NSArray))
                case .failure(let error):
                    let e = self.handleError(response: response, action: "Get Responses for Request", error: error)
                    print("fetchResponses ERROR:")
                    print(error)
                    reject(e)
                }
            }
        })
    }
    
    func fetchTrophies()->Promise<[Trophy]>{
        return Promise(in: .background,  {resolve, reject, _ in
            Alamofire.request(self.serviceRoot+"odata/feedback.xsodata/TrophiesAggregated", method: .get, headers: self.getHttpHeaders()).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    let results = json["d"]["results"]
                    if results.array != nil{
                        var trophies:[Trophy] = []
                        for jsonObject in results.array!{
                            if let pushStr = jsonObject["pushtext"].string, let trophy = Trophy.getTrophy(pushtext: pushStr){
                                trophies.append(trophy)
                            }
                        }
                        resolve(trophies)
                    }
                case .failure(let error):
                    let e = self.handleError(response: response, action: "Get Trophies", error: error)
                    print("fetchTrophies ERROR:")
                    print(error)
                    reject(e)
                }
            }
        })
    }
    
    func fetchNewRequest(requestID: Int, repetition: Int)->Promise<RequestModel?>{
        return Promise(in: .background,  {resolve, reject, _ in
            Alamofire.request(self.serviceRoot+"odata/feedback.xsodata/RequestResponsePending(requestID=\(requestID),repetition=\(repetition))?$expand=UserMasterdata", method: .get, headers: self.getHttpHeaders()).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    if let dict = json["d"].dictionaryObject{
                        resolve(RequestModel.init(dictionary: dict as NSDictionary))
                    }else{
                        resolve(nil)
                    }
                case .failure(let error):
                    let e = self.handleError(response: response, action: "Get New Single Request", error: error)
                    print("fetchNewRequest ERROR:")
                    print(error)
                    reject(e)
                }
            }
        })
    }
    
    func fetchAnsweredRequest(requestID: Int, repetition: Int, raterUID: String)->Promise<RequestModel?>{
        return Promise(in: .background,  {resolve, reject, _ in
            Alamofire.request(self.serviceRoot+"odata/feedback.xsodata/RequestStateWithData(\(requestID))?$expand=Response/UserMasterdata", method: .get, headers: self.getHttpHeaders()).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    if let dict = json["d"].dictionaryObject, let model = RequestModel.init(dictionary: dict as NSDictionary), let responses = model.responses{
                        model.responses = responses.filter({respModel in return respModel.raterUID == raterUID})
                        resolve(model)
                    }else{
                        resolve(nil)
                    }
                case .failure(let error):
                    let e = self.handleError(response: response, action: "Get Single Response", error: error)
                    print("fetchAnsweredRequest ERROR:")
                    print(error)
                    reject(e)
                }
            }
        })
    }
    
    func fetchReceivedRecognition(recognitionID: Int, recognizerUID: String)->Promise<RecognitionReceivedModel?>{
        return Promise(in: .background,  {resolve, reject, _ in
            Alamofire.request(self.serviceRoot+"odata/feedback.xsodata/RecognitionReceived(recognitionID=\(recognitionID),repetition=1,recognizerUID='\(recognizerUID)')?$expand=UserMasterdata", method: .get, headers: self.getHttpHeaders()).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    if let dict = json["d"].dictionaryObject{
                        resolve(RecognitionReceivedModel.init(dictionary: dict as NSDictionary))
                    }else{
                        resolve(nil)
                    }
                case .failure(let error):
                    let e = self.handleError(response: response, action: "Get Recognitions Received", error: error)
                    print("fetchReceivedRecognition ERROR:")
                    print(error)
                    reject(e)
                }
            }
        })
    }
    
    func userSettingsUpdate(params: [String:Any])->Promise<(success:Bool, message:String?)>{
        return Promise(in: .background,  {resolve, reject, _ in
            Alamofire.request(self.serviceRoot+"xsjs/UserSettingsUpdate.xsjs", method: .post, parameters: params, encoding: JSONEncoding.default, headers: self.getHttpHeaders()).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    resolve((true, nil))
                case .failure(let error):
                    let e = self.handleError(response: response, action: "Post User Settings Update", error: error)
                    print("userSettingsUpdate ERROR:")
                    print(error)
                    reject(e)
                }
            }
        })
    }
    
    func userSettingsGet(params: [String:Any])->Promise<UserSettingsModel?>{
        return Promise(in: .background,  {resolve, reject, _ in
            Alamofire.request(self.serviceRoot+"xsjs/UserSettingsGet.xsjs", method: .post, parameters: params, encoding: JSONEncoding.default, headers: self.getHttpHeaders()).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    let results = json["0"]
                    resolve(UserSettingsModel.init(dictionary: results.dictionaryObject as! NSDictionary))
                case .failure(let error):
                    let e = self.handleError(response: response, action: "Post userSettings Get", error: error)
                    print("userSettingsGet ERROR:")
                    print(error)
                    reject(error)
                }
            }
        })
    }
    
    func thankYouNoteHide(recognitionID: Int, receiverUID: String, repetition:Int?)->Promise<(success:Bool, message:String?)>{
        return Promise(in: .background,  {resolve, reject, _ in
            let params: [String:Any] = [
                "recognitionID": recognitionID,
                "repetition": repetition ?? 1,
                "receiverUID": receiverUID,
                "mobileVersion" : Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String
            ]
            Alamofire.request(self.serviceRoot+"xsjs/ThankYouHide.xsjs", method: .post, parameters: params, encoding: JSONEncoding.default, headers: self.getHttpHeaders()).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    print(json)
                    resolve((true, json["responseText"].string))
                case .failure(let error):
                    let e = self.handleError(response: response, action: "Hide Thank You Note", error: error)
                    print("Thank You Note Hide ERROR:")
                    print(error)
                    reject(e)
                }
            }
        })
    }
    
    func thankYouNoteRead(recognitionID: Int, receiverUID: String, repetition:Int?)->Promise<(success:Bool, message:String?)>{
        return Promise(in: .background,  {resolve, reject, _ in
            let params: [String:Any] = [
                "recognitionID": recognitionID,
                "repetition": repetition ?? 1,
                "receiverUID": receiverUID,
                "mobileVersion" : Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String
            ]
            Alamofire.request(self.serviceRoot+"xsjs/ThankYouRead.xsjs", method: .post, parameters: params, encoding: JSONEncoding.default, headers: self.getHttpHeaders()).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    print(json)
                    resolve((true, json["responseText"].string))
                case .failure(let error):
                    let e = self.handleError(response: response, action: "Read Thank You Note", error: error)
                    print("recognitionRead ERROR:")
                    print(error)
                    reject(e)
                }
            }
        })
    }
    
    func recognitionCreate(competencyID: String, text: String, receiverUserIDs: [String])->Promise<(success:Bool, message:String?)>{
        return Promise(in: .background,  {resolve, reject, _ in
            var userIdArray:[[String:String]] = []
            for userId in receiverUserIDs{
                userIdArray.append(["appraiseeUID" : userId])
            }
            let params:[String: Any] = [
                "competencyID": competencyID,
                "text": text,
                "appraiseeUIDs": userIdArray,
                "mobileVersion" : Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String
            ]
            Alamofire.request(self.serviceRoot+"xsjs/RecognitionCreate.xsjs", method: .post, parameters: params, encoding: JSONEncoding.default, headers: self.getHttpHeaders()).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    print(json)
                    resolve((true, json["responseText"].string))
                    //let results = json["d"]["results"]
                case .failure(let error):
                    let e = self.handleError(response: response, action: "Recognition Create", error: error)
                    print("recognitionCreate ERROR:")
                    print(error)
                    reject(e)
                }
            }
        })
    }
    
    func recognitionDelete(recognitionID: Int)->Promise<(success:Bool, message:String?)>{
        return Promise(in: .background,  {resolve, reject, _ in
            let params: [String:Any] = [
                "recognitionID": recognitionID,
                "mobileVersion" : Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String
            ]
            Alamofire.request(self.serviceRoot+"xsjs/RecognitionHide.xsjs", method: .post, parameters: params, encoding: JSONEncoding.default, headers: self.getHttpHeaders()).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    print(json)
                    resolve((true, json["responseText"].string))
                //let results = json["d"]["results"]
                case .failure(let error):
                    let e = self.handleError(response: response, action: "Recognition Delete", error: error)
                    print("recognitionDelete ERROR:")
                    print(error)
                    reject(e)
                }
            }
        })
    }
    
    func recognitionHide(params: [String:Any])->Promise<(success:Bool, message:String?)>{
        return Promise(in: .background,  {resolve, reject, _ in
            
        })
    }
    
    func recognitionRead(recognitionID: Int)->Promise<(success:Bool, message:String?)>{
        return Promise(in: .background,  {resolve, reject, _ in
            let params: [String:Any] = [
                "recognitionID": recognitionID,
                "mobileVersion" : Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String
            ]
            Alamofire.request(self.serviceRoot+"xsjs/RecognitionRead.xsjs", method: .post, parameters: params, encoding: JSONEncoding.default, headers: self.getHttpHeaders()).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    print(json)
                    resolve((true, json["responseText"].string))
                case .failure(let error):
                    let e = self.handleError(response: response, action: "Recognition Read", error: error)
                    print("recognitionRead ERROR:")
                    print(error)
                    reject(e)
                }
            }
        })
    }
    
    func recognitionThankYou(recognitionID: Int)->Promise<(success:Bool, message:String?)>{
        return Promise(in: .background,  {resolve, reject, _ in
            let params: [String:Any] = [
                "recognitionID": recognitionID,
                "mobileVersion" : Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String
            ]
            Alamofire.request(self.serviceRoot+"xsjs/RecognitionThankYou.xsjs", method: .post, parameters: params, encoding: JSONEncoding.default, headers: self.getHttpHeaders()).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    resolve((true, json["responseText"].string))
                case .failure(let error):
                    let e = self.handleError(response: response, action: "Recognition Thank You", error: error)
                    print("recognitionThankYou ERROR:")
                    print(error)
                    reject(e)
                }
            }
        })
    }
    
    func requestDelete(requestID: Int)->Promise<(success:Bool, message:String?)>{
        return Promise(in: .background,  {resolve, reject, _ in
            let params: [String:Any] = [
                "requestID": requestID,
                "mobileVersion" : Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String
            ]
            Alamofire.request(self.serviceRoot+"xsjs/RequestDelete.xsjs", method: .post, parameters: params, encoding: JSONEncoding.default, headers: self.getHttpHeaders()).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    print(json)
                    resolve((true, json["responseText"].string))
                //let results = json["d"]["results"]
                case .failure(let error):
                    let e = self.handleError(response: response, action: "Request Delete", error: error)
                    print("requestDelete ERROR:")
                    print(error)
                    reject(e)
                }
            }
        })
    }
    
    func requestCreate(params: [String:Any])->Promise<(success:Bool, message:String?)>{
        return Promise(in: .background,  {resolve, reject, _ in
          var paramsTemp = params
            paramsTemp.updateValue(Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String, forKey: "mobileVersion")
            Alamofire.request(self.serviceRoot+"xsjs/RequestCreate.xsjs", method: .post, parameters: paramsTemp, encoding: JSONEncoding.default, headers: self.getHttpHeaders()).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    print(json)
                    resolve((true, json["responseText"].string))
                    //resolve((true, "Success"))
                //let results = json["d"]["results"]
                case .failure(let error):
                    let e = self.handleError(response: response, action: "Request Create", error: error)
                    print("requestCreate ERROR:")
                    print(error)
                    reject(e)
                }
            }
        })
    }
    
    func requestThankAll(requestID: Int)->Promise<(success:Bool, message:String?)>{
        return Promise(in: .background,  {resolve, reject, _ in
            let params: [String:Any] = [
                "requestID": requestID,
                "mobileVersion" : Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String
            ]
            Alamofire.request(self.serviceRoot+"xsjs/RequestThankYouAll.xsjs", method: .post, parameters: params, encoding: JSONEncoding.default, headers: self.getHttpHeaders()).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    resolve((true, json["responseText"].string))
                case .failure(let error):
                    let e = self.handleError(response: response, action: "Request Thank All", error: error)
                    print("requestThankAll ERROR:")
                    print(error)
                    reject(e)
                }
            }
        })
    }
    
    func requestThank(requestID: Int, repetition: Int, raterUID: String)->Promise<(success:Bool, message:String?)>{
        return Promise(in: .background,  {resolve, reject, _ in
            let params: [String:Any] = [
                "requestID": requestID,
                "repetition": repetition,
                "raterUID": raterUID,
                "mobileVersion" : Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String
            ]
            Alamofire.request(self.serviceRoot+"xsjs/RequestThankYou.xsjs", method: .post, parameters: params, encoding: JSONEncoding.default, headers: self.getHttpHeaders()).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    resolve((true, json["responseText"].string))
                case .failure(let error):
                    let e = self.handleError(response: response, action: "Request Thank", error: error)
                    print("requestThank ERROR:")
                    print(error)
                    reject(e)
                }
            }
        })
    }
    
    func requestReadAll(requestID: Int)->Promise<(success:Bool, message:String?)>{
        return Promise(in: .background,  {resolve, reject, _ in
            let params: [String:Any] = [
                "requestID": requestID,
                "mobileVersion" : Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String
            ]
            Alamofire.request(self.serviceRoot+"xsjs/RequestResponseReadAll.xsjs", method: .post, parameters: params, encoding: JSONEncoding.default, headers: self.getHttpHeaders()).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    resolve((true, json["responseText"].string))
                case .failure(let error):
                    let e = self.handleError(response: response, action: "Request Read All", error: error)
                    print("requestReadAll ERROR:")
                    print(error)
                    reject(e)
                }
            }
        })
    }
    func requestRespond(requestID: Int, repetition: Int, responseText: String, responseRating: Int)->Promise<(success:Bool, message:String?)>{
        return Promise(in: .background,  {resolve, reject, _ in
            let params: [String:Any] = [
                "requestID": requestID,
                "repetition": repetition,
                "responseText": responseText,
                "responseRating": responseRating,
                "mobileVersion" : Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String
            ]
            Alamofire.request(self.serviceRoot+"xsjs/RequestRespond.xsjs", method: .post, parameters: params, encoding: JSONEncoding.default, headers: self.getHttpHeaders()).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    resolve((true, json["responseText"].string))
                case .failure(let error):
                    let e = self.handleError(response: response, action: "Request Respond", error: error)
                    print("requestRespond ERROR:")
                    print(error)
                    reject(e)
                }
            }
        })
    }
    
    func requestHide(requestID: Int, repetition: Int)->Promise<(success:Bool, message:String?)>{
        return Promise(in: .background,  {resolve, reject, _ in
            let params: [String:Any] = [
            "requestID": requestID,
            "repetition": repetition,
            "mobileVersion" : Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String
            ]
            Alamofire.request(self.serviceRoot+"xsjs/RequestHide.xsjs", method: .post, parameters: params, encoding: JSONEncoding.default, headers: self.getHttpHeaders()).validate().responseJSON { response in
                switch response.result {
                    case .success(let value):
                        let json = JSON(value)
                        resolve((true, json["responseText"].string))
                    case .failure(let error):
                        let e = self.handleError(response: response, action: "Request Hide", error: error)
                        print("requestHide ERROR:")
                        print(error)
                        reject(e)
                }
            }
        })
    }
    
    func responseRead(requestId: Int, repetition: Int, raterUID: String)->Promise<Any?>{
        return Promise(in: .background,  {resolve, reject, _ in
            let params: [String:Any] = [
                "requestID": requestId,
                "repetition": repetition,
                "raterUID": raterUID,
                "mobileVersion" : Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String
            ]
            Alamofire.request(self.serviceRoot+"xsjs/RequestResponseRead.xsjs", method: .post, parameters: params, encoding: JSONEncoding.default, headers: self.getHttpHeaders()).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    resolve((true, json["responseText"].string))
                case .failure(let error):
                    let e = self.handleError(response: response, action: "Request Response Read", error: error)
                    print("responseRead ERROR:")
                    print(error)
                    reject(e)
                }
            }
        })
    }
    
    func registerDevice(considerKeyChainValue: Bool)->Promise<(success:Bool, appCid: String?)> {
        let registrationInfo: Parameters = [
            "DeviceType": UIDevice.current.model,
            "DeviceModel": "\(UIDevice.current.systemName) \(UIDevice.current.systemVersion)",
            "UserName": UIDevice.current.name
        ]
        return Promise(in: .background,  {resolve, reject, _ in

            if(considerKeyChainValue)
            {
                if let appCid = KeychainSwift().get("appCid"){
                    resolve((success: true, appCid: appCid))
                    return
                }
            }
            Alamofire.request("https://mobile-\(self.scpAccountId).hana.ondemand.com/odata/applications/v4/de.merck.myfeedback/Connections", method: .post, parameters: registrationInfo, encoding: JSONEncoding.default, headers: self.getHttpHeaders()).responseString { response in
                
                if response.response?.statusCode != 201 && response.response?.statusCode != 403 {
                    print(response.response?.statusCode)
                    resolve((success: false, appCid: nil))
                } else if response.response?.statusCode == 403 {
                    var noErrorWith403:Bool = true; // Check if the 403 without any error message
                    if let responseText :String = String(data: response.data!, encoding: .utf8)
                    {
                        noErrorWith403 = responseText == "<html><head><title>Error report</title></head><body><h1>HTTP Status 403 - </h1></body></html>";
                    }
                    
                    if (!considerKeyChainValue && !noErrorWith403)
                    {
                       resolve((success: false, appCid: nil))
                    }
                    else if let cookies = HTTPCookieStorage.shared.cookies?.filter({cookied in return cookied.name == "X-SMP-APPCID"}), cookies.count > 0 {
                        let appCid = cookies[0].value
                        KeychainSwift().set(appCid, forKey: "appCid")
                        self.httpHeaders.updateValue(appCid, forKey: "X-SMP-APPCID")
                        resolve((success: true, appCid: appCid))
                    } else {
                        resolve((success: false, appCid: nil))
                    }
                    
                } else {
                    // Retrieve Application Connection ID
                    if let data = response.data {
                        let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments)
                        guard let root = json as? [String: Any], let d = root["d"] as? [String: Any], let appCid = d["ApplicationConnectionId"] as? String else {
                            print("Unable to find Application Connection ID in response payload")
                            reject(webError.webServiceError("Unable to find Application Connection ID in response payload"))
                            return
                        }
                        
                        // It's a success:
                        KeychainSwift().set(appCid, forKey: "appCid")
                        self.httpHeaders.updateValue(appCid, forKey: "X-SMP-APPCID")
                        
                        resolve((success: true, appCid: appCid))
                    } else {
                        resolve((success: false, appCid: nil))
                    }
                }
            }
        })
    }
    
    func registerDevice()->Promise<(success:Bool, appCid: String?)>{
        let registrationInfo: Parameters = [
            "DeviceType": UIDevice.current.model,
            "DeviceModel": "\(UIDevice.current.systemName) \(UIDevice.current.systemVersion)",
            "UserName": UIDevice.current.name
        ]
        
        return Promise(in: .background,  {resolve, reject, _ in
            Alamofire.request("https://mobile-\(self.scpAccountId).hana.ondemand.com/odata/applications/v4/de.merck.myfeedback/Connections", method: .post, parameters: registrationInfo, encoding: JSONEncoding.default, headers: self.getHttpHeaders()).responseString { response in
                
                if response.response?.statusCode != 201 && response.response?.statusCode != 403{
                    print(response.response?.statusCode.description ?? "")
                    reject(webError.webServiceError("\(response.response!.statusCode)"))
                    
                }else if response.response?.statusCode == 403{
                    if let cookies = HTTPCookieStorage.shared.cookies?.filter({cookied in return cookied.name == "X-SMP-APPCID"}), cookies.count > 0{
                        let appCid = cookies[0].value
                        KeychainSwift().set(appCid, forKey: "appCid")
                        self.httpHeaders.updateValue(appCid, forKey: "X-SMP-APPCID")
                        resolve((success: true, appCid: appCid))
                    }
                    
                }else{
                    // Retrieve Application Connection ID
                    if let data = response.data{
                        let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments)
                        guard let root = json as? [String: Any], let d = root["d"] as? [String:Any], let appCid = d["ApplicationConnectionId"] as? String else {
                            return
                        }
                        
                        // It's a success:
                        KeychainSwift().set(appCid, forKey: "appCid")
                        self.httpHeaders.updateValue(appCid, forKey: "X-SMP-APPCID")
                        
                        resolve((success: true, appCid: appCid))
                    }else if let e = response.error{
                        reject(e)
                    }
                }
            }
        })
    }
    
    private func handleError(response: DataResponse<Any>, action: String, error: Error?)->Error{
        if let data = response.data, let dataStr = String(data: data, encoding: .utf8){
            let url = response.request?.url?.absoluteString.replacingOccurrences(of: serviceRoot, with: "/")
            if let contentType = response.response?.allHeaderFields["Content-Type"] as? String{
                print("contentType:")
                print(contentType)
                print("Body:")
                print(dataStr)
                print("Status Code:")
                print(response.response?.statusCode)
                print("Url")
                print(response.response?.url)
                switch contentType{
                case "application/json":
                    let json = JSON.init(parseJSON: dataStr)
                    if let errorJson = json["error"].dictionary, let errorObject = errorJson["message"]?.dictionary, let errorString = errorObject["value"]?.string{
                        self.writeToLog(action: action, status: "ERROR", message: errorString)
                        return webError.webServiceError(errorString+"\n(url: \(url ?? action)")
                    }else if let jsonDict = json.dictionaryObject, let warning = jsonDict["warning"] as? String{
                        self.writeToLog(action: action, status: "WARNING", message: warning)
                        return webError.webServiceError(warning+"\n(url: \(url ?? action)")
                    }else if let jsonDict = json.dictionaryObject, let eString = jsonDict["responseText"] as? String{
                        self.writeToLog(action: action, status: "ERROR", message: eString)
                        return webError.webServiceError(eString+"\n(url: \(url ?? action)")
                    }
                case "text/html", "text/html;charset=utf-8":
                    self.writeToLog(action: action, status: "WARNING", message: "Session Expired")
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.redirect2LoginVC();
                    break
                default:
                    break
                }
            }
        }
        if error != nil{
            self.writeToLog(action: action, status: "ERROR", message: error?.localizedDescription)
            return error!
        }
        self.writeToLog(action: action, status: "ERROR", message: "Unknown Error\n Status Code: \(response.response?.statusCode ?? 0)")
        return webError.webServiceError("Unknown Error\n Status Code: \(response.response?.statusCode ?? 0)")
    }
    
    func writeUsalLog(action: String){
        //self.writeToLog(action: action, status: "SUCCESS", message: nil)
        let params:[String: Any] = [
            "UserID": loggedInUser.value?.userId,
            "Application": "FEED",
            "Action": action,
            "Type": "Mobile",
            "Version": Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String,
            ]
        Alamofire.request(self.usalRoot+"InsertLog.xsjs", method: .post, parameters: params, encoding: JSONEncoding.default, headers: SAPcpms.shared.getHttpHeaders()).responseJSON(completionHandler: { (response) in
            
        })
    }
    
    private func writeToLog(action: String, status: String, message: String?){
        let params: [String: Any] = [
            "status": status,
            "action": action,
            "message": message,
            "mobileVersion": Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String
        ]
        
        
        let servicePath = "/log/writeLog.xsjs"
        
        Alamofire.request(self.serviceRoot+"xsjs/WriteLog.xsjs", method: .post, parameters: params, encoding: JSONEncoding.default, headers: SAPcpms.shared.getHttpHeaders()).responseJSON(completionHandler: { (response) in
            print("\(response.response?.statusCode) - \(action) - \(message)")
        })
    }
    
    func dicToJson(dic: Dictionary<String, String>) -> Data {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            return jsonData
        } catch {
            return "{}".data(using: .utf8)!
        }
    }
}
