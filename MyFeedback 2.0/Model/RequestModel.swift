//
//  RequestModel.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 19.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import Foundation

public class RequestModel{
    public var requestID : Int?
    public var repetition : Int?
    public var requestorUID : String?
    public var requestCategoryID : String?
    public var type : String?
    public var text : String?
    public var scaleMinText : String?
    public var scaleMaxText : String?
    public var title : String?
    public var responseText : String?
    public var responseRating : Int?
    public var readFlag : Int?
    public var thankYouFlag : Int?
    public var thankYouReadFlag : Int?
    public var repetitionTimestamp : String?
    public var userMasterdata : UserMasterdata?
    public var responses: [ResponseModel]?
    public var sendTimestamp : String?
    public var continuousInterval: String?
    public var continuousRepetitions: Int?
    public var averageResponseRating: String?
    public var percentage0: String?
    public var percentage1: String?
    public var percentage2: String?
    public var percentage3: String?
    public var ratingCount0: Int?
    public var ratingCount1: Int?
    public var ratingCount2: Int?
    public var ratingCount3: Int?
    public var noThankYouCount: String?
    public var responseUnreadCount: String?
    
    
    /**
     Returns an array of models based on given dictionary.
     
     Sample usage:
     let results_list = Results.modelsFromDictionaryArray(someDictionaryArrayFromJSON)
     
     - parameter array:  NSArray from JSON dictionary.
     
     - returns: Array of Results Instances.
     */
    public class func modelsFromDictionaryArray(array:NSArray) -> [RequestModel]
    {
        var models:[RequestModel] = []
        for item in array
        {
            if let dict = item as? NSDictionary, let model = RequestModel(dictionary: dict){
                models.append(model)
            }
            
        }
        return models
    }
    
    /**
     Constructs the object based on the given dictionary.
     
     Sample usage:
     let results = Results(someDictionaryFromJSON)
     
     - parameter dictionary:  NSDictionary from JSON.
     
     - returns: Results Instance.
     */
    required public init?(dictionary: NSDictionary) {
        
        requestID = dictionary["requestID"] as? Int
        repetition = dictionary["repetition"] as? Int
        requestorUID = dictionary["requestorUID"] as? String
        requestCategoryID = dictionary["requestCategoryID"] as? String
        type = dictionary["type"] as? String
        text = dictionary["text"] as? String
        scaleMinText = dictionary["scaleMinText"] as? String
        scaleMaxText = dictionary["scaleMaxText"] as? String
        title = dictionary["title"] as? String
        responseText = dictionary["responseText"] as? String
        responseRating = dictionary["responseRating"] as? Int
        readFlag = dictionary["readFlag"] as? Int
        thankYouFlag = dictionary["thankYouFlag"] as? Int
        thankYouReadFlag = dictionary["thankYouReadFlag"] as? Int
        repetitionTimestamp = dictionary["repetitionTimestamp"] as? String
        if (dictionary["UserMasterdata"] != nil && dictionary["UserMasterdata"] as? NSDictionary != nil) { userMasterdata = UserMasterdata(dictionary: dictionary["UserMasterdata"] as! NSDictionary) }
        if (dictionary["Response"] as? NSDictionary != nil && (dictionary["Response"] as! NSDictionary)["results"] != nil) { responses = ResponseModel.modelsFromDictionaryArray(array: (dictionary["Response"] as! NSDictionary)["results"] as! NSArray) }
        sendTimestamp = dictionary["sendTimestamp"] as? String
        continuousInterval = dictionary["continuousInterval"] as? String
        continuousRepetitions = dictionary["continuousRepetitions"] as? Int
        averageResponseRating = dictionary["averageResponseRating"] as? String
        percentage0 = dictionary["percentage0"] as? String
        percentage1 = dictionary["percentage1"] as? String
        percentage2 = dictionary["percentage2"] as? String
        percentage3 = dictionary["percentage3"] as? String
        ratingCount0 = dictionary["ratingCount0"] as? Int
        ratingCount1 = dictionary["ratingCount1"] as? Int
        ratingCount2 = dictionary["ratingCount2"] as? Int
        ratingCount3 = dictionary["ratingCount3"] as? Int
        noThankYouCount = dictionary["noThankYouCount"] as? String
        responseUnreadCount = dictionary["responseUnreadCount"] as? String
    }
    
    
    /**
     Returns the dictionary representation for the current instance.
     
     - returns: NSDictionary.
     */
    public func dictionaryRepresentation() -> NSDictionary {
        
        let dictionary = NSMutableDictionary()
        
        dictionary.setValue(self.requestID, forKey: "requestID")
        dictionary.setValue(self.repetition, forKey: "repetition")
        dictionary.setValue(self.requestorUID, forKey: "requestorUID")
        dictionary.setValue(self.requestCategoryID, forKey: "requestCategoryID")
        dictionary.setValue(self.type, forKey: "type")
        dictionary.setValue(self.text, forKey: "text")
        dictionary.setValue(self.scaleMinText, forKey: "scaleMinText")
        dictionary.setValue(self.scaleMaxText, forKey: "scaleMaxText")
        dictionary.setValue(self.title, forKey: "title")
        dictionary.setValue(self.responseText, forKey: "responseText")
        dictionary.setValue(self.responseRating, forKey: "responseRating")
        dictionary.setValue(self.readFlag, forKey: "readFlag")
        dictionary.setValue(self.thankYouFlag, forKey: "thankYouFlag")
        dictionary.setValue(self.thankYouReadFlag, forKey: "thankYouReadFlag")
        dictionary.setValue(self.repetitionTimestamp, forKey: "repetitionTimestamp")
        dictionary.setValue(self.userMasterdata?.dictionaryRepresentation(), forKey: "UserMasterdata")
        dictionary.setValue(self.responses?.map({rec in rec.dictionaryRepresentation()}), forKey: "Response")
        dictionary.setValue(self.sendTimestamp, forKey: "sendTimestamp")
        dictionary.setValue(self.continuousInterval, forKey: "continuousInterval")
        dictionary.setValue(self.continuousRepetitions, forKey: "continuousRepetitions")
        dictionary.setValue(self.averageResponseRating, forKey: "averageResponseRating")
        dictionary.setValue(self.percentage0, forKey: "percentage0")
        dictionary.setValue(self.percentage1, forKey: "percentage1")
        dictionary.setValue(self.percentage2, forKey: "percentage2")
        dictionary.setValue(self.percentage3, forKey: "percentage3")
        dictionary.setValue(self.ratingCount0, forKey: "ratingCount0")
        dictionary.setValue(self.ratingCount1, forKey: "ratingCount1")
        dictionary.setValue(self.ratingCount2, forKey: "ratingCount2")
        dictionary.setValue(self.ratingCount3, forKey: "ratingCount3")
        dictionary.setValue(self.noThankYouCount, forKey: "noThankYouCount")
        dictionary.setValue(self.responseUnreadCount, forKey: "responseUnreadCount")
        
        return dictionary
    }
    
    func emptyResponse(emp: Employee?)->RequestResponse?{
        let employee: Employee = emp ?? Employee.invalid()
        guard self.requestID != nil, self.repetition != nil else{
            return nil
        }
        let response = RequestResponse(requestId: self.requestID!, rater: employee, repetition: self.repetition!, text: nil, rating: nil, responseTimestamp: nil, readFlag: nil, thankYouFlag: nil)
        
        return response
    }
    
    func request()->Request?{
        let employee:Employee?
        var responses:[RequestResponse] = []
        if self.responses != nil{
            employee = loggedInUser.value
            for model in self.responses!{
                if let response = model.response(){
                    responses.append(response)
                }
            }
        }else{
            employee = userMasterdata?.employee() ?? Employee.invalid()
        }
        guard employee != nil, self.requestCategoryID != nil, self.type != nil, self.text != nil else{
            return nil
        }
        let category = RequestCategory.fromId(name: self.requestCategoryID!)
        let type = RequestType.fromKey(key: self.type!)
        let request = Request.init(id: self.requestID, requester: employee!, category: category, type: type, text: self.text!, title: self.title, date: getDate(str: self.sendTimestamp ?? self.repetitionTimestamp), continuousInterval: nil, continuousRepetitions: self.continuousRepetitions ?? self.repetition ?? 1, scaleTextMin: self.scaleMinText, scaleTextMax: self.scaleMaxText, averageResponseRating: Double.init(self.averageResponseRating ?? "nil"))
        request.responses = responses
        if RCValues.sharedInstance.getBoolValue(ValueKey.showPercentage){
            if type.key == RequestType.YesNo.key && self.percentage0 != nil && self.percentage1 != nil{
                if let perc0 = Double(self.percentage0!), let perc1 = Double(self.percentage1!){
                    request.ratingDistribution = [perc0*100, perc1*100]
                }
                
            }else if type.key == RequestType.Smileys.key && self.percentage1 != nil && self.percentage2 != nil && self.percentage3 != nil{
                if let perc1 = Double(self.percentage1!), let perc2 = Double(self.percentage2!), let perc3 = Double(self.percentage3!){
                    request.ratingDistribution = [perc1*100, perc2*100, perc3*100]
                }
                
            }
        }else{
            if type.key == RequestType.YesNo.key && self.ratingCount0 != nil && self.ratingCount1 != nil{
                request.ratingDistribution = [Double(self.ratingCount0!), Double(self.ratingCount1!)]
                
            }else if type.key == RequestType.Smileys.key && self.ratingCount1 != nil && self.ratingCount2 != nil && self.ratingCount3 != nil{
                request.ratingDistribution = [ Double(self.ratingCount1!),  Double(self.ratingCount2!),  Double(self.ratingCount3!)]
                
            }
        }
        
        
        request.thankYouAllowed = self.noThankYouCount != nil && Int(self.noThankYouCount!) != nil && Int(self.noThankYouCount!)! > 0
        request.unreadCount = Int(self.responseUnreadCount ?? "0") ?? 0
        
        return request
    }
    
    func getDate(str: String?)->Date?{
        if str == nil{
            return nil
        }
        var splittetTimeStamp = str!.split(separator: "(")
        splittetTimeStamp = splittetTimeStamp[1].split(separator: ")")
        let timeStampStr = String(splittetTimeStamp[0])
        let timeInterval = Double(timeStampStr)
        if timeInterval != nil{
            let resultDate = Date(timeIntervalSince1970: timeInterval!/1000)
            return resultDate
        }else{
            return nil
        }
    }
    
}
