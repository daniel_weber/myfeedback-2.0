//
//  EmployeeModel.swift
//  MIA
//
//  Created by Daniel Weber on 16.10.17.
//  Copyright © 2017 zero2one. All rights reserved.
//

import Foundation
import SwiftyJSON
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class EmployeeModel: NSObject, NSCoding {
    public var gID : Int?
    public var mUID : String?
    public var email : String?
    public var firstName : String?
    public var lastName : String?
    public var department : String?
    public var departmentText : String?
    public var departmentOrg : String?
    public var firstNameLower : String?
    public var lastNameLower : String?
    public var userID : String?
    public var fullName : String?
    
    /**
     Returns an array of models based on given dictionary.
     
     Sample usage:
     let employeeModel_list = EmployeeModel.modelsFromDictionaryArray(someDictionaryArrayFromJSON)
     
     - parameter array:  NSArray from JSON dictionary.
     
     - returns: Array of EmployeeModel Instances.
     */
    public class func modelsFromDictionaryArray(array:NSArray) -> [EmployeeModel]
    {
        var models:[EmployeeModel] = []
        for item in array
        {
            models.append(EmployeeModel(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    /**
     Constructs the object based on the given dictionary.
     
     Sample usage:
     let employeeModel = EmployeeModel(someDictionaryFromJSON)
     
     - parameter dictionary:  NSDictionary from JSON.
     
     - returns: EmployeeModel Instance.
     */
    required public init?(dictionary: NSDictionary) {
        
        gID = dictionary["GID"] as? Int
        mUID = dictionary["MUID"] as? String
        email = dictionary["email"] as? String
        firstName = dictionary["firstNameGlobal"] as? String
        lastName = dictionary["lastNameGlobal"] as? String
        department = dictionary["department"] as? String
        departmentText = dictionary["departmentText"] as? String
        departmentOrg = dictionary["departmentOrg"] as? String
        firstNameLower = dictionary["firstName"] as? String
        lastNameLower = dictionary["lastName"] as? String
        userID = dictionary["UserID"] as? String
        fullName = dictionary["fullName"] as? String
    }
    required public init?(json: JSON) {
        
        gID = json["GID"] as? Int
        mUID = json["MUID"] as? String
        email = json["email"] as? String
        firstName = json["firstNameGlobal"] as? String
        lastName = json["lastNameGlobal"] as? String
        department = json["department"] as? String
        departmentText = json["departmentText"] as? String
        departmentOrg = json["departmentOrg"] as? String
        firstNameLower = json["firstName"] as? String
        lastNameLower = json["lastName"] as? String
        userID = json["UserID"] as? String
        fullName = json["fullName"] as? String
    }
    
    
    /**
     Returns the dictionary representation for the current instance.
     
     - returns: NSDictionary.
     */
    public func dictionaryRepresentation() -> NSDictionary {
        
        let dictionary = NSMutableDictionary()
        
        dictionary.setValue(self.gID, forKey: "GID")
        dictionary.setValue(self.mUID, forKey: "MUID")
        dictionary.setValue(self.email, forKey: "email")
        dictionary.setValue(self.firstName, forKey: "firstNameGlobal")
        dictionary.setValue(self.lastName, forKey: "lastNameGlobal")
        dictionary.setValue(self.department, forKey: "department")
        dictionary.setValue(self.departmentText, forKey: "departmentText")
        dictionary.setValue(self.departmentOrg, forKey: "departmentOrg")
        dictionary.setValue(self.firstNameLower, forKey: "firstName")
        dictionary.setValue(self.lastNameLower, forKey: "lastName")
        dictionary.setValue(self.userID, forKey: "UserID")
        dictionary.setValue(self.fullName, forKey: "fullName")
        
        return dictionary
    }
    
    func EmployeeFromModel() -> Employee?{
        guard userID != nil, mUID != nil, firstNameLower != nil, lastNameLower != nil, email != nil, department != nil else{
            return nil
        }
        return Employee(userId: userID!, muid: mUID!, firstName: firstNameLower!, lastName: lastNameLower!, email: email!, orgUnitName: department!)
    }
    
    //MARK: Archiving
    
    
    
    public func encode(with aCoder: NSCoder) {
        
        aCoder.encode(self.dictionaryRepresentation(), forKey: "dictonary")
        
        /*aCoder.encode(self.gID, forKey: "GID")
         aCoder.encode(self.mUID, forKey: "MUID")
         aCoder.encode(self.email, forKey: "email")
         aCoder.encode(self.firstName, forKey: "firstNameGlobal")
         aCoder.encode(self.lastName, forKey: "lastNameGlobal")
         aCoder.encode(self.department, forKey: "department")
         aCoder.encode(self.departmentText, forKey: "departmentText")
         aCoder.encode(self.departmentOrg, forKey: "departmentOrg")
         aCoder.encode(self.firstNameLower, forKey: "firstName")
         aCoder.encode(self.lastNameLower, forKey: "lastName")
         aCoder.encode(self.userID, forKey: "UserID")
         aCoder.encode(self.fullName, forKey: "fullName")*/
    }
    
    required convenience public init?(coder aDecoder: NSCoder) {
        // The name is required. If we cannot decode a name string, the initializer should fail.
        
        if let dict = aDecoder.decodeObject(forKey: "dictonary") as? NSDictionary{
            self.init(dictionary: dict)
        }else{
            return nil
        }
        
    }
    
    //MARK: Archiving Paths
    
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("employeeModel")
    
}


