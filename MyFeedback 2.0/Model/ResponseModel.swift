//
//  ResponseModel.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 19.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//
import Foundation

public class ResponseModel {
    public var requestID : Int?
    public var raterUID : String?
    public var repetition : Int?
    public var responseText : String?
    public var responseRating : Int?
    public var responseTimestamp : String?
    public var responseFlag : Int?
    public var responseReadFlag : Int?
    public var thankYouFlag : Int?
    public var userMasterdata : UserMasterdata?
    
    /**
     Returns an array of models based on given dictionary.
     
     Sample usage:
     let json4Swift_Base_list = Json4Swift_Base.modelsFromDictionaryArray(someDictionaryArrayFromJSON)
     
     - parameter array:  NSArray from JSON dictionary.
     
     - returns: Array of Json4Swift_Base Instances.
     */
    public class func modelsFromDictionaryArray(array:NSArray) -> [ResponseModel]
    {
        var models:[ResponseModel] = []
        for item in array
        {
            models.append(ResponseModel(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    /**
     Constructs the object based on the given dictionary.
     
     Sample usage:
     let json4Swift_Base = Json4Swift_Base(someDictionaryFromJSON)
     
     - parameter dictionary:  NSDictionary from JSON.
     
     - returns: Json4Swift_Base Instance.
     */
    required public init?(dictionary: NSDictionary) {
        
        requestID = dictionary["requestID"] as? Int
        raterUID = dictionary["raterUID"] as? String
        repetition = dictionary["repetition"] as? Int
        responseText = dictionary["responseText"] as? String
        responseRating = dictionary["responseRating"] as? Int
        responseTimestamp = dictionary["responseTimestamp"] as? String
        responseFlag = dictionary["responseFlag"] as? Int
        responseReadFlag = dictionary["responseReadFlag"] as? Int
        thankYouFlag = dictionary["thankYouFlag"] as? Int
        if (dictionary["UserMasterdata"] as? NSDictionary != nil) { userMasterdata = UserMasterdata(dictionary: dictionary["UserMasterdata"] as! NSDictionary) }
    }
    
    
    /**
     Returns the dictionary representation for the current instance.
     
     - returns: NSDictionary.
     */
    public func dictionaryRepresentation() -> NSDictionary {
        
        let dictionary = NSMutableDictionary()
        
        dictionary.setValue(self.requestID, forKey: "requestID")
        dictionary.setValue(self.raterUID, forKey: "raterUID")
        dictionary.setValue(self.repetition, forKey: "repetition")
        dictionary.setValue(self.responseText, forKey: "responseText")
        dictionary.setValue(self.responseRating, forKey: "responseRating")
        dictionary.setValue(self.responseTimestamp, forKey: "responseTimestamp")
        dictionary.setValue(self.responseFlag, forKey: "responseFlag")
        dictionary.setValue(self.responseReadFlag, forKey: "responseReadFlag")
        dictionary.setValue(self.thankYouFlag, forKey: "thankYouFlag")
        dictionary.setValue(self.userMasterdata?.dictionaryRepresentation(), forKey: "UserMasterdata")
        
        return dictionary
    }
    
    func response()->RequestResponse?{
        let emp = userMasterdata?.employee() ?? Employee.invalid()
        guard self.requestID != nil, self.repetition != nil else{
            return nil
        }
        let response = RequestResponse(requestId: self.requestID!, rater: emp, repetition: self.repetition!, text: self.responseText, rating: self.responseRating, responseTimestamp: getDate(str: self.responseTimestamp), readFlag: self.responseReadFlag, thankYouFlag: self.thankYouFlag)
        return response
    }
    
    func getDate(str: String?)->Date?{
        if str == nil{
            return nil
        }
        var splittetTimeStamp = str!.split(separator: "(")
        splittetTimeStamp = splittetTimeStamp[1].split(separator: ")")
        let timeStampStr = String(splittetTimeStamp[0])
        let timeInterval = Double(timeStampStr)
        if timeInterval != nil{
            let resultDate = Date(timeIntervalSince1970: timeInterval!/1000)
            return resultDate
        }else{
            return nil
        }
    }
    
}
