//
//  RecognitionReceivedModel.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 21.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import Foundation

public class RecognitionReceivedModel {
    public var recognitionID : Int?
    public var repetition : Int?
    public var recognizerUID : String?
    public var competencyID : String?
    public var text : String?
    public var sendTimestamp : String?
    public var readFlag : Int?
    public var thankYouFlag : Int?
    public var userMasterdata : UserMasterdata?
    
    /**
     Returns an array of models based on given dictionary.
     
     Sample usage:
     let json4Swift_Base_list = Json4Swift_Base.modelsFromDictionaryArray(someDictionaryArrayFromJSON)
     
     - parameter array:  NSArray from JSON dictionary.
     
     - returns: Array of Json4Swift_Base Instances.
     */
    public class func modelsFromDictionaryArray(array:NSArray) -> [RecognitionReceivedModel]
    {
        var models:[RecognitionReceivedModel] = []
        for item in array
        {
            models.append(RecognitionReceivedModel(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    /**
     Constructs the object based on the given dictionary.
     
     Sample usage:
     let json4Swift_Base = Json4Swift_Base(someDictionaryFromJSON)
     
     - parameter dictionary:  NSDictionary from JSON.
     
     - returns: Json4Swift_Base Instance.
     */
    required public init?(dictionary: NSDictionary) {
        
        recognitionID = dictionary["recognitionID"] as? Int
        repetition = dictionary["repetition"] as? Int
        recognizerUID = dictionary["recognizerUID"] as? String
        competencyID = dictionary["competencyID"] as? String
        text = dictionary["text"] as? String
        sendTimestamp = dictionary["sendTimestamp"] as? String
        readFlag = dictionary["readFlag"] as? Int
        thankYouFlag = dictionary["thankYouFlag"] as? Int
        if (dictionary["UserMasterdata"] as? NSDictionary != nil) { userMasterdata = UserMasterdata(dictionary: dictionary["UserMasterdata"] as! NSDictionary) }
    }
    
    
    /**
     Returns the dictionary representation for the current instance.
     
     - returns: NSDictionary.
     */
    public func dictionaryRepresentation() -> NSDictionary {
        
        let dictionary = NSMutableDictionary()
        
        dictionary.setValue(self.recognitionID, forKey: "recognitionID")
        dictionary.setValue(self.repetition, forKey: "repetition")
        dictionary.setValue(self.recognizerUID, forKey: "recognizerUID")
        dictionary.setValue(self.competencyID, forKey: "competencyID")
        dictionary.setValue(self.text, forKey: "text")
        dictionary.setValue(self.sendTimestamp, forKey: "sendTimestamp")
        dictionary.setValue(self.readFlag, forKey: "readFlag")
        dictionary.setValue(self.thankYouFlag, forKey: "thankYouFlag")
        dictionary.setValue(self.userMasterdata?.dictionaryRepresentation(), forKey: "UserMasterdata")
        
        return dictionary
    }
    
    func recognition()->Recognition?{
        let readTimestamp: Date?
        if self.readFlag == 1{
            readTimestamp = Date()
        }else{
            readTimestamp = nil
        }
        let thankYouTimestamp: Date?
        if self.thankYouFlag == 1{
            thankYouTimestamp = Date()
        }else{
            thankYouTimestamp = nil
        }
        let receiver = RecognitionReceiver(recognitionId: self.recognitionID!, receiver: loggedInUser.value!, readTimestamp: readTimestamp, thankYouTimestamp: thankYouTimestamp, hideTimestamp: nil)
        let emp = self.userMasterdata?.employee() ?? Employee.invalid()
        guard self.recognitionID != nil, self.text != nil, self.competencyID != nil, let competency = Competency.getCompetency(name: self.competencyID!) else{
            return nil
        }
        let recognition = Recognition(id: self.recognitionID!, sender: emp, competency: competency, text: self.text!, date: getDate(str: self.sendTimestamp), repetition: self.repetition)
        recognition.receivers = [receiver]
        return recognition
    }
    
    func getDate(str: String?)->Date?{
        if str == nil{
            return nil
        }
        var splittetTimeStamp = str!.split(separator: "(")
        splittetTimeStamp = splittetTimeStamp[1].split(separator: ")")
        let timeStampStr = String(splittetTimeStamp[0])
        let timeInterval = Double(timeStampStr)
        if timeInterval != nil{
            let resultDate = Date(timeIntervalSince1970: timeInterval!/1000)
            return resultDate
        }else{
            return nil
        }
    }
    
}
