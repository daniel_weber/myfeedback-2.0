//
//  ViewController.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 25.02.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import UIKit
import MessageUI
import IQKeyboardManagerSwift

class ViewController: UIViewController {

    @IBOutlet weak var container: UIView!
    @IBOutlet weak var searchBar: BaseSearchBar!
    @IBOutlet weak var navigationBar: AlternativeNavigationBar!
    
    var blurredView: UIView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Dip.registerNavigationBar(navigationBar: self.navigationBar)
        self.searchBar.isCollapsed = true
        Dip.registerSearchBar(searchBar: self.searchBar)
        
        let width = self.view!.frame.width
        let bg = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 20))
        bg.backgroundColor = MerckColor.VibrantGreen
        Dip.registerStatusBar(statusBar: bg)
        self.view.addSubview(bg)
        
        let mvc = try! Dip.container.resolve() as ModalViewController
        
        self.view.backgroundColor = MerckColor.VibrantGreen
        
        view.addSubview(mvc.view)
        mvc.view.edges(to: view)
        
        IQKeyboardManager.sharedManager().canAdjustAdditionalSafeAreaInsets = true
        IQKeyboardManager.sharedManager().layoutIfNeededOnUpdate = true
        
        loggedInUser.observeNext(with: {e in
            if(e == nil){
                 self.showBlur()
            }else{
                self.hideBlur()
            }
        })
        if(loggedInUser.value == nil){
            self.showBlur()
        }
        
        //let devc = try! Dip.container.resolve() as DummyEmailViewController
        //view.addSubview(devc.view)
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "embedNavigationController") {
            self.navigationBar.navigationController = segue.destination as? UINavigationController
        }
    }
    
    func showBlur(){
        self.hideBlur()
        let blurEffect = UIBlurEffect(style: .dark)
        let blurredEffectView = UIVisualEffectView(effect: blurEffect)
        blurredEffectView.frame = UIScreen.main.bounds
        self.blurredView = blurredEffectView
        self.view.addSubview(blurredEffectView)
    }
    
    func hideBlur(){
        self.blurredView?.removeFromSuperview()
    }


}

