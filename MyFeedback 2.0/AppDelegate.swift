//
//  AppDelegate.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 25.02.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import UserNotifications
import Firebase
import Fabric
import Crashlytics
import KeychainSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    let service = try! Dip.container.resolve() as FeedbackService
    let messageService = try! Dip.container.resolve() as MessageHandling
    let deeplinking = try! Dip.container.resolve() as Deeplinking
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        let center = UNUserNotificationCenter.current()
        center.delegate = messageService
        center.requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in
            // Enable or disable features based on authorization.
        }
        
        // Background Fetch
        UIApplication.shared.setMinimumBackgroundFetchInterval(UIApplicationBackgroundFetchIntervalMinimum)
        
        FirebaseApp.configure()
        Fabric.with([Crashlytics.self])
        let rcValues = RCValues.sharedInstance
        
        
        /*
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let main = UIStoryboard.init(name: "Main", bundle: nil)
        
        let loginVC = main.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        loginVC.result = {(employee, success) in
            loggedInUser.value = employee
            SAPcpms.shared.registerDevice().then({resp in
                let rootVC = main.instantiateInitialViewController()
                self.window?.rootViewController = rootVC
                self.window?.makeKeyAndVisible()
                Loading.show()
                let mvc : ModalViewController = try! Dip.container.resolve() as ModalViewController
                if let trophyIDStr = KeychainSwift().get("trophy"), let trophyID = Int(trophyIDStr), let trophy = Trophy.getTrophy(key: trophyID){
                    let trophyView = TrophyModalView()
                    trophyView.isInOverview = false
                    trophyView.trophy = trophy
                    trophyView.result = {str, success in
                        KeychainSwift().delete("trophy")
                        if success{
                            self.deeplinking.redirectToTrophies().then({_ in
                                Loading.hide()
                            })
                        }
                        return success
                    }
                    mvc.present(modal: trophyView)
                }
                if KeychainSwift().get("lastOpened") == nil{
                    Onboarding.show()
                }
                KeychainSwift().set(Date().string(), forKey: "lastOpened")
                application.registerForRemoteNotifications()
                self.service.getSettings().then({settings in
                    if settings != nil{
                        self.service.userSettings.value = settings!
                        self.service.update().then({_ in
                            self.checkForUpdate()
                        }).catch({e in
                            ErrorView.show(error: e)
                        })
                    }
                }).catch({e in
                    ErrorView.show(error: e)
                })
                
            })
            return true
        }
        
        self.window?.rootViewController = loginVC
        self.window?.makeKeyAndVisible()
        */
        
        
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().toolbarDoneBarButtonItemText = "Done".localized()
        IQKeyboardManager.sharedManager().toolbarBarTintColor = MerckColor.VibrantGreen
        IQKeyboardManager.sharedManager().toolbarTintColor = MerckColor.RichPurple
        IQKeyboardManager.sharedManager().shouldResignOnTouchOutside = true
        
        self.service.userSettings.observe(with: {_ in
            IQKeyboardManager.sharedManager().toolbarDoneBarButtonItemText = "Done".localized()
        })

        DispatchQueue.main.async {
            application.applicationIconBadgeNumber = self.service.amountTotal.value
            self.service.amountTotal.observeNext(with: {value in
                DispatchQueue.main.async {
                    application.applicationIconBadgeNumber = value
                }
            })
        }
        
        /*Timer.scheduledTimer(withTimeInterval: 60, repeats: true, block: {_ in
            self.service.getSettings().then({settings in
                if settings != nil{
                    self.service.userSettings.value = settings!
                }
            })
        })*/
        
        rcValues.fetchCloudValues().then({_ in
            self.redirect2LoginVC();
        }).catch({e in
            print(e)
            self.redirect2LoginVC();
        })
        return true
    }
    
    // Support for background fetch
    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        completionHandler(UIBackgroundFetchResult.newData)
        self.service.update().then({_ in })
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        messageService.apnsToken = deviceToken
    }
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        Messaging.messaging().appDidReceiveMessage(userInfo)
        messageService.handleTrophyPayload(userInfo: userInfo)
        self.service.update().then({_ in })
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        
        if userActivity.activityType == NSUserActivityTypeBrowsingWeb{
            let webURL = userActivity.webpageURL!
            
            if let query = webURL.query, query.contains("YJ5F"){
                // Handler for Open From Email
                self.service.AnalyticsOpenApplicationFromMail()
            }
        }
        
        return true
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack
    
    func checkForUpdate(){
        if !RCValues.sharedInstance.checkLatestVersion(){
            AlertModalView.show(title: RCValues.sharedInstance.getStringValue(.updateMessageTitle), message: RCValues.sharedInstance.getStringValue(.updateMessageBody))
            
        }else{
            Loading.hide()
        }
        
    }
    
    //Methods written as part of Stabilization
    func redirect2LoginVC()
    {
        let authStoryboard = UIStoryboard(name: "Authentication", bundle: nil)
        let preRootViewController: UIViewController?
        if(RCValues.sharedInstance.demoMode()){
            preRootViewController = authStoryboard.instantiateViewController(withIdentifier: "loginViewController") as? AuthenticationLoginMaskViewController
            
        }else{
            preRootViewController = authStoryboard.instantiateViewController(withIdentifier: "PreRootViewController") as? LoginViewController
        }
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        
        self.window?.rootViewController = preRootViewController
        self.window?.backgroundColor = MerckColor.RichPurple
        self.window?.makeKeyAndVisible()
    }
    
    func redirect2MainVC(){
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        if let mainViewController = mainStoryBoard.instantiateInitialViewController()
        {
            self.window?.rootViewController = mainViewController
            self.window?.contentMode = UIViewContentMode.scaleToFill
            
            mainViewController.view.edges(to: self.window!)
            
            Loading.show()
            let mvc : ModalViewController = try! Dip.container.resolve() as ModalViewController
            if let trophyIDStr = KeychainSwift().get("trophy"), let trophyID = Int(trophyIDStr), let trophy = Trophy.getTrophy(key: trophyID){
                let trophyView = TrophyModalView()
                trophyView.isInOverview = false
                trophyView.trophy = trophy
                trophyView.result = {str, success in
                    KeychainSwift().delete("trophy")
                    if success{
                        self.deeplinking.redirectToTrophies().then({_ in
                            Loading.hide()
                        })
                    }
                    return success
                }
                mvc.present(modal: trophyView)
            }
            if KeychainSwift().get("lastOpened") == nil{
                Onboarding.show()
            }
            KeychainSwift().set(Date().string(), forKey: "lastOpened")
            UIApplication.shared.registerForRemoteNotifications()
            self.service.getSettings().then({settings in
                if settings != nil{
                    self.service.userSettings.value = settings!
                    self.service.update().then({_ in
                        self.checkForUpdate()
                    }).catch({e in
                        ErrorView.show(error: e)
                    })
                }
                Timer.scheduledTimer(withTimeInterval: 60, repeats: true, block: {_ in
                    self.service.getSettings().then({settings in
                        if settings != nil{
                            self.service.userSettings.value = settings!
                        }
                    })
                })
            }).catch({e in
                ErrorView.show(error: e)
            }).always {
                self.service.AnalyticsOpenApplication()
            }
        }
    }

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "MyFeedback_2_0")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

