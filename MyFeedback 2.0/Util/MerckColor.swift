//
//  MerckColor.swift
//  MIA
//
//  Created by Daniel Weber on 12.10.17.
//  Copyright © 2017 zero2one. All rights reserved.
//

import Foundation
import UIKit
import DynamicColor

struct MerckColor {
    
    static let RichPurple : UIColor = UIColor(red: 80.0/255.0, green: 50.0/255.0, blue: 145.0/255.0, alpha: 1.0);
    static let VibrantMagenta : UIColor = UIColor(red: 235.0/255.0, green: 60.0/255.0, blue: 150.0/255.0, alpha: 1.0);
    static let SensitivePink : UIColor = UIColor(red: 225.0/255.0, green: 195.0/255.0, blue: 205.0/255.0, alpha: 1.0);
    
    static let RichBlue : UIColor = UIColor(red: 15.0/255.0, green: 105.0/255.0, blue: 175.0/255.0, alpha: 1.0);
    static let VibrantCyan : UIColor = UIColor(red: 45.0/255.0, green: 190.0/255.0, blue: 205.0/255.0, alpha: 1.0);
    static let SensitiveBlue : UIColor = UIColor(red: 150.0/255.0, green: 215.0/255.0, blue: 210.0/255.0, alpha: 1.0);
    
    static let RichGreen : UIColor = UIColor(red: 20.0/255.0, green: 155.0/255.0, blue: 95.0/255.0, alpha: 1.0);
    static let VibrantGreen : UIColor = UIColor(red: 165.0/255.0, green: 205.0/255.0, blue: 80.0/255.0, alpha: 1.0);
    static let SensitiveGreen : UIColor = UIColor(red: 180.0/255.0, green: 220.0/255.0, blue: 150.0/255.0, alpha: 1.0);
    
    static let RichRed : UIColor = UIColor(red: 230.0/255.0, green: 30.0/255.0, blue: 80.0/255.0, alpha: 1.0);
    static let VibrantYellow : UIColor = UIColor(red: 255.0/255.0, green: 200.0/255.0, blue: 50.0/255.0, alpha: 1.0);
    static let SensitiveYellow : UIColor = UIColor(red: 255.0/255.0, green: 220.0/255.0, blue: 185.0/255.0, alpha: 1.0);
    
    static let Black : UIColor = UIColor(hexString: "#1B1B1B")
    static let VeryDarkGrey : UIColor = UIColor(hexString: "#222222")
    static let DarkerGrey : UIColor = UIColor(hexString: "#555555")
    static let Grey : UIColor = UIColor(hexString: "#888888")
    static let LightGrey : UIColor = UIColor(hexString: "#A5A5A5")
    static let LighterGrey : UIColor = UIColor(hexString: "#BBBBBB")
    static let VeryLightGrey : UIColor = UIColor(hexString: "#F2F2F6")
    
}

