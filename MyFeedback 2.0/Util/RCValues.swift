//
//  RCValues.swift
//  FeedBack
//
//  Created by Daniel Weber on 16.09.17.
//  Copyright © 2017 zero2one. All rights reserved.
//

import Foundation
import Firebase
import Hydra

enum ValueKey: String {
    case techUserBasic = "techUserBasic"
    case latestVersion = "latestVersion"
    case updateMessageTitle = "updateMessageTitle"
    case updateMessageBody = "updateMessageBody"
    case updateMessageButton = "updateMessageButton"
    case checkForLatestVersion = "checkForLatestVersion"
    case disableApp = "disableApp"
    case demoAppleNew = "demoAppleNew"
    case demoNo = "demoNo"
    case showPercentage = "showPercentage"
}

class RCValues {
    
    static let sharedInstance = RCValues()
    
    private init() {
        loadDefaultValues()
        fetchCloudValues()
    }
    
    func loadDefaultValues() {
        let appDefaults: [String: NSObject] = [
            ValueKey.disableApp.rawValue: "false" as NSObject,
            ValueKey.showPercentage.rawValue : "false" as NSObject
        ]
        RemoteConfig.remoteConfig().setDefaults(appDefaults)
    }
    
    func fetchCloudValues()->Promise<Bool> {
        return Promise(in: .background,  {resolve, reject, _ in
            #if DEBUG
            // WARNING: Don't actually do this in production!
            let fetchDuration: TimeInterval = 0
            self.activateDebugMode()
            RemoteConfig.remoteConfig().fetch(withExpirationDuration: fetchDuration) {
                [weak self] (status, error) in
                
                guard error == nil else {
                    print ("Uh-oh. Got an error fetching remote values \(error)")
                    reject(error!)
                    return
                }
                
                // 2
                RemoteConfig.remoteConfig().activateFetched()
                
                print ("Retrieved values from the cloud!")
                resolve(true)
                return
            }
            #else
            RemoteConfig.remoteConfig().fetch(completionHandler: {
                [weak self] (status, error) in
                
                guard error == nil else {
                    print ("Uh-oh. Got an error fetching remote values \(error)")
                    reject(error!)
                    return
                }
                RemoteConfig.remoteConfig().activateFetched()
                print ("Retrieved values from the cloud!")
                resolve(true)
                return
                //self?.checkLatestVersion()
            })
            #endif
        })
    }
    
    func checkLatestVersion()->Bool{
        if !self.getBoolValue(.checkForLatestVersion){
            return true
        }
        print("installed version: \(Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String)")
        print("latest version: \(self.getStringValue(.latestVersion))")
        let currentVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
        let latestVersion = self.getStringValue(.latestVersion)
        
        return (currentVersion >= latestVersion!)
    }
    
    func activateDebugMode() {
        let debugSettings = RemoteConfigSettings(developerModeEnabled: true)
        RemoteConfig.remoteConfig().configSettings = debugSettings!
    }
    
    func getStringValue(_ key: ValueKey)->String!{
        return RemoteConfig.remoteConfig().configValue(forKey: key.rawValue).stringValue!
    }
    
    func getBoolValue(_ key: ValueKey)->Bool{
        return RemoteConfig.remoteConfig().configValue(forKey: key.rawValue).boolValue
    }
    
    func demoMode()->Bool{
        return self.getBoolValue(ValueKey.demoAppleNew) && self.getStringValue(ValueKey.demoNo) == "2"
    }
}


