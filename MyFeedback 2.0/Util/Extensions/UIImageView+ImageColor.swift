//
//  UIImageView+ImageColor.swift
//  MIA
//
//  Created by Daniel Weber on 12.10.17.
//  Copyright © 2017 zero2one. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {
    func tintImageColor(color : UIColor) {
        self.image = self.image!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        self.tintColor = color
    }
}
