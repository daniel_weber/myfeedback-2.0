//
//  UIViewController+ShareExtension.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 24.04.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import UIKit

extension UIViewController {
    
    fileprivate var isActivityViewControllerWindowPresented: Bool {
        return activityViewControllerWindow?.isKeyWindow ?? false
    }
    
    func presentActivityViewController(_ activityViewController: UIActivityViewController, animated: Bool = true, completion: (() -> Void)? = nil) {
        if isActivityViewControllerWindowPresented {
            return
        }
        
        let window = UIWindow(frame: view.window!.frame)
        previousWindow = UIApplication.shared.keyWindow
        activityViewControllerWindow = window
        previousWindow?.resignKey()
        window.rootViewController = UIViewController()
        window.makeKeyAndVisible()
        
        let activityCompletionClosure = activityViewController.completionWithItemsHandler
        activityViewController.completionWithItemsHandler = { [weak self] (activityType, completed, returnedItems, activityError) in
            self?.cleanUpActivityViewControllerWindow()
            activityCompletionClosure?(activityType, completed, returnedItems, activityError)
        }
        window.rootViewController?.present(activityViewController, animated: animated, completion: completion)
    }
    
    fileprivate func cleanUpActivityViewControllerWindow() {
        previousWindow?.makeKeyAndVisible()
        activityViewControllerWindow?.rootViewController = nil
        activityViewControllerWindow = nil
    }
    
}
