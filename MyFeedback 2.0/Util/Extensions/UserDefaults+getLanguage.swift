//
//  UserDefaults+getLanguage.swift
//  FeedBack
//
//  Created by Daniel Weber on 12.09.17.
//  Copyright © 2017 zero2one. All rights reserved.
//

import Foundation

extension UserDefaults{
    
    func getLanguage()->String?{
        if let langs = UserDefaults.standard.value(forKey: "AppleLanguages") as? [String], langs.count > 0{
            return langs[0]
        }
        return nil
    }
    
    func getLanguageCode()->String{
        
        if let lang = self.getLanguage(){
            let splitted = lang.components(separatedBy: "-")
            if splitted.count>0{
                return splitted[0].lowercased()
            }
        }
        
        return "en"
    }
    
}

