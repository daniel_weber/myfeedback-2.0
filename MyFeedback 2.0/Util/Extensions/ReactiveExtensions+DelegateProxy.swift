//
//  ReactiveExtensions+DelegateProxy.swift
//  MIA
//
//  Created by Daniel Weber on 13.10.17.
//  Copyright © 2017 zero2one. All rights reserved.
//

import Foundation
import Bond
import ReactiveKit

extension ReactiveExtensions where Base: UITableView {
    public var delegate: ProtocolProxy {
        return base.protocolProxy(for: UITableViewDelegate.self, setter: NSSelectorFromString("setDelegate:"))
    }
}
