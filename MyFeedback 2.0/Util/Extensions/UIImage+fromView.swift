//
//  UIImage+fromView.swift
//  MIA
//
//  Created by Daniel Weber on 14.10.17.
//  Copyright © 2017 zero2one. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    class func imageWithView(view: UIView) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.isOpaque, 0.0)
        view.drawHierarchy(in: view.bounds, afterScreenUpdates: true)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img
    }
}

