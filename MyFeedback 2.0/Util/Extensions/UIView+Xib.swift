//
//  UIView+Xib.swift
//  MIA
//
//  Created by Daniel Weber on 17.08.17.
//  Copyright © 2017 Merck KGaA. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    // Loads a XIB file into a view and returns this view.
    internal func viewFromNibForClass() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        
        /* Usage for swift < 3.x
         let bundle = NSBundle(forClass: self.dynamicType)
         let nib = UINib(nibName: String(self.dynamicType), bundle: bundle)
         let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
         */
        
        return view
    }
    
    // A very basic implementation of hittesting a view. This point must be relative to the view.
    internal func pointHitView(point : CGPoint?) -> Bool {
        
        guard let p = point else {
            return false
        }
        
        return p.x >= self.bounds.origin.x && p.y >= self.bounds.origin.y && p.x <= self.bounds.origin.x + self.bounds.size.width && p.y <= self.bounds.origin.y + self.bounds.size.height
        
    }
}
