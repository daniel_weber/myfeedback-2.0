//
//  UIViewController+HandleError.swift
//  MIA
//
//  Created by Daniel Weber on 16.10.17.
//  Copyright © 2017 zero2one. All rights reserved.
//

import Foundation
import UIKit
import Hydra
import Firebase

extension UIViewController{
    
    func handleAlert(alertText: String){
        self.handleAlert(alertText: alertText, alertTitle: nil, alertIcon: nil)
    }
    
    func handleAlert(alertText: String, alertTitle: String?, alertIcon: UIImage?){
        let mvc : ModalViewController = try! Dip.container.resolve() as ModalViewController
        let errorView = ErrorModalView()
        errorView.error = alertText
        errorView.errorTitleLabel.text = alertTitle
        errorView.errorIconImageView.image = alertIcon
        mvc.present(modal: errorView)
    }
    
    func handleError(_ error: Error)->Promise<Any>{
        return Promise({resolve, reject, _ in
            if error is webError, case webError.webServiceError(let message) = error{
                self.handleError(message).then({_ in resolve(true)})
            }else{
                self.handleError(error.localizedDescription).then({_ in resolve(true)})
            }
        })
        
    }
    func handleError(_ errorString: String)->Promise<Any>{
        let errorShort: String
        if errorString.characters.count > 100{
            errorShort = errorString.substring(to: errorString.index(errorString.startIndex, offsetBy: 99))
        }else{
            errorShort = errorString
        }
        Analytics.logEvent("error_message", parameters: ["message": errorShort])
        return Promise(in: .main, {resolve, reject, _ in
            
            if errorString.smartContains("JSON could not be serialized"){
                /*if let delegate = UIApplication.shared.delegate as? AppDelegate{
                    delegate.launchAndAuthenticate().then({_ in
                        delegate.loadMain().then({_ in })
                    })
                }*/
            }else{
                let mvc : ModalViewController = try! Dip.container.resolve() as ModalViewController
                let errorView = ErrorModalView()
                errorView.error = errorString
                mvc.present(modal: errorView)
            }
        })
    }
}
