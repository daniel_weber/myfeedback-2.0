//
//  String+ParameterEncoding.swift
//  MIA
//
//  Created by Daniel Weber on 19.10.17.
//  Copyright © 2017 zero2one. All rights reserved.
//

import Foundation
import Alamofire

extension String: ParameterEncoding {
    
    public func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
        var request = try urlRequest.asURLRequest()
        request.httpBody = data(using: .utf8, allowLossyConversion: false)
        return request
    }
    
}
