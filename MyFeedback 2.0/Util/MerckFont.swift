//
//  MerckFont.swift
//  MIA
//
//  Created by Daniel Weber on 12.10.17.
//  Copyright © 2017 zero2one. All rights reserved.
//

import Foundation
import UIKit

class MerckFont {
    
    static func font(size: Double) -> UIFont?{
        
        return UIFont(name: "Merck", size: CGFloat(size))
    }
}

