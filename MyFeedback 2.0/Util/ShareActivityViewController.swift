//
//  ShareActivityViewController.swift
//  FeedBack
//
//  Created by Daniel Weber on 08.06.17.
//  Copyright © 2017 zero2one. All rights reserved.
//

import UIKit

class ShareActivityViewController: UIActivityViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.completionWithItemsHandler = {(type: UIActivityType?, success: Bool, items: [Any]?, error: Error?) -> Void in
            print("Open Share View")
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
