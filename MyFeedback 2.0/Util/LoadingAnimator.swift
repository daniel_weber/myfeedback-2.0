//
//  LoadingAnimator.swift
//  CostCenters
//
//  Created by Prateek Pande on 10/04/18.
//  Copyright © 2018 Merck KGaA. All rights reserved.
//

import Foundation
import NVActivityIndicatorView

class LoadingAnimator{
    
    private static var activityIndicator: NVActivityIndicatorView!
    
    static func startAnimating(containerView: UIView, referenceView: UIView, frame: CGRect, isReplaceable: Bool=false){
        
        //        if activityIndicator != nil && activityIndicator.isAnimating{return}
        if activityIndicator != nil && activityIndicator.isAnimating{stopAnimating(referenceView: nil)}
        activityIndicator = NVActivityIndicatorView(frame: frame, type: .ballPulse, color: MerckColor.RichPurple, padding: nil)
        activityIndicator.startAnimating()
        containerView.addSubview(activityIndicator)
        if isReplaceable{
            activityIndicator.alpha = 0
            transitionView(sourceView: referenceView, destinationView: activityIndicator)
        }
    }
    
    /**
     Shows activity loading animation
     - parameter containerView: activity indicator subviewed inside container
     - parameter referenceView: activity indicator's frame
     - parameter isReplaceable: if true, indicator replaces referenceView
     */
    static func startAnimating(containerView: UIView, referenceView: UIView, isReplaceable: Bool=false){
        
        //        if activityIndicator != nil && activityIndicator.isAnimating{return}
        if activityIndicator != nil && activityIndicator.isAnimating{stopAnimating(referenceView: nil)}
        activityIndicator = NVActivityIndicatorView(frame: referenceView.frame, type: .ballPulse, color: MerckColor.VibrantGreen, padding: nil)
        activityIndicator.startAnimating()
        containerView.addSubview(activityIndicator)
        if isReplaceable{
            activityIndicator.alpha = 0
            transitionView(sourceView: referenceView, destinationView: activityIndicator)
        }
    }
    
    /**
     Hides activity loading animation
     - parameter referenceView: replace indicator with referenceView
     */
    static func stopAnimating(referenceView: UIView?){
        guard self.activityIndicator != nil else{return}
        if self.activityIndicator.isAnimating{
            self.activityIndicator.stopAnimating()
            if let view = referenceView{
                transitionView(sourceView: activityIndicator, destinationView: view)
            }
            self.activityIndicator.removeFromSuperview()
        }
    }
    
    private static func transitionView(sourceView: UIView, destinationView: UIView){
        
        UIView.animate(withDuration: 0.5, animations: {
            sourceView.alpha = 0
            destinationView.alpha = 1
        })
    }
}
