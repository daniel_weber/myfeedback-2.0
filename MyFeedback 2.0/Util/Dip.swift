//
//  Dip.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 08.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import Foundation
import Dip
import Bond

var loggedInUser : Observable<Employee?> = Observable<Employee?>(nil)
var previousWindow: UIWindow?
var activityViewControllerWindow: UIWindow?

class Dip {
    static let container: DependencyContainer = DependencyContainer { c in
        
        c.register(.singleton) { FeedbackService() }
        c.register(.singleton) { ModalViewController(nibName: "ModalViewController", bundle: nil) as ModalViewController}
        c.register(.singleton) { DeeplinkHandler() as Deeplinking}
        c.register(.singleton) { FirebaseMessageHandler() as MessageHandling}
        
        c.register(.singleton) { AzureLoginInterpreter() as LoginInterpreting}
        /*#if MOCK
            c.register(.singleton) { CostCenterServiceMock() as CostCenterServicing }
        #else
            c.register(.singleton) { CostCenterService() as CostCenterServicing }
        #endif
        
        c.register(.singleton) { AuthenticationController() as Authenticating}
        c.register(.singleton) { AzureLoginInterpreter() as LoginInterpreting}
        
        c.register(.singleton) { ModalViewController(nibName: "ModalViewController", bundle: nil) as ModalViewController}
        
        c.register(.singleton) { DummyEmailViewController() as DummyEmailViewController }*/
        
    }
    
    static func registerNavigationBar(navigationBar: AlternativeNavigationBar) {
        
        Dip.container.register(.singleton) { navigationBar as AlternativeNavigationBar}
    }
    static func registerShareController(shareController: ShareActivityViewController) {
        
        Dip.container.register(.singleton) { shareController as ShareActivityViewController}
    }
    static func registerSearchBar(searchBar: BaseSearchBar) {
        
        Dip.container.register(.singleton) { searchBar as BaseSearchBar}
    }
    static func registerBaseNavigationController(navigationController: BaseNavigationController) {
        
        Dip.container.register(.singleton) { navigationController as BaseNavigationController}
    }
    static func registerStatusBar(statusBar: UIView) {
        
        Dip.container.register(.singleton) { statusBar as UIView}
    }
}
