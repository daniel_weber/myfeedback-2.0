//
//  Authenticating.swift
//  CostCenters
//
//  Created by Marco Koeppel on 14.09.17.
//  Copyright © 2017 Merck KGaA. All rights reserved.
//

import Foundation
import Hydra
import Bond

protocol Authenticating {
    
    func authenticate() -> Promise<AuthenticationState>
    func setCredentials(username: String, password: String)
    func setToken(_ token: String)
    func registerDevice()->Promise<Bool>
    //func registerPushNotifications()
    func loadCookies() -> Bool
    func storeCookies()
    func deleteCookies()
    func loginWithTouchId() -> Promise<Bool>
    func observeIntepreter()
    
    var authenticationState: Observable<AuthenticationState?> {get set}
}

enum AuthenticationState {
    case authenticated
    case loginRequired
    case tokenRequired(message: String)
    case authenticationFailed(error: String)
    case hideView
    //case deviceNotRegistered
    //case noValidSession
    
}
