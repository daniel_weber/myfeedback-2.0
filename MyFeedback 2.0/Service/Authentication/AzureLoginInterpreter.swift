//
//  AzureLoginInterpreter.swift
//  CostCenters
//
//  Created by Marco Koeppel on 14.09.17.
//  Copyright © 2017 Merck KGaA. All rights reserved.
//

import Foundation
import UIKit
import Hydra
import NVActivityIndicatorView
import Bond
import KeychainSwift

class AzureLoginInterpreter: NSObject, LoginInterpreting {
    var webView: UIWebView?
    var loginRequired: Observable<Bool?> = Observable(nil)
    
    var loginResponse: Observable<LoginResponse?> = Observable(nil)
    
    var actionRequired: Observable<(AuthenticationMethod?, String?)> = Observable((nil,nil))
    
    var xmlElement = ""
    
    var muid: String? = nil
    var firstName: String? = nil
    var lastName: String? = nil
    var email: String? = nil
    
    var employee: Employee? = nil
    
    func submit(username: String, password: String) -> Promise<LoginResponse> {
        return Promise<LoginResponse>({ (resolve, _, _) in
            if username.isEmpty || password.isEmpty {
                resolve(LoginResponse.Error(message: "Username and Password must not be empty."))
                return
            }
            
            // Select "Keep me signed in" checkbox
            var js = "var myelement = document.getElementById(\"kmsiInput\");myelement.click();"
            // Fill Azure id into respective Field
            js += "var myelement = document.getElementById(\"userNameInput\");myelement.value= \"\(username)\";"
            // Fill  Windows password into respective Field
            js += "var myelement = document.getElementById(\"passwordInput\");myelement.value= \"\(password)\";"
            // Submit the login form
            js += "document.forms[\'loginForm\'].submit();"
            DispatchQueue.main.async {
                _ = self.webView?.stringByEvaluatingJavaScript(from: js)
            }
            
            // Wait before checking for messages, otherwise the old error message will be displayed again
            usleep(2000)
            self.checkWebView()
            
            _ = self.actionRequired.observeNext(with: {(authenticationMethod, message) in
                if authenticationMethod != nil && message != nil {
                    switch authenticationMethod! {
                    case .authenticatorApp:
                        self.openAuthApp()
                    case.phoneCall:
                        break
                    default:
                        self.loginResponse.value = LoginResponse.TokenRequired(message: message!)
                        resolve(.TokenRequired(message: message!))
                    }
                }
            })
            
            let dis = self.loginResponse.observeNext(with: {(response) in
                
                if response != nil {
                    switch response! {
                    case .Error:
                        resolve(response!)
                    default:
                        break
                    }
                    
                }
            })
            dis.dispose()
            
        })
    }
    
    func submit(token: String) -> Promise<LoginResponse> {
        
        return Promise<LoginResponse>({ (resolve, _, _) in
            // Fill token into respective field
            var js = "var myelement = document.getElementById(\"tfa_code_inputtext\");myelement.value= \"\(token)\";"
            // Submit the login form
            js += "var myelement = document.getElementById(\"tfa_signin_button\");myelement.click();"
            DispatchQueue.main.async {
                _ = self.webView?.stringByEvaluatingJavaScript(from: js)
            }
            usleep(2000)
            self.checkWebView()
            _ = self.actionRequired.observeNext(with: {(authenticationMethod, message) in
                if authenticationMethod != nil && message != nil {
                    switch authenticationMethod! {
                    case .authenticatorApp:
                        self.openAuthApp()
                    case.phoneCall:
                        break
                    default:
                        resolve(.TokenRequired(message: message!))
                    }
                }
            })
            _ = self.loginResponse.observeNext(with: {(response) in
                if response != nil {
                    switch response! {
                    case .Error:
                        resolve(response!)
                    default:
                        break
                    }
                    
                }
            })
        })
    }
    
    
    func checkWebView() {
        DispatchQueue.main.async {
            
            //check for SAML Response
            if let samlResponse = self.webView?.stringByEvaluatingJavaScript(from: "document.getElementsByName(\"SAMLResponse\")[0].value"){
                if !samlResponse.isEmpty{
                    self.getSamlResponsefromHTML(encodedXML: samlResponse)
                }
            }
            
            // Get element for loading animation
            let progressBarJS = "document.getElementById(\"redirect_dots_animation\").outerHTML"
            let progressBarHTML = self.webView?.stringByEvaluatingJavaScript(from: progressBarJS)?.trimmingCharacters(in: .whitespacesAndNewlines)
            // Check if loafing animation is visivle (=>page is loading)
            let isLoading = !(progressBarHTML?.isEmpty == true || progressBarHTML!.contains("display: none;"))
            if  !isLoading {
                // Define JS to check which message regarding token input is displayed
                let authenticatorToken = "$('#tfa_results_container').children('.tfa_results_text:visible').hasClass('30166');"
                let smsToken = "$(\"#idDiv_SAOTCC_Description\").innerHTML.indexOf(\"SMS\") > -1"
                let phoneCall = "$('#tfa_results_container').children('.tfa_results_text:visible').hasClass('30050');"
                let authenticatorApp = "$('#tfa_results_container').children('.tfa_results_text:visible').hasClass('30051');"
                
                let tfaInputHtml = self.webView?.stringByEvaluatingJavaScript(from: """
                    document.getElementsByName("rememberMFA").length
                """)
                
                let tfaIsEmpty = tfaInputHtml?.isEmpty ?? true
                
                // Check if "Remember Me" checkbox is available
                if !tfaIsEmpty && Int(tfaInputHtml ?? "") == 1 {
                    self.loginResponse.value = LoginResponse.HideView
                    
                    self.webView?.stringByEvaluatingJavaScript(from: """
                        let element = document.getElementsByName("rememberMFA")[0]

                        if (!element.checked) {
                            element.click()
                        }
                    """)
                    
                    return
                    
                    // Select "Remember Me" checkbox
                    //                    let js = "var myelement = document.getElementById(\"cred_remember_mfa_checkbox\");myelement.click();"
                    //                    _ = self.webView?.stringByEvaluatingJavaScript(from: js)
                }
                
                //                print("---")
                //                dump(self.webView?.request?.url?.absoluteString)
                //                print("---")
                // Get token message
                let infoText = self.webView?.stringByEvaluatingJavaScript(from: "$('#tfa_results_container').children('.tfa_results_text:visible').eq(0).html();")?.trimmingCharacters(in: .whitespacesAndNewlines)
                // Check if any token input is required
                if self.webView?.stringByEvaluatingJavaScript(from: authenticatorToken) == "true"{
                    self.actionRequired.value = (.authenticatorToken, infoText)
                    self.loginResponse.value = LoginResponse.TokenRequired(message: infoText!)
                } else if self.webView?.stringByEvaluatingJavaScript(from: authenticatorApp) == "true"{
                    self.actionRequired.value = (.authenticatorApp, infoText)
                } else if self.webView?.stringByEvaluatingJavaScript(from: phoneCall) == "true"{
                    self.actionRequired.value = (.phoneCall, infoText)
                } else if self.webView?.stringByEvaluatingJavaScript(from: smsToken) == "true"{
                    self.actionRequired.value = (.smsToken, infoText)
                    self.loginResponse.value = LoginResponse.TokenRequired(message: infoText!)
                } else {
                    // Check if any error message is displayed
                    let inputField = self.webView?.stringByEvaluatingJavaScript(from: "document.getElementById(\"passwordInput\").value")
                    //                    print(inputField)
                    if let errorMessage = self.webView?.stringByEvaluatingJavaScript(from: "$('.tfa_error_text:visible').eq(0).html()")?.trimmingCharacters(in: .whitespacesAndNewlines), !errorMessage.isEmpty, inputField == ""{
                        self.loginResponse.value = LoginResponse.Error(message: errorMessage)
                    } else if let errorMessage = self.webView?.stringByEvaluatingJavaScript(from: "document.getElementById(\"errorText\").innerHTML")?.trimmingCharacters(in: .whitespacesAndNewlines), !errorMessage.isEmpty, inputField == ""{
                        self.loginResponse.value = LoginResponse.Error(message: errorMessage)
                    } else if let errorMessage = self.webView?.stringByEvaluatingJavaScript(from: "document.getElementById(\"tfa_error_container\").innerHTML")?.trimmingCharacters(in: .whitespacesAndNewlines), errorMessage.contains("style=\"display: block;\""), inputField == ""{
                        self.loginResponse.value = LoginResponse.Error(message: errorMessage)
                    }
                    Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: {_ in
                        self.checkWebView()
                    })
                }
            } else {
                Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: {_ in
                    self.checkWebView()
                })
            }
        }
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        //check for SAML Response
        if let samlResponse = self.webView?.stringByEvaluatingJavaScript(from: "document.getElementsByName(\"SAMLResponse\")[0].value"){
            if !samlResponse.isEmpty{
                self.getSamlResponsefromHTML(encodedXML: samlResponse)
            }
        }
        
        // CHeck if login form is shown
        if let userNameInputHtml = webView.stringByEvaluatingJavaScript(from: "document.getElementById(\"userNameInput\").outerHTML"), !userNameInputHtml.isEmpty {
            loginResponse.value = .CredentialsRequired
        } else  if webView.request?.url?.absoluteString.contains("wsignoutcleanup1.0") ?? false {
            loginRequired.silentUpdate(value: true)
            //            self.loginRequired.value = true
            
            if loginResponse.value != nil {
                switch loginResponse.value! {
                case .Logout:
                    break
                default:
                    loginResponse.value = .Logout
                }
            } else {
                loginResponse.value = .Logout
            }
            
            actionRequired.silentUpdate(value: (nil, nil))
            //            self.actionRequired.value = (nil, nil)
        } else
            // Check for empty html document => Indicates that login was successful
            if webView.stringByEvaluatingJavaScript(from: "document.documentElement.outerHTML") == "<html><head></head><body></body></html>"{
                loginResponse.value = .Success
        }
    }
    
    func openAuthApp() {
        // Try to open authenticator app on phone
        let url = URL(string: "msauth://code/cca%3A%2F%2F\(Bundle.main.bundleIdentifier!)")
        if UIApplication.shared.canOpenURL(url!) {
            UIApplication.shared.open(url!, options: [:], completionHandler: nil)
        }
    }
    
    // MARK: XMLParser Delegate
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        if elementName == "NameID" {
            xmlElement = elementName
        }else if attributeDict.first(where: {(k,v) in return k == "Name" && v == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/givenname"}) != nil{
            xmlElement = "firstName"
        }else if let kvp = attributeDict.first(where: {(k,v) in return k == "Name" && v == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/surname"}){
            xmlElement = "lastName"
        }else if let kvp = attributeDict.first(where: {(k,v) in return k == "Name" && v == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress"}){
            xmlElement = "email"
        }else if xmlElement == "firstName" || xmlElement == "lastName" || xmlElement == "email"{
            xmlElement = xmlElement+elementName
        }else{
            xmlElement = ""
        }
        
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        if xmlElement == "NameID"{
            KeychainSwift().set(string, forKey: "userId")
            self.muid = string
        }else if xmlElement == "firstNameAttributeValue"{
            KeychainSwift().set(string, forKey: "firstName")
            self.firstName = string
        }else if xmlElement == "lastNameAttributeValue"{
            self.lastName = string
            KeychainSwift().set(string, forKey: "lastName")
        }else if xmlElement == "emailAttributeValue"{
            self.email = string
            KeychainSwift().set(string, forKey: "email")
        }
        
        if(self.muid != nil && self.firstName != nil && self.lastName != nil && self.email != nil){
            self.employee = Employee(userId: muid!, muid: muid!, firstName: firstName!, lastName: lastName!, email: email!, orgUnitName: "")
            loggedInUser.value = employee
        }
    }
    
    func getSamlResponsefromHTML(encodedXML:String){
        if let data = Data(base64Encoded: encodedXML){
            let parser = XMLParser(data: data)
            parser.delegate = self
            parser.parse()
        }
    }
}
