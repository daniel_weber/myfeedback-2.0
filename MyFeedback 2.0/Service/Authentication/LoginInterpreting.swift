//
//  LoginInterpreting.swift
//  CostCenters
//
//  Created by Marco Koeppel on 14.09.17.
//  Copyright © 2017 Merck KGaA. All rights reserved.
//

import Foundation
import UIKit
import Hydra
import Bond

protocol LoginInterpreting: UIWebViewDelegate, XMLParserDelegate {
    
    var webView: UIWebView? { get set }
    //var loginRequired : ((Bool) -> Void)? { get set }
    var loginRequired: Observable<Bool?> { get set }
    var loginResponse: Observable<LoginResponse?> { get set }
    var actionRequired: Observable<(AuthenticationMethod?, String?)> { get set }
    func checkWebView()
    func submit(username: String, password: String) -> Promise<LoginResponse>
    func submit(token: String) -> Promise<LoginResponse>
}

enum LoginResponse {
    case Success
    case CredentialsRequired
    case TokenRequired(message: String)
    case Error(message: String)
    case HideView
    case Logout
}

enum AuthenticationMethod {
    case smsToken
    case authenticatorToken
    case phoneCall
    case authenticatorApp
}
