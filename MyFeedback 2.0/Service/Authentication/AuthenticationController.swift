// Om Sri Sai Ram
//  AuthenticationController.swift
//  CostCenters
//
//  Created by Marco Koeppel on 14.09.17.
//  Copyright © 2017 Merck KGaA. All rights reserved.
//

import Foundation
import Hydra
import Bond
import Alamofire
import KeychainSwift
import LocalAuthentication

class AuthenticationController: Authenticating {
    
    private static var sharedInstance:AuthenticationController?;
    
    class func createInstaceWithAppID(appCID: String) -> AuthenticationController? {
        //Shared instance get allocated, when valid session exists...
        if (nil == sharedInstance)
        {
            sharedInstance = AuthenticationController(appConnectionID: appCID);
            return sharedInstance!;
        }
        else if(sharedInstance?.appCID == appCID)
        {
            return sharedInstance!;
        }
        return nil;
    }
    
    class func sharedController() -> AuthenticationController? {
        return sharedInstance!;
    }
    
    private class func releaseInstance()
    {
        //Call
        sharedInstance = nil;
        
        //Release other business related objects when auth lost...
    }
    
    let appCID: String;
    var pushServerPreference: Bool = false;
    private init(appConnectionID: String)  //Make private to block the instance to be created form out side..
    {
        self.appCID = appConnectionID;
    }
    
    func logout() {
        KeychainSwift().delete("azureID")
        KeychainSwift().delete("azureId")
        KeychainSwift().delete("windowsPassword")
        KeychainSwift().delete("password")
        KeychainSwift().delete("appCid")
        UserDefaults.standard.removeObject(forKey: "savedCookies")
        
        AuthenticationController.clearCookies();
        
        AuthenticationController.releaseInstance(); //allow other user to login...
    }
    
    
    var interpreter = try! Dip.container.resolve() as LoginInterpreting
    var previousLoginState: LoginResponse?
    var authenticationState: Observable<AuthenticationState?> = Observable(nil)
    
    func authenticate() -> Promise<AuthenticationState> {
        return Promise<AuthenticationState>(in: .main, token: nil, { (resolve, _, _) in
            if self.loadCookies() {
                let callback = { (_: Bool) in
                    resolve(AuthenticationState.authenticated)
                }
                callback(true)
            } else {
                self.observeIntepreter()
            }
            self.observeIntepreter()
        })
    }
    
    func observeIntepreter() {
        _ = self.interpreter.loginResponse.observe(with: {_ in
            let loginResponse = self.interpreter.loginResponse.value
            if loginResponse != nil, case LoginResponse.CredentialsRequired = loginResponse! {
                if let (azureID, password) = self.getCredentials() {
                    self.loginWithTouchId().then(in: .main, {_ in
                        self.interpreter.submit(username: azureID, password: password).then(in: .main, {response in
                            switch response {
                            case .Success:
                                self.loginCompleted()
                            default:
                                if case LoginResponse.TokenRequired(let message) = response {
                                    self.authenticationState.value = AuthenticationState.tokenRequired(message: message)
                                } else if case LoginResponse.HideView = response {
                                    self.authenticationState.value = AuthenticationState.hideView
                                }
                                
                            }
                        }).catch({_ in
                            switch loginResponse! {
                            case .Logout:
                                return
                            default:
                                self.authenticationState.value = AuthenticationState.loginRequired
                            }
                        })
                    })
                } else {
                    switch loginResponse! {
                    case .Logout:
                        return
                    default:
                        self.authenticationState.value = AuthenticationState.loginRequired
                    }
                    //                    self.authenticationState.value = AuthenticationState.loginRequired
                }
            } else if loginResponse != nil {
                if self.previousLoginState != nil {
                    switch (loginResponse!, self.previousLoginState!) {
                    case (.Error, .Error):
                        break
                    default:
                        self.handleLoginResponse(loginResponse!, username: nil, password: nil)
                    }
                } else {
                    self.handleLoginResponse(loginResponse!, username: nil, password: nil)
                }
                
            }
        })
        //        self.authenticationState.value = AuthenticationState.hideView
        self.interpreter.checkWebView()
        
    }
    
    func setCredentials(username: String, password: String) {
        self.interpreter.submit(username: username, password: password).then(in: .main, {loginResponse in
            self.handleLoginResponse(loginResponse, username: username, password: password)
        })
        
    }
    
    func setToken(_ token: String) {
        self.interpreter.submit(token: token).then(in: .main, {loginResponse in
            self.handleLoginResponse(loginResponse, username: nil, password: nil)
        })
    }
    
    func getCredentials() -> (String, String)? {
        if let azureID = KeychainSwift().get("azureID"), let password = KeychainSwift().get("password") {
            return (azureID, password)
        }
        return nil
    }
    
    func handleLoginResponse(_ loginResponse: LoginResponse, username: String?, password: String?) {
        switch loginResponse {
        case .Success:
            if password != nil && username != nil {
                KeychainSwift().set(username!, forKey: "azureID")
                KeychainSwift().set(password!, forKey: "password")
            }
            self.loginCompleted()
        case .TokenRequired:
            if case .TokenRequired(let message) = loginResponse {
                self.authenticationState.value = AuthenticationState.tokenRequired(message: message)
            }
        case .Error:
            KeychainSwift().delete("azureID")
            KeychainSwift().delete("password")
            if case .Error(let message) = loginResponse {
                self.authenticationState.value = AuthenticationState.authenticationFailed(error: message)
            }
        case .CredentialsRequired:
            self.authenticationState.value = AuthenticationState.loginRequired
        case .HideView:
            self.authenticationState.value = AuthenticationState.hideView
        case .Logout:
            authenticationState.value = AuthenticationState.loginRequired
        }
    }
    
    func loginWithTouchId() -> Promise<Bool> {
        return Promise<Bool>(in: .background, token: nil, { (resolve, reject, _) in
            // Get the local authentication context.
            let context: LAContext = LAContext()
            
            // Declare a NSError variable.
            var error: NSError?
            
            // Set the reason string that will appear on the authentication alert.
            let reasonString = "Authentication is needed to access your Invoices."
            
            // Check if the device can evaluate the policy.
            
            if context.canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error: &error) {
                context.evaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, localizedReason: reasonString, reply: { [unowned self] (success, evalPolicyError) -> Void in
                    if success {
                        resolve(true)
                    } else {
                        let error = evalPolicyError as! LAError
                        // If authentication failed then show a message to the console with a short description.
                        // In case that the error is a user fallback, then show the password alert view.
                        print(evalPolicyError?.localizedDescription ?? "")
                        reject(error)
                    }
                })
            } else {
                reject(error!)
            }
        })
        
    }
    
    func loginCompleted() {
        if checkRegistration() {
            authenticationState.value = AuthenticationState.authenticated
        } else {
            registerDevice().then(in: .main, {(success) in
                if success {
                    self.authenticationState.value = AuthenticationState.authenticated
                } else {
                    self.authenticationState.value = AuthenticationState.authenticationFailed(error: "Unknown error occured while trying to register at SAP Mobile Services")
                }
            }).catch({(error) in
                self.authenticationState.value = AuthenticationState.authenticationFailed(error: error.localizedDescription)
            })
        }
        storeCookies()
    }
    
    func checkRegistration() -> Bool {
        if let appCid = KeychainSwift().get("appCid") {
            SAPcpms.shared.httpHeaders.updateValue(appCid, forKey: "X-SMP-APPCID")
            return true
        }
        return false
    }
    
    func registerDevice()->Promise<Bool> {
        return Promise<Bool>(in: .background, token: nil, { (resolve, _, _) in
            SAPcpms.shared.registerDevice(considerKeyChainValue:true).then({(success, appCid) in
                if appCid != nil {
                    resolve(true)
                } else {
                    resolve(false)
                }
            })
        })
    }
    
    func storeCookies() { //Backup the current auth cookies to NS user defaults...
        guard let cookies = HTTPCookieStorage.shared.cookies?.filter({(cookie) -> Bool in return cookie.domain.contains("hana.ondemand.com")}) else {
            print("No cookies found")
            return
        }
        var cookieArray = [[HTTPCookiePropertyKey: Any]]()
        for cookie in cookies {
            cookieArray.append(cookie.properties!)
        }
        
        UserDefaults.standard.set(cookieArray, forKey: "savedCookies")
        let date = Date().timeIntervalSince1970
        let timeIntervalString = try! date.string()
        KeychainSwift().set(timeIntervalString, forKey: "cookiesSavedDate")
        UserDefaults.standard.synchronize()
    }
    
    
    func deleteCookies() {
        KeychainSwift().delete("appCid")
        UserDefaults.standard.removeObject(forKey: "savedCookies")
    }
    
    func loadCookies() -> Bool {
        guard let cookieArray = UserDefaults.standard.array(forKey: "savedCookies") as? [[HTTPCookiePropertyKey: Any]], let cookiesSavedDate = KeychainSwift().get("cookiesSavedDate") else { return false}
        if let cokiesTimeInterval = TimeInterval(cookiesSavedDate), Date().timeIntervalSince1970 - cokiesTimeInterval > 1800{
            self.deleteCookies()
            return false
        }
        
        for cookieProperties in cookieArray {
            if let cookie = HTTPCookie(properties: cookieProperties) {
                if cookie.expiresDate != nil && cookie.expiresDate! <= Date() {
                    return false
                }
                HTTPCookieStorage.shared.setCookie(cookie)
            }
        }
        return true
    }
    
    
    class func customizeUserAgent(){
        let webView : UIWebView = UIWebView(frame: CGRect(x: 0, y: 0, width: 10, height: 10));
        let defaultUA = webView.stringByEvaluatingJavaScript(from: "navigator.userAgent")
        var overridingUA = defaultUA;
        if let appDisplayName:String = Bundle.main.infoDictionary?["CFBundleName"] as? String, defaultUA?.isEmpty == false
        {
            overridingUA = overridingUA?.replacingOccurrences(of: "Mozilla", with: appDisplayName); //Mozilla is causing the problem with auth dialog
            UserDefaults.standard.register(defaults: ["UserAgent": overridingUA])
        }
    }
    
    class func resetUserAgent(){
        UserDefaults.standard.removeObject(forKey: "UserAgent");
    }
    
    class func clearCookies(){
        URLCache.shared.removeAllCachedResponses()
        URLCache.shared.diskCapacity = 0
        URLCache.shared.memoryCapacity = 0
        
        let cookieJar = HTTPCookieStorage.shared
        for cookie in cookieJar.cookies! {
            cookieJar.deleteCookie(cookie)
        }
    }
    
    class func restoreCookies() -> Bool { //Copy of load cookies to restore the cookies to cookies store form USerDefaults
        guard let cookieArray = UserDefaults.standard.array(forKey: "savedCookies") as? [[HTTPCookiePropertyKey: Any]] else { return false}
        
        for cookieProperties in cookieArray {
            if let cookie = HTTPCookie(properties: cookieProperties) {
                if cookie.expiresDate != nil && cookie.expiresDate! <= Date() {
                    return false
                }
                HTTPCookieStorage.shared.setCookie(cookie)
            }
        }
        return true
    }
}
