//
//  Deeplinking.swift
//  MIA
//
//  Created by Daniel Weber on 24.10.17.
//  Copyright © 2017 zero2one. All rights reserved.
//

import Foundation
import Hydra

/**
 Protocol for a Deeplink Handling implementation.
 */

protocol Deeplinking{
    
    /**
     Redirects to the Request View, showing a new Request.
     - parameter request: Request which should be displayed
     - returns: A Promise with an RequestViewController. See [Hydra](https://github.com/malcommac/Hydra) for details.
     */
    func redirectToNewRequest(_ request: Request)->Promise<RequestViewController>
    
    /**
     Redirects to the Request View, showing an answered Request.
     - parameter request: Request which should be displayed
     - returns: A Promise with an RequestViewController. See [Hydra](https://github.com/malcommac/Hydra) for details.
     */
    func redirectToAnsweredRequest(_ request: Request)->Promise<RequestViewController>
    
    /**
     Redirects to the Recognition Card View, showing an received Recognition.
     - parameter recognition: Recognition which should be displayed
     - returns: A Promise with an RecognitionCardViewController. See [Hydra](https://github.com/malcommac/Hydra) for details.
     */
    func redirectToRecognition(_ recognition: Recognition)->Promise<RecognitionCardViewController>
    
    /**
     Redirects to the ThankYou Card View, showing an received ThankYou Note.
     - parameter thankYou: ThankYou Note which should be displayed
     - returns: A Promise with an ThankYouCardViewController. See [Hydra](https://github.com/malcommac/Hydra) for details.
     */
    func redirectToThankYou(_ thankYou: Recognition)->Promise<ThankYouCardViewController>
    
    /**
     Redirects to the Trophy Overview.
     - returns: A Promise with an TrophiesViewController. See [Hydra](https://github.com/malcommac/Hydra) for details.
     */
    func redirectToTrophies()->Promise<TrophiesViewController>
}

