import UIKit
import Bond
import Hydra

class DeeplinkHandler: Deeplinking{
    func redirectToNewRequest(_ request: Request) -> Promise<RequestViewController> {
        return Promise(in: .main,  {resolve, reject, _ in
            let main = UIStoryboard.init(name: "Main", bundle: nil)
            let rootVC = main.instantiateInitialViewController()
            UIApplication.shared.keyWindow?.currentViewController()?.present(rootVC!, animated: false, completion: {
                let navigationcontroller = try! Dip.container.resolve() as BaseNavigationController
                let requestVC = main.instantiateViewController(withIdentifier: "RequestViewController") as! RequestViewController
                requestVC.request = request
                requestVC.response = request.responses.first
                navigationcontroller.pushViewController(requestVC, animated: true)
                resolve(requestVC)
            })
            
        })
    }
    
    func redirectToAnsweredRequest(_ request: Request) -> Promise<RequestViewController> {
        return Promise(in: .main,  {resolve, reject, _ in
            let main = UIStoryboard.init(name: "Main", bundle: nil)
            let rootVC = main.instantiateInitialViewController()
            UIApplication.shared.keyWindow?.currentViewController()?.present(rootVC!, animated: false, completion: {
                let navigationcontroller = try! Dip.container.resolve() as BaseNavigationController
                let requestVC = main.instantiateViewController(withIdentifier: "RequestViewController") as! RequestViewController
                requestVC.request = request
                requestVC.response = request.responses.first
                navigationcontroller.pushViewController(requestVC, animated: true)
                resolve(requestVC)
            })
        })
    }
    
    func redirectToRecognition(_ recognition: Recognition) -> Promise<RecognitionCardViewController> {
        return Promise(in: .main,  {resolve, reject, _ in
            let main = UIStoryboard.init(name: "Main", bundle: nil)
            let rootVC = main.instantiateInitialViewController()
            UIApplication.shared.keyWindow?.currentViewController()?.present(rootVC!, animated: false, completion: {
                let navigationcontroller = try! Dip.container.resolve() as BaseNavigationController
                let recognitionVC = main.instantiateViewController(withIdentifier: "RecognitionCardViewController") as! RecognitionCardViewController
                recognitionVC.isInEditMode = false
                recognitionVC.recognition.value = recognition
                navigationcontroller.pushViewController(recognitionVC, animated: true)
                resolve(recognitionVC)
            })
        })
    }
    
    func redirectToThankYou(_ thankYou: Recognition) -> Promise<ThankYouCardViewController> {
        return Promise(in: .main,  {resolve, reject, _ in
            let main = UIStoryboard.init(name: "Main", bundle: nil)
            let rootVC = main.instantiateInitialViewController()
            UIApplication.shared.keyWindow?.currentViewController()?.present(rootVC!, animated: false, completion: {
                let navigationcontroller = try! Dip.container.resolve() as BaseNavigationController
                let recognitionVC = main.instantiateViewController(withIdentifier: "ThankYouCardViewController") as! ThankYouCardViewController
                recognitionVC.recognition.value = thankYou
                navigationcontroller.pushViewController(recognitionVC, animated: true)
                resolve(recognitionVC)
            })
        })
    }
    
    func redirectToTrophies()->Promise<TrophiesViewController>{
        return Promise(in: .main,  {resolve, reject, _ in
            let main = UIStoryboard.init(name: "Main", bundle: nil)
            let rootVC = main.instantiateInitialViewController()
            UIApplication.shared.keyWindow?.currentViewController()?.present(rootVC!, animated: false, completion: {
                let navigationcontroller = try! Dip.container.resolve() as BaseNavigationController
                let trophyVC = main.instantiateViewController(withIdentifier: "TrophiesViewController") as! TrophiesViewController
                navigationcontroller.pushViewController(trophyVC, animated: true)
                resolve(trophyVC)
            })
        })
    }
    
    
}
