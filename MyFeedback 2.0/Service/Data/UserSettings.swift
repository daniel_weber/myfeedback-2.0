//
//  UserSettings.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 08.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import Foundation
import Localize_Swift

class UserSettings: NSObject, NSCopying{
    
    var language: Language
    var showReceivedDuration: DurationSetting
    var showSendDuration: DurationSetting
    var pushNotification: NotificationSetting
    var mailNotification: NotificationSetting
    var lastOpenedMobileTimestamp: Date?
    var lastOpenedMobileVersion: String?
    
    var background: String?
    
    var isFirstStart: Bool{
        get{
            return lastOpenedMobileTimestamp == nil
        }
    }
    
    var hasBeenUpdated: Bool{
        get{
            return lastOpenedMobileVersion != nil && lastOpenedMobileVersion! < Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
        }
    }
    
    init(language: Language, showReceivedDuration: DurationSetting, showSendDuration: DurationSetting, pushNotification: NotificationSetting, mailNotification: NotificationSetting, lastOpenedMobileTimestamp: Date?, lastOpenedMobileVersion: String?){
        self.language = language
        self.showReceivedDuration = showReceivedDuration
        self.showSendDuration = showSendDuration
        self.pushNotification = pushNotification
        self.mailNotification = mailNotification
        self.lastOpenedMobileVersion = lastOpenedMobileVersion
        self.lastOpenedMobileTimestamp = lastOpenedMobileTimestamp
    }
    
    static let standard = UserSettings(language: Language.English, showReceivedDuration: .TwelveMonths, showSendDuration: .TwelveMonths, pushNotification: .Always, mailNotification: .Always, lastOpenedMobileTimestamp: Date(), lastOpenedMobileVersion: Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String)
    
    func copy(with zone: NSZone? = nil) -> Any {
        let copy = UserSettings(language: language, showReceivedDuration: showReceivedDuration, showSendDuration: showSendDuration, pushNotification: pushNotification, mailNotification: mailNotification, lastOpenedMobileTimestamp: lastOpenedMobileTimestamp, lastOpenedMobileVersion: lastOpenedMobileVersion)
        return copy
    }
}

public struct DurationSetting{
    let key: String
    var localizedName:String{
        get{
           return self.key.localized()
        }
    }
    
    public static let TwelveMonths = DurationSetting(key: "12 Months")
    public static let ThreeMonths = DurationSetting(key: "3 Months")
    public static let OneMonth = DurationSetting(key: "1 Month")
    public static let OneWeek = DurationSetting(key: "1 Week")
    
    static let Durations:[DurationSetting] = [
        .TwelveMonths,
        .ThreeMonths,
        .OneMonth,
        .OneWeek
    ]
    
    public static func getDuration(key: String) -> DurationSetting{
        if let duration = Durations.first(where: {duration in duration.key == key}){
            return duration
        }
        return .TwelveMonths
    }
    
}

public struct NotificationSetting{
    let key: String
    var localizedName:String{
        get{
            return self.key.localized()
        }
    }
    
    public static let Always = NotificationSetting(key: "Always")
    public static let Never = NotificationSetting(key: "Never")
    public static let ForReceivedFeedbackOnly = NotificationSetting(key: "For received Feedback only")
    
    static let Notifications:[NotificationSetting] = [
        .Always,
        .Never,
        .ForReceivedFeedbackOnly
    ]
    
    public static func getNotificationSetting(key: String) -> NotificationSetting{
        if let notification = Notifications.first(where: {notification in notification.key == key}){
            return notification
        }
        return .Always
    }
}

public struct Language{
    let key: String
    let technical: String
    let labelString: String
    
    public static let English = Language(key: "EN", technical: "en", labelString: "English")
    public static let Chinese = Language(key: "ZH", technical: "zh-Hans", labelString: "简体中文 (Simplified Chinese)")
    public static let German = Language(key: "DE", technical: "de", labelString: "Deutsch (German)")
    public static let Spanish = Language(key: "ES", technical: "es", labelString: "Español (Spanish)")
    public static let French = Language(key: "FR", technical: "fr", labelString: "Français (French)")
    public static let Japanese = Language(key: "JA", technical: "ja", labelString: "日本語 (Japanese)")
    public static let Portuguese = Language(key: "PT", technical: "pt-BR", labelString: "Português (Brazilian Portugese)")
    
    public static let Languages:[Language] = [
        .English,
        .Chinese,
        .German,
        .Spanish,
        .French,
        .Japanese,
        .Portuguese
    ]
    
    public static func getLanguage(key: String)->Language{
        if let language = Languages.first(where: {lang in return lang.key == key}){
            return language
        }
        return .English
    }
    
    public static func getLanguage(labelString: String)->Language{
        if let language = Languages.first(where: {lang in return lang.labelString == labelString}){
            return language
        }
        return .English
    }
    
    public static func getLanguage(technical: String)->Language{
        if let language = Languages.first(where: {lang in return lang.technical == technical}){
            return language
        }
        return .English
    }
    
    public static func getCurrentLanguage()->Language{
        return getLanguage(technical: Localize.currentLanguage())
    }
    
}
