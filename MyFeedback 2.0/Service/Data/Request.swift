//
//  Request.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 06.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import Foundation
import Bond
import Localize_Swift

class Request: NSObject, Searchable {
    // MARK Attributes
    
    var id: Int?
    var requester: Employee
    @objc var requesterName: String{
        get{
            return requester.getName()
        }
    }
    var category: RequestCategory?{
        didSet{
            self.isValid.value = self.checkIfValid()
        }
    }
    @objc var categoryName: String?{
        get{
            return self.category?.localName
        }
    }
    var type: RequestType?{
        didSet{
            self.isValid.value = self.checkIfValid()
        }
    }
    @objc var typeName: String?{
        get{
            return self.type?.localName
        }
    }
    @objc var text: String{
        didSet{
            self.isValid.value = self.checkIfValid()
        }
    }
    @objc var title: String?{
        didSet{
            self.isValid.value = self.checkIfValid()
        }
    }
    
    @objc var overviewTitle: String{
        get{
            if self.title != nil && !self.title!.isEmpty{
                return self.title!
            }else{
                return self.text
            }
        }
    }
    @objc var date: Date?
    var continuousInterval: ContinuousInterval?{
        didSet{
            self.isValid.value = self.checkIfValid()
            if continuousInterval?.key == ContinuousInterval.OneTime.key{
                self.continuousRepetitions = 1
            }else if oldValue?.key == ContinuousInterval.OneTime.key && continuousInterval?.key != ContinuousInterval.OneTime.key{
                self.continuousRepetitions = 2
            }
        }
    }
    var continuousRepetitions: Int{
        didSet{
            self.isValid.value = self.checkIfValid()
        }
    }
    var scaleTextMin: String?
    var scaleTextMax: String?
    
    var averageResponseRating: Double?
    var ratingDistribution: [Double] = []
    
    var responses: [RequestResponse] = []
    var hasAnswers: Bool{
        get{
            for response in self.responses{
                if response.isAnswered{
                    return true
                }
            }
            return false
        }
    }
    var thankYouFlag:Bool{
        // return true is there is at least 1 answered response and all answered responses have been thanked for
        get{
            for response in self.responses{
                if !response.isThanked && response.isAnswered{
                    return false
                }
            }
            return self.responses.contains(where: {resp in return resp.isAnswered})
        }
    }
    
    var thankYouAllowed: Bool = false
    
    var readFlag:Bool{
        get{
            if self.responses.count > 0{
                return !self.responses.contains(where: {response in return !response.isRead})
            }
            return self.unreadCount != nil &&  self.unreadCount! <= 0
        }
    }
    
    var unreadCount:Int?
    
    var requesterNames: String{
        get{
            var names = ""
            for(index, response) in self.responses.enumerated(){
                names += response.rater.getName()
                if index < self.responses.count-1{
                    names += ", "
                }
            }
            return names
        }
    }
    
    var _searchTerm : String
    public var searchTerm : String {
        get {
            self.updateSearchTerm()
            return _searchTerm.lowercased()
        }
    }
    
    var isEditable: Bool {
        get{
            return self.responses.first?.rater.userId == loggedInUser.value?.userId && !self.hasAnswers
        }
    }
    
    var isValid: Observable<Bool> = Observable<Bool>(false)
    
    override init(){
        self.requester = Employee.invalid()
        self.text = ""
        self.continuousRepetitions = 0
        
        _searchTerm = ""
        super.init()
        self.updateSearchTerm()
    }
    
    init(id: Int?, requester: Employee, category: RequestCategory?, type: RequestType?, text: String, title: String?, date: Date?, continuousInterval: ContinuousInterval?, continuousRepetitions: Int, scaleTextMin: String?, scaleTextMax: String?, averageResponseRating: Double?){
        self.id = id
        self.requester = requester
        self.category = category
        self.type = type
        self.text = text
        self.title = title
        self.date = date
        self.continuousInterval = continuousInterval
        self.continuousRepetitions = continuousRepetitions
        self.scaleTextMin = scaleTextMin
        self.scaleTextMax = scaleTextMax
        self.averageResponseRating = averageResponseRating
        
        _searchTerm = ""
        super.init()
        self.updateSearchTerm()
    }
    
    init(id: Int?, requester: Employee, category: RequestCategory?, type: RequestType?, text: String, title:
        String?, date: Date?, continuousInterval: ContinuousInterval?, continuousRepetitions: Int, scaleTextMin: String?, scaleTextMax: String?, averageResponseRating: Double?, responses: [RequestResponse]){
        self.id = id
        self.requester = requester
        self.category = category
        self.type = type
        self.text = text
        self.title = title
        self.date = date
        self.continuousInterval = continuousInterval
        self.continuousRepetitions = continuousRepetitions
        self.scaleTextMin = scaleTextMin
        self.scaleTextMax = scaleTextMax
        self.responses = responses
        self.averageResponseRating = averageResponseRating
        
        _searchTerm = ""
        super.init()
        self.updateSearchTerm()
    }
    
    func checkIfValid() -> Bool{
        return self.category != nil && self.type != nil && !self.text.isEmpty && self.continuousInterval != nil && (continuousRepetitions > 1 || continuousInterval?.key == ContinuousInterval.OneTime.key)
    }
    
    static func invalid() -> Request {
        return Request()
    }
    
    func updateSearchTerm(){
        self._searchTerm = self.title ?? ""
        self._searchTerm = "\(self._searchTerm)\(self.text)"
        self._searchTerm = "\(self._searchTerm)\(self.requester.searchTerm)"
        for response in self.responses{
            self._searchTerm = "\(self._searchTerm)\(response.rater.searchTerm)"
        }
        
    }
    
}

class RequestResponse {
    // MARK Attributes
    
    var requestId: Int
    var rater: Employee
    var repetition: Int
    var text: String?
    var rating: Int?
    var responseTimestamp: Date?
    var readFlag: Int?
    var thankYouFlag: Int?
    
    var isAnswered: Bool{
        get{
            return self.responseTimestamp != nil
        }
    }
    var isRead: Bool{
        get{
            return self.readFlag == 1 || !self.isAnswered
        }
        
    }
    var isThanked: Bool{
        get{
            return self.thankYouFlag == 1
        }
    }
    
    var isValid: Bool{
        get{
            return self.text != nil && self.rating != nil && !self.text!.isEmpty
        }
    }
    
    init(requestId: Int, rater: Employee, repetition: Int, text: String?, rating: Int?, responseTimestamp: Date?, readFlag: Int?, thankYouFlag: Int?){
        self.requestId = requestId
        self.rater = rater
        self.repetition = repetition
        self.text = text
        self.rating = rating
        self.responseTimestamp = responseTimestamp
        self.readFlag = readFlag
        self.thankYouFlag = thankYouFlag
    }
    
}


public struct RequestCategory{
    let key: Int
    let name: String
    var localName:String{
        get{
            return self.name.localized()
        }
    }
    
    static let Innovation = RequestCategory(key: 0, name: "Innovation")
    static let LeadershipSkills = RequestCategory(key: 1, name: "Leadership skills")
    static let PersonalEffectiveness = RequestCategory(key: 2, name: "Personal effectiveness")
    static let TeamworkCollaboration = RequestCategory(key: 3, name: "Teamwork/collaboration")
    static let Others = RequestCategory(key: 4, name: "Others")
    static let Categories:[RequestCategory] = [
        .Innovation,
        .LeadershipSkills,
        .PersonalEffectiveness,
        .TeamworkCollaboration,
        .Others
    ]
    static func fromId(id: Int) -> RequestCategory{
        if let result = Categories.first(where: {cat in return cat.key == id}){
            return result
        }
        return .Others
    }
    static func fromId(name: String) -> RequestCategory{
        if let result = Categories.first(where: {cat in return cat.name.lowercased() == name.lowercased()}){
            return result
        }
        return .Others
    }
}

public struct RequestType{
    let key: String
    let name: String
    let rateMax: Int
    var localName:String{
        get{
            return self.name.localized()
        }
    }
    
    static let Smileys = RequestType(key: "smiley", name: "Smiley", rateMax: 3)
    static let Stars = RequestType(key: "rating", name: "Rating", rateMax: 5)
    static let YesNo = RequestType(key: "yes/no", name: "Yes/No", rateMax: 1)
    static let Types:[RequestType] = [
        .Smileys,
        .Stars,
        .YesNo,
    ]
    static func fromKey(key: String) -> RequestType{
        if let result = Types.first(where: {type in return type.key == key}){
            return result
        }
        return .Smileys
    }
}

public struct ContinuousInterval{
    let key: String
    var localName:String{
        get{
            return self.key.localized()
        }
    }
    
    static let OneTime = ContinuousInterval(key: "One Time")
    static let Weekly = ContinuousInterval(key: "Weekly")
    static let BiWeekly = ContinuousInterval(key: "Bi-Weekly")
    static let Monthly = ContinuousInterval(key: "Monthly")
    static let Quarterly = ContinuousInterval(key: "Quarterly")
    static let Intervals:[ContinuousInterval] = [
        .OneTime,
        .Weekly,
        .BiWeekly,
        .Monthly,
        .Quarterly
        ]
    static func fromKey(key: String) -> ContinuousInterval{
        if let result = Intervals.first(where: {interval in return interval.key == key}){
            return result
        }
        return .OneTime
    }
}
