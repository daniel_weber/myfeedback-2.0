//
//  Trophy.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 08.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import UIKit

public struct Trophy{
    let key: Int
    let name: String
    let level: Int
    let pushText: String
    let descripton:String
    
    var cardText:String{
        get{
            let str = self.descripton.localized()
            if self.level > 0{
                return str+"\n"+("Keep going!".localized())
            }
            return str
            
        }
    }
    var cardImage: UIImage{
        get{
            if let image = UIImage(named: "\(self.pushText)_\(Language.getCurrentLanguage().key.lowercased())"){
                return image
            }else if let image = UIImage(named: self.pushText){
                return image
            }
            return UIImage()
        }
    }
    var overViewImage: UIImage{
        get{
                if let image = UIImage(named: "\(self.pushText)Overview"){
                    return image
                }
                return UIImage()
            }
    }
    
    public static let DevelopmentDriverLevel0 = Trophy(key: 10, name: "Development Driver", level: 0, pushText: "trophyDevelopmentLevel0", descripton: "")
    public static let AllStarLevel0 = Trophy(key: 11, name: "Allstar", level: 0, pushText: "trophyAllstar0", descripton: "")
    public static let CompetencyChampionLevel0 = Trophy(key: 12, name: "Competency Champion", level: 0, pushText: "trophyCompetencyLevel0", descripton: "")
    public static let ExplorerLevel0 = Trophy(key: 13, name: "Explorer", level: 0, pushText: "trophyExplorer0", descripton: "")
    public static let FeedbackGuruLevel0 = Trophy(key: 14, name: "Feedback Guru", level: 0, pushText: "trophyFeedbackGuruLevel0", descripton: "")
    public static let RecognitionHeroLevel0 = Trophy(key: 15, name: "Recognition Hero", level: 0, pushText: "trophyRecognitionLevel0", descripton: "")
    
    public static let ExplorerLevel1 = Trophy(key: 1, name: "Explorer", level: 1, pushText: "trophyExplorer", descripton: "You've just taken your first action.")
    public static let DevelopmentDriverLevel1 = Trophy(key: 2, name: "Development Driver", level: 1, pushText: "trophyDevelopmentLevel1", descripton: "You've just sent your 5th Feedback Request.")
    public static let DevelopmentDriverLevel2 = Trophy(key: 4, name: "Development Driver", level: 2, pushText: "trophyDevelopmentLevel2", descripton: "You've just sent your 20th Feedback Request.")
    public static let DevelopmentDriverLevel3 = Trophy(key: 8, name: "Development Driver", level: 3, pushText: "trophyDevelopmentLevel3", descripton: "You've just sent your 100th Feedback Request.")
    public static let RecognitionHeroLevel1 = Trophy(key:16, name: "Recognition Hero", level: 1, pushText: "trophyRecognitionLevel1", descripton: "You've just sent your 5th Recognition Card.")
    public static let RecognitionHeroLevel2 = Trophy(key: 32, name: "Recognition Hero", level: 2, pushText: "trophyRecognitionLevel2", descripton: "You've just sent your 20th Recognition Card.")
    public static let RecognitionHeroLevel3 = Trophy(key: 64, name: "Recognition Hero", level: 3, pushText: "trophyRecognitionLevel3", descripton: "You've just sent your 100th Recognition Card.")
    public static let FeedbackGuruLevel1 = Trophy(key: 128, name: "Feedback Guru", level: 1, pushText: "trophyFeedbackGuruLevel1", descripton: "You've just answered your 5th Feedback Request.")
    public static let FeedbackGuruLevel2 = Trophy(key: 256, name: "Feedback Guru", level: 2, pushText: "trophyFeedbackGuruLevel2", descripton: "You've just answered your 20th Feedback Request.")
    public static let FeedbackGuruLevel3 = Trophy(key: 512, name: "Feedback Guru", level: 3, pushText: "trophyFeedbackGuruLevel3", descripton: "You've just answered your 100th Feedback Request.")
    public static let CompetencyChampionLevel1 = Trophy(key: 1024, name: "Competency Champion", level: 1, pushText: "trophyCompetencyLevel1", descripton: "You've just received all Recognition Cards for the first time.")
    public static let CompetencyChampionLevel2 = Trophy(key: 2048, name: "Competency Champion", level: 2, pushText: "trophyCompetencyLevel2", descripton: "You've just received all Recognition Cards for the 5th time.")
    public static let CompetencyChampionLevel3 = Trophy(key: 4096, name: "Competency Champion", level: 3, pushText: "trophyCompetencyLevel3", descripton: "You've just received all Recognition Cards for the 10th time.")
    public static let AllStarLevel1 = Trophy(key: 8192, name: "Allstar", level: 1, pushText: "trophyAllstar", descripton: "You've just tried out all MyFeedback functionalities.")
    
    
    
    public static let Trophies:[Trophy] = [
        .DevelopmentDriverLevel0,
        .DevelopmentDriverLevel1,
        .DevelopmentDriverLevel2,
        .DevelopmentDriverLevel3,
        .AllStarLevel0,
        .AllStarLevel1,
        .CompetencyChampionLevel0,
        .CompetencyChampionLevel1,
        .CompetencyChampionLevel2,
        .CompetencyChampionLevel3,
        .ExplorerLevel0,
        .ExplorerLevel1,
        .FeedbackGuruLevel0,
        .FeedbackGuruLevel1,
        .FeedbackGuruLevel2,
        .FeedbackGuruLevel3,
        .RecognitionHeroLevel0,
        .RecognitionHeroLevel1,
        .RecognitionHeroLevel2,
        .RecognitionHeroLevel3
    ]
    public static func getTrophy(pushtext: String) -> Trophy?{
        return Trophies.first(where: {trophy in return trophy.pushText == pushtext})
    }
    public static func getTrophy(key: Int) -> Trophy?{
        return Trophies.first(where: {trophy in return trophy.key == key})
    }
    public static func getTrophy(name: String, level: Int) -> Trophy?{
        return Trophies.first(where: {trophy in return trophy.name == name && trophy.level == level})
    }
    
}
