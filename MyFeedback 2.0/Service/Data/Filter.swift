//
//  Filter.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 08.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import Foundation

import Foundation

enum SortOrder{
    case ascending
    case descending
}
struct SortType{
    
    let label: String
    let key: String
    
    static let Date = SortType(label: "Date".localized(), key: "date")
    static let RequestType = SortType(label: "Type".localized(), key: "typeName")
    static let Title = SortType(label: "Title".localized(), key: "overviewTitle")
    static let RequestCategory = SortType(label: "Category".localized(), key: "categoryName")
    static let Sender = SortType(label: "Sender".localized(), key: "sender")
    
    static let Request:[SortType] = [
        .Date,
        .Title,
        .RequestType,
        .RequestCategory
    ]
    static let Recognition:[SortType] = [
        .Date,
        .Sender    ]
}

enum FilterType:String {
    case Category = "Category"
    case Employee = "Sender/Requester"
    case RequestType = "Type"
    case SeriesOnly = "Series only"
    case Series = "Series"
    case DateFrom = "Date From"
    case DateTo = "Date To"
    case Competency = "Competency"
}

struct Filter{
    let type: FilterType
    let value: Any
}
