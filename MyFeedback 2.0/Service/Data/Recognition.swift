//
//  Recognition.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 06.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import UIKit
import Bond

class Recognition: NSObject, Searchable {
    var id: Int?
    var competency: Competency
    var sender: Employee
    
    @objc var requesterName: String{
        get{
            return sender.getName()
        }
    }
    var repetition: Int?
    var text: String{
        didSet{
            isValid.value = checkIfValid()
        }
    }
    @objc var overviewTitle: String{
        get{
            return self.text
        }
    }
    @objc var date: Date?
    var receivers: [RecognitionReceiver] = []
    
    var isValid: Observable<Bool> = Observable<Bool>(false)
    
    var isRead: Bool{
        get{
            for rec in self.receivers{
                if rec.readTimestamp == nil{
                    return false
                }
            }
            return true
        }
    }
    
    var _searchTerm : String
    public var searchTerm : String {
        get {
            self.updateSearchTerm()
            return _searchTerm.lowercased()
        }
    }
    
    override init(){
        self.sender = Employee.invalid()
        self.competency = Competency.Collaborative
        self.text = ""
        _searchTerm = ""
        super.init()
        self.updateSearchTerm()
    }
    
    init(id: Int?, sender: Employee, competency: Competency, text: String, date: Date?, repetition: Int?){
        self.id = id
        self.sender = sender
        self.competency = competency
        self.text = text
        self.date = date
        self.repetition = repetition
        
        _searchTerm = ""
        super.init()
        self.updateSearchTerm()
    }
    
    init(id: Int?, sender: Employee, competency: Competency, text: String, date: Date?, repetition: Int?, receivers: [RecognitionReceiver]){
        self.id = id
        self.sender = sender
        self.competency = competency
        self.text = text
        self.date = date
        self.receivers = receivers
        self.repetition = repetition
        
        _searchTerm = ""
        super.init()
        self.updateSearchTerm()
    }
    
    func checkIfValid() -> Bool{
        return !self.text.isEmpty
    }
    
    func updateSearchTerm(){
        self._searchTerm = self.text
        self._searchTerm = "\(self._searchTerm)\(self.sender.searchTerm)"
        for receiver in self.receivers{
            self._searchTerm = "\(self._searchTerm)\(receiver.receiver.searchTerm)"
        }
    }
    
    static func invalid() -> Recognition {
        return Recognition()
    }
    
}

class RecognitionReceiver{
    var recognitionId: Int
    var receiver: Employee
    var readTimestamp: Date?
    var thankYouTimestamp: Date?
    var hideTimestamp: Date?
    
    var isRead: Bool{
        get{
            return self.readTimestamp != nil
        }
    }
    var isThanked: Bool{
        get{
            return self.thankYouTimestamp != nil
        }
    }
    var isHidden: Bool{
        get{
            return self.hideTimestamp != nil
        }
    }
    
    init(recognitionId: Int, receiver: Employee, readTimestamp: Date?, thankYouTimestamp: Date?, hideTimestamp: Date?){
        self.recognitionId = recognitionId
        self.receiver = receiver
        self.readTimestamp = readTimestamp
        self.thankYouTimestamp = thankYouTimestamp
        self.hideTimestamp = hideTimestamp
    }
}

public struct Competency{
    let key: String
    let name: String
    let slogan: String
    let textColor: UIColor
    let highlightColor: UIColor
    
    var amountTotal: Int?
    var amountUnread: Int?
    
    var icon: UIImage{
        get{
            if let image = UIImage(named: "Recognition_Icon_\(self.key)_\(Language.getCurrentLanguage().key.lowercased())"){
                return image
            }else if let image = UIImage(named: "Recognition_Icon_\(self.key)"){
                return image
            }
            return UIImage()
        }
    }
    var bubble: UIImage{
        get{
            if let image = UIImage(named: "Recognition_Bubble_\(self.key)_\(Language.getCurrentLanguage().key.lowercased())"){
                return image
            }else if let image = UIImage(named: "Recognition_Bubble_\(self.key)"){
                return image
            }else if let image = UIImage(named: "Recognition_Bubble_\(self.key)_en"){
                return image
            }
            return UIImage()
        }
    }
    var badge: UIImage{
        get{
            if let image = UIImage(named: "Recognition_Badge_\(self.key)_\(Language.getCurrentLanguage().key.lowercased())"){
                return image
            }else if let image = UIImage(named: "Recognition_Badge_\(self.key)_en"){
                return image
            }
            return UIImage()
        }
    }
    var card: UIImage{
        get{
            if let image = UIImage(named: "Recognition_Card_\(self.key)_\(Language.getCurrentLanguage().key.lowercased())"){
                return image
            }else if let image = UIImage(named: "Recognition_Card_\(self.key)_en"){
                return image
            }
            return UIImage()
        }
    }
    var localName:String{
        get{
            if self.key == "thank-you"{
                return "Thank You Note".localized()
            }
            return "Be \(self.name)".localized()
        
        }
    }
    var localSlogan:String{
        get{
            return self.slogan.localized()
        }
    }
    
    static let FutureOriented = Competency(key: "future-oriented", name: "Future Oriented", slogan: "shape the future", textColor: MerckColor.RichPurple, highlightColor: MerckColor.VibrantMagenta, amountTotal: nil, amountUnread: nil)
    static let Purposeful = Competency(key: "purposeful", name: "Purposeful", slogan: "Make great things happen", textColor: MerckColor.RichPurple, highlightColor: MerckColor.RichRed, amountTotal: nil, amountUnread: nil)
    static let ResultsDriven = Competency(key: "results-driven", name: "Results Driven", slogan: "take ownership", textColor: MerckColor.RichPurple, highlightColor: MerckColor.VibrantGreen, amountTotal: nil, amountUnread: nil)
    static let Innovative = Competency(key: "innovative", name: "Innovative", slogan: "take calculated risks", textColor: MerckColor.RichPurple, highlightColor: MerckColor.VibrantCyan, amountTotal: nil, amountUnread: nil)
    static let Collaborative = Competency(key: "collaborative", name: "Collaborative", slogan: "have an inclusive mindset", textColor: MerckColor.RichPurple, highlightColor: MerckColor.VibrantYellow, amountTotal: nil, amountUnread: nil)
    static let Empowering = Competency(key: "empowering", name: "Empowering", slogan: "inspire other people to reach their full potential", textColor: MerckColor.RichPurple, highlightColor: MerckColor.VibrantGreen, amountTotal: nil, amountUnread: nil)
    static let ThankYouNote = Competency(key: "thank-you", name: "Thank You", slogan: "Thank You", textColor: MerckColor.RichPurple, highlightColor: MerckColor.VibrantGreen, amountTotal: nil, amountUnread: nil)
    static let Competencies:[Competency] = [
        .FutureOriented,
        .Purposeful,
        .ResultsDriven,
        .Innovative,
        .Collaborative,
        .Empowering
    ]
    static func getCompetency(id: String) -> Competency?{
        if let result = Competencies.first(where: {competency in return competency.key == id}){
            return result
        }else if id == ThankYouNote.key{
            return ThankYouNote
        }
        return nil
    }
    static func getCompetency(name: String) -> Competency?{
        if let result = Competencies.first(where: {competency in return competency.name == name}){
            return result
        }else if name == ThankYouNote.name{
            return ThankYouNote
        }
        return nil
    }
    static func getCompetency(name: String, amountTotal: Int?, amountUnread: Int?) -> Competency?{
        if var result = Competencies.first(where: {competency in return competency.name == name}){
            result.amountTotal = amountTotal
            result.amountUnread = amountUnread
            return result
        }else if name == ThankYouNote.name{
            var thankYou = ThankYouNote
            thankYou.amountTotal = amountTotal
            thankYou.amountUnread = amountUnread
            return thankYou
        }
        return nil
    }
}


