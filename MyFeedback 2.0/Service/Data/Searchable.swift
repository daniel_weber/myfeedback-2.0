//
//  Searchable.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 31.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import Foundation
import Hydra
import Bond

protocol Searchable {
    // MARK Attributes
    
    var id:Int? {get set}
    var _searchTerm : String {get set}
    var searchTerm : String {get}
    
    var isValid: Observable<Bool>{get set}
    
    func updateSearchTerm()
}
