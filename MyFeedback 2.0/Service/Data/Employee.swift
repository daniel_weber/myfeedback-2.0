//
//  Employee.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 06.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import Foundation

class Employee{
    var userId: String
    var muid: String
    var firstName: String
    var lastName: String
    var email: String
    var orgUnitName: String
    
    var isValid = true
    var isSelected = false
    var isSuggested = false
    
    private var _searchTerm : String
    public var searchTerm : String {
        get {
            return _searchTerm
        }
    }
    
    init(userId: String, muid: String, firstName: String, lastName: String, email: String, orgUnitName: String){
        self.userId = userId
        self.muid = muid
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.orgUnitName = orgUnitName
        
        _searchTerm = ""
        self.updateSearchTerm()
    }
    
    private init(){
        self.firstName = ""
        self.lastName = ""
        self.email = ""
        self.orgUnitName = ""
        self.userId = "invalid"
        self.muid = "invalid"
        self.isValid = false
        
        _searchTerm = ""
        self.updateSearchTerm()
    }
    
    func getName()->String{
        return "\(capitalizeNames(name: firstName)) \(capitalizeNames(name: lastName))"
    }
    
    func updateSearchTerm(){
        self._searchTerm = "\(self.userId.lowercased())"
        self._searchTerm = "\(self._searchTerm)\(self.getName().lowercased())"
        self._searchTerm = "\(self._searchTerm)\(self.orgUnitName.lowercased())"
    }
    
    static func invalid() -> Employee {
        return Employee()
    }
    
    func capitalizeNames(name:String)->String{
        var fullName = ""
        var nameHyphon = [String]()
        let namesSpace = name.split(separator: " ").map(String.init)
        for splittedName in namesSpace{
            nameHyphon.append(contentsOf: splittedName.split(separator: "-").map(String.init))
        }
        for (index, singleName) in nameHyphon.enumerated(){
            fullName += singleName.capitalized
            if index < nameHyphon.count-1{
                fullName += " "
            }
        }
        return fullName
        
    }
}
