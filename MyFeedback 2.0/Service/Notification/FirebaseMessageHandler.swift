//
//  FirebaseMessageHandler.swift
//  MIA
//
//  Created by Daniel Weber on 19.10.17.
//  Copyright © 2017 zero2one. All rights reserved.
//

import Foundation
import UIKit
import Hydra
import Bond
import Firebase
import FirebaseMessaging
import UserNotifications
import SwiftyJSON
import KeychainSwift

enum NotificationActionIdentifiers: String{
    case RecognitionThank = "action.recognition.thank"
    case RequestThank = "action.request.thank"
    case RequestDismiss = "action.request.dismiss"
    case RequestAnswer = "action.request.answer"
}
enum NotificationCategoryIdentifiers: String{
    case RequestAnswered = "request.answered"
    case RequestReceived = "request.received"
    case requestThanked = "request.thanked"
    case RecognitionReceived = "recognition.received"
    case RecognitionThanked = "recognition.thanked"
    case TrophyAchieved = "trophy.achieved"
}

class FirebaseMessageHandler: NSObject, MessageHandling, MessagingDelegate{
    
    var token:String? = Messaging.messaging().fcmToken
    
    let service = try! Dip.container.resolve() as FeedbackService
    let deeplinking = try! Dip.container.resolve() as Deeplinking
    
    var apnsToken: Data?{
        didSet{
            Messaging.messaging().apnsToken = apnsToken
            self.registerForPushNotifications().then({_ in })
        }
    }
    var apnsTokenString: String?{
        get{
            if self.apnsToken == nil{
                return nil
            }
            return self.apnsToken!.reduce("", {$0 + String(format: "%02X", $1)})
        }
    }
    
    override init(){
        super.init()
        Messaging.messaging().delegate = self
        self.setCategories()
    }
    
    // MARK: Firebase Messaging Delegate Methods
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        self.token = fcmToken
        self.registerForPushNotifications().then({_ in})
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print(remoteMessage.appData)
    }
    
    func application(received remoteMessage: MessagingRemoteMessage) {
         print(remoteMessage.appData)
    }
    
    // MARK: UserNotificationCenter Delegate Methods
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        print("Handle push from background or closed")
        // if you set a member variable in didReceiveRemoteNotification, you  will know if this is from closed or background
        print("\(response.notification.request.content.userInfo)")
        let request = response.notification.request
        let content = request.content
        let userInfo = content.userInfo
        self.service.update().then({_ in
            
        })
        self.handleActionPayload(userInfo: userInfo)
        completionHandler()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,  willPresent notification: UNNotification, withCompletionHandler   completionHandler: @escaping (_ options:   UNNotificationPresentationOptions) -> Void) {
        print("Handle push from foreground")
        // custom code to handle push while app is in the foreground
        Messaging.messaging().appDidReceiveMessage(notification.request.content.userInfo)
        self.handleCounts(userInfo: notification.request.content.userInfo)
        self.service.update().then({_ in
            
        })
        completionHandler([.badge, .alert, .sound])
        print("body: \(notification.request.content.body)")
    }
    
    func handleCounts(userInfo: [AnyHashable: Any]){
        let json = JSON.init(parseJSON: (userInfo["data"] as? String) ?? "")
        if let dict = json.dictionaryObject, let counts = dict["counts"] as? [String: Int]{
            print(counts)
            self.service.amountNewRequests.value = counts["RequestReceivedPending"] ?? 0
            self.service.amountUnreadResponses.value = counts["RequestSentResponseUnread"] ?? 0
            self.service.amountUnreadRecognitions.value = counts["RecognitionReceivedUnread"] ?? 0
            self.service.amountRequestsTotal.value = counts["RequestSent"] ?? 0
            self.service.amountRecognitionsTotal.value = counts["RecognitionReceived"] ?? 0
            self.service.amountUnreadTotal.value = counts["UnreadItems"] ?? 0
        }
            
    }
    
    func handleTrophyPayload(userInfo: [AnyHashable: Any]){
        self.handleCounts(userInfo: userInfo)
        let json = JSON.init(parseJSON: (userInfo["data"] as? String) ?? "{}")
        if let type = json.dictionaryObject!["type"] as? String{
            switch type{
            case "trophy":
                if let id = json.dictionaryObject!["ID"] as? Int, let trophy = Trophy.getTrophy(key: id){
                    KeychainSwift().set("\(id)", forKey: "trophy")
                    let mvc : ModalViewController = try! Dip.container.resolve() as ModalViewController
                    let trophyView = TrophyModalView()
                    trophyView.isInOverview = false
                    trophyView.trophy = trophy
                    trophyView.result = {str, success in
                        KeychainSwift().delete("trophy")
                        if success{
                            self.deeplinking.redirectToTrophies().then({_ in
                                Loading.hide()
                            })
                        }
                        return success
                    }
                    mvc.present(modal: trophyView)
                }
            default:
                break
            }
        }
    }
    
    func handleActionPayload(userInfo: [AnyHashable: Any]){
        self.service.AnalyticsOpenApplicationFromPush()
        self.handleCounts(userInfo: userInfo)
        let json = JSON.init(parseJSON: (userInfo["data"] as? String) ?? "{}")
        if let type = json.dictionaryObject!["type"] as? String{
            switch type{
            case "request":
                if let id = json.dictionaryObject!["ID"] as? Int, let repetition = json.dictionaryObject!["repetition"] as? Int{
                    SAPcpms.shared.fetchNewRequest(requestID: id, repetition: repetition).then({model in
                        if let request = model?.request(){
                            if loggedInUser.value != nil, let response = model?.emptyResponse(emp: loggedInUser.value!){
                                request.responses.append(response)
                            }
                            self.deeplinking.redirectToNewRequest(request).then({_ in
                                Loading.hide()
                            })
                        }
                    })
                }
            case "answered":
                if let id = json.dictionaryObject!["ID"] as? Int, let repetition = json.dictionaryObject!["repetition"] as? Int, let raterUID = json.dictionaryObject!["pushUserID"] as? String{
                    SAPcpms.shared.fetchAnsweredRequest(requestID: id, repetition: repetition, raterUID: raterUID).then({model in
                        if let request = model?.request(){
                            self.deeplinking.redirectToAnsweredRequest(request).then({_ in
                                Loading.hide()
                            })
                        }
                    })
                }
            case "recognition":
                if let id = json.dictionaryObject!["ID"] as? Int, let pushUID = json.dictionaryObject!["pushUserID"] as? String{
                    SAPcpms.shared.fetchReceivedRecognition(recognitionID: id, recognizerUID: pushUID).then({model in
                        if let recognition = model?.recognition(){
                            self.deeplinking.redirectToRecognition(recognition).then({_ in
                                Loading.hide()
                            })
                        }
                    })
                }
            case "thanks":
                if let id = json.dictionaryObject!["ID"] as? Int, let pushUID = json.dictionaryObject!["pushUserID"] as? String{
                    SAPcpms.shared.fetchReceivedRecognition(recognitionID: id, recognizerUID: pushUID).then({model in
                        if let recognition = model?.recognition(){
                            self.deeplinking.redirectToThankYou(recognition).then({_ in
                                Loading.hide()
                            })
                        }
                    })
                }
            case "trophy":
                if let id = json.dictionaryObject!["ID"] as? Int, let trophy = Trophy.getTrophy(key: id){
                    KeychainSwift().set("\(id)", forKey: "trophy")
                    let mvc : ModalViewController = try! Dip.container.resolve() as ModalViewController
                    let trophyView = TrophyModalView()
                    trophyView.isInOverview = false
                    trophyView.trophy = trophy
                    trophyView.result = {str, success in
                        KeychainSwift().delete("trophy")
                        if success{
                            self.deeplinking.redirectToTrophies().then({_ in
                                Loading.hide()
                            })
                        }
                        return success
                    }
                    mvc.present(modal: trophyView)
                }
            default:
                break
            }
        }
    }
    
    
    func didReceiveMessage(userInfo: [AnyHashable: Any]){
        //self.handlePayload(userInfo: userInfo)
        print(userInfo)
    }
    
    func registerForPushNotifications()->Promise<Any>{
        return Promise(in: .background,  {resolve, reject, _ in
            guard loggedInUser.value != nil else {
                reject(webError.webServiceError("Tokens not Set"))
                return
            }
            Messaging.messaging().subscribe(toTopic: loggedInUser.value!.userId)
            Messaging.messaging().subscribe(toTopic: "de.merck.myFeedback")
           
        })
        
        
        
    }
    
    func setCategories(){
        let dismissAction = UNNotificationAction(
            identifier: NotificationActionIdentifiers.RequestDismiss.rawValue,
            title: "Dismiss",
            options: [.foreground])
        let answerAction = UNNotificationAction(
            identifier: NotificationActionIdentifiers.RequestAnswer.rawValue,
            title: "Answer",
            options: [.foreground])
        let thankRecognitionAction = UNNotificationAction(
            identifier: NotificationActionIdentifiers.RecognitionThank.rawValue,
            title: "Thank You",
            options: [.foreground])
        let thankRequestAction = UNNotificationAction(
            identifier: NotificationActionIdentifiers.RequestThank.rawValue,
            title: "Thank You",
            options: [.foreground])
        let requestAnsweredCategory = UNNotificationCategory(
            identifier: NotificationCategoryIdentifiers.RequestAnswered.rawValue,
            actions: [thankRequestAction],
            intentIdentifiers: [],
            options: [])
        let requestReceivedCategory = UNNotificationCategory(
            identifier: NotificationCategoryIdentifiers.RequestReceived.rawValue,
            actions: [answerAction, dismissAction],
            intentIdentifiers: [],
            options: [])
        let recognitionReceivedCategory = UNNotificationCategory(
            identifier: NotificationCategoryIdentifiers.RecognitionReceived.rawValue,
            actions: [thankRecognitionAction],
            intentIdentifiers: [],
            options: [])
        UNUserNotificationCenter.current().setNotificationCategories(
            [requestAnsweredCategory, requestReceivedCategory, recognitionReceivedCategory])
    }
    
    func getRequestFromBase64(_ str: String)->Request?{
        if let data = Data(base64Encoded: str){
            do {
                let json = try JSON(data: data)
                let requestModel = RequestModel(dictionary: json.dictionaryObject as! NSDictionary)
                return requestModel?.request()
            } catch {
                return nil
            }
        }
        return nil
    }
    
    func getRecognitionFromBase64(_ str: String)->Recognition?{
        if let data = Data(base64Encoded: str){
            do {
                let json = try JSON(data: data)
                let recognitionModel = RecognitionModel(dictionary: json.dictionaryObject as! NSDictionary)
                return recognitionModel?.recognition()
            } catch {
                return nil
            }
        }
        return nil
    }
    
}

