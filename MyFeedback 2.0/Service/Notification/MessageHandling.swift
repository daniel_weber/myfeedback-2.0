//
//  MessageHandling
//  MIA
//
//  Created by Daniel Weber on 24.10.17.
//  Copyright © 2017 zero2one. All rights reserved.
//

import Foundation
import Hydra
import UserNotifications

/**
 Protocol for a Push Notification Handling implementation.
 - **apnsToken : Data?** Apples APNS token, returned after successful registration to the APNS service.
 - **apnsTokenString : String?** APNS token represented as String
 */
protocol MessageHandling: UNUserNotificationCenterDelegate{
    
    var apnsToken: Data?{ get set }
    var apnsTokenString: String? { get }
    
    /**
     Handles the Payload of a Notification.
     - parameter userInfo: userInfo provided by the Notification
     */
    func handleTrophyPayload(userInfo: [AnyHashable: Any])
    /**
     Handles the Payload of a Notification.
     - parameter userInfo: userInfo provided by the Notification
     */
    func handleActionPayload(userInfo: [AnyHashable: Any])
    
    /**
     Registers the user in the respective backend (does not handle the registration for the APNS Service itself)
     */
    func registerForPushNotifications()->Promise<Any>
    
}

