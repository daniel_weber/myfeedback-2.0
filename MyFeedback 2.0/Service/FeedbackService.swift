//
//  FeedbackService.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 08.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import Foundation
import Bond
import Hydra
import Dip
import Alamofire
import KeychainSwift
import EventKit
import SwiftyJSON
import SwiftDate
import Localize_Swift

class FeedbackService{
    
    
    var amountNewRequests: Observable<Int> = Observable<Int>(0)
    var amountUnreadResponses: Observable<Int> = Observable<Int>(0)
    var amountUnreadRecognitions: Observable<Int> = Observable<Int>(0)
    var amountRequestsTotal: Observable<Int> = Observable<Int>(0)
    var amountRecognitionsTotal: Observable<Int> = Observable<Int>(0)
    
    var amountUnreadTotal: Observable<Int> = Observable<Int>(0)
    var amountTotal: Observable<Int> = Observable<Int>(0)
    
    var requestDraft: Observable<Request?> = Observable<Request?>(nil)
    var recognitionDraft: Observable<Recognition?> = Observable<Recognition?>(nil)
    
    var myRequests: MutableObservableArray<Request> = MutableObservableArray<Request>([])
    var myRecognition: MutableObservableArray<Recognition> = MutableObservableArray<Recognition>([])
    var myTrophies: MutableObservableArray<Trophy> = MutableObservableArray<Trophy>([])
    
    var myRequestsFiltered: [Request]{
        get{
            return self.myRequests.array.filter({request in return self.filter(request)})
        }
    }
    var myRecognitionFiltered: [Recognition]{
        get{
            return self.myRecognition.array.filter({recognition in return self.filter(recognition)})
        }
    }
    
    var competencies: MutableObservableArray<Competency> = MutableObservableArray<Competency>([])
    
    var newRequests: MutableObservableArray<Request> = MutableObservableArray<Request>([])
    var sentResponses: MutableObservableArray<Request> = MutableObservableArray<Request>([])
    var sentRecognitions: MutableObservableArray<Recognition> = MutableObservableArray<Recognition>([])
    
    var newRequestsFiltered: [Request]{
        get{
            return self.newRequests.array.filter({request in return self.filter(request)})
        }
    }
    var sentResponsesFiltered: [Request]{
        get{
            return self.sentResponses.array.filter({request in return self.filter(request)})
        }
    }
    var sentRecognitionsFiltered: [Recognition]{
        get{
            return self.sentRecognitions.array.filter({recognition in return self.filter(recognition)})
        }
    }
    
    var userSettings: Observable<UserSettings> = Observable<UserSettings>(UserSettings.standard)
    
    var employeeList: MutableObservableArray<Employee> = MutableObservableArray<Employee>([])
    
    var selectedEmployees:[Employee] = []
    
    var suggestedEmployees: MutableObservableArray<Employee> = MutableObservableArray<Employee>([])
    
    var activeFilters: MutableObservableArray<Filter> = MutableObservableArray<Filter>([])
    
    var sortBy: Observable<(SortType, SortOrder)> = Observable<(SortType, SortOrder)>((SortType.Date, SortOrder.ascending))
    
    var events: [EKEvent] = []
    
    init(){
        myRequests.observeNext(with: {e in
            self.amountRequestsTotal.value = e.dataSource.array.count
            self.amountUnreadResponses.value = e.dataSource.array.filter({req in return !req.readFlag}).count
        })
        myRecognition.observeNext(with: {e in
            self.amountRecognitionsTotal.value = e.dataSource.array.count
            self.amountUnreadRecognitions.value = e.dataSource.array.filter({rec in return !rec.isRead}).count
        })
        newRequests.observeNext(with: {e in
            self.amountNewRequests.value = e.dataSource.array.count
        })
        
        amountNewRequests.observeNext(with: {_ in
            self.setTotalCount()
        })
        amountUnreadRecognitions.observeNext(with: {_ in
            self.setTotalCount()
            self.setUnreadCount()
        })
        amountUnreadResponses.observeNext(with: {_ in
            self.setTotalCount()
            self.setUnreadCount()
        })
        
        
    }
    
    func setTotalCount(){
        var total = 0
        total += self.amountUnreadRecognitions.value
        total += self.amountUnreadResponses.value
        total += self.amountNewRequests.value
        self.amountTotal.value = total
    }
    
    func setUnreadCount(){
        var total = 0
        total += self.amountUnreadRecognitions.value
        total += self.amountUnreadResponses.value
        self.amountUnreadTotal.value = total
    }
    
    func updateCountNumbers(){
        self.amountRequestsTotal.value = self.myRequests.array.count
        self.amountUnreadResponses.value = self.myRequests.array.filter({req in return !req.readFlag}).count
        self.amountRecognitionsTotal.value = self.myRecognition.array.count
        self.amountUnreadRecognitions.value = self.myRecognition.array.filter({rec in return !rec.isRead}).count
        self.amountNewRequests.value = self.newRequests.array.count
    }
    
    func checkIfValid(_ recognition: Recognition?) -> Bool{
        return recognition?.isValid.value == true && self.selectedEmployees.count > 0
    }
    
    func checkIfValid(_ request: Request?) -> Bool{
        return request?.isValid.value == true && self.selectedEmployees.count > 0
    }
    
    
    func sendFeedbackRequest(request: Request, receivers:[Employee]) -> Promise<(success:Bool, message:String?)>{
        return Promise(in: .background, {resolve, reject, _ in
            var raterUIDs:[[String:String]] = []
            for user in receivers{
                raterUIDs.append(["raterUID" : user.userId])
            }
            let params:[String:Any] = [
                "requestCategoryID": request.category?.name,
                "type": request.type?.key,
                "text": request.text,
                "continuousInterval": request.continuousInterval?.key,
                "continuousRepetitions": request.continuousRepetitions,
                "scaleMinText": request.scaleTextMin,
                "scaleMaxText": request.scaleTextMax,
                "title": request.title,
                "raterUIDs": raterUIDs
            ]
            SAPcpms.shared.requestCreate(params: params).then({response in
                if response.success{
                    self.deselectEmployees()
                    self.requestDraft.value = nil
                }
                resolve(response)
                self.updateSentRequests().then({_ in})
            }).catch({e in reject(e)})
        })
    }
    
    func sendRecognition(recognition: Recognition, receivers: [Employee]) -> Promise<(success:Bool, message:String?)>{
        return Promise(in: .background, {resolve, reject, _ in
            SAPcpms.shared.recognitionCreate(competencyID: recognition.competency.name, text: recognition.text, receiverUserIDs: receivers.map{$0.userId}).then({response in
                if response.success{
                    self.deselectEmployees()
                    self.recognitionDraft.value = nil
                    self.updateAnswerSection().then({_ in})
                    
                }
                resolve(response)
            }).catch({e in reject(e)})
        })
    }
    
    func answerRequest(request: Request, requestResponse: RequestResponse) -> Promise<(success:Bool, message:String?)>{
        return Promise(in: .background, {resolve, reject, _ in
            guard request.id != nil, requestResponse.text != nil, requestResponse.rating != nil else{
                reject(webError.webServiceError("An Error occured"))
                return
            }
            SAPcpms.shared.requestRespond(requestID: request.id!, repetition: requestResponse.repetition, responseText: requestResponse.text!, responseRating: requestResponse.rating!).then({response in
                if response.success{
                    requestResponse.responseTimestamp = Date()
                    request.responses = [requestResponse]
                    if let index = self.newRequests.array.index(where: {req in return req.id == request.id && req.responses.first?.repetition == requestResponse.repetition}){
                        self.newRequests.remove(at: index)
                        self.sentResponses.append(request)
                    }
                    self.updateAnswerSection().then({_ in})
                }
                resolve(response)
            }).catch({e in reject(e)})
        })
    }
    
    func dismissRequest(request: Request, requestResponse: RequestResponse) -> Promise<(success:Bool, message:String?)>{
        return Promise(in: .background, {resolve, reject, _ in
            guard request.id != nil else{
                reject(webError.webServiceError("An Error occured"))
                return
            }
            SAPcpms.shared.requestHide(requestID: request.id!, repetition: requestResponse.repetition).then({response in
                if response.success{
                    if let index = self.newRequests.array.index(where: {req in return req.id == request.id && req.responses.first?.repetition == requestResponse.repetition}){
                        self.newRequests.remove(at: index)
                    }
                }
                resolve(response)
            }).catch({e in reject(e)})
        })
    }
    
    func deleteRequest(request: Request)->Promise<(success:Bool, message:String?)>{
        return Promise(in: .background, {resolve, reject, _ in
            guard request.id != nil else{
                let e = webError.webServiceError("Recognition ID missing")
                ErrorView.show(error: e)
                reject(e)
                return
            }
            SAPcpms.shared.requestDelete(requestID: request.id!).then({response in
                if let index = self.myRequests.array.index(where: {req in return req.id == request.id}){
                    self.myRequests.remove(at: index)
                }
                resolve(response)
            }).catch({e in reject(e)})
        })
    }
    
    func deleteRecognition(recognition: Recognition)->Promise<(success:Bool, message:String?)>{
        return Promise(in: .background, {resolve, reject, _ in
            SAPcpms.shared.recognitionDelete(recognitionID: recognition.id!).then({response in
                if let index = self.myRecognition.array.index(where: {rec in return rec.id == recognition.id}){
                    self.myRecognition.remove(at: index)
                }
                resolve(response)
            }).catch({e in reject(e)})
        })
    }
    
    func hideRecognition() -> Promise<Recognition?>{
        return Promise(in: .background, {resolve, reject, _ in
            resolve(nil)
        })
    }
    
    func hideThankYouNote(recognition: Recognition) -> Promise<Recognition?>{
        return Promise(in: .background, {resolve, reject, _ in
            if recognition.id != nil{
                SAPcpms.shared.thankYouNoteHide(recognitionID: recognition.id!, receiverUID: recognition.sender.userId, repetition: recognition.repetition).then({_ in
                    if let index = self.myRecognition.array.index(where: {rec in return rec.id == recognition.id}){
                        self.myRecognition.remove(at: index)
                    }
                    self.updateCountNumbers()
                })
            }
            resolve(nil)
        })
    }
    
    func readThankYouNote(recognition: Recognition) -> Promise<Recognition?>{
        return Promise(in: .background, {resolve, reject, _ in
            if recognition.id != nil && !recognition.isRead{
                SAPcpms.shared.thankYouNoteRead(recognitionID: recognition.id!, receiverUID: recognition.sender.userId, repetition: recognition.repetition).then({_ in
                    recognition.receivers.first?.readTimestamp = Date()
                    self.updateCountNumbers()
                })
            }
            resolve(nil)
        })
    }
    
    
    func readResponse(response: RequestResponse) -> Promise<Request?>{
        return Promise(in: .background, {resolve, reject, _ in
            if response.isRead || response.readFlag == 1{
                resolve(nil)
                return
            }
            SAPcpms.shared.responseRead(requestId: response.requestId, repetition: response.repetition, raterUID: response.rater.userId).then({_ in
                response.readFlag = 1
                if let request = self.myRequests.array.first(where: {req in return req.id == response.requestId}){
                    request.unreadCount = max((request.unreadCount ?? 0) - 1, 0)
                }
                self.updateCountNumbers()
                resolve(nil)
            }).catch({e in reject(e)})
        })
    }
    
    func readAllResponses(request: Request) -> Promise<Request?>{
        return Promise(in: .background, {resolve, reject, _ in
            guard request.id != nil else{
                let e = webError.webServiceError("Recognition ID missing")
                ErrorView.show(error: e)
                reject(e)
                return
            }
            if request.unreadCount == 0 || request.readFlag{
                resolve(nil)
                return
            }
            SAPcpms.shared.requestReadAll(requestID: request.id!).then({_ in
                for response in request.responses{
                    response.readFlag = 1
                }
                request.unreadCount = 0
                self.updateCountNumbers()
                resolve(nil)
            }).catch({e in reject(e)})
        })
    }
    
    func readRecognition(recognition: Recognition) -> Promise<Recognition?>{
        return Promise(in: .background, {resolve, reject, _ in
            if recognition.id != nil && recognition.sender.userId != loggedInUser.value?.userId && !recognition.isRead{
                SAPcpms.shared.recognitionRead(recognitionID: recognition.id!).then({_ in
                    recognition.receivers.first?.readTimestamp = Date()
                    self.updateCountNumbers()
                })
            }
            resolve(nil)
        })
    }
    
    func thankForResponse(_ response: RequestResponse) -> Promise<(success:Bool, message:String?)>{
        return Promise(in: .background, {resolve, reject, _ in
            SAPcpms.shared.requestThank(requestID: response.requestId, repetition: response.repetition, raterUID: response.rater.userId).then({resp in
                if resp.success{
                    response.thankYouFlag = 1
                }
                resolve(resp)
            }).catch({e in reject(e)})
        })
    }
    
    func thankForAllResponses(request: Request) -> Promise<(success:Bool, message:String?)>{
        return Promise(in: .background, {resolve, reject, _ in
            guard request.id != nil else{
                let e = webError.webServiceError("Recognition ID missing")
                reject(e)
                return
            }
            SAPcpms.shared.requestThankAll(requestID: request.id!).then({resp in
                if resp.success{
                    for response in request.responses{
                        if response.isAnswered{
                            response.thankYouFlag = 1
                        }
                    }
                }
                resolve(resp)
            }).catch({e in reject(e)})
        })
    }
    
    func thankForRecognition(recognition: Recognition) -> Promise<(success:Bool, message:String?)>{
        return Promise(in: .background, {resolve, reject, _ in
            SAPcpms.shared.recognitionThankYou(recognitionID: recognition.id!).then({response in
                recognition.receivers.first?.thankYouTimestamp = Date()
                resolve(response)
            }).catch({e in reject(e)})
        })
    }
    
    func getResponses(request: Request) -> Promise<Request?>{
        return Promise(in: .background, {resolve, reject, _ in
            SAPcpms.shared.fetchResponses(requestID: request.id!).then({responseModels in
                for model in responseModels{
                    if let response = model.response(){
                        request.responses.append(response)
                    }
                }
                resolve(request)
            })
        })
    }
    
    func updateSuggestedEmployees()->Promise<[Employee]>{
        return Promise(in: .background, {resolve, reject, _ in
            let status = EKEventStore.authorizationStatus(for: EKEntityType.event)
            let eventStore = EKEventStore()
            
            let callback = { (_ es: EKEventStore)->Promise<[Employee]> in
                return Promise(in: .background, {resolve, reject, _ in
                    self.loadCalendars(eventStore: es).then({calendars in
                        self.loadEvents(calendars: calendars, eventStore: es).then({events in
                            self.getParticipantsFromEvents(events: events).then({emails in
                                self.updateSuggestedEmployees(emails: emails).then({employees in
                                    resolve(employees)
                                })
                            }).catch({e in reject(e)})
                        }).catch({e in reject(e)})
                    }).catch({e in reject(e)})
                })
            }
            
            switch (status) {
            case EKAuthorizationStatus.notDetermined:
                // This happens on first-run
                self.requestAccessToCalendar(eventStore: eventStore).then({accessGranted in
                    if accessGranted{
                        callback(eventStore).then({employees in
                            resolve(employees)
                        }).catch({e in reject(e)})
                    }else{
                        reject(webError.webServiceError("Access Restricted"))
                    }
                })
                break
            case EKAuthorizationStatus.authorized:
                // Things are in line with being able to show the calendars in the table view
                callback(eventStore).then({employees in
                    resolve(employees)
                }).catch({e in reject(e)})
                break
            case EKAuthorizationStatus.restricted, EKAuthorizationStatus.denied:
                reject(webError.webServiceError("Access Restricted"))
            }
        })
        
    }
    
    private func requestAccessToCalendar(eventStore: EKEventStore) ->Promise<Bool>{
        return Promise(in: .background, {resolve, reject, _ in
            eventStore.requestAccess(to: EKEntityType.event, completion: {
                (accessGranted: Bool, error: Error?) in
                if accessGranted == true {
                    resolve(true)
                } else if error != nil {
                    reject(error!)
                } else {
                    resolve(false)
                }
            })
        })
    }
    
    private func loadCalendars(eventStore: EKEventStore)  -> Promise<[EKCalendar]>{
        return Promise(in: .background, {resolve, reject, _ in
            resolve(eventStore.calendars(for: EKEntityType.event).filter({cal in cal.type == EKCalendarType.exchange}))
        })
    }
    
    private func getParticipantsFromEvents(events: [EKEvent]) ->Promise<[String]>{
        return Promise(in: .background, {resolve, reject, _ in
            var emailsFound:[String] = []
            for event in events{
                
                if(event.organizer != nil){
                    let idx = event.organizer!.url.absoluteString.index(of: ":")
                    let idx2 = event.organizer!.url.absoluteString.index(after: idx!)
                    let email = event.organizer!.url.absoluteString.substring(from: idx2).uppercased()
                    emailsFound.append(email.uppercased())
                }
                
                if(event.attendees != nil){
                    for attandee in event.attendees!{
                        if(attandee.participantType == .person && attandee.name?.contains("@") != true){
                            if let idx = attandee.url.absoluteString.index(of: ":"){
                                let idx2 = attandee.url.absoluteString.index(after: idx)
                                let email = attandee.url.absoluteString.substring(from: idx2).uppercased()
                                emailsFound.append(email.uppercased())
                            }
                        }
                    }
                }
            }
            resolve(emailsFound)
        })
    }
    
    private func loadEvents(calendars: [EKCalendar], eventStore: EKEventStore) -> Promise<[EKEvent]>{
        return Promise(in: .background, {resolve, reject, _ in
            
            // Create start and end date NSDate instances to build a predicate for which events to select
            let startDate = Date(timeInterval: -60*60*24, since: Date())
            let endDate = Date()
            
            // Use an event store instance to create and properly configure an NSPredicate
            let eventsPredicate = eventStore.predicateForEvents(withStart: startDate, end: endDate, calendars: calendars)
            
            // Use the configured NSPredicate to find and return events in the store that match
            let events = eventStore.events(matching: eventsPredicate).filter({e in e.attendees != nil && e.attendees!.count<50}).sorted(){
                (e1: EKEvent, e2: EKEvent) -> Bool in
                return e1.startDate.compare(e2.startDate) == ComparisonResult.orderedAscending
            }
            self.events = events
            resolve(events)
        })
        
    }
    
    func toggleEmployeeSelection(employee: Employee){
        if employee.isSelected{
            self.deSelectEmployee(employee: employee)
        }else if self.selectedEmployees.count < 50{
            self.selectEmployee(employee: employee)
        }
    }
    
    func selectEmployee(employee: Employee){
        employee.isSelected = true
        self.selectedEmployees.append(employee)
    }
    
    func deSelectEmployee(employee: Employee){
        employee.isSelected = false
        if let index = selectedEmployees.index(where: {emp in return emp.userId == employee.userId}){
            self.selectedEmployees.remove(at: index)
        }
    }
    
    func deselectEmployees(){
        for emp in self.selectedEmployees{
            emp.isSelected = false
        }
        self.selectedEmployees = []
    }
    
    func getCountsForCompetency(competency: Competency)->(total: Int, unread: Int?){
        let total =  self.myRecognition.array.filter({rec in return self.filter(rec) &&  rec.competency.name == competency.name}).count
        let unread =  self.myRecognition.array.filter({rec in return self.filter(rec) &&  rec.competency.name == competency.name && !rec.isRead}).count
        return(total, unread)
    }
    
    func searchEmployees(searchString: String)->Promise<[Employee]>{
        return Promise(in: .background, {resolve, reject, _ in
            guard loggedInUser.value != nil else{
                reject(webError.webServiceError("Not logged in"))
                return
            }
            var searchWords = searchString.split(separator: " ").map(String.init)
            SAPcpms.shared.searchUserMasterdata(searchWords: searchWords, userId: loggedInUser.value!.userId).then({userMasterdata in
                var employees: [Employee] = []
                for model in userMasterdata{
                    if let employee = model.employee(), !self.suggestedEmployees.array.contains(where: {emp in return emp.userId == employee.userId}), !self.selectedEmployees.contains(where: {emp in return emp.userId == employee.userId}){
                        employees.append(employee)
                    }
                }
                if employees.count == 0{
                    employees = [Employee.invalid()]
                }
                self.employeeList.replace(with: employees)
                resolve(employees)
            }).catch({e in reject(e)})
        })
    }
    
    func updateNewRequests() -> Promise<[Request]>{
        return Promise(in: .background, {resolve, reject, _ in
            SAPcpms.shared.fetchNewRequests().then({requestModels in
                var requests: [Request] = []
                for model in requestModels{
                    if let request = model.request(){
                        if loggedInUser.value != nil, let response = model.emptyResponse(emp: loggedInUser.value!){
                            request.responses.append(response)
                        }
                        requests.append(request)
                        
                    }
                }
                self.newRequests.replace(with: requests.sorted(by: {(a, b) in return (a.date ?? Date()) > (b.date ?? Date())}))
                resolve(requests)
            }).catch({e in
                ErrorView.show(error: e)
            })
        })
    }
    
    func updateSentRequests() -> Promise<[Request]>{
        return Promise(in: .background, {resolve, reject, _ in
            SAPcpms.shared.fetchSentRequests().then({requestModels in
                var requests: [Request] = []
                for model in requestModels{
                    if let request = model.request(){
                        let responseModel = ResponseModel(dictionary: model.dictionaryRepresentation())
                        if let response = responseModel?.response(){
                            request.responses.append(response)
                        }
                        requests.append(request)
                        
                    }
                }
                self.myRequests.replace(with: requests.sorted(by: {(a, b) in return (a.date ?? Date()) > (b.date ?? Date())}))
                resolve(requests)
            }).catch({e in
                ErrorView.show(error: e)
            })
        })
    }
    
    func updateSentResponses() -> Promise<[Request]>{
        return Promise(in: .background, {resolve, reject, _ in
            SAPcpms.shared.fetchSentResponses().then({requestModels in
                var requests: [Request] = []
                for model in requestModels{
                    if let request = model.request(){
                        let responseModel = ResponseModel(dictionary: model.dictionaryRepresentation())
                        if let response = responseModel?.response(), loggedInUser.value != nil{
                            response.rater = loggedInUser.value!
                            response.responseTimestamp = Date()
                            request.responses.append(response)
                        }
                        requests.append(request)
                        
                    }
                }
                self.sentResponses.replace(with: requests.sorted(by: {(a, b) in return (a.date ?? Date()) > (b.date ?? Date())}))
                resolve(requests)
            }).catch({e in
                ErrorView.show(error: e)
            })
        })
    }
    
    func updateSentRecognitions() -> Promise<[Recognition]>{
        return Promise(in: .background, {resolve, reject, _ in
            SAPcpms.shared.fetchSentRecognitions().then({recognitionModels in
                var recognitions: [Recognition] = []
                for model in recognitionModels{
                    if let recognition = model.recognition(){
                        recognitions.append(recognition)
                    }
                }
                self.sentRecognitions.replace(with: recognitions.sorted(by: {(a, b) in return (a.date ?? Date()) > (b.date ?? Date())}))
                resolve(recognitions)
            }).catch({e in
                ErrorView.show(error: e)
            })
        })
    }
    
    func updateMyFeedback() -> Promise<[Request]>{
        return Promise(in: .background, {resolve, reject, _ in
            
        })
    }
    
    func updateMyRecognitions() -> Promise<[Recognition]>{
        return Promise(in: .background, {resolve, reject, _ in
            SAPcpms.shared.fetchReceivedRecognitions().then({recognitionModels in
                var recognitions: [Recognition] = []
                for model in recognitionModels{
                    if let recognition = model.recognition(){
                        recognitions.append(recognition)
                    }
                }
                self.myRecognition.replace(with: recognitions)
                resolve(recognitions)
            }).catch({e in
                ErrorView.show(error: e)
            })
        })
    }
    
    func updateCompetencies() -> Promise<[Competency]>{
        return Promise(in: .background, {resolve, reject, _ in
            resolve(Competency.Competencies)
        })
    }
    
    func updateMyTrophies() -> Promise<[Trophy]>{
        return Promise(in: .background, {resolve, reject, _ in
            SAPcpms.shared.fetchTrophies().then({trophies in
                self.myTrophies.replace(with: trophies)
                resolve(trophies)
            }).catch({e in
                ErrorView.show(error: e)
            })
        })
    }
    
    func updateAnswerSection() -> Promise<Any?>{
        return Promise(in: .background, {resolve, reject, _ in
            all(self.updateNewRequests().void, self.updateSentRequests().void, self.updateSentRecognitions().void).then({_ in
                resolve(nil)
            }).catch({e in
                ErrorView.show(error: e)
            })
        })
    }
    
    func updateMyFeedbackSection() -> Promise<Any?>{
        return Promise(in: .background, {resolve, reject, _ in
            all(self.updateMyFeedback().void, self.updateMyRecognitions().void, self.updateMyTrophies().void).then({_ in
                resolve(nil)
            }).catch({_ in resolve(nil)})
        })
    }
    /*func updateEmployees() -> Promise<[Employee]>{
        return Promise(in: .background, {resolve, reject, _ in
            print("Update Employee List START")
            print(Date().string())
            if let file = Bundle.main.url(forResource: "employee_list", withExtension: "json"){
                Alamofire.request(file, method: .get).validate().responseJSON { response in
                    switch response.result {
                    case .success:
                        let parsedData = try! JSONSerialization.jsonObject(with: response.data!, options: .allowFragments) as! [String:Any]
                        if let jsonObject = parsedData["d"] as? [String:Any], let results = jsonObject["results"] as? [NSDictionary]{
                            var newEmpList = [Employee]()
                            
                            for result in results{
                                if let employee = EmployeeModel(dictionary: result)?.EmployeeFromModel(), employee.muid != loggedInUser.value?.muid{
                                    newEmpList.append(employee)
                                }
                            }
                            //let newEmpList = results.map{EmployeeModel(dictionary: $0)?.EmployeeFromModel() ?? Employee.invalid()}
                            print("Update Employee List END")
                            print(newEmpList.count)
                            print(Date().string())
                            
                            self.employeeList.replace(with: newEmpList)
                            resolve(newEmpList)
                        }
                    case .failure(let error):
                        print(error)
                        reject(error)
                    }
                }
            }
        })
    }*/
    
    func updateEmployees(page: Int) -> Promise<[Employee]>{
        return Promise(in: .background, {resolve, reject, _ in
            SAPcpms.shared.fetchUserMasterdata(from: page, amount: 20).then({models in
                var employees:[Employee] = []
                for model in models!{
                    if let employee = model.employee(), !self.suggestedEmployees.array.contains(where: {emp in return emp.userId == employee.userId}), !self.selectedEmployees.contains(where: {emp in return emp.userId == employee.userId}){
                        employees.append(employee)
                    }
                }
                /*if page == 0{
                    self.employeeList.replace(with: employees)
                }else{
                    self.employeeList.insert(contentsOf: employees, at: self.employeeList.array.count)
                }*/
                resolve(employees)
            })
        })
    }
    
    func updateSuggestedEmployees(emails: [String])->Promise<[Employee]>{
        return Promise(in: .background, {resolve, reject, _ in
            guard loggedInUser.value != nil else{
                reject(webError.webServiceError("Not logged in"))
                return
            }
            guard emails.count > 0 else{
                resolve([])
                return
            }
            SAPcpms.shared.getSuggestedUserMasterdata(emails: emails, userId: loggedInUser.value!.userId).then({userMasterdata in
                var employees: [Employee] = []
                for model in userMasterdata{
                    if let employee = model.employee(){
                        employee.isSuggested = true
                        employees.append(employee)
                    }
                }
                self.suggestedEmployees.replace(with: employees)
                resolve(employees)
            }).catch({e in reject(e)})
        })
    }
    
    func update() -> Promise<Any?>{
        return Promise(in: .background, {resolve, reject, _ in
            all(self.updateNewRequests().void, self.updateSentRequests().void, self.updateSentRecognitions().void, self.updateSentResponses().void, self.updateMyRecognitions().void, self.updateMyTrophies().void, self.updateEmployees(page: 0).void).then({_ in
                self.updateSuggestedEmployees().then({_ in resolve(nil)}).catch({_ in resolve(nil)})
                //resolve(nil)
            }).catch({e in
                ErrorView.show(error: e)
            })
        })
    }
    
    func getSettings() -> Promise<UserSettings?>{
        return Promise(in: .background, {resolve, reject, _ in
            SAPcpms.shared.userSettingsGet(params: ["mobileVersion" : Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String]).then({model in
                if let settings = model?.settings(){
                    Localize.setCurrentLanguage(settings.language.technical)
                    resolve(settings)
                }else{
                    resolve(UserSettings.standard)
                }
            }).catch({e in reject(e)})
        })
    }
    
    func writeSettings(userSettings: UserSettings) -> Promise<UserSettings?>{
        return Promise(in: .background, {resolve, reject, _ in
            let params:[String:Any] = [
                "showReceivedDuration": userSettings.showReceivedDuration.key,
                "showSendDuration": userSettings.showSendDuration.key,
                "pushNotification": userSettings.pushNotification.key,
                "mailNotification": userSettings.mailNotification.key,
                "language": userSettings.language.key,
                "mobileVersion" : Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String,
                "background": userSettings.background
            ]
            if self.userSettings.value.language.key != userSettings.language.key{
                Localize.setCurrentLanguage(userSettings.language.technical)
            }
            SAPcpms.shared.userSettingsUpdate(params: params).then({response in
                if response.success{
                    self.userSettings.value = userSettings
                    resolve(userSettings)
                }else{
                    resolve(nil)
                }
            }).catch({e in reject(e)})
        })
    }
    
    // Checks if an Recognition applies to the active
    func filter(_ recognition: Recognition)->Bool{
        for filter in self.activeFilters.array{
            switch filter.type{
            case .Competency:
                if !self.activeFilters.array.filter({f in return f.type == FilterType.Competency}).contains(where: {f in return recognition.competency.name == (f.value as? Competency)?.name}){
                    return false
                }
            case .Employee:
                if !recognition.receivers.contains(where: {receiver in return receiver.receiver.userId == (filter.value as? Employee)?.userId}) && recognition.sender.userId != (filter.value as? Employee)?.userId{
                    return false
                }
            case .DateFrom:
                if recognition.date! < filter.value as! Date{
                    return false
                }
            case .DateTo:
                if recognition.date! > filter.value as! Date{
                    return false
                }
            default:
                break
            }
        }
        return true
    }
    
    // Checks if an Request applies to the active
    func filter(_ request: Request)->Bool{
        for filter in self.activeFilters.array{
            switch filter.type{
            case .Category:
                if !self.activeFilters.array.filter({f in return f.type == FilterType.Category}).contains(where: {f in return request.category?.name == (f.value as? RequestCategory)?.name}){
                    return false
                }
            case .RequestType:
                if !self.activeFilters.array.filter({f in return f.type == FilterType.RequestType}).contains(where: {f in return request.type?.key == (f.value as? RequestType)?.key}){
                    return false
                }
            case .Employee:
                if !request.responses.contains(where: {resp in return resp.rater.userId == (filter.value as? Employee)?.userId}) && request.requester.userId != (filter.value as? Employee)?.userId{
                    return false
                }
            case .DateFrom:
                if request.date! < filter.value as! Date{
                    return false
                }
            case .DateTo:
                if request.date! > filter.value as! Date{
                    return false
                }
            case .SeriesOnly:
                if request.continuousRepetitions == 1{
                    return false
                }
            case .Series:
                if !self.activeFilters.array.filter({f in return f.type == FilterType.Series}).contains(where: {f in return request.id == (f.value as? (id:Int?, title:String?))?.id}) && self.activeFilters.array.contains(where: {f in return f.type == FilterType.SeriesOnly}){
                    return false
                }
            default:
                break
            }
        }
        return true
    }
    
    // USAL Logs
    
    func AnalyticsOpenViewTrophyOverview(){
        SAPcpms.shared.writeUsalLog(action: "Open Trophy Overview")
    }
    func AnalyticsOpenApplication(){
        SAPcpms.shared.writeUsalLog(action: "Open Application")
    }
    func AnalyticsOpenViewMyFeedback(){
        SAPcpms.shared.writeUsalLog(action: "Navigation to MyFeedback")
    }
    func AnalyticsActionRequestFeedback(){
        SAPcpms.shared.writeUsalLog(action: "Open Get Feedback")
    }
    func AnalyticsActionSendRecognition(){
        SAPcpms.shared.writeUsalLog(action: "Open Send Recognition")
    }
    func AnalyticsOpenViewSettings(){
        SAPcpms.shared.writeUsalLog(action: "Navigation to Settings")
    }
    func AnalyticsOpenViewOnboarding(){
        SAPcpms.shared.writeUsalLog(action: "Open Help")
    }
    func AnalyticsActionShareFeedback(){
        SAPcpms.shared.writeUsalLog(action: "Open Share Feedback")
    }
    func AnalyticsOpenApplicationFromMail(){
        SAPcpms.shared.writeUsalLog(action: "Open Application from Email")
    }
    func AnalyticsOpenApplicationFromPush(){
        SAPcpms.shared.writeUsalLog(action: "Open Application from Notification")
    }
}


