//
//  FeedbackService.swift
//  MyFeedback 2.0
//
//  Created by Daniel Weber on 08.03.18.
//  Copyright © 2018 zero2one. All rights reserved.
//

import Foundation
import Bond
import Hydra
import Dip
import Alamofire
import KeychainSwift
import EventKit
import SwiftyJSON
import SwiftDate

class FeedbackServiceMock{
    let marius = Employee(userId: "M210275", muid: "M210275", firstName: "MARIUS", lastName: "HEUSSNER", email: "MARIUS.HEUSSNER@MERCKGROUP.COM", orgUnitName: "BX-TGC Corp. Business Tech. Platforms")
    let daniel = Employee(userId: "M202676", muid: "M202676", firstName: "DANIEL", lastName: "WEBER", email: "DANIEL.WEBER@MERCKGROUP.COM", orgUnitName: "BX-TGC Corp. Business Tech. Platforms")
    let bjoern = Employee(userId: "M165566", muid: "M165566", firstName: "BJOERN", lastName: "EBELING", email: "BJOERN.EBELING@MERCKGROUP.COM", orgUnitName: "BX-TGC Corp. Business Tech. Platforms")
    
    var amountNewRequests: Observable<Int> = Observable<Int>(0)
    var amountUnreadResponses: Observable<Int> = Observable<Int>(0)
    var amountUnreadRecognitions: Observable<Int> = Observable<Int>(0)
    var amountRequestsTotal: Observable<Int> = Observable<Int>(0)
    var amountRecognitionsTotal: Observable<Int> = Observable<Int>(0)
    
    var amountUnreadTotal: Observable<Int> = Observable<Int>(0)
    var amountTotal: Observable<Int> = Observable<Int>(0)
    
    var requestDraft: Observable<Request?> = Observable<Request?>(nil)
    var recognitionDraft: Observable<Recognition?> = Observable<Recognition?>(nil)
    
    var myRequests: MutableObservableArray<Request> = MutableObservableArray<Request>([])
    var myRecognition: MutableObservableArray<Recognition> = MutableObservableArray<Recognition>([])
    var myTrophies: MutableObservableArray<Trophy> = MutableObservableArray<Trophy>([])
    
    var competencies: MutableObservableArray<Competency> = MutableObservableArray<Competency>([])
    
    var newRequests: MutableObservableArray<Request> = MutableObservableArray<Request>([])
    var sentResponses: MutableObservableArray<Request> = MutableObservableArray<Request>([])
    var sentRecognitions: MutableObservableArray<Recognition> = MutableObservableArray<Recognition>([])
    
    var userSettings: Observable<UserSettings> = Observable<UserSettings>(UserSettings.standard)
    
    var employeeList: MutableObservableArray<Employee> = MutableObservableArray<Employee>([])
    
    var activeFilters: MutableObservableArray<Filter> = MutableObservableArray<Filter>([])
    
    func checkIfValid(_ recognition: Recognition?) -> Bool{
        return recognition?.isValid.value == true && self.employeeList.array.filter({emp in return emp.isSelected}).count > 0
    }
    
    func checkIfValid(_ request: Request?) -> Bool{
        return request?.isValid.value == true && self.employeeList.array.filter({emp in return emp.isSelected}).count > 0
    }
    
    
    func sendFeedbackRequest() -> Promise<Request?>{
        return Promise(in: .background, {resolve, reject, _ in
            resolve(nil)
        })
    }
    
    func sendRecognition() -> Promise<Recognition?>{
        return Promise(in: .background, {resolve, reject, _ in
            resolve(nil)
        })
    }
    
    func answerRequest() -> Promise<Request?>{
        return Promise(in: .background, {resolve, reject, _ in
            resolve(nil)
        })
    }
    
    func dismissRequest() -> Promise<Request?>{
        return Promise(in: .background, {resolve, reject, _ in
            resolve(nil)
        })
    }
    
    func deleteRequest() -> Promise<Request?>{
        return Promise(in: .background, {resolve, reject, _ in
            resolve(nil)
        })
    }
    
    func hideRecognition() -> Promise<Recognition?>{
        return Promise(in: .background, {resolve, reject, _ in
            resolve(nil)
        })
    }
    
    func readResponse() -> Promise<Request?>{
        return Promise(in: .background, {resolve, reject, _ in
            resolve(nil)
        })
    }
    
    func readAllResponses() -> Promise<Request?>{
        return Promise(in: .background, {resolve, reject, _ in
            resolve(nil)
        })
    }
    
    func readRecognition() -> Promise<Recognition?>{
        return Promise(in: .background, {resolve, reject, _ in
            resolve(nil)
        })
    }
    
    func thankForResponse() -> Promise<Request?>{
        return Promise(in: .background, {resolve, reject, _ in
            resolve(nil)
        })
    }
    
    func thankForAllResponses() -> Promise<Request?>{
        return Promise(in: .background, {resolve, reject, _ in
            resolve(nil)
        })
    }
    
    func thankForRecognition() -> Promise<Recognition?>{
        return Promise(in: .background, {resolve, reject, _ in
            resolve(nil)
        })
    }
    
    func getRequestDetails() -> Promise<Request?>{
        return Promise(in: .background, {resolve, reject, _ in
            resolve(nil)
        })
    }
    
    func updateSuggestedEmployees()->Promise<[Employee]>{
        return Promise(in: .background, {resolve, reject, _ in
            let status = EKEventStore.authorizationStatus(for: EKEntityType.event)
            let eventStore = EKEventStore()
            
            let callback = { (_ es: EKEventStore)->Promise<[Employee]> in
                return Promise(in: .background, {resolve, reject, _ in
                    self.loadCalendars(eventStore: es).then({calendars in
                        self.loadEvents(calendars: calendars, eventStore: es).then({events in
                            self.getParticipantsFromEvents(events: events).then({emails in
                                let suggestedEmployees = self.employeeList.array.filter({emp in
                                    return emails.contains(emp.email.uppercased())
                                })
                                for emp in suggestedEmployees{
                                    emp.isSuggested = true
                                }
                                resolve(suggestedEmployees)
                            }).catch({e in reject(e)})
                        }).catch({e in reject(e)})
                    }).catch({e in reject(e)})
                })
            }
            
            switch (status) {
            case EKAuthorizationStatus.notDetermined:
                // This happens on first-run
                self.requestAccessToCalendar(eventStore: eventStore).then({accessGranted in
                    if accessGranted{
                        callback(eventStore).then({employees in
                            resolve(employees)
                        }).catch({e in reject(e)})
                    }else{
                        reject(webError.webServiceError("Access Restircted"))
                    }
                })
                break
            case EKAuthorizationStatus.authorized:
                // Things are in line with being able to show the calendars in the table view
                callback(eventStore).then({employees in
                    resolve(employees)
                }).catch({e in reject(e)})
                break
            case EKAuthorizationStatus.restricted, EKAuthorizationStatus.denied:
                reject(webError.webServiceError("Access Restircted"))
            }
        })
        
    }
    
    private func requestAccessToCalendar(eventStore: EKEventStore) ->Promise<Bool>{
        return Promise(in: .background, {resolve, reject, _ in
            eventStore.requestAccess(to: EKEntityType.event, completion: {
                (accessGranted: Bool, error: Error?) in
                if accessGranted == true {
                    resolve(true)
                } else if error != nil {
                    reject(error!)
                } else {
                    resolve(false)
                }
            })
        })
    }
    
    private func loadCalendars(eventStore: EKEventStore)  -> Promise<[EKCalendar]>{
        return Promise(in: .background, {resolve, reject, _ in
            resolve(eventStore.calendars(for: EKEntityType.event).filter({cal in cal.type == EKCalendarType.exchange}))
        })
    }
    
    private func getParticipantsFromEvents(events: [EKEvent]) ->Promise<[String]>{
        return Promise(in: .background, {resolve, reject, _ in
            var emailsFound:[String] = []
            for event in events{
                
                if(event.organizer != nil){
                    let idx = event.organizer!.url.absoluteString.index(of: ":")
                    let idx2 = event.organizer!.url.absoluteString.index(after: idx!)
                    let email = event.organizer!.url.absoluteString.substring(from: idx2).uppercased()
                    emailsFound.append(email.uppercased())
                }
                
                if(event.attendees != nil){
                    for attandee in event.attendees!{
                        if(attandee.participantType == .person && attandee.name?.contains("@") != true){
                            if let idx = attandee.url.absoluteString.index(of: ":"){
                                let idx2 = attandee.url.absoluteString.index(after: idx)
                                let email = attandee.url.absoluteString.substring(from: idx2).uppercased()
                                emailsFound.append(email.uppercased())
                            }
                        }
                    }
                }
            }
            resolve(emailsFound)
        })
    }
    
    private func loadEvents(calendars: [EKCalendar], eventStore: EKEventStore) -> Promise<[EKEvent]>{
        return Promise(in: .background, {resolve, reject, _ in
            
            // Create start and end date NSDate instances to build a predicate for which events to select
            let startDate = Date(timeInterval: -60*60*24, since: Date())
            let endDate = Date()
            
            // Use an event store instance to create and properly configure an NSPredicate
            let eventsPredicate = eventStore.predicateForEvents(withStart: startDate, end: endDate, calendars: calendars)
            
            // Use the configured NSPredicate to find and return events in the store that match
            let events = eventStore.events(matching: eventsPredicate).filter({e in e.attendees != nil && e.attendees!.count<50}).sorted(){
                (e1: EKEvent, e2: EKEvent) -> Bool in
                return e1.startDate.compare(e2.startDate) == ComparisonResult.orderedAscending
            }
            resolve(events)
        })
        
    }
    
    func deselectEmployees(){
        for emp in self.employeeList.array.filter({employee in return employee.isSelected}){
            emp.isSelected = false
        }
    }
    
    func getCountsForCompetency(competency: Competency)->(total: Int, unread: Int?){
        let total =  self.myRecognition.array.filter({rec in return rec.competency.name == competency.name}).count
        let unread =  self.myRecognition.array.filter({rec in return rec.competency.name == competency.name && !rec.isRead}).count
        return(total, unread)
    }
    
    func getCounts(){
        
    }
    
    func updateNewRequests() -> Promise<[Request]>{
        return Promise(in: .background, {resolve, reject, _ in
            let req1 = Request(id: 1, requester: self.marius, category: RequestCategory.Innovation, type: RequestType.Smileys, text: "This is a New Request", title: "New Request", date: Date(), continuousInterval: ContinuousInterval.OneTime, continuousRepetitions: 1, scaleTextMin: nil, scaleTextMax: nil, averageResponseRating: nil)
            let resp11 = RequestResponse(requestId: 1, rater: self.daniel, repetition: 1, text: nil, rating: nil, responseTimestamp: nil, readFlag: nil, thankYouFlag: nil)
            req1.responses = [resp11]
            self.newRequests.replace(with: [req1])
            resolve([req1])
        })
    }
    
    func updateSentRequests() -> Promise<[Request]>{
        return Promise(in: .background, {resolve, reject, _ in
            let req2 = Request(id: 2, requester: self.bjoern, category: RequestCategory.Innovation, type: RequestType.Smileys, text: "This is a New Request", title: "New Request", date: Date(), continuousInterval: ContinuousInterval.OneTime, continuousRepetitions: 1, scaleTextMin: nil, scaleTextMax: nil, averageResponseRating: nil)
            let resp21 = RequestResponse(requestId: 2, rater: self.daniel, repetition: 1, text: "Das ist eine Antwort", rating: 3, responseTimestamp: Date(), readFlag: nil, thankYouFlag: nil)
            req2.responses = [resp21]
            self.sentResponses.replace(with: [req2])
            resolve([req2])
        })
    }
    
    func updateSentRecognitions() -> Promise<[Recognition]>{
        return Promise(in: .background, {resolve, reject, _ in
            let rec = Recognition(id: 3, sender: self.daniel, competency: Competency.Collaborative, text: "Super!", date: Date(), repetition: nil)
            let receiver = RecognitionReceiver(recognitionId: 3, receiver: self.marius, readTimestamp: nil, thankYouTimestamp: nil, hideTimestamp: nil)
            rec.receivers = [receiver]
            self.sentRecognitions.replace(with: [rec])
            resolve([rec])
        })
    }
    
    func updateMyFeedback() -> Promise<[Request]>{
        return Promise(in: .background, {resolve, reject, _ in
            let req4 = Request(id: 4, requester: self.daniel, category: RequestCategory.Innovation, type: RequestType.Stars, text: "This is a New Single Request", title: "Single Request", date: Date(), continuousInterval: ContinuousInterval.OneTime, continuousRepetitions: 1, scaleTextMin: nil, scaleTextMax: nil, averageResponseRating: nil)
            let resp41 = RequestResponse(requestId: 4, rater: self.marius, repetition: 1, text: "Das ist eine Antwort", rating: 3, responseTimestamp: Date(), readFlag: nil, thankYouFlag: nil)
            let resp42 = RequestResponse(requestId: 4, rater: self.bjoern, repetition: 1, text: "Das ist eine Antwort", rating: 4, responseTimestamp: Date(), readFlag: nil, thankYouFlag: nil)
            req4.responses = [resp41,resp42]
            let req5 = Request(id: 5, requester: self.daniel, category: RequestCategory.Innovation, type: RequestType.Smileys, text: "This is a New Multi Request", title: "Multi Request", date: Date(), continuousInterval: ContinuousInterval.OneTime, continuousRepetitions: 1, scaleTextMin: nil, scaleTextMax: nil, averageResponseRating: nil)
            let resp51 = RequestResponse(requestId: 5, rater: self.marius, repetition: 1, text: "Das ist eine Antwort", rating: 3, responseTimestamp: Date(), readFlag: nil, thankYouFlag: nil)
            let resp52 = RequestResponse(requestId: 5, rater: self.bjoern, repetition: 1, text: "Das ist eine Antwort", rating: 3, responseTimestamp: Date(), readFlag: nil, thankYouFlag: nil)
            let resp53 = RequestResponse(requestId: 5, rater: self.bjoern, repetition: 1, text: "Das ist eine Antwort", rating: 3, responseTimestamp: Date(), readFlag: nil, thankYouFlag: nil)
            let resp54 = RequestResponse(requestId: 5, rater: self.bjoern, repetition: 1, text: "Das ist eine Antwort", rating: 3, responseTimestamp: Date(), readFlag: nil, thankYouFlag: nil)
            let resp55 = RequestResponse(requestId: 5, rater: self.bjoern, repetition: 1, text: "Das ist eine Antwort", rating: 3, responseTimestamp: Date(), readFlag: nil, thankYouFlag: nil)
            let resp56 = RequestResponse(requestId: 5, rater: self.bjoern, repetition: 1, text: "Das ist eine Antwort", rating: 3, responseTimestamp: Date(), readFlag: nil, thankYouFlag: nil)
            let resp57 = RequestResponse(requestId: 5, rater: self.bjoern, repetition: 1, text: "Das ist eine Antwort", rating: 3, responseTimestamp: Date(), readFlag: nil, thankYouFlag: nil)
            let resp58 = RequestResponse(requestId: 5, rater: self.bjoern, repetition: 1, text: "Das ist eine Antwort", rating: 3, responseTimestamp: Date(), readFlag: nil, thankYouFlag: nil)
            let resp59 = RequestResponse(requestId: 5, rater: self.bjoern, repetition: 1, text: "Das ist eine Antwort", rating: 3, responseTimestamp: Date(), readFlag: nil, thankYouFlag: nil)
            let resp510 = RequestResponse(requestId: 5, rater: self.bjoern, repetition: 1, text: "Das ist eine Antwort", rating: 3, responseTimestamp: Date(), readFlag: nil, thankYouFlag: nil)
            req5.responses = [resp51,resp52,resp53,resp54,resp55,resp56,resp57,resp58,resp59,resp510]
            let req6 = Request(id: 6, requester: self.daniel, category: RequestCategory.LeadershipSkills, type: RequestType.YesNo, text: "This is a New Request", title: "New Request", date: Date(), continuousInterval: ContinuousInterval.OneTime, continuousRepetitions: 1, scaleTextMin: nil, scaleTextMax: nil, averageResponseRating: nil)
            let resp61 = RequestResponse(requestId: 6, rater: self.marius, repetition: 1, text: "Das ist eine Antwort", rating: 0, responseTimestamp: Date(), readFlag: nil, thankYouFlag: nil)
            let resp62 = RequestResponse(requestId: 6, rater: self.bjoern, repetition: 1, text: "Das ist eine Antwort", rating: 0, responseTimestamp: Date(), readFlag: nil, thankYouFlag: nil)
            req6.responses = [resp61,resp62]
            self.myRequests.replace(with: [req4, req5, req6])
            resolve([])
        })
    }
    
    func updateMyRecognitions() -> Promise<[Recognition]>{
        return Promise(in: .background, {resolve, reject, _ in
            var recs:[Recognition] = []
            let rec = Recognition(id: 6, sender: self.marius, competency: Competency.Collaborative, text: "Super!", date: Date(), repetition: nil)
            let receiver = RecognitionReceiver(recognitionId: 6, receiver: self.daniel, readTimestamp: nil, thankYouTimestamp: nil, hideTimestamp: nil)
            rec.receivers = [receiver]
            
            recs.append(rec)
            self.myRecognition.replace(with: recs)
            resolve(recs)
        })
    }
    
    func updateCompetencies() -> Promise<[Competency]>{
        return Promise(in: .background, {resolve, reject, _ in
            resolve(Competency.Competencies)
        })
    }
    
    func updateMyTrophies() -> Promise<[Trophy]>{
        return Promise(in: .background, {resolve, reject, _ in
            var trophies:[Trophy] = []
            trophies.append(Trophy.AllStarLevel1)
            trophies.append(Trophy.ExplorerLevel1)
            trophies.append(Trophy.FeedbackGuruLevel2)
            trophies.append(Trophy.DevelopmentDriverLevel3)
            trophies.append(Trophy.RecognitionHeroLevel2)
            trophies.append(Trophy.CompetencyChampionLevel1)
            self.myTrophies.replace(with: trophies)
            resolve(trophies)
        })
    }
    
    func updateAnswerSection() -> Promise<Any?>{
        return Promise(in: .background, {resolve, reject, _ in
            all(self.updateNewRequests().void, self.updateSentRequests().void, self.updateSentRecognitions().void).then({_ in
                resolve(nil)
            })
        })
    }
    
    func updateMyFeedbackSection() -> Promise<Any?>{
        return Promise(in: .background, {resolve, reject, _ in
            all(self.updateMyFeedback().void, self.updateMyRecognitions().void, self.updateMyTrophies().void).then({_ in
                resolve(nil)
            })
        })
    }
    func updateEmployees() -> Promise<[Employee]>{
        return Promise(in: .background, {resolve, reject, _ in
            print("Update Employee List START")
            print(Date().string())
            if let file = Bundle.main.url(forResource: "employee_list", withExtension: "json"){
                Alamofire.request(file, method: .get).validate().responseJSON { response in
                    switch response.result {
                    case .success:
                        let parsedData = try! JSONSerialization.jsonObject(with: response.data!, options: .allowFragments) as! [String:Any]
                        if let jsonObject = parsedData["d"] as? [String:Any], let results = jsonObject["results"] as? [NSDictionary]{
                            var newEmpList = [Employee]()
                            
                            for result in results{
                                if let employee = EmployeeModel(dictionary: result)?.EmployeeFromModel(), employee.muid != loggedInUser.value?.muid{
                                    newEmpList.append(employee)
                                }
                            }
                            //let newEmpList = results.map{EmployeeModel(dictionary: $0)?.EmployeeFromModel() ?? Employee.invalid()}
                            print("Update Employee List END")
                            print(newEmpList.count)
                            print(Date().string())
                            
                            self.employeeList.replace(with: newEmpList)
                            resolve(newEmpList)
                        }
                    case .failure(let error):
                        print(error)
                        reject(error)
                    }
                }
            }
        })
    }
    
    func update() -> Promise<Any?>{
        return Promise(in: .background, {resolve, reject, _ in
            all(self.updateNewRequests().void, self.updateSentRequests().void, self.updateSentRecognitions().void, self.updateMyFeedback().void, self.updateMyRecognitions().void, self.updateMyTrophies().void, self.fetchSettings().void, self.updateEmployees().void).then({_ in
                self.updateSuggestedEmployees().then({_ in resolve(nil)}).catch({_ in resolve(nil)})
                //resolve(nil)
            })
        })
    }
    
    func fetchSettings() -> Promise<UserSettings?>{
        return Promise(in: .background, {resolve, reject, _ in
            resolve(nil)
        })
    }
    
    func writeSettings() -> Promise<UserSettings?>{
        return Promise(in: .background, {resolve, reject, _ in
            resolve(nil)
        })
    }
}

